```bash
$list_app = File::directories(app_path());
$list_modules = File::directories(app()->basePath().'/Modules');
```

```bash
$ php artisan vendor:publish --force
```

Modules [Link](https://nwidart.com/laravel-modules/v1/advanced-tools/artisan-commands)

Utility commands

1. module:make

Generate a new module or multiple modules at once.

```bash
$ php artisan module:make Contacts
$ php artisan module:make Contacts Blog User Auth
```

2. module:use

Use a given module. This allows you to not specific the module name on other commands requiring the module name as an argument.

```bash
$ php artisan module:use Contacts
```

3. module:list

List all available modules.

```bash
$ php artisan module:list
```

4. module:migrate

Migrate the given module, or without a module an argument, migrate all modules.

```bash
$ php artisan module:migrate Contacts
```


5. module:migrate-rollback

Rollback the given module, or without an argument, rollback all modules.

```bash
$ php artisan module:migrate-rollback Contacts
```


6. module:migrate-refresh

Refresh the migration for the given module, or without a specified module refresh all modules migrations.

```bash
$ php artisan module:migrate-refresh Contacts
```

7. module:migrate-reset Contacts

Reset the migration for the given module, or without a specified module reset all modules migrations.

```bash
$ php artisan module:migrate-reset Contacts
```

8. module:seed

Seed the given module, or without an argument, seed all modules

```bash
$ php artisan module:seed Contacts
```

9. module:publish-migration

Publish the migration files for the given module, or without an argument publish all modules migrations.

```bash
$ php artisan module:publish-migration Contacts
```

10. module:publish-config

Publish the given module configuration files, or without an argument publish all modules configuration files.

```bash
$ php artisan module:publish-config Contacts
```

11. module:publish-translation

Publish the translation files for the given module, or without a specified module publish all modules migrations.

```bash
$ php artisan module:publish-translation Contacts
```

12. module:enable

Enable the given module.

```bash
$ php artisan module:enable Contacts
```

13. module:disable

Disable the given module.

```bash
$ php artisan module:disable Contacts
```

14. module:update

Update the given module.

```bash
$ php artisan module:update Contacts
```
