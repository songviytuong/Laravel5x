<?php

Route::group(['prefix' => 'docs', 'namespace' => 'Modules\Docs\Http\Controllers'], function() {
    Route::get('/', function(){
        return "...";
    });

    Route::get('/testing', 'TestController@index')->name('docs.index');
    Route::get('/testing/master', function() {
        return view('docs::master');
    });
    
    Route::get('/testing/import-testcases', 'TestController@importTestCaseToDatabase')->name('import.testcase.to.database');
    Route::get('/testing/export-testcases', 'TestController@exportTestCaseFromDatabase')->name('export.testcase.from.database');
});

Route::get('/assets-docs/{ext}/{filepath}', [
    function($ext, $filepath) {
        $ext = ucfirst($ext);
        $path = app_path("../Modules/Docs/Resources/$ext/$filepath");
        $last = last(explode('.', $path));
        if (\File::exists($path)) {
            if ($last == 'js') {
                return response()->file($path, array('Content-Type' => 'application/javascript'));
            } else {
                return response()->file($path, array('Content-Type' => 'text/css'));
            }
        }
        return response()->json([], 404);
    }
])->where('filepath', '(.*)');
