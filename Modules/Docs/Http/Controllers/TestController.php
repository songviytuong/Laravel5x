<?php

namespace Modules\Docs\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Helpers\Business;
use SEO;
use Modules\Docs\Entities\TestCases;
use Carbon\Carbon;
use File;
use ReflectionClass;
use ReflectionMethod;
use ReflectionFunction;
use Cache;

class TestController extends Controller
{
    public $data = [];

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        SEO::setTitle('Testing Project');

        $documents = Business::cms_getDocuments();

        foreach ($documents as $key => $document) {
            $navbar['folder'][] = $document['folder'];
            $navbar['file'][] = $document['file'];
        }
        $this->data['documents']['folder'] = $navbar['folder'];
        $this->data['documents']['file'] = $navbar['file'];
        return view('docs::index', array('data' => $this->data));
        // TODO: Hello Peace
    }

    public function importTestCaseToDatabase()
    {
        $documents = Business::cms_listAllFunctionsTest();
        echo "<pre>";
        var_dump($documents);
        // TODO: Update Function Test into Database
    }

    public function exportTestCaseFromDatabase()
    {
        $testcases = TestCases::get()->toArray();
        foreach ($testcases as $key => $testcase) {
            $functionTest = $testcase['testcase'];

            $arr[$key]['className'] = $testcase['class'];
            $arr[$key]['testType'] = $testcase['type'];
            $arr[$key]['functionTest'] = $functionTest;
            $arr[$key]['functionName'] = Business::cms_validTestFirst($functionTest);
            $arr[$key]['functionBody'] = '$this->assertTrue(true);';

            $date = Carbon::now()->toDateString();
            $documents = <<<EOT
    /**
     * @Description: This method test $functionTest
     * @Params:
     * @Author: 
     * @Date: $date
     * @Notes:
     * @Action:
     */
EOT;

            $arr[$key]['functionDocuments'] = ($testcase['documents']) ? $testcase['documents'] : $documents;
        }

        TestCases::updateTestCases($arr);
        echo "Function Test has created!";
    }
}
