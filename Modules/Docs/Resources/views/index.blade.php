<?php

use Modules\Docs\Constants\GlobalDefine;
?>
<!DOCTYPE html>
<!--http://shtheme.com/demosd/thedocs/-->
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{!! SEO::getTitle() !!}</title>
        <link rel='stylesheet' id='theDocs.all.min.css-css' href='{{ Asset::get('/assets-docs/assets/css/theDocs.all.min.css') }}' type='text/css' media='all' />
        <link rel='stylesheet' id='custom.css-css' href='{{ Asset::get('/assets-docs/assets/css/custom.css') }}' type='text/css' media='all' />
        <link rel='stylesheet' id='fonts.googleapis.com-css' href='http://fonts.googleapis.com/css?family=Raleway%3A100%2C300%2C400%2C500%7CLato%3A300%2C400&#038;ver=4.9.5' type='text/css' media='all' />
        <link rel='stylesheet' id='style-css' href='{{ Asset::get('/assets-docs/assets/css/style.css') }}' type='text/css' media='all' />
        <!-- Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Raleway:100,300,400,500%7CLato:300,400' rel='stylesheet' type='text/css'>
        <!-- Favicons -->
    </head>

    <body class="page-template page-template-page-templates page-template-page-other page-template-page-templatespage-other-php page page-id-253 wpb-js-composer js-comp-ver-4.11.2 vc_responsive">

        <header class="site-header sticky navbar-fullwidth navbar-transparent">

            <!-- Top navbar & branding -->
            <nav class="navbar navbar-default">
                <div class="container">

                    <!-- Toggle buttons and brand -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar" aria-expanded="true" aria-controls="navbar">
                            <span class="glyphicon glyphicon-option-vertical"></span>
                        </button>

                        <button type="button" class="navbar-toggle for-sidebar" data-toggle="offcanvas">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <a class="navbar-brand" href="{{ route('docs.index') }}">
                            <img class="logo" alt="logo" src="{{ Asset::get('/assets-docs/assets/img/logo.png') }}" /></a>
                    </div>
                    <!-- END Toggle buttons and brand -->

                    <!-- Top navbar -->
                    <div id="navbar" class="navbar-collapse collapse" aria-expanded="true" role="banner">
                        <ul class="nav navbar-nav navbar-right  ">
                            <li id="menu-item-317" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home drop-normal"><a title="DUCUMENTATION" href="">DUCUMENTATION</a></li>
                            <li id="menu-item-318" class="menu-item menu-item-type-post_type menu-item-object-page drop-normal"><a title="FAQ" href="">FAQ</a></li>
                            <li id="menu-item-18" class="menu-item menu-item-type-custom menu-item-object-custom drop-normal"><a title="SUPPORT" href="">SUPPORT</a></li>
                            <li id="menu-item-19" class="menu-item menu-item-type-custom menu-item-object-custom drop-normal"><a title="TESTING" href="">TESTING</a></li>
                        </ul>
                    </div>
                    <!-- END Top navbar -->

                </div>
            </nav>
            <!-- END Top navbar & branding -->

        </header>
        <main class="container">
            <div class="row">
                <!-- Sidebar -->
                <aside class="col-md-3 col-sm-3 sidebar">
                    <ul class="sidenav {{ (!GlobalDefine::NAV_OPEN == 'ON') ? 'dropable' : '' }} sticky">
                        <li id="menu-item-108" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home drop-normal"><a title="Overview" href="{{ route('docs.index') }}">Tests</a></li>
                        @foreach ($data['documents']['folder'] as $key=>$navbar)

                        <li id="menu-item-1" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children drop-normal submenu"><a title="Feature" href="">{!! $navbar !!}</a>
                            <ul role="menu" class="">
                                @foreach ($data['documents']['file'][$key] as $class)
                                <?php
                                $cl = explode('\\', $class['class']);
                                $sl = strtolower(str_replace('\\', '-', $class['class']));
                                if (count($class['funs']) > 0) {
                                    ?>
                                    <li id="menu-item-1" class="menu-item menu-item-type-post_type menu-item-object-page drop-normal menuLeft"><a href="#class-{!! $sl !!}"><kbd>(class)</kbd> {!! $cl[2] !!}</a></li>
                                <?php } ?>
                                @endforeach
                            </ul>
                        </li>
                        @endforeach
                    </ul>
                </aside>
                <!-- END Sidebar -->
                <!-- Main content -->
                <article class="col-md-9 col-sm-9 main-content" role="main">
                    <header>
                        <h1>TESTING HELPER</h1>
                        <p>One of the most frequently used component of each documentation, is display a preview for a code snippet and describing it. In this page we will show different code view components of theDocs.</p>
                        <ol class="toc">
                            @foreach ($data['documents']['folder'] as $key=>$navbar)
                            <li>
                                <a href="#test-{!! strtolower($navbar) !!}">{!! $navbar !!}</a></p>
                                <ol>
                                    @foreach ($data['documents']['file'][$key] as $class)
                                    <?php
                                    $cl = explode('\\', $class['class']);
                                    $sl = strtolower(str_replace('\\', '-', $class['class']));
                                    if (count($class['funs']) > 0) {
                                        ?>
                                        <li><a href="#class-{!! $sl !!}">{!! $class['class'] !!}</a></li>
                                    <?php } ?>
                                    @endforeach
                                </ol>
                            </li>
                            @endforeach
                        </ol>
                    </header>

                    <section>
                        @foreach ($data['documents']['folder'] as $key=>$navbar)
                        <h2 id="test-{!! strtolower($navbar) !!}"><a href="#test-{!! strtolower($navbar) !!}">{!! $navbar !!}</a></h2>
                        @foreach ($data['documents']['file'][$key] as $k=>$class)
                        <?php
                        $cl = explode('\\', $class['class']);
                        $sl = strtolower(str_replace('\\', '-', $class['class']));
                        ?>
                        <p class='text-primary' id="class-{!! $sl !!}">
                            <?php
                            if (count($class['funs']) > 0) {
                                ?>
                                Class:: {!! $class['class'] !!} ({!! count($class['funs']) !!})</p>
                        <?php } ?>
                        @foreach ($class['funs'] as $t=>$fun)
                        <h3 id="func-{!! $fun['function_name'] !!}"><a href="#code-{!! $fun['function_name'] !!}">{!! $t+1 !!}. <small><i>(function)</i></small> {!! $fun['function_name'] !!}</a></h3>
                        <style>
                            .desc:before{content: 'Note';font-weight: 500;font-size:11px;}
                            .desc p{font-weight: 500;font-size:13px;color:#ccc;}
                        </style>

                        @php
                        $descs = json_decode($fun['function_desc']);
                        @endphp
                        <div class="toc desc" id="code-{!! $fun['function_name'] !!}">
                            @foreach ($descs as $desc)
                            <?php
                            $de = explode(':', $desc);
                            if (isset($de[1]) && App\Helpers\Business::cms_betterStripTags($de[1])) {
                                echo '<p style="height:14px;">' . $de[0] . ': ' . $de[1] . '</p>';
                            } else {
                                ?>
                            <?php } ?>
                            @endforeach
                        </div>

                        <pre class="line-numbers">
@php
    $details = unserialize($fun['function_detail']);
    $details = str_replace('        ', '    ', $details);
    $details = str_replace('    }', '}', $details);
@endphp
<code class="language-php">
{!! nl2br($details) !!}
</code>
                        </pre>
                        @endforeach
                        @endforeach
                        @endforeach

                    </section>

                </article>
                <!-- END Main content -->
            </div>
        </main>
        <!-- Footer -->
        <footer class="site-footer">
            <div class="container">
                <a id="scroll-up" href="#"><i class="fa fa-angle-up"></i></a>
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <p>©2018. All right reserved</p>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <ul class="footer-menu">
                            <li id="menu-item-22" class="menu-item menu-item-type-custom menu-item-object-custom drop-normal"><a title="Contact Us" href="http://mailto:support@shamsoft.net">@Lee Peace</a></li>
                        </ul>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
        <!-- END Footer -->
        <!-- Scripts -->
        <script type='text/javascript' src='{{ Asset::get('/assets-docs/assets/js/theDocs.all.min.js') }}'></script>
        <script type='text/javascript' src='{{ Asset::get('/assets-docs/assets/js/custom.js') }}'></script>
    </body>
</html>