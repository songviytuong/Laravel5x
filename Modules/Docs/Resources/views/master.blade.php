<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <title>TheDocs -   Code view</title>
        <link rel='stylesheet' id='theDocs.all.min.css-css'  href='{{ Asset::get('/assets-docs/assets/css/theDocs.all.min.css') }}' type='text/css' media='all' />
        <link rel='stylesheet' id='custom.css-css'  href='{{ Asset::get('/assets-docs/assets/css/custom.css') }}' type='text/css' media='all' />
        <link rel='stylesheet' id='fonts.googleapis.com-css'  href='http://fonts.googleapis.com/css?family=Raleway%3A100%2C300%2C400%2C500%7CLato%3A300%2C400&#038;ver=4.9.5' type='text/css' media='all' />
        <link rel='stylesheet' id='style-css'  href='{{ Asset::get('/assets-docs/assets/css/style.css') }}' type='text/css' media='all' />
        <!-- Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Raleway:100,300,400,500%7CLato:300,400' rel='stylesheet' type='text/css'>
        <!-- Favicons -->
    </head>

    <body class="page-template page-template-page-templates page-template-page-other page-template-page-templatespage-other-php page page-id-253 wpb-js-composer js-comp-ver-4.11.2 vc_responsive">

        <header class="site-header sticky navbar-fullwidth navbar-transparent">

            <!-- Top navbar & branding -->
            <nav class="navbar navbar-default">
                <div class="container">

                    <!-- Toggle buttons and brand -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar" aria-expanded="true" aria-controls="navbar">
                            <span class="glyphicon glyphicon-option-vertical"></span>
                        </button>

                        <button type="button" class="navbar-toggle for-sidebar" data-toggle="offcanvas">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <a class="navbar-brand" href="{{ route('docs.index') }}">
                            <img class="logo" alt="logo" src="{{ Asset::get('/assets-docs/assets/img/logo.png') }}" /></a>
                    </div>
                    <!-- END Toggle buttons and brand -->

                    <!-- Top navbar -->
                    <div id="navbar" class="navbar-collapse collapse" aria-expanded="true" role="banner">
                        <ul class="nav navbar-nav navbar-right  "><li id="menu-item-317" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home drop-normal"><a  title="DUCUMENTATION" href="">DUCUMENTATION</a></li>
                            <li id="menu-item-318" class="menu-item menu-item-type-post_type menu-item-object-page drop-normal"><a  title="FAQ" href="">FAQ</a></li>
                            <li id="menu-item-18" class="menu-item menu-item-type-custom menu-item-object-custom drop-normal"><a  title="SUPPORT" href="">SUPPORT</a></li>
                            <li id="menu-item-19" class="menu-item menu-item-type-custom menu-item-object-custom drop-normal"><a  title="TESTING" href="">TESTING</a></li>
                        </ul>          </div>
                    <!-- END Top navbar -->

                </div>
            </nav>
            <!-- END Top navbar & branding -->

        </header>    <main class="container">
            <div class="row">
                <!-- Sidebar -->
                <aside class="col-md-3 col-sm-3 sidebar">

                    <ul class="sidenav dropable sticky  "><li id="menu-item-108" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home drop-normal"><a  title="Overview" href="{{ route('docs.index') }}">Tests</a></li>
                        <li id="menu-item-1" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children drop-normal submenu"><a title="Feature" href="">Feature</a>
                            <ul role="menu" class="">
                                <li id="menu-item-1" class="menu-item menu-item-type-post_type menu-item-object-page drop-normal"><a title="" href="">File 1</a></li>
                            </ul>
                        </li>
                        <li id="menu-item-2" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children drop-normal submenu"><a title="Feature" href="">Unit</a>
                            <ul role="menu" class="">
                                <li id="menu-item-1" class="menu-item menu-item-type-post_type menu-item-object-page drop-normal"><a title="" href="">File 1</a></li>
                            </ul>
                        </li>
                    </ul>
                </aside>
                <!-- END Sidebar -->
                <!-- Main content -->
                <article class="col-md-9 col-sm-9 main-content" role="main">


                    <header>
                        <h1>Code view</h1>
                        <p>One of the most frequently used component of each documentation, is display a preview for a code snippet and describing it. In this page we will show different code view components of theDocs.</p>
                        <ol class="toc">
                            <li>
                                <a href="#">Feature</a></p>
                                <ol>
                                    <li><a href="#code-blocks-inline">File 1</a></li>
                                    <li><a href="#code-blocks-inline">File 2</a></li>
                                </ol>
                            </li>
                            <li>
                                <a href="#">Unit</a></p>
                                <ol>
                                    <li><a href="#code-blocks-inline">File 1</a></li>
                                    <li><a href="#code-blocks-inline">File 2</a></li>
                                </ol>
                            </li>
                        </ol>
                    </header>
                    <section>
                        <h2 id="simple-code-blocks">Simple code blocks</h2>
                        <p>Here is basic usage of tags like <code>code</code>, <code>pre</code>, <code>kbd</code>, <code>var</code> and <code>samp</code>.</p>
                        <h3 id="code-blocks-inline">Inline</h3>
                        <p>Wrap inline snippets of code with <code>&lt;code&gt;</code>.</p>
                        <div class="code-snippet">
                            <div class="code-preview">
                                For example, <code>&lt;section&gt;</code> should be wrapped as inline.
                            </div>
                            <pre><code class="language-markup">For example, &lt;code&gt;&amp;lt;section&amp;gt;&lt;/code&gt; should be wrapped as inline.</code></pre>
                            </p></div>
                        <h3 id="code-blocks-pre">Basic block</h3>
                        <p>Use <code>&lt;pre&gt;</code> for multiple lines of code. Be sure to escape any angle brackets in the code for proper rendering.</p>
                        <div class="code-snippet">
                            <div class="code-preview">
                                <pre>Sample text here...</pre>
                                </p></div>
                            <pre><code class="language-markup">&lt;pre&gt;Sample text here...&lt;/pre&gt;</code></pre>
                            </p></div>
                        <p>You may optionally add the <code>.pre-scrollable</code> class, which will set a max-height of 350px and provide a y-axis scrollbar.</p>
                        <h3 id="code-blocks-kbd">User input</h3>
                        <p>Use the <code>&lt;kbd&gt;</code> to indicate input that is typically entered via keyboard.</p>
                        <div class="code-snippet">
                            <div class="code-preview">
                                To switch directories, type <kbd>cd</kbd> followed by the name of the directory.<br />
                                To edit settings, press <kbd><kbd>ctrl</kbd> + <kbd>,</kbd></kbd>
                            </div>
                            <pre><code class="language-markup">
To switch directories, type &lt;kbd&gt;cd&lt;/kbd&gt; followed by the name of the directory.&lt;br&gt;
To edit settings, press &lt;kbd&gt;&lt;kbd&gt;ctrl&lt;/kbd&gt; + &lt;kbd&gt;,&lt;/kbd&gt;&lt;/kbd&gt;</code></pre>
                            </p></div>
                        <h3 id="code-blocks-var">Variables</h3>
                        <p>For indicating variables use the <code>&lt;var&gt;</code> tag.</p>
                        <div class="code-snippet">
                            <div class="code-preview">
                                <var>y</var> = <var>m</var><var>x</var> + <var>b</var>
                            </div>
                            <pre><code class="language-markup">&lt;var&gt;y&lt;/var&gt; = &lt;var&gt;m&lt;/var&gt;&lt;var&gt;x&lt;/var&gt; + &lt;var&gt;b&lt;/var&gt;</code></pre>
                            </p></div>
                        <h3 id="code-blocks-samp">Sample output</h3>
                        <p>For indicating blocks sample output from a program use the <code>&lt;samp&gt;</code> tag.</p>
                        <div class="code-snippet">
                            <div class="code-preview">
                                <samp>This text is meant to be treated as sample output from a computer program.</samp>
                            </div>
                            <pre><code class="language-markup">&lt;samp&gt;This text is meant to be treated as sample output from a computer program.&lt;/samp&gt;</code></pre>
                            </p></div>
                    </section>
                    <section>
                        <h2 id="syntax-highlighter">Syntax highlighter</h2>
                        <p>theDocs uses <code>PrismJs</code> to syntax highlight code snippets. You have to put your code inside <code>&lt;pre&gt;&lt;code class=&quot;language-xxxx&quot;&gt;</code> which <code>xxxx</code> is the name of language.</p>
                        <pre><code class="language-markup">
&lt;p&gt;Something to show in &lt;strong&gt;bold&lt;/strong&gt; text.&lt;/p&gt;
...
&lt;i>Italic text&lt;/i>
</code></pre>
                        <p>If you need to include line numbers, add class <code>.line-numbers</code> to the <code>&lt;pre></code> tag.</p>
                        <pre class="line-numbers"><code class="language-markup">
&lt;p&gt;Something to show in &lt;strong&gt;bold&lt;/strong&gt; text.&lt;/p&gt;
...
&lt;i>Italic text&lt;/i>
</code></pre>
                        <h3 id="supported-languages">Supported languages</h3>
                        <p>This is the list of all languages currently supported by Prism, with their corresponding alias, to use in place of <code>xxxx</code> in the <code>language-xxxx</code> class:</p>
                        <ul class="prism-languages">
                            <li>HTML/XML <code>markup</code></li>
                            <li>CSS <code>css</code></li>
                            <li>C-like <code>clike</code></li>
                            <li>JavaScript <code>javascript</code></li>
                            <li>ASP.NET (C#) <code>aspnet</code></li>
                            <li>Bash <code>bash</code></li>
                            <li>C <code>c</code></li>
                            <li>C# <code>csharp</code></li>
                            <li>C++ <code>cpp</code></li>
                            <li>CoffeeScript <code>coffeescript</code></li>
                            <li>Git <code>git</code></li>
                            <li>HTTP <code>http</code></li>
                            <li>Ini <code>ini</code></li>
                            <li>Java <code>java</code></li>
                            <li>LaTeX <code>latex</code></li>
                            <li>Less <code>less</code></li>
                            <li>MATLAB <code>matlab</code></li>
                            <li>Objective-C <code>objectivec</code></li>
                            <li>Perl <code>perl</code></li>
                            <li>PHP <code>php</code></li>
                            <li>Python <code>python</code></li>
                            <li>Ruby <code>ruby</code></li>
                            <li>Sass (Scss) <code>scss</code></li>
                            <li>SQL <code>sql</code></li>
                            <li>Swift <code>swift</code></li>
                        </ul>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <h2 id="code-wrappers">Code wrappers</h2>
                        <p>If your code snippet includes several languages or a preview of result, it&#8217;s better to use one of the following code wrappers.</p>
                        <p>            <!-- Code Window --></p>
                        <h3 id="code-window">Code window</h3>
                        <p>Wrap your code and preview inside <code>&lt;div class=&quot;code-window&quot;&gt;...&lt;/div&gt;</code> to show them in a window style with different tabs. In this way, reader can see one code snippet in a same time.</p>
                        <div class="code-window">
                            <div class="code-preview">
                                <p>This is a normal paragraph without any contextual classes.</p>
                                <p class="text-primary">A paragraph with .text-primary class.</p>
                                <p class="text-success">A paragraph with .text-success class.</p>
                                <p class="text-info">A paragraph with .text-info class.</p>
                                <p class="text-warning">A paragraph with .text-warning class.</p>
                                <p class="text-danger">A paragraph with .text-danger class.</p>
                                <p class="text-purple">A paragraph with .text-purple class.</p>
                                <p class="text-teal">A paragraph with .text-teal class.</p>
                                <p class="text-gray">A paragraph with .text-gray class.</p>
                                <p class="text-dark">A paragraph with .text-dark class.</p>
                                </p></div>
                            <pre class="line-numbers"><code class="language-markup">
&lt;p&gt;This is a normal paragraph without any contextual classes.&lt;/p&gt;
&lt;p class=&quot;text-primary&quot;&gt;A paragraph with .text-primary class.&lt;/p&gt;
&lt;p class=&quot;text-success&quot;&gt;A paragraph with .text-success class.&lt;/p&gt;
&lt;p class=&quot;text-info&quot;&gt;A paragraph with .text-info class.&lt;/p&gt;
&lt;p class=&quot;text-warning&quot;&gt;A paragraph with .text-warning class.&lt;/p&gt;
&lt;p class=&quot;text-danger&quot;&gt;A paragraph with .text-danger class.&lt;/p&gt;
&lt;p class=&quot;text-purple&quot;&gt;A paragraph with .text-purple class.&lt;/p&gt;
&lt;p class=&quot;text-teal&quot;&gt;A paragraph with .text-teal class.&lt;/p&gt;
&lt;p class=&quot;text-gray&quot;&gt;A paragraph with .text-gray class.&lt;/p&gt;
&lt;p class=&quot;text-dark&quot;&gt;A paragraph with .text-dark class.&lt;/p&gt;
</code></pre>
                            <pre class="line-numbers"><code class="language-css">
.text-primary { color: #2196F3; }
.text-success { color: #4CAF50; }
.text-info    { color: #29B6F6; }
.text-warning { color: #FF9800; }
.text-danger  { color: #F44336; }
.text-purple  { color: #6D5CAE; }
.text-teal    { color: #00BFA5; }
.text-gray    { color: #bbbbbb; }
.text-dark    { color: #424242; }
.text-white   { color: #ffffff; }
</code></pre>
                            <pre class="line-numbers"><code class="language-javascript">
$('.sidenav.dropable > li > a').click(function(e){
  if ( 0 == $(this).next("ul").size() || 0 == $(this).next("ul:hidden").size() ) {
      return;
  }
  e.preventDefault();
  $(this).parents(".sidenav").find("ul").not(":hidden").slideUp(300);
  $(this).next("ul").slideDown(300);
});
</code></pre>
                            </p></div>
                        <p>Here is the code which we used to draw above code window:</p>
                        <pre class="line-numbers">
<code class="language-php">
public function index() {
    $data = $this->getDocuments();
    print_r($data);
    return view('docs::index');
}
</code></pre>
                        <p>            <!-- Code Tabs --></p>
                        <h3 id="code-tabs">Code tabs</h3>
                        <p>Wrap your code and preview inside <code>&lt;div class=&quot;code-tabs&quot;&gt;...&lt;/div&gt;</code> to show them in a horizontal tab style.</p>
                        <div class="code-tabs">
                            <div class="code-preview">
                                <p>This is a normal paragraph without any contextual classes.</p>
                                <p class="text-primary">A paragraph with .text-primary class.</p>
                                <p class="text-success">A paragraph with .text-success class.</p>
                                <p class="text-info">A paragraph with .text-info class.</p>
                                <p class="text-warning">A paragraph with .text-warning class.</p>
                                <p class="text-danger">A paragraph with .text-danger class.</p>
                                <p class="text-purple">A paragraph with .text-purple class.</p>
                                <p class="text-teal">A paragraph with .text-teal class.</p>
                                <p class="text-gray">A paragraph with .text-gray class.</p>
                                <p class="text-dark">A paragraph with .text-dark class.</p>
                                </p></div>
                            <pre class="line-numbers"><code class="language-markup">
&lt;p&gt;This is a normal paragraph without any contextual classes.&lt;/p&gt;
&lt;p class=&quot;text-primary&quot;&gt;A paragraph with .text-primary class.&lt;/p&gt;
&lt;p class=&quot;text-success&quot;&gt;A paragraph with .text-success class.&lt;/p&gt;
&lt;p class=&quot;text-info&quot;&gt;A paragraph with .text-info class.&lt;/p&gt;
&lt;p class=&quot;text-warning&quot;&gt;A paragraph with .text-warning class.&lt;/p&gt;
&lt;p class=&quot;text-danger&quot;&gt;A paragraph with .text-danger class.&lt;/p&gt;
&lt;p class=&quot;text-purple&quot;&gt;A paragraph with .text-purple class.&lt;/p&gt;
&lt;p class=&quot;text-teal&quot;&gt;A paragraph with .text-teal class.&lt;/p&gt;
&lt;p class=&quot;text-gray&quot;&gt;A paragraph with .text-gray class.&lt;/p&gt;
&lt;p class=&quot;text-dark&quot;&gt;A paragraph with .text-dark class.&lt;/p&gt;
</code></pre>
                            <pre class="line-numbers"><code class="language-css">
.text-primary { color: #2196F3; }
.text-success { color: #4CAF50; }
.text-info    { color: #29B6F6; }
.text-warning { color: #FF9800; }
.text-danger  { color: #F44336; }
.text-purple  { color: #6D5CAE; }
.text-teal    { color: #00BFA5; }
.text-gray    { color: #bbbbbb; }
.text-dark    { color: #424242; }
.text-white   { color: #ffffff; }
</code></pre>
                            <pre class="line-numbers"><code class="language-javascript">
$('.sidenav.dropable > li > a').click(function(e){
  if ( 0 == $(this).next("ul").size() || 0 == $(this).next("ul:hidden").size() ) {
      return;
  }
  e.preventDefault();
  $(this).parents(".sidenav").find("ul").not(":hidden").slideUp(300);
  $(this).next("ul").slideDown(300);
});
</code></pre>
                            </p></div>
                        <p>Here is the code which we used to draw above code tabs:</p>
                        <pre class="line-numbers"><code class="language-markup">
&lt;div class=&quot;code-tabs&quot;&gt;
  &lt;div class=&quot;code-preview&quot;&gt;...&lt;/div&gt;
  &lt;pre class=&quot;line-numbers&quot;&gt;&lt;code class=&quot;language-markup&quot;&gt;...&lt;/code&gt;&lt;/pre&gt;
  &lt;pre class=&quot;line-numbers&quot;&gt;&lt;code class=&quot;language-css&quot;&gt;...&lt;/code&gt;&lt;/pre&gt;
  &lt;pre class=&quot;line-numbers&quot;&gt;&lt;code class=&quot;language-javascript&quot;&gt;...&lt;/code&gt;&lt;/pre&gt;
&lt;/div&gt;
</code></pre>
                        <p>            <!-- Code Snippet --></p>
                        <h3 id="code-snippet">Code snippet</h3>
                        <p>Wrap your code and preview inside <code>&lt;div class=&quot;code-snippet&quot;&gt;...&lt;/div&gt;</code> to show them in a vertical style. Use this style to show all of the codes in a same view.</p>
                        <div class="code-snippet">
                            <div class="code-preview">
                                <p>This is a normal paragraph without any contextual classes.</p>
                                <p class="text-primary">A paragraph with .text-primary class.</p>
                                <p class="text-success">A paragraph with .text-success class.</p>
                                <p class="text-info">A paragraph with .text-info class.</p>
                                <p class="text-warning">A paragraph with .text-warning class.</p>
                                <p class="text-danger">A paragraph with .text-danger class.</p>
                                <p class="text-purple">A paragraph with .text-purple class.</p>
                                <p class="text-teal">A paragraph with .text-teal class.</p>
                                <p class="text-gray">A paragraph with .text-gray class.</p>
                                <p class="text-dark">A paragraph with .text-dark class.</p>
                                </p></div>
                            <pre class="line-numbers"><code class="language-markup">
&lt;p&gt;This is a normal paragraph without any contextual classes.&lt;/p&gt;
&lt;p class=&quot;text-primary&quot;&gt;A paragraph with .text-primary class.&lt;/p&gt;
&lt;p class=&quot;text-success&quot;&gt;A paragraph with .text-success class.&lt;/p&gt;
&lt;p class=&quot;text-info&quot;&gt;A paragraph with .text-info class.&lt;/p&gt;
&lt;p class=&quot;text-warning&quot;&gt;A paragraph with .text-warning class.&lt;/p&gt;
&lt;p class=&quot;text-danger&quot;&gt;A paragraph with .text-danger class.&lt;/p&gt;
&lt;p class=&quot;text-purple&quot;&gt;A paragraph with .text-purple class.&lt;/p&gt;
&lt;p class=&quot;text-teal&quot;&gt;A paragraph with .text-teal class.&lt;/p&gt;
&lt;p class=&quot;text-gray&quot;&gt;A paragraph with .text-gray class.&lt;/p&gt;
&lt;p class=&quot;text-dark&quot;&gt;A paragraph with .text-dark class.&lt;/p&gt;
</code></pre>
                            <pre class="line-numbers"><code class="language-css">
.text-primary { color: #2196F3; }
.text-success { color: #4CAF50; }
.text-info    { color: #29B6F6; }
.text-warning { color: #FF9800; }
.text-danger  { color: #F44336; }
.text-purple  { color: #6D5CAE; }
.text-teal    { color: #00BFA5; }
.text-gray    { color: #bbbbbb; }
.text-dark    { color: #424242; }
.text-white   { color: #ffffff; }
</code></pre>
                            <pre class="line-numbers"><code class="language-javascript">
$('.sidenav.dropable > li > a').click(function(e){
  if ( 0 == $(this).next("ul").size() || 0 == $(this).next("ul:hidden").size() ) {
      return;
  }
  e.preventDefault();
  $(this).parents(".sidenav").find("ul").not(":hidden").slideUp(300);
  $(this).next("ul").slideDown(300);
});
</code></pre>
                            </p></div>
                        <p>Here is the code which we used to draw above code snippet:</p>
                        <pre class="line-numbers"><code class="language-markup">
&lt;div class=&quot;code-snippet&quot;&gt;
  &lt;div class=&quot;code-preview&quot;&gt;...&lt;/div&gt;
  &lt;pre class=&quot;line-numbers&quot;&gt;&lt;code class=&quot;language-markup&quot;&gt;...&lt;/code&gt;&lt;/pre&gt;
  &lt;pre class=&quot;line-numbers&quot;&gt;&lt;code class=&quot;language-css&quot;&gt;...&lt;/code&gt;&lt;/pre&gt;
  &lt;pre class=&quot;line-numbers&quot;&gt;&lt;code class=&quot;language-javascript&quot;&gt;...&lt;/code&gt;&lt;/pre&gt;
&lt;/div&gt;
</code></pre>
                        <div class="jumbotron jumbotron-sm">
                            <p>As you can see, switching between three wrapper types is only by changing classes between <code>.code-window</code>, <code>.code-snippet</code> and <code>.code-tabs</code>.</p>
                            </p></div>
                    </section>




                </article>
                <!-- END Main content -->
            </div>


        </main>

        <!-- Footer -->
        <footer class="site-footer">
            <div class="container">
                <a id="scroll-up" href="#"><i class="fa fa-angle-up"></i></a>

                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <p>                        Copyright © 2015. All right reserved                        </p>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <ul class="footer-menu">
                            <ul class="footer-menu  "><li id="menu-item-320" class="menu-item menu-item-type-post_type menu-item-object-page drop-normal"><a  title="Changelog" href="/changelog/">Changelog</a></li>
                                <li id="menu-item-319" class="menu-item menu-item-type-post_type menu-item-object-page drop-normal"><a  title="Credits" href="/credits/">Credits</a></li>
                                <li id="menu-item-22" class="menu-item menu-item-type-custom menu-item-object-custom drop-normal"><a  title="Contact Us" href="http://mailto:support@shamsoft.net">Contact Us</a></li>
                            </ul>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
        <!-- END Footer -->

        <!-- Scripts -->
        <script type='text/javascript' src='{{ Asset::get('/assets-docs/assets/js/theDocs.all.min.js') }}'></script>
        <script type='text/javascript' src='{{ Asset::get('/assets-docs/assets/js/custom.js') }}'></script>
    </body>
</html>

