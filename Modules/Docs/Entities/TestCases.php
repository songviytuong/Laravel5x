<?php

namespace Modules\Docs\Entities;

use Illuminate\Database\Eloquent\Model;
use Cache;
use File;

class TestCases extends Model {

    protected $timestamp = false;
    const UPDATED_AT = null;

    protected $fillable = [
        'id', 'type', 'class', 'testcase', 'body', 'documents',
    ];
    protected $table = "5x_testcases";

    public static function updateTestCases($methods) {

        foreach ($methods as $key => $method) {
            $className = $method['className'];
            $folder = $method['testType'];

            $file = app_path() . '/../tests/' . $folder . '/' . $className . '.php';
            $class = "\\Tests\\" . $folder . "\\" . $className;
            if (file_exists($file)) {

                $fh = fopen($file, 'r');
                $linecount = 0;
                $array = [];

                while (!feof($fh)) {
                    $line = fgets($fh);
                    if ($line == '') {
                        continue;
                    }
                    $array[] = $line;
                    $linecount++;
                }
                $fruit = array_pop($array);

                $test = new $class;
                $method = $methods[$key];
                if (!method_exists($test, $method['functionName'])) {
                    if (!($method === reset($methods))) {
                        $array[] = "\n";
                    }
                    $array[] = $method['functionDocuments'] . "\n";
                    $array[] = "    public final function " . $method['functionName'] . '() {' . "\n";
                    $array[] = "        " . $method['functionBody'] . "\n";
                    $array[] = "    }" . "\n";
                }

                $array[] = '}' . "\n";
                $content = implode($array, '');
                $fh1 = fopen($file, 'w');
                fwrite($fh1, $content);
                fclose($fh);
                fclose($fh1);
            }
        }

        if (Cache::has('cms_getDocuments')) {
            Cache::forget('cms_getDocuments');
        }
    }

    public static function syncTestCases() {
      // TODO: Hello Peace
    }

}
