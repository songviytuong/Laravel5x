<?php

Route::group(['prefix' => 'contacts', 'namespace' => 'Modules\Contacts\Http\Controllers'], function()
{
    Route::get('/', 'ContactsController@index');
});
