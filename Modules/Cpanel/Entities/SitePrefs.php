<?php

namespace Modules\Cpanel\Entities;

use Illuminate\Database\Eloquent\Model;

class SitePrefs extends Model
{
    protected $fillable = [];
    protected $table = '5x_siteprefs';
}
