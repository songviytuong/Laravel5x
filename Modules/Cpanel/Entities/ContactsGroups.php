<?php

namespace Modules\Cpanel\Entities;

use stdClass;
use ArrayObject;
use Illuminate\Database\Eloquent\Model;

class ContactsGroups extends Model {

    protected $fillable = [];
    protected $table = '5x_contacts_groups';
    public $timestamps = false;

    public static function listContactsGroups() {
        $conditionAnd = [];
        $conditionAnd['status'] = 1;
        $conditionAnd['deleted_at'] = null;

        $result = ContactsGroups::where($conditionAnd)->get();
        $MyObjects = array([]);
        
        if ($result) {
            foreach ($result as $key=>$items) {
                $MyObject = new stdClass();
                $MyObject->id = $items->id;
                $MyObject->group_name = $items->group_name;
                $MyObject->total = Contacts::where('group_id', $items->id)->count();
                $MyObjects[$key] = $MyObject;
            }
            return $MyObjects;
        }
    }

    public static function getIDFromGroupName($group_name) {
        if ($group_name) {
            $group_id = ContactsGroups::where('group_name', $group_name)->select('id')->get()->first();
            return $group_id->id;
        } return false;
    }

}
