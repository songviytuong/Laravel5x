<?php

namespace Modules\Cpanel\Entities;

use Illuminate\Database\Eloquent\Model;

class Activities extends Model {

    protected $fillable = [];
    protected $table = '5x_activities';
    public $timestamps = false;

    public static function listActivities($orderBy = 'id', $sort = 'DESC') {

        $conditionAnd = [];
        $conditionAnd['deleted_at'] = null;

        $result = Activities::where($conditionAnd)
                ->orderBy($orderBy, $sort)
                ->get();
        if ($result) {
            return $result;
        }
    }

}
