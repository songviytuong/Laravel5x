<?php

namespace Modules\Cpanel\Entities;

use Illuminate\Database\Eloquent\Model;

class Tasks extends Model {

    protected $fillable = [];
    protected $table = '5x_tasks';
    public $timestamps = false;

    public static function listTasks($request) {

        $conditionAnd = [];
        $conditionAnd['deleted_at'] = null;

        $type = $request->type;
        $arr = ['pendingTasks', 'completedTasks', 'allTasks'];
        if ($type && in_array($type, $arr)) {
            switch ($type) {
                case 'pendingTasks':
                    $conditionAnd['status'] = 0;
                    break;
                case 'completedTasks':
                    $conditionAnd['status'] = 1;
                    break;
                case 'allTasks':
                    break;
            }
        }

        if ($request->project_id) {
            $conditionAnd['project_id'] = $request->project_id;
        }

        if ($request->alias) {
            $project = Projects::where(['alias' => $request->alias])->select('id')->get()->first();
            if ($project) {
                $conditionAnd['project_id'] = $project->id;
            }
        }

        $result = Tasks::where($conditionAnd)->get();
        if ($result) {
            return $result;
        }
    }

}
