<?php

namespace Modules\Cpanel\Entities;

use Illuminate\Database\Eloquent\Model;

class Contacts extends Model {

    protected $fillable = [];
    protected $table = '5x_contacts';
    public $timestamps = false;

    public static function listContacts($params = []) {
        $conditionAnd = [];
        $conditionAnd['status'] = 1;
        $conditionAnd['deleted_at'] = null;

        if (!empty($params['all'])) {
            
        } else {
            if (isset($params['user_id'])) {
                $conditionAnd['user_id'] = $params['user_id'];
            }

            if (isset($params['group_id'])) {
                $conditionAnd['group_id'] = $params['group_id'];
            }
        }

        $result = Contacts::where($conditionAnd)->get();

        // $result->leftJoin('5x_contacts_groups', function ($join) {
        //     $join->on('5x_contacts_groups.id', '=', '5x_contacts.group_id');
        // })->select('*')->get();
//        var_dump($params);
//        echo Contacts::where($conditionAnd)->toSql();
        if ($result) {
            return Contacts::where($conditionAnd)->paginate(config('cpanel.records_per_page'));
        }
    }
    
    

}
