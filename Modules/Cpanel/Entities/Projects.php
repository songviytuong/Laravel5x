<?php

namespace Modules\Cpanel\Entities;

use App\Helpers\Business;
use Illuminate\Database\Eloquent\Model;

class Projects extends Model
{
    protected $fillable = [];
    protected $table = '5x_projects';
    public $timestamps = false;

    public static function listProjects()
    {
        $conditionAnd = [];
        $conditionAnd['status'] = 1;
        $conditionAnd['deleted_at'] = null;

        $result = Projects::where($conditionAnd)->get();
        if ($result) {
            return $result;
        }
    }

    public static function alias_exists($alias, $except = [])
    {
        if ($alias) {

            $conditionAnd = [];
            $conditionAnd['status'] = 1;
            $conditionAnd['alias'] = $alias;
            $conditionAnd['deleted_at'] = null;

            if (Projects::where($conditionAnd)->exists()) {
                return true;
            } elseif (in_array($alias, $except)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static function getIDByAlias($alias)
    {
        $projects = Projects::where(['alias' => $alias])->get()->first();
        $project_id = ($projects) ? $projects->id : 1;
        return $project_id;
    }

    public static function getProjectById($id)
    {
        if ($id) {
            return Projects::where(['id' => $id])->get()->first();
        } else {
            return false;
        }
    }
}
