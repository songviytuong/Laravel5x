<?php

namespace Modules\Cpanel\Jobs;

use Illuminate\Bus\Queueable;
use Modules\Cpanel\Entities\Tasks;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class JTasks implements ShouldQueue
{
    use Dispatchable,
        InteractsWithQueue,
        Queueable,
        SerializesModels;

    protected $request;
    protected $table = 'jobs';

    /**
     * Create a new job instance.
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Execute the job.
     */
    public function handle()
    {
        switch ($this->request['type']) {
            case 'status':
                $updated = [
                    'status' => $this->request['status'],
                ];
                Tasks::where(['task_name' => $this->request['task_name']])->update($updated);
                break;
            case 'task_name':
                $updated = [
                    'task_name' => $this->request['task_name'],
                ];
                Tasks::where(['id' => $this->request['task_id']])->update($updated);
                break;
        }
    }
}
