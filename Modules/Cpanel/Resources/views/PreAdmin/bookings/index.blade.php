@extends('cpanel::PreAdmin.layouts.master')
@section('body-content')
<div class="main-wrapper">
    @include('cpanel::PreAdmin.layouts.header')
    @include('cpanel::PreAdmin.layouts.sidebar')
    <div class="page-wrapper">
        <div class="content container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="page-title">Booking</h4>
                    {{--
                    <div class="alert update_status alert-warning alert-dismissible fade hide" role="alert">
                        <div class="message"></div>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    --}}
                                    </div>
                                    <!--<div class="col-sm-11 col-7 text-right m-b-30">-->
                                    <!--                    <div class="btn-group">
                                                            <span class="update_status btn"></span>
                                        <a href="#" class="btn btn-secondary sync-all"><i class="fa fa-refresh"></i> Sync</a>
                                    </div>-->
                    <!--                </div>-->
                </div>
                <div class="row filter-row">
                    <div class="col-sm-6 col-md-3 col-lg-3 col-xl-2 col-12">
                        <div class="form-group form-focus select-focus">
                            <label class="focus-label">Purchased By</label>
                            <select class="select floating">
                                <option> -- Select -- </option>
                                <option>Loren Gatlin</option>
                                <option>Tarah Shropshire</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3 col-lg-3 col-xl-2 col-12">
                        <div class="form-group form-focus select-focus">
                            <label class="focus-label">Paid By</label>
                            <select class="select floating">
                                <option> -- Select -- </option>
                                <option> Cash </option>
                                <option> Cheque </option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3 col-lg-3 col-xl-2 col-12">
                        <div class="form-group form-focus">
                            <label class="focus-label">From</label>
                            <div class="cal-icon">
                                <input class="form-control floating datetimepicker" type="text">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3 col-lg-3 col-xl-2 col-12">
                        <div class="form-group form-focus">
                            <label class="focus-label">To</label>
                            <div class="cal-icon">
                                <input class="form-control floating datetimepicker" type="text">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3 col-lg-3 col-xl-2 col-12">
                        <a href="#" class="btn btn-secondary sync-all btn-block"><i class="fa fa-refresh"></i> Sync Data </a>
                    </div>
                    <div class="col-sm-6 col-md-3 col-lg-3 col-xl-2 col-12">
                        <a href="#" class="btn btn-success btn-block"> Search </a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <div class="card-block">
                                <!--<h6 class="card-title text-bold">Default Datatable</h6>-->

                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="table-responsive loadBooking">
                                            <table id="bookings" class="table table-striped custom-table m-b-0 _datatable">
                                                <thead>
                                                    <tr>
                                                        <th>OrderID</th>
                                                        <th>Customer Name</th>
                                                        <th>Booking Date</th>
                                                        <th>Phone</th>
                                                        <th>Amount</th>
                                                        <th>Paid By</th>
                                                        <th class="text-center">Status</th>
                                                        <th class="text-right">Actions</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="delete_group" class="modal custom-modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content modal-md">
                    <div class="modal-header">
                        <h4 class="modal-title">Delete Group</h4>
                    </div>
                    <div class="modal-body card-box">
                        <p>Are you sure want to delete this?</p>
                        <div class="m-t-20"> <a href="#" class="btn btn-white" data-dismiss="modal">Close</a>
                            <button type="button" class="btn btn-danger removeGroup">Delete</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @stop
    @section('script')
    <script>
        var vdata = "<?= count($bookings); ?>";
        $(document).ready(function () {
            setTimeout(function () {
                if (vdata == 0) {
                    $('.sync-all').addClass("disabled");
                }
            }, 100);
        });

        //Sync All
        $('.sync-all').click(function () {
            $('.update_status').removeClass('alert-success');
            $('.update_status').addClass('alert-warning');
            $('#bookings').find('tr').find('td:eq(5)').text('Pending');
            $('#bookings').find('tr').find('td:eq(5)').removeClass('text-success');
            $('#bookings').find('tr').find('td:eq(5)').removeClass('text-danger');
            var num = $('#bookings').find('tbody').children('tr').length;
            $('.fa-refresh').addClass("fa-spin");
            $(this).addClass("disabled");
            send_ajax(num, 1);
        });
        function send_ajax(num, index) {

            if (index > num || ($('#bookings').find('tr:eq(' + index + ')').find('td:eq(5)').text() == "")) {
                $('.update_status').removeClass('alert-warning');
                $('.update_status').addClass('alert-success');
                $('.message').text('Booking has been successfully updated at: ' + '{!! Carbon\Carbon::now()->toDateTimeString(); !!}');
                $('.fa-refresh').removeClass("fa-spin");
                $('.sync-all').removeClass("disabled");
                return false;
            } else {
                $.ajax({
                    url: "{{route('cpanel.bookings.ajax.syncAllData')}}",
                    dataType: "JSON",
                    type: "POST",
                    context: this,
                    beforeSend: function () {
                        //some condition 
                        $('#bookings').find('tr:eq(' + index + ')').find('td:eq(5)').addClass('text-danger');
                        $('#bookings').find('tr:eq(' + index + ')').find('td:eq(5)').text('Updating...')
                    },
                    success: function (result) {
                        if (!$('.alert').hasClass('update_status')) {
                            $('.page-title').after('<div class="alert update_status alert-warning alert-dismissible fade hide" role="alert"><div class="message"></div><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button></div>');
                        }
                        $('.update_status').addClass('show');
                        if (result.Updated) {
                            $('#bookings').find('tr:eq(' + index + ')').find('td:eq(5)').text('Updated');
                            $('#bookings').find('tr:eq(' + index + ')').find('td:eq(5)').removeClass('text-danger');
                            $('#bookings').find('tr:eq(' + index + ')').find('td:eq(5)').addClass('text-success');
                        } else {
                            //                    $('#sync_data_' + index).removeClass('text-warning').addClass('text-danger');
                            //                    $('#sync_data_' + index).html(result.CurrentStatus);
                        }
                    }
                }).always(function () {
                    console.log($('#bookings').find('tr:eq(' + index + ')').attr('id'));
                    $('.message').text('Booking is updating: ' + index + '/' + num);
                    send_ajax(num, ++index);
                });
            }


        }

        var myTable = $('#bookings').dataTable({

            "processing": true,
            "ajax": {
                "url": "{{route('cpanel.bookings.ajax.load_bookings')}}",
                "type": "POST",
                "data": {'bid': 43}
            },
            "bSearchable": false,
            "aoColumns": [
                {"data": "orderid"},
                {"data": "customer_name"},
                {"data": "booking"},
                {"data": "phone"},
                {
                    "data": "amount",
                    "render": function (data, type, row) {
                        return "$" + data;
                    }
                },
                {"mData": "paid",
                    "mRender": function (data, type, row) {
                        return "Internet Banking";
                    },
                    sClass: "update"
                },
                {
                    "mData": "active",
                    "mRender": function (data, type, row) {
                        var _active = 'Pending';
                        var _class = 'text-danger';
                        if (data == 1) {
                            _class = 'text-success';
                            _active = 'Approved';
                        }
                        return '<div class="dropdown action-label">\n\
                    <a class="btn btn-white btn-sm btn-rounded dropdown-toggle" href="#" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-dot-circle-o ' + _class + '"></i> ' + _active + '\n\
    </a>\n\
    <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="#"><i class="fa fa-dot-circle-o text-danger"></i> Pending</a><a class="dropdown-item" href="#"><i class="fa fa-dot-circle-o text-success"></i> Approved</a></div></div>';
                    },
                    sClass: "text-center"
                },
                {
                    "mData": "Action",
                    "mRender": function (data, type, row) {
                        return '<div class="dropdown dropdown-action">\n\
                    <a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a><div class="dropdown-menu dropdown-menu-right">\n\
    <a class="dropdown-item" href="javascript:void(0);" title="Edit" data-toggle="modal" data-target="#edit_expense"><i class="fa fa-pencil m-r-5"></i> Edit</a>\n\
    <a class="dropdown-item removeBooking" data-id="' + row.booking_id + '" href="javascript:void(0);" title="Delete" data-toggle="modal" data-target="#delete_expense"><i class="fa fa-trash-o m-r-5"></i> Delete</a></div></div>';
                    },
                    sClass: "text-right"
                }
            ]
        });

        function RefreshTable(tableId, urlData)
        {
            $.post(urlData, null, function (json)
            {
                table = $(tableId).dataTable();
                table.fnDraw();
            }, "json");
        }


        function AutoReload()
        {
            //        console.log('Reload');
            //        myTable.fnReloadAjax();
            //        setTimeout(function () {
            //            AutoReload();
            //        }, 30000);
        }
        $(document).ready(function () {
            //        setTimeout(function () {
            //            AutoReload();
            //        }, 30000);
        });


        $(document).on('click', '.removeBooking', function () {
            console.log('Xoa' + $(this).attr('data-id'));
            var row = $(this).parents('tr');
            row.remove();
            RefreshTable('#bookings', "{{route('cpanel.bookings.ajax.load_bookings')}}");
            //        myTable.fnReloadAjax();
        });

        $('.contact-cat').on('click', '.load-contacts', function (e) {
            e.preventDefault();
        });
        $(document).on('click', '.delete-group', function () {

        });
        $('#delete_group').on('click', '.removeGroup', function () {
            //            var _id = $(this).attr('data-id');
            //            var _token = $(this).attr('data-token');
            //            $.ajax({
            //                headers: {
            //                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            //                },
            //                type: 'POST',
            //                url: "{{route('cpanel.contacts.remove.contact')}}",
            //                data: {
            //                    id: _id,
            //                    token: _token
            //                },
            //                dataType: "JSON",
            //                cache: false,
            //                success: function (result) {
            //                    if (result.status) {
            //                        $('#delete_group').modal('hide');
            //                        window.location.href = "{{route('cpanel.contacts.index')}}";
            //                    }
            //                }
            //            });

        });

    </script>
    @endsection