{{-- file: /app/views/layouts/master.blade.php --}}
<!DOCTYPE html>
<html lang="{{App::getLocale()}}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        {{-- Part: site title with default value in parent --}}
        @section('head-title')
        <title>{!!SEO::getTitle()!!}</title>
        @stop
        @yield('head-title')

        @section('head-meta')
        {!!SEO::getMeta()!!}
        @stop

        {{-- Part: all meta-related contents --}}
        @yield('head-meta')
        <meta name="csrf-token" content="{{ csrf_token() }}" />

        {{-- Part: load fonts --}}
        @yield('head-fonts')
        {{-- Part: load styles for the page --}}
        @section('head-styles')
        {!!SEO::getCss()!!}
        @stop
        @yield('head-styles')
        {{-- Part: load scripts needed --}}
        @section('head-scripts')

        @stop
        @yield('head-scripts')
        {{-- Part: anything else in head --}}
        @yield('head-extra')
        <link href="https://fonts.googleapis.com/css?family=Fira+Sans:400,500,600,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        <link rel="stylesheet" type="text/css" href="{!! Asset::get('/assets/cpanel/PreAdmin/light/assets/css/bootstrap.min.css')!!}">
        <link rel="stylesheet" type="text/css" href="{!! Asset::get('/assets/cpanel/PreAdmin/light/assets/css/font-awesome.min.css')!!}">
        @if (in_array(\Request::route()->getName(),['cpanel.tasks.index']))

        @elseif (in_array(\Request::route()->getName(),['cpanel.bookings.index']))
        <link rel="stylesheet" type="text/css" href="{!! Asset::get('/assets/cpanel/PreAdmin/light/assets/css/dataTables.bootstrap4.min.css')!!}">
        @else
        <link rel="stylesheet" type="text/css" href="{!! Asset::get('/assets/cpanel/PreAdmin/light/assets/css/fullcalendar.min.css')!!}">
        <link rel="stylesheet" type="text/css" href="{!! Asset::get('/assets/cpanel/PreAdmin/light/assets/css/dataTables.bootstrap.min.css')!!}">
        <link rel="stylesheet" type="text/css" href="{!! Asset::get('/assets/cpanel/PreAdmin/light/assets/plugins/morris/morris.css')!!}">
        @endif
        <link rel="stylesheet" type="text/css" href="{!! Asset::get('/assets/cpanel/PreAdmin/light/assets/css/select2.min.css')!!}">
        <link rel="stylesheet" type="text/css" href="{!! Asset::get('/assets/cpanel/PreAdmin/light/assets/css/bootstrap-datetimepicker.min.css')!!}">
        <link rel="stylesheet" type="text/css" href="{!! Asset::get('/assets/cpanel/PreAdmin/'.$data['settings']['color_active'].'/assets/css/style.css')!!}">
        <link rel="shortcut icon" type="image/x-icon" href="/assets/cpanel/PreAdmin/light/assets/img/favicon.png">
    </head>
    <body>
        {{-- Part: something at start of body --}}
        @yield('body-start')

        {{-- Part: create main content of the page --}}
        @yield('body-content')

        <script type="text/javascript">
            var BASEPATH = '/cpanel/';
        </script>

        {{-- Part: load scripts --}}
        @section('body-scripts')
        {!!SEO::getJs()!!}
        @stop
        @yield('body-scripts')
        {{-- Part: something else to do --}}
        @yield('body-others')
        {{-- Part: finalize stuffs if there is --}}
        {{-- Part: footer --}}
        @yield('body-end')

        <div class="sidebar-overlay" data-reff=""></div>

        @if (!in_array(\Request::route()->getName(),[]))
        <script type="text/javascript" src="{!! Asset::get('/assets/cpanel/PreAdmin/light/assets/js/jquery-3.2.1.min.js')!!}"></script>
        <script type="text/javascript" src="{!! Asset::get('/assets/cpanel/PreAdmin/light/assets/js/popper.min.js')!!}"></script>
        <script type="text/javascript" src="{!! Asset::get('/assets/cpanel/PreAdmin/light/assets/js/bootstrap.min.js')!!}"></script>
        <script type="text/javascript" src="{!! Asset::get('/assets/cpanel/PreAdmin/light/assets/js/jquery.dataTables.min.js')!!}"></script>
        <script type="text/javascript" src="{!! Asset::get('/assets/cpanel/PreAdmin/light/assets/js/fnReloadAjax.js')!!}"></script>
        <script type="text/javascript" src="{!! Asset::get('/assets/cpanel/PreAdmin/light/assets/js/dataTables.bootstrap4.min.js')!!}"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
        <script type="text/javascript" src="{!! Asset::get('/assets/cpanel/PreAdmin/light/assets/js/jquery.slimscroll.js')!!}"></script>
        <script type="text/javascript" src="{!! Asset::get('/assets/cpanel/PreAdmin/light/assets/js/select2.min.js')!!}"></script>
        <script type="text/javascript" src="{!! Asset::get('/assets/cpanel/PreAdmin/light/assets/js/moment.min.js')!!}"></script>
        <script type="text/javascript" src="{!! Asset::get('/assets/cpanel/PreAdmin/light/assets/js/bootstrap-datetimepicker.min.js')!!}"></script>
        <script type="text/javascript" src="{!! Asset::get('/assets/cpanel/PreAdmin/light/assets/plugins/morris/morris.min.js')!!}"></script>
        <script type="text/javascript" src="{!! Asset::get('/assets/cpanel/PreAdmin/light/assets/plugins/raphael/raphael-min.js')!!}"></script>
        <script type="text/javascript" src="{!! Asset::get('/assets/cpanel/PreAdmin/light/assets/js/md5.min.js')!!}"></script>
        <script type="text/javascript" src="{!! Asset::get('/assets/cpanel/PreAdmin/light/assets/js/app.js')!!}"></script>
        @endif

        <div class="notification-popup hide">
            <p>
                <span class="task"></span>
                <span class="notification-text"></span>
            </p>
        </div>

        @yield('script_sidebar')
        @yield('script')

    </body>
</html>