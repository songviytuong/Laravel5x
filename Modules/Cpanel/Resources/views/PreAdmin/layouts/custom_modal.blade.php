<div id="create_project" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div class="modal-content modal-lg">
            <div class="modal-header">
                <h4 class="modal-title">Create Project</h4>
            </div>
            <div class="modal-body">
                <form id="frmProjects" name="frmProjects" method="post">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Project Name</label>
                                <input class="form-control check-input" name="project_name" type="text">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Client</label>
                                <select class="select">
                                    <option>Global Technologies</option>
                                    <option>Delta Infotech</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Start Date</label>
                                <div class="cal-icon">
                                    <input class="form-control datetimepicker" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>End Date</label>
                                <div class="cal-icon">
                                    <input class="form-control datetimepicker" type="text">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Rate</label>
                                <input placeholder="$50" class="form-control" type="text">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>&nbsp;</label>
                                <select class="select">
                                    <option>Hourly</option>
                                    <option>Fixed</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Priority</label>
                                @php $priorities = config('cpanel.priorities'); @endphp
                                <select class="select">
                                    @foreach($priorities as $key=>$priority)
                                    <option value="{!!$key!!}">{!!$priority!!}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Add Project Leader</label>
                                <input class="form-control" type="text">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Team Leader</label>
                                <div class="project-members">
                                    <a href="#" data-toggle="tooltip" title="Jeffery Lalor">
                                        <img src="/assets/cpanel/PreAdmin/light/assets/img/user.jpg" class="avatar" alt="Jeffery Lalor" height="20" width="20">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Add Team</label>
                                <input class="form-control" type="text">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Team Members</label>
                                <div class="project-members">
                                    <a href="#" data-toggle="tooltip" title="John Doe">
                                        <img src="/assets/cpanel/PreAdmin/light/assets/img/user.jpg" class="avatar" alt="John Doe" height="20" width="20">
                                    </a>
                                    <a href="#" data-toggle="tooltip" title="Richard Miles">
                                        <img src="/assets/cpanel/PreAdmin/light/assets/img/user.jpg" class="avatar" alt="Richard Miles" height="20" width="20">
                                    </a>
                                    <a href="#" data-toggle="tooltip" title="John Smith">
                                        <img src="/assets/cpanel/PreAdmin/light/assets/img/user.jpg" class="avatar" alt="John Smith" height="20" width="20">
                                    </a>
                                    <a href="#" data-toggle="tooltip" title="Mike Litorus">
                                        <img src="/assets/cpanel/PreAdmin/light/assets/img/user.jpg" class="avatar" alt="Mike Litorus" height="20" width="20">
                                    </a>
                                    <span class="all-team">+2</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <textarea rows="4" cols="5" name="description" class="form-control" placeholder="Enter your message here"></textarea>
                    </div>
                    <div class="m-t-20 text-center">
                        <button type="button" id="create_btn" class="btn btn-primary btn-lg">Create Project</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="edit_project" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div class="modal-content modal-lg">
            <div class="modal-header">
                <h4 class="modal-title">Edit Project</h4>
            </div>
            <div class="modal-body">
                <form id="frmEditProject" name="frmEditProject" method="post">
                    <input type="hidden" name="project_id" id="project_id" value="" /> {{ csrf_field() }}
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Project Name</label>
                                <input class="form-control check-input" name="project_name" id="project_name" type="text">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Client</label>
                                <select class="select">
                                    <option>Global Technologies</option>
                                    <option>Delta Infotech</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Start Date</label>
                                <div class="cal-icon">
                                    <input class="form-control datetimepicker" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>End Date</label>
                                <div class="cal-icon">
                                    <input class="form-control datetimepicker" type="text">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Rate</label>
                                <input placeholder="$50" class="form-control" type="text">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>&nbsp;</label>
                                <select class="select">
                                    <option>Hourly</option>
                                    <option>Fixed</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Priority</label>
                                @php $priorities = config('cpanel.priorities'); @endphp
                                <select class="select">
                                    @foreach($priorities as $key=>$priority)
                                    <option value="{!!$key!!}">{!!$priority!!}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Add Project Leader</label>
                                <input class="form-control" type="text">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Team Leader</label>
                                <div class="project-members">
                                    <a href="#" data-toggle="tooltip" title="Jeffery Lalor">
                                        <img src="/assets/cpanel/PreAdmin/light/assets/img/user.jpg" class="avatar" alt="Jeffery Lalor" height="20" width="20">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Add Team</label>
                                <input class="form-control" type="text">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Team Members</label>
                                <div class="project-members">
                                    <a href="#" data-toggle="tooltip" title="John Doe">
                                        <img src="/assets/cpanel/PreAdmin/light/assets/img/user.jpg" class="avatar" alt="John Doe" height="20" width="20">
                                    </a>
                                    <a href="#" data-toggle="tooltip" title="Richard Miles">
                                        <img src="/assets/cpanel/PreAdmin/light/assets/img/user.jpg" class="avatar" alt="Richard Miles" height="20" width="20">
                                    </a>
                                    <a href="#" data-toggle="tooltip" title="John Smith">
                                        <img src="/assets/cpanel/PreAdmin/light/assets/img/user.jpg" class="avatar" alt="John Smith" height="20" width="20">
                                    </a>
                                    <a href="#" data-toggle="tooltip" title="Mike Litorus">
                                        <img src="/assets/cpanel/PreAdmin/light/assets/img/user.jpg" class="avatar" alt="Mike Litorus" height="20" width="20">
                                    </a>
                                    <span class="all-team">+2</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <textarea rows="4" cols="5" name="description" class="form-control" placeholder="Enter your message here"></textarea>
                    </div>
                    <div class="m-t-20 text-center">
                        <button type="button" id="save_project_btn" class="btn btn-primary btn-lg">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="assignee" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Assign to this task</h3>
            </div>
            <div class="modal-body">
                <div class="input-group m-b-30">
                    <input placeholder="Search to add" class="form-control search-input" type="text">
                    <span class="input-group-append">
                        <button class="btn btn-primary">Search</button>
                    </span>
                </div>
                <div>
                    <ul class="chat-user-list">
                        <li>
                            <a href="#">
                                <div class="media">
                                    <span class="avatar">R</span>
                                    <div class="media-body align-self-center text-nowrap">
                                        <div class="user-name">Richard Miles</div>
                                        <span class="designation">Web Developer</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="media">
                                    <span class="avatar">J</span>
                                    <div class="media-body align-self-center text-nowrap">
                                        <div class="user-name">John Smith</div>
                                        <span class="designation">Android Developer</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="media">
                                    <span class="avatar">
                                        <img src="/assets/cpanel/PreAdmin/light/assets/img/user.jpg" alt="John Doe">
                                    </span>
                                    <div class="media-body align-self-center text-nowrap">
                                        <div class="user-name">Jeffery Lalor</div>
                                        <span class="designation">Team Leader</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="m-t-50 text-center">
                    <button class="btn btn-primary btn-lg">Assign</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="task_followers" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Add followers to this task</h3>
            </div>
            <div class="modal-body">
                <div class="input-group m-b-30">
                    <input placeholder="Search to add" class="form-control search-input" id="btn-input" type="text">
                    <span class="input-group-append">
                        <button class="btn btn-primary">Search</button>
                    </span>
                </div>
                <div>
                    <ul class="chat-user-list">
                        <li>
                            <a href="#">
                                <div class="media">
                                    <span class="avatar">J</span>
                                    <div class="media-body media-middle text-nowrap">
                                        <div class="user-name">Jeffery Lalor</div>
                                        <span class="designation">Team Leader</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="media">
                                    <span class="avatar">C</span>
                                    <div class="media-body media-middle text-nowrap">
                                        <div class="user-name">Catherine Manseau</div>
                                        <span class="designation">Android Developer</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="media">
                                    <span class="avatar">W</span>
                                    <div class="media-body media-middle text-nowrap">
                                        <div class="user-name">Wilmer Deluna</div>
                                        <span class="designation">Team Leader</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="m-t-50 text-center">
                    <button class="btn btn-primary btn-lg">Add to Follow</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="delete_project" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content modal-md">
            <div class="modal-header">
                <h4 class="modal-title">Delete Project</h4>
            </div>
            <div class="modal-body card-box">
                <p>Are you sure want to delete this?</p>
                <div class="m-t-20"> <a href="#" class="btn btn-white" data-dismiss="modal">Close</a>
                    <button type="button" class="btn btn-danger removeProject">Delete</button>
                </div>
            </div>
        </div>
    </div>
</div>