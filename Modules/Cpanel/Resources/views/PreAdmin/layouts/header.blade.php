<?php
use Modules\Cpanel\Entities\Activities;
$activities = Activities::listActivities();
?>
<div class="header">
    <div class="header-left">
        <a href="{{route('cpanel.index')}}" class="logo">
            <img src="/assets/cpanel/PreAdmin/light/assets/img/logo.png" width="40" height="40" alt="">
        </a>
    </div>
    <div class="page-title-box pull-left">
        <h3>Preadmin</h3>
    </div>
    <a id="mobile_btn" class="mobile_btn pull-left" href="#sidebar"><i class="fa fa-bars" aria-hidden="true"></i></a>
    <ul class="nav user-menu pull-right">
        <li class="nav-item dropdown d-none d-sm-block">
            <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown"><i class="fa fa-bell-o"></i> <span class="badge badge-pill bg-primary pull-right">{!! count($activities) !!}</span></a>
            <div class="dropdown-menu notifications">
                <div class="topnav-dropdown-header">
                    <span>Notifications</span>
                </div>
                <div class="drop-scroll">
                    <ul class="notification-list">
                        <?php
                        foreach ($activities as $activity) {
                            ?>
                            <li class="notification-message">
                                <a href="javascript:void(0)">
                                    <div class="media">
                                        <span class="avatar">
                                            <img alt="John Doe" src="/assets/cpanel/PreAdmin/light/assets/img/user.jpg" class="img-fluid">
                                        </span>
                                        {{--<span class="avatar">V</span>--}}
                                        <div class="media-body">
                                            <p class="noti-details"><span class="noti-title">John Doe</span> added new task <span class="noti-title">Patient appointment booking</span></p>
                                            <p class="noti-time"><span class="notification-time">{{Business::cms_timeElapsed($activity->created_at)}}</span></p>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="topnav-dropdown-footer">
                    <a href="{{route('cpanel.activities.index')}}">View all Notifications</a>
                </div>
            </div>
        </li>
        <li class="nav-item dropdown d-none d-sm-block">
            <a href="javascript:void(0);" id="open_msg_box" class="hasnotifications nav-link"><i class="fa fa-comment-o"></i> <span class="badge badge-pill bg-primary pull-right">8</span></a>
        </li>
        <li class="nav-item dropdown has-arrow">
            <a href="#" class="dropdown-toggle nav-link user-link" data-toggle="dropdown">
                <span class="user-img">
                    <img class="rounded-circle" src="/assets/cpanel/PreAdmin/light/assets/img/user.jpg" width="40" alt="Admin">
                    <span class="status online"></span>
                </span>
                <span>Admin</span>
            </a>
            <div class="dropdown-menu">
                <a class="dropdown-item" href="profile.html">My Profile</a>
                <a class="dropdown-item" href="edit-profile.html">Edit Profile</a>
                <a class="dropdown-item" href="settings.html">Settings</a>
                <a class="dropdown-item" href="{!!route('cpanel.logout')!!}">Logout</a>
            </div>
        </li>
    </ul>
    <div class="dropdown mobile-user-menu pull-right">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
        <div class="dropdown-menu dropdown-menu-right">
            <a class="dropdown-item" href="profile.html">My Profile</a>
            <a class="dropdown-item" href="edit-profile.html">Edit Profile</a>
            <a class="dropdown-item" href="settings.html">Settings</a>
            <a class="dropdown-item" href="{!!route('cpanel.logout')!!}">Logout</a>
        </div>
    </div>
</div>

@include('cpanel::PreAdmin.layouts.notification_box')