<?php
$project_id = isset($data['screen']['project_id']) ? $data['screen']['project_id'] : 0;
?>
@extends('cpanel::PreAdmin.layouts.master')
@section('body-content')
<div class="main-wrapper">
    @include('cpanel::PreAdmin.layouts.header')
    @include('cpanel::PreAdmin.projects.sidebar')
    <div class="page-wrapper">
        <div class="chat-main-row">
            <div class="chat-main-wrapper">
                <div class="col-lg-7 message-view task-view task-left-sidebar">
                    <div class="chat-window">
                        <div class="fixed-header">
                            <div class="navbar">
                                <div class="pull-left mr-auto">
                                    <div class="add-task-btn-wrapper">
                                        @if ($project_id != 0)
                                        <span class="add-task-btn btn btn-white btn-sm">Add Task</span>
                                        @endif
                                    </div>
                                </div>
                                <a class="task-chat profile-rightbar pull-right" href="#chat_sidebar">
                                    <i class="fa fa fa-comment"></i>
                                </a>
                                <ul class="nav pull-right custom-menu">
                                    <li class="nav-item dropdown dropdown-action">
                                        <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                            <i class="fa fa-cog"></i>
                                        </a>
                                        <div class="dropdown-menu" id="option-task-list">
                                            <a class="dropdown-item pendingTasks" href="javascript:void(0)">Pending Tasks</a>
                                            <a class="dropdown-item completedTasks" href="javascript:void(0)">Completed Tasks</a>
                                            <a class="dropdown-item allTasks" href="javascript:void(0)">All Tasks</a>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="chat-contents">
                            <div class="chat-content-wrap">
                                <div class="chat-wrap-inner">
                                    <div class="chat-box">
                                        <div class="task-wrapper">
                                            <div class="task-list-container">
                                                <div class="task-list-body">
                                                    <ul id="task-list">
                                                        @foreach ($data['screen']['tasks'] as $task)
                                                        <li class="task {{($task->status == 1) ? 'completed':''}}">
                                                            <div class="task-container">
                                                                <span class="task-action-btn task-check">
                                                                    <span class="action-circle large complete-btn" title="Mark Complete">
                                                                        <i class="material-icons">check</i>
                                                                    </span>
                                                                </span>
                                                                <span class="task-label" data-id="{{$task->id}}" contenteditable="true">{{$task->task_name}}</span>
                                                                <span class="task-action-btn task-btn-right">
                                                                    <span class="action-circle large detail-btn" title="Detail">
                                                                        <i class="material-icons">find_replace</i>
                                                                    </span>
                                                                    <span class="action-circle large assign-btn" title="Assign">
                                                                        <i class="material-icons">person_add</i>
                                                                    </span>
                                                                    <span class="action-circle large delete-btn" title="Delete Task">
                                                                        <i class="material-icons">delete</i>
                                                                    </span>
                                                                </span>
                                                            </div>
                                                        </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                                <div class="task-list-footer">
                                                    <div class="new-task-wrapper">
                                                        <textarea id="new-task" placeholder="Enter new task here. . ."></textarea>
                                                        <span class="error-message hidden">You need to enter a task first</span>
                                                        <span class="add-new-task-btn btn" id="add-task">Add Task</span>
                                                        <span class="cancel-btn btn" id="close-task-panel">Close</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 message-view task-chat-view task-right-sidebar" id="chat_sidebar">
                    <div class="chat-window">
                        <div class="fixed-header">
                            <div class="navbar">
                                <div class="task-assign">
                                    <span class="assign-title">Assigned to </span>
                                    <a href="#" data-toggle="tooltip" data-placement="bottom" title="John Doe">
                                        <img src="/assets/cpanel/PreAdmin/light/assets/img/user.jpg" class="avatar" alt="" height="20" width="20">
                                    </a>
                                    <a href="#" class="followers-add" title="Add Assignee" data-toggle="modal" data-target="#assignee">
                                        <i class="material-icons">add</i>
                                    </a>
                                </div>
                                <ul class="nav pull-right custom-menu">
                                    <li class="dropdown dropdown-action">
                                        <a href="" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                            <i class="fa fa-ellipsis-v"></i>
                                        </a>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item editProjectBtn" href="javascript:void(0)">Edit Project</a>
                                            <a class="dropdown-item" href="javascript:void(0)">Settings</a>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="chat-contents task-chat-contents">
                            <div class="chat-content-wrap">
                                <div class="chat-wrap-inner">
                                    <div class="chat-box">
                                        <div class="chats">
                                            <h4>Hospital Administration Phase 1</h4>
                                            <hr class="task-line">
                                            <div class="task-information">
                                                <span class="task-info-line">
                                                    <a class="task-user" href="#">Lesley Grauer</a>
                                                    <span class="task-info-subject">created task</span>
                                                </span>
                                                <div class="task-time">Jan 26, 2015</div>
                                            </div>
                                            <div class="task-information">
                                                <span class="task-info-line">
                                                    <a class="task-user" href="#">Lesley Grauer</a>
                                                    <span class="task-info-subject">added to Hospital Administration</span>
                                                </span>
                                                <div class="task-time">Jan 26, 2015</div>
                                            </div>
                                            <div class="task-information">
                                                <span class="task-info-line">
                                                    <a class="task-user" href="#">Lesley Grauer</a>
                                                    <span class="task-info-subject">assigned to John Doe</span>
                                                </span>
                                                <div class="task-time">Jan 26, 2015</div>
                                            </div>
                                            <hr class="task-line">
                                            <div class="task-information">
                                                <span class="task-info-line">
                                                    <a class="task-user" href="#">John Doe</a>
                                                    <span class="task-info-subject">changed the due date to Sep 28</span>
                                                </span>
                                                <div class="task-time">9:09pm</div>
                                            </div>
                                            <div class="task-information">
                                                <span class="task-info-line">
                                                    <a class="task-user" href="#">John Doe</a>
                                                    <span class="task-info-subject">assigned to you</span>
                                                </span>
                                                <div class="task-time">9:10pm</div>
                                            </div>
                                            <div class="chat chat-left">
                                                <div class="chat-avatar">
                                                    <a href="profile.html" class="avatar">
                                                        <img alt="John Doe" src="/assets/cpanel/PreAdmin/light/assets/img/user.jpg" class="img-fluid rounded-circle">
                                                    </a>
                                                </div>
                                                <div class="chat-body">
                                                    <div class="chat-bubble">
                                                        <div class="chat-content">
                                                            <span class="task-chat-user">John Doe</span>
                                                            <span class="chat-time">8:35 am</span>
                                                            <p>Im just looking around.</p>
                                                            <p>Will you tell me something about yourself? </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="completed-task-msg">
                                                <span class="task-success">
                                                    <a href="#">John Doe</a> completed this task.</span>
                                                <span class="task-time">Today at 9:27am</span>
                                            </div>
                                            <div class="chat chat-left">
                                                <div class="chat-avatar">
                                                    <a href="profile.html" class="avatar">
                                                        <img alt="John Doe" src="/assets/cpanel/PreAdmin/light/assets/img/user.jpg" class="img-fluid rounded-circle">
                                                    </a>
                                                </div>
                                                <div class="chat-body">
                                                    <div class="chat-bubble">
                                                        <div class="chat-content">
                                                            <span class="task-chat-user">John Doe</span>
                                                            <span class="file-attached">attached 3 files
                                                                <i class="fa fa-paperclip" aria-hidden="true"></i>
                                                            </span>
                                                            <span class="chat-time">Dec 17, 2014 at 4:32am</span>
                                                            <ul class="attach-list">
                                                                <li>
                                                                    <i class="fa fa-file"></i>
                                                                    <a href="#">project_document.avi</a>
                                                                </li>
                                                                <li>
                                                                    <i class="fa fa-file"></i>
                                                                    <a href="#">video_conferencing.psd</a>
                                                                </li>
                                                                <li>
                                                                    <i class="fa fa-file"></i>
                                                                    <a href="#">landing_page.psd</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="chat chat-left">
                                                <div class="chat-avatar">
                                                    <a href="profile.html" class="avatar">
                                                        <img alt="John Doe" src="/assets/cpanel/PreAdmin/light/assets/img/user.jpg" class="img-fluid rounded-circle">
                                                    </a>
                                                </div>
                                                <div class="chat-body">
                                                    <div class="chat-bubble">
                                                        <div class="chat-content">
                                                            <span class="task-chat-user">Jeffery Lalor</span>
                                                            <span class="file-attached">attached file
                                                                <i class="fa fa-paperclip" aria-hidden="true"></i>
                                                            </span>
                                                            <span class="chat-time">Yesterday at 9:16pm</span>
                                                            <ul class="attach-list">
                                                                <li class="pdf-file">
                                                                    <i class="fa fa-file-pdf-o"></i>
                                                                    <a href="#">Document_2016.pdf</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="chat chat-left">
                                                <div class="chat-avatar">
                                                    <a href="profile.html" class="avatar">
                                                        <img alt="John Doe" src="/assets/cpanel/PreAdmin/light/assets/img/user.jpg" class="img-fluid rounded-circle">
                                                    </a>
                                                </div>
                                                <div class="chat-body">
                                                    <div class="chat-bubble">
                                                        <div class="chat-content">
                                                            <span class="task-chat-user">Jeffery Lalor</span>
                                                            <span class="file-attached">attached file
                                                                <i class="fa fa-paperclip" aria-hidden="true"></i>
                                                            </span>
                                                            <span class="chat-time">Today at 12:42pm</span>
                                                            <ul class="attach-list">
                                                                <li class="img-file">
                                                                    <div class="attach-img-download">
                                                                        <a href="#">avatar-1.jpg</a>
                                                                    </div>
                                                                    <div class="task-attach-img">
                                                                        <img src="/assets/cpanel/PreAdmin/light/assets/img/user.jpg"
                                                                            alt="">
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="task-information">
                                                <span class="task-info-line">
                                                    <a class="task-user" href="#">John Doe</a>
                                                    <span class="task-info-subject">marked task as incomplete</span>
                                                </span>
                                                <div class="task-time">1:16pm</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="chat-footer">
                            <div class="message-bar">
                                <div class="message-inner">
                                    <a class="link attach-icon" href="#">
                                        <img src="/assets/cpanel/PreAdmin/light/assets/img/attachment.png" alt="">
                                    </a>
                                    <div class="message-area">
                                        <div class="input-group">
                                            <textarea class="form-control" placeholder="Type message..."></textarea>
                                            <span class="input-group-append">
                                                <button class="btn btn-primary" type="button">
                                                    <i class="fa fa-send"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="project-members task-followers">
                                <span class="followers-title">Followers</span>
                                <a href="#" data-toggle="tooltip" title="Jeffery Lalor">
                                    <img src="/assets/cpanel/PreAdmin/light/assets/img/user.jpg" class="avatar" alt="Jeffery Lalor" height="20" width="20">
                                </a>
                                <a href="#" data-toggle="tooltip" title="Richard Miles">
                                    <img src="/assets/cpanel/PreAdmin/light/assets/img/user.jpg" class="avatar" alt="Richard Miles" height="20" width="20">
                                </a>
                                <a href="#" data-toggle="tooltip" title="John Smith">
                                    <img src="/assets/cpanel/PreAdmin/light/assets/img/user.jpg" class="avatar" alt="John Smith" height="20" width="20">
                                </a>
                                <a href="#" data-toggle="tooltip" title="Mike Litorus">
                                    <img src="/assets/cpanel/PreAdmin/light/assets/img/user.jpg" class="avatar" alt="Mike Litorus" height="20" width="20">
                                </a>
                                <a href="#" class="followers-add" data-toggle="modal" data-target="#task_followers">
                                    <i class="material-icons">add</i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('cpanel::PreAdmin.layouts.custom_modal')
    </div>
</div>

@stop

@section('script')
<script>
    function insertTask(newTask) {
        var project_id = "{{$project_id}}";
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'POST',
            url: "{{route('cpanel.tasks.new.task')}}",
            data: {
                task_name: newTask,
                project_id: project_id
            },
            dataType: "JSON",
            cache: false,
            success: function () {
                ajaxLoadTasks('allTasks', project_id);
            }
        });
    }

    function updateTask(taskText, type, task_id) {
        console.log(taskText, type, task_id);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'POST',
            url: "{{route('cpanel.tasks.update.task')}}",
            data: {
                task_name: taskText,
                type: type,
                task_id: task_id
            },
            dataType: "JSON",
            cache: false,
            success: function () {}
        });
    }

    function ajaxLoadTasks(type, project_id) {
        $.ajax({
            type: "GET",
            url: "{{route('cpanel.tasks.ajax.load_tasks')}}",
            data: {
                type: type,
                project_id: project_id
            },
            dataType: "html",
            cache: false,
            success: function (result) {
                $('.task-list-body').html(result);
            }
        });
    }

    $(document).on('click', '.editProjectBtn', function () {
        var project_id = "{{$project_id}}";
        $.ajax({
            type: "GET",
            url: "{{route('cpanel.projects.ajax.load_edit_project')}}",
            data: {
                'project_id': project_id
            },
            dataType: "JSON",
            cache: false,
            success: function (result) {
                if (result.status) {
                    $('#project_id').val(project_id);
                    $('#project_name').val(result.data.project_name);
                    $('#edit_project').modal('show');
                }
            }
        });
    });

    var validator = $("#frmEditProject").validate({
        rules: {
            project_name: {
                required: true,
            },
        },
        messages: {
            project_name: {
                required: "This field is required",
            },

        }
    });

    function validAll() {
        return true;
    }
    $('#save_project_btn').on("click", function (e) {
        e.preventDefault();
        var flag_valid = validAll();
        if ($("#frmEditProject").valid() && flag_valid) {
            var frmData = new FormData($("#frmEditProject")[0]);
            frmData.append('module', 'projects');
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'POST',
                url: "{{route('cpanel.projects.save.project')}}",
                data: frmData,
                dataType: "JSON",
                cache: false,
                processData: false,
                contentType: false,
                success: function (result) {
                    if (result.status) {
                        var taskText = $("#frmEditProject").find('input[name=project_name]').val();
                        updateNotification(taskText, ' has been added.', 'success');
                    }

                    setTimeout(function () {
                        $('.close').click();
                        window.location.href = "{{route('cpanel.tasks.index')}}/" + result.alias;
                    }, 300);
                }
            });
        } else {
            validator.focusInvalid();
            $('html, body').animate({
                scrollTop: ($('#frmEditProject').offset().top - 50)
            }, 800);
            return false;
        }
    });

    $(document).ready(function ($) {
        var project_id = "{{$project_id}}";
        $('#option-task-list').on('click', '.pendingTasks', function (e) {
            e.preventDefault();
            ajaxLoadTasks('pendingTasks', project_id);
        });
        $('#option-task-list').on('click', '.completedTasks', function (e) {
            e.preventDefault();
            ajaxLoadTasks('completedTasks', project_id);
        });
        $('#option-task-list').on('click', '.allTasks', function (e) {
            e.preventDefault();
            ajaxLoadTasks('allTasks', project_id);
        });
    });
</script>
@endsection