<ul id="task-list">
    @foreach ($data['screen']['tasks'] as $task)
    <li class="task {{($task->status == 1) ? 'completed':''}}">
        <div class="task-container">
            <span class="task-action-btn task-check">
                <span class="action-circle large complete-btn" title="Mark Complete">
                    <i class="material-icons">check</i>
                </span>
            </span>
            <span class="task-label" data-id="{{$task->id}}" contenteditable="true">{{$task->task_name}}</span>
            <span class="task-action-btn task-btn-right">
                <span class="action-circle large" title="Assign">
                    <i class="material-icons">person_add</i>
                </span>
                <span class="action-circle large delete-btn" title="Delete Task">
                    <i class="material-icons">delete</i>
                </span>
            </span>
        </div>
    </li>
    @endforeach
</ul>