@extends('cpanel::PreAdmin.layouts.master')
@section('body-content')
<div class="main-wrapper">
    @include('cpanel::PreAdmin.layouts.header')
    @include('cpanel::PreAdmin.projects.sidebar')
    <div class="page-wrapper">
        <div class="content container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="page-title">Activities</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="activity">
                        <div class="activity-box">
                            <ul class="activity-list">
                                @foreach ($data['screen']['activities'] as $activity)
                                <li>
                                    <div class="activity-user">
                                        <a href="profile.html" title="Lesley Grauer" data-toggle="tooltip" class="avatar">
                                            <!--<a href="profile.html" class="avatar" title="Jeffery Lalor" data-toggle="tooltip">L</a>-->
                                            <img alt="Lesley Grauer" src="/assets/cpanel/PreAdmin/light/assets/img/user.jpg" class="img-fluid rounded-circle">
                                        </a>
                                    </div>
                                    <div class="activity-content">
                                        <div class="timeline-content">
                                            {!! $activity->message !!}
                                            <span class="time">{{Business::cms_timeElapsed($activity->created_at)}}</span>
                                        </div>
                                    </div>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

@stop
