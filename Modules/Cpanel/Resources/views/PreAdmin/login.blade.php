<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <link rel="shortcut icon" type="image/x-icon" href="{!! Asset::get('/assets/cpanel/PreAdmin/'.$data['settings']['color_active'].'/assets/img/favicon.png')!!}">
        <title>{!!SEO::getTitle()!!}</title>
        <base href="/">
        <link href="https://fonts.googleapis.com/css?family=Fira+Sans:400,500,600,700" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{ Asset::get('/assets/cpanel/PreAdmin/light/assets/css/bootstrap.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ Asset::get('/assets/cpanel/PreAdmin/light/assets/css/font-awesome.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ Asset::get('/assets/cpanel/PreAdmin/'.$data['settings']['color_active'].'/assets/css/style.css') }}">
    </head>

    <body>

        <div class="main-wrapper">
            <div class="account-page" id="login">
                <div class="container">
                    <h3 class="account-title">Login</h3>
                    <div class="account-box">
                        <div class="account-wrapper">
                            <div class="account-logo">
                                <a href="{{ route('cpanel.index') }}"><img src="{{ Asset::get('/assets/cpanel/PreAdmin/'.$data['settings']['color_active'].'/assets/img/logo.png')}}" alt="Preadmin"></a>
                            </div>
                            <!--<form method="POST" action="{{ url('/cpanel/check-login') }}">-->
                            <form v-on:submit.prevent="doLogin">
@section('body-content')
<section class="h-100">
    <div class="container h-100">
        <div class="row justify-content-md-center h-100">
            <div class="card-wrapper">
                <div class="brand">
                    <img src="{!!asset('admin/images/logo.jpg')!!}">
                </div>
                <div class="card fat">
                    <div class="card-body">
                        @if (Auth::guest())
                        <h4 class="card-title">Login</h4>
                        @else
                        <h4 class="card-title">Welcome {{ Auth::user()->name }}</h4>
                        <span>Email: {{ Auth::user()->email }} [<a href="{{ url('/administrator/logout') }}">Logout</a>]</span>
                        @endif
                        @if (Auth::guest())
                        <form  method="POST" action="{{ url('/administrator/check') }}">
                            {{ csrf_field() }}
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="control-label">E-Mail Address</label>
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="control-label">Password
                                    <a href="forgot.html" class="float-right">
                                        Forgot Password?
                                    </a>
                                </label>
                                <input id="password" type="password" class="form-control" name="password" required data-eye>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label>
                                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                </label>
                            </div>

                            <div class="form-group no-margin">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>
                            </div>
                            <div class="margin-top20 text-center">
                                Don't have an account? <a href="register.html">Create One</a>
                            </div>
                        </form>
                        @endif
                    </div>
                </div>
                <div class="footer">
                    Copyright &copy; 2017 &mdash; Your Company 
                </div>
            </div>
        </div>
    </div>
</section>
@stop
                                <div class="form-group form-focus">
                                    <label class="focus-label">Username or Email</label>
                                    <input v-model='item.email' class="form-control floating" placeholder="" value="{{ old('email') }}" required autofocus/>
                                </div>
                                <div class="form-group form-focus">
                                    <label class="focus-label">Password</label>
                                    <input v-model='item.password' type="password" class="form-control floating" required data-eye placeholder=""/>
                                </div>
                                <div class="form-group text-center">
                                    <button v-on:click="doLogin()" class="btn btn-primary btn-block account-btn">Login</button>
                                </div>
                                <div class="text-center">
                                    <a href="#!">Forgot your password?</a>
                                </div>
                            </form>
                        </div>
                    </div>
                    @include('cpanel::PreAdmin.layouts.themes')
                </div>
            </div>
        </div>

        @include('cpanel::init')
        <script type="text/javascript" src="{{ Asset::get('/assets/cpanel/PreAdmin/light/assets/js/bootstrap.min.js')}}"></script>
        <script type="text/javascript" src="{{ Asset::get('/assets/cpanel/PreAdmin/light/assets/js/app.js')}}"></script>
        @include('cpanel::global')
    </body>
</html>
