@if(isset($contacts))
{{ $contacts->links('cpanel::PreAdmin.contacts.ajax_pagination', ['paging' => $paging]) }}
<ul class="contact-list">
    @foreach($contacts as $contact)
    <li>
        <div class="contact-cont">
            <div class="pull-left user-img m-r-10">
                <a href="profile.html" title="John Doe"><img src="/assets/cpanel/PreAdmin/light/assets/img/user.jpg" alt="" class="w-40 rounded-circle"><span class="status online"></span></a>
            </div>
            <div class="contact-info">
                <span class="contact-name text-ellipsis">{{$contact->firstname or ''}} {{$contact->lastname or ''}}</span>
                <span class="contact-date">Web Developer</span>
            </div>
            <ul class="contact-action">
                <li class="dropdown dropdown-action">
                    <a href="" class="dropdown-toggle action-icon" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="javascript:void(0)">Edit</a>
                        <a class="dropdown-item" href="javascript:void(0)">Delete</a>
                    </div>
                </li>
            </ul>
        </div>
    </li>
    @endforeach
</ul>
<br/>
{{--pagination info--}}
<!--div class="col-md-12 col-sm12 col-xs-12 clearfix txt16">
    @if($contacts->total() != 0)
    <span class="txtff6a4a txt30">{{ $contacts->total() }}</span>件中
    @if($contacts->total() == 1)
    <span class="txtff6a4a">{{ $contacts->firstItem() }}～{{ $contacts->lastItem() }}</span> 件を表示
    @endif
    @else
    <div class="col-md-12 col-sm12 col-xs-12 text-center clearfix mg_b40">
        <p class="txt20 fw-bold color-red">No record</p>
    </div>
    @endif
    @if($contacts->total() != 1 && $contacts->total() > 0)
    <span class="txtff6a4a">{{ $contacts->firstItem() }}～{{ $contacts->lastItem() }}</span> 件を表示
    @endif
</div-->
@endif