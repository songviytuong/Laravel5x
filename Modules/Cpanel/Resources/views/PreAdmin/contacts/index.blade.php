@extends('cpanel::PreAdmin.layouts.master')
@section('body-content')
<?php
$formData = session()->get('ajaxSearchFromData');
?>
<div class="main-wrapper">
    @include('cpanel::PreAdmin.layouts.header')
    @include('cpanel::PreAdmin.layouts.sidebar')
    <div class="page-wrapper">
        <div class="chat-main-row">
            <div class="chat-main-wrapper">
                <div class="col-lg-12 message-view">
                    <div class="chat-window">
                        <div class="fixed-header">
                            <div class="row">
                                <div class="col-6">
                                    <h4 class="page-title m-b-0 m-t-5">Contacts</h4>
                                </div>
                                <div class="col-6">
                                    <div class="navbar justify-content-end">
                                        <div class="search-box m-t-0">
                                            <div class="input-group input-group-sm">
                                                <input type="text" class="form-control" placeholder="Search" required="">
                                                <span class="input-group-append">
                                                    <button class="btn" type="button"><i class="fa fa-search"></i></button>
                                                </span>
                                            </div>
                                        </div>
                                        <ul class="nav pull-right custom-menu">
                                            <li class="nav-item dropdown dropdown-action">
                                                <a href="javascript:void(0)" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-cog"></i></a>
                                                <div class="dropdown-menu option_group_category">
                                                    <a class="dropdown-item edit-group" data-id="" href="javascript:void(0)">Edit Group</a>
                                                    <a class="dropdown-item delete-group" data-id="" data-token="" href="javascript:void(0)">Delete Group</a>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="chat-contents">
                            <div class="chat-content-wrap">
                                <div class="chat-wrap-inner">
                                    <div class="contact-box">
                                        <div class="row">
                                            <div class="contact-cat col-sm-4 col-lg-3">
                                                <a href="{{route('cpanel.contacts.add')}}" class="btn btn-primary btn-block"><i class="fa fa-plus"></i> Add Contact</a>
                                                <div class="roles-menu">
                                                    <ul>
                                                        <li class="active"><a href="javascript:void(0);" onclick="loadAll('all')">All</a></li>
                                                        @foreach($contacts_groups as $group)
                                                        <li><a href="javascript:void(0);" class="load-contacts" data-id="{!!$group->id!!}">{!!$group->group_name!!}<span class="badge badge-pill bg-primary pull-right">{{$group->total or 0}}</span></a></li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="contacts-list col-sm-8 col-lg-9" data-type="" data-id=""></div>
                                            <div class="contact-alphapets">
                                                <div class="alphapets-inner">
                                                    <a href="#">A</a>
                                                    <a href="#">B</a>
                                                    <a href="#">C</a>
                                                    <a href="#">D</a>
                                                    <a href="#">E</a>
                                                    <a href="#">F</a>
                                                    <a href="#">G</a>
                                                    <a href="#">H</a>
                                                    <a href="#">I</a>
                                                    <a href="#">J</a>
                                                    <a href="#">K</a>
                                                    <a href="#">L</a>
                                                    <a href="#">M</a>
                                                    <a href="#">N</a>
                                                    <a href="#">O</a>
                                                    <a href="#">P</a>
                                                    <a href="#">Q</a>
                                                    <a href="#">R</a>
                                                    <a href="#">S</a>
                                                    <a href="#">T</a>
                                                    <a href="#">U</a>
                                                    <a href="#">V</a>
                                                    <a href="#">W</a>
                                                    <a href="#">X</a>
                                                    <a href="#">Y</a>
                                                    <a href="#">Z</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="delete_group" class="modal custom-modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content modal-md">
                <div class="modal-header">
                    <h4 class="modal-title">Delete Group</h4>
                </div>
                <div class="modal-body card-box">
                    <p>Are you sure want to delete this?</p>
                    <div class="m-t-20"> <a href="#" class="btn btn-white" data-dismiss="modal">Close</a>
                        <button type="button" class="btn btn-danger removeGroup">Delete</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @stop
    @section('script')
    <script>
        var type = "all";
        var id = "1";

        setTimeout(function () {
            $('.contacts-list').attr('data-type', type);
            $('.contacts-list').attr('data-id', id);



            ajaxLoadContacts(type, id, 1);
        }, 200);

        function loadAll(type) {
            $('.contacts-list').attr('data-type', type);
            ajaxLoadContacts(type, 1, 1);
        }

        $('.contact-cat').on('click', '.load-contacts', function (e) {
            e.preventDefault();

            $(".contact-cat li").removeClass('active');
            $(this).parent().addClass('active');

            var id = $(this).attr('data-id');
            type = 'category';
            $('.contacts-list').attr('data-type', type);
            ajaxLoadContacts(type, id, 1);

            $('.delete-group').attr('data-id', id);
            $('.delete-group').attr('data-token', md5('REMOVEGROUP' + id));
        });


        $(document).on('click', '.delete-group', function () {
            $('.removeGroup').attr('data-id', $(this).attr('data-id'));
            $('.removeGroup').attr('data-token', $(this).attr('data-token'));
            $('#delete_group .modal-body p').text('Are you sure want to delete this?');
            $('#delete_group').modal('show');
        });

        $('#delete_group').on('click', '.removeGroup', function () {
            var _id = $(this).attr('data-id');
            var _token = $(this).attr('data-token');
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'POST',
                url: "{{route('cpanel.contacts.remove.contact')}}",
                data: {
                    id: _id,
                    token: _token
                },
                dataType: "JSON",
                cache: false,
                success: function (result) {
                    if (result.status) {
                        $('#delete_group').modal('hide');
                        window.location.href = "{{route('cpanel.contacts.index')}}";
                    }
                }
            });

        });

        $('.contact-box').on('click', '.first', function () {
            var type = $('.contacts-list').attr('data-type');
            var id = $('.contacts-list').attr('data-id');
            ajaxLoadContacts(type, id, 1);
        });

        $('.contact-box').on('click', '.prev', function () {
            var currentPage = $('.notice-pagination').data('current-page');
            var type = $('.contacts-list').attr('data-type');
            var id = $('.contacts-list').attr('data-id');
            ajaxLoadContacts(type, id, currentPage - 1);
        });
        $('.contact-box').on('click', '.next', function () {
            var currentPage = $('.notice-pagination').data('current-page');
            var type = $('.contacts-list').attr('data-type');
            var id = $('.contacts-list').attr('data-id');
            ajaxLoadContacts(type, id, currentPage + 1);
        });

        $('.contact-box').on('click', '.last', function () {
            var lastPage = $(this).data('last-page');
            var type = $('.contacts-list').attr('data-type');
            var id = $('.contacts-list').attr('data-id');
            ajaxLoadContacts(type, id, lastPage);
        });

        $('.contact-box').on('click', '.items', function () {
            var pageItem = $(this).data('page-item');
            var type = $('.contacts-list').attr('data-type');
            var id = $('.contacts-list').attr('data-id');
            ajaxLoadContacts(type, id, pageItem);
        });

        function ajaxLoadContacts(type, id, page) {
//            console.log('Lan1' + type, id, page);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'POST',
                url: "{{route('cpanel.contacts.ajax.load_contacts')}}",
                data: {
                    type: type,
                    id: id,
                    page: page
                },
                dataType: "html",
                cache: false,
                success: function (result) {
                    $('.contacts-list').html(result);
                }
            });
        }
    </script>
    @endsection