@extends('cpanel::PreAdmin.layouts.master')
@section('body-content')
<?php
$formData = session()->get('ajaxSearchFromData');
?>
<div class="main-wrapper">
    @include('cpanel::PreAdmin.layouts.header')
    @include('cpanel::PreAdmin.layouts.sidebar')
    <div class="page-wrapper">
        <div class="content container-fluid">
            <div class="row">
                <div class="col-md-8 offset-md-2">
                    <h4 class="page-title">Add Contact</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 offset-md-2">
                    <form name="frmAddContact" id="frmAddContact" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label>First Name (*)</label>
                            <input class="form-control check-input" type="text" name="firstname" placeholder="First Name">
                        </div>
                        <div class="form-group">
                            <label>Last Name (*)</label>
                            <input class="form-control" type="text" name="lastname" placeholder="Last Name">
                        </div>
                        <div class="form-group">
                            <label>Phone</label>
                            <input class="form-control" type="text" name="phone" placeholder="Phone Number">
                        </div>
                        <div class="form-group">
                            <label>Email Address</label>
                            <input class="form-control" type="text" name="email" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <label>Address</label>
                            <input class="form-control" type="text" name="address" placeholder="Address">
                        </div>
                        <div class="form-group">
                            <label class="display-block">Gender</label>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="gender" id="male" value="0" checked>
                                <label class="form-check-label" for="male">
                                    Male
                                </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="gender" id="female" value="1">
                                <label class="form-check-label" for="female">
                                    Female
                                </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="gender" id="other" value="2">
                                <label class="form-check-label" for="other">
                                    Other
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Groups</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <button type="button" class="btn btn-white dropdown-toggle" id="optiton_groups" data-toggle="dropdown" aria-expanded="false">Select Group</button>
                                    <div class="dropdown-menu">
                                        @foreach($contacts_groups as $group)
                                        <a class="dropdown-item" data-value="{!!$group->id!!}" href="javascript:void(0);">{!!$group->group_name!!}</a>
                                        @endforeach
                                    </div>
                                </div>
                                <input type="text" name="group_text" id="group_text" class="form-control" placeholder="Other">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="display-block">Contact Status</label>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="status" id="contact_active" value="1" checked>
                                <label class="form-check-label" for="contact_active">
                                    Active
                                </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="status" id="contact_inactive" value="0">
                                <label class="form-check-label" for="contact_inactive">
                                    Inactive
                                </label>
                            </div>
                        </div>

                        <div class="m-t-20 text-center">
                            <button type="button" id="add_btn" class="btn btn-primary btn-lg">Add Contact</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @stop
    @section('script')
    <script>
        $(document).on('click', '.dropdown-item', function () {
            $('#group_text').val($(this).text());
        });

        var validator = $("#frmAddContact").validate({
            rules: {
                firstname: {
                    required: true,
                },
                lastname: {
                    required: true,
                }
            },
            messages: {
                firstname: {
                    required: "This field is required",
                },
                lastname: {
                    required: "This field is required",
                }

            }
        });

        function validGroupName() {
            var group_name = $('#group_text').val();
            if (!group_name) {
                $('#group_text').focus();
                return false;
            }
            return true;
        }

        function validAll() {
            var flg_group_name = validGroupName();
            if (flg_group_name) {
                return true;
            }
            return false;
        }

        $('#add_btn').click(function () {
            var frmData = new FormData($("#frmAddContact")[0]);
            var flag_valid = validAll();
            if ($("#frmAddContact").valid() && flag_valid) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'POST',
                    url: "{{route('cpanel.contacts.add')}}",
                    data: frmData,
                    dataType: "JSON",
                    cache: false,
                    processData: false,
                    contentType: false,
                    success: function (result) {
                        if (result.status) {
                            var taskText = $("#frmAddContact").find('input[name=firstname]').val();
                            var group_name = $("#frmAddContact").find('input[name=group_text]').val();
                            updateNotification(taskText, ' has been added to group ' + group_name, 'success');
                            clearForm($("#frmAddContact"));

                        }
                    }
                });
            } else {
                validator.focusInvalid();
                $('html, body').animate({
                    scrollTop: ($('#frmAddContact').offset().top - 150)
                }, 800);
                return false;
            }
        });

        function clearForm($form)
        {
            $form.find(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
            $form.find(':checkbox, :radio').prop('checked', false);
        }

    </script>
    @endsection