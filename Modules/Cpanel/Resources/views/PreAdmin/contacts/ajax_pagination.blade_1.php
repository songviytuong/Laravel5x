@if ($paginator->hasPages())
<ul class="pagination notice-pagination" data-current-page="{{ $paginator->currentPage() }}">
    {{-- Previous Page Link --}}
    @if ($paginator->onFirstPage())
    <li class="page-item disabled"><a class="page-link prev">Prev</a></li>
    @else
    <li class="page-item"><a href="javascript:void(0)" class="page-link prev" data-current-page="{{ $paginator->currentPage() }}" >Prev</a></li>
    @endif

    {{-- Pagination Elements --}}
    @foreach ($elements as $element)
    {{-- "Three Dots" Separator --}}
        @if (is_string($element))
        <li class="page-item"><a class="page-link disabled">{{ $element }}</a></li>
        @endif
        {{-- Array Of Links --}}
        @if (is_array($element))
            @foreach ($element as $page => $url)
                @if ($page == $paginator->currentPage())
                <li class="page-item active"><a class="page-link">{{ $page }}</a></li>
                @else
                <li class="page-item"><a class="page-link items" data-page-item="{{ $page }}">{{ $page }}</a></li>
                @endif
            @endforeach
        @endif
    @endforeach

    {{-- Next Page Link --}}
    @if ($paginator->hasMorePages())
    <li class="page-item"><a href="javascript:void(0)" class="page-link next" data-current-page="{{ $paginator->currentPage() }}">Next</a></li>
    @else
    <li class="page-item disabled"><a class="page-link">Next</a></li>
    @endif
</ul>
@endif
