@php
$link_limit = 3;
@endphp
@if ($paginator->hasPages())
<ul class="pagination notice-pagination" data-current-page="{{ $paginator->currentPage() }}">
    {{-- Previous Page Link --}}
    @if($paginator->onFirstPage())
    <li class="page-item disabled"><a class="page-link first">First</a></li>
    @else
    <li class="page-item"><a class="page-link first" href="javascript:void(0)">First</a></li>
    @endif
    @if ($paginator->onFirstPage())
    <li class="page-item disabled">
        <a class="page-link prev">
            <span aria-hidden="true">«</span>
            <span class="sr-only">Prev</span>
        </a>
    </li>
    @else
    <li class="page-item">
        <a href="javascript:void(0)" class="page-link prev" data-current-page="{{ $paginator->currentPage() }}" >
            <span aria-hidden="true">«</span>
            <span class="sr-only">Prev</span>
        </a>
    </li>
    @endif

    @for ($i = 1; $i <= $paginator->lastPage(); $i++)
        <?php
        $half_total_links = floor($link_limit / 2);
        $from = $paginator->currentPage() - $half_total_links;
        $to = $paginator->currentPage() + $half_total_links;
        if ($paginator->currentPage() < $half_total_links) {
            $to += $half_total_links - $paginator->currentPage();
        }
        if ($paginator->lastPage() - $paginator->currentPage() < $half_total_links) {
            $from -= $half_total_links - ($paginator->lastPage() - $paginator->currentPage()) - 1;
        }
        ?>
        @if ($from < $i && $i < $to)
        <li class="page-item{{ ($paginator->currentPage() == $i) ? ' active' : '' }}"><a class="page-link items" data-page-item="{{ $i }}">{{ $i }}</a></li>
        @endif
    @endfor



    {{-- Next Page Link --}}
    @if ($paginator->hasMorePages())
    <li class="page-item">
        <a href="javascript:void(0)" class="page-link next" data-current-page="{{ $paginator->currentPage() }}">
            <span aria-hidden="true">»</span>
            <span class="sr-only">Next</span>
        </a>
    </li>
    @else
    <li class="page-item disabled">
        <a class="page-link">
            <span aria-hidden="true">»</span>
            <span class="sr-only">Next</span>
        </a>
    </li>
    @endif
    
    @if($paginator->currentPage() == $paginator->lastPage())
    <li class="page-item disabled">
        <a class="page-link last">Last</a>
    </li>
    @else
    <li class="page-item">
        <a class="page-link last" href="javascript:void(0)" data-last-page="{{$paginator->lastPage()}}">Last</a>
    </li>
    @endif
</ul>
@endif
