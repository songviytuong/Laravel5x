<?php

use Modules\Cpanel\Entities\Projects;

#---Show SideBar
$projects = Projects::listProjects();
?>
<div class="sidebar" id="sidebar">
    <div class="sidebar-inner slimscroll">
        <div class="sidebar-menu">
            <ul>
                <li>
                    <a href="{{route('cpanel.index')}}"><i class="fa fa-home back-icon"></i> Back to Home</a>
                </li>
                <li class="menu-title">Projects <a href="#" class="add-user-icon" data-toggle="modal" data-target="#create_project"><i class="fa fa-plus"></i></a></li>
                @foreach ($projects as $project)
                <li {{ (collect(request()->segments())->last() === $project->alias) ? 'class=active':''}}><a href="{{ route('cpanel.tasks.index') }}/{{Business::munge_string_to_url($project->project_name)}}"><span class="btn-hover">{{$project->project_name}}</span> </a> 
                    <span class="btn-right action-circle large delete_project-btn" data-id="{!!$project->id!!}" title="Delete Task">
                        <i class="material-icons">delete</i>
                    </span></li>
                @endforeach
            </ul>
        </div>
    </div>
</div>

@section('script_sidebar')
<script>
    function ajaxLoadProjects() {
        $.ajax({
            type: 'POST',
            url: "{{route('cpanel.projects.ajax.load_projects')}}",
            dataType: "html",
            cache: false,
            success: function (result) {
                $('#sidebar').html(result);
            }
        });
    }
    function insertTask(newTask) {
        var project_id = "{{$data['screen']['project_id'] or 0}}";
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'POST',
            url: "{{route('cpanel.tasks.new.task')}}",
            data: {task_name: newTask, project_id: project_id},
            dataType: "JSON",
            cache: false,
            success: function (result) {

            }
        });
    }

    $(document).on('click','.delete-btn',function(){
        var project_id = $(this).attr('data-id');
        $('.removeProject').attr('onclick','removeProject('+project_id+');');
        $('#delete_project').modal('show');
    });

    function removeProject(project_id){
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'POST',
            url: "{{route('cpanel.projects.remove.project')}}",
            data: {project_id: project_id},
            dataType: "JSON",
            cache: false,
            success: function (result) {
                if(result.status){
                    $('#delete_project').modal('hide');
                    ajaxLoadProjects();
                }
            }
        });
    }

    $(document).ready(function ($) {
        var validator = $("#frmProjects").validate({
            rules: {
                project_name: {
                    required: true,
                },
            },
            messages: {
                project_name: {
                    required: "This field is required",
                },

            }
        });
        function validAll() {
            return true;
        }
        $('#create_btn').on("click", function (e) {
            e.preventDefault();
            var flag_valid = validAll();
            if ($("#frmProjects").valid() && flag_valid) {
                var frmData = new FormData($("#frmProjects")[0]);
                frmData.append('module', 'projects');
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'POST',
                    url: "{{route('cpanel.projects.add.project')}}",
                    data: frmData,
                    dataType: "JSON",
                    cache: false,
                    processData: false,
                    contentType: false,
                    success: function (result) {
                        if (result.status) {
                            var taskText = $("#frmProjects").find('input[name=project_name]').val();
                            updateNotification(taskText, ' has been added.', 'success');
                        }

                        setTimeout(function () {
                            $('.close').click();
                            ajaxLoadProjects();
                        }, 300);
                    }
                });
            } else {
                validator.focusInvalid();
                $('html, body').animate({
                    scrollTop: ($('#frmProjects').offset().top - 50)
                }, 800);
                return false;
            }
        });
    });
</script>
@endsection