<div class="sidebar-inner slimscroll">
    <div class="sidebar-menu">
        <ul>
            <li>
                <a href="{{route('cpanel.index')}}"><i class="fa fa-home back-icon"></i> Back to Home</a>
            </li>
            <li class="menu-title">Projects <a href="#" class="add-user-icon" data-toggle="modal" data-target="#create_project"><i class="fa fa-plus"></i></a></li>
            @foreach ($data['sidebar']['projects'] as $project)
            <li {{ (collect(request()->segments())->last() === $project->alias) ? 'class=active':''}}><a href="{{ route('cpanel.tasks.index') }}/{{Business::munge_string_to_url($project->project_name)}}"><span class="btn-hover">{{$project->project_name}}</span> </a> 
                <span class="btn-right action-circle large delete_project-btn" data-id="{!!$project->id!!}" title="Delete Task">
                    <i class="material-icons">delete</i>
                </span></li>
            @endforeach
        </ul>
    </div>
</div>