<div id="treeAjaxHTML" class="jstree jstree-3 jstree-default" role="tree" aria-multiselectable="true" tabindex="0" aria-activedescendant="j3_loading" aria-busy="false">
    <ul class="jstree-container-ul jstree-children" role="group">
        <li id="j3_loading" class="jstree-initial-node jstree-loading jstree-leaf jstree-last" role="tree-item"><i class="jstree-icon jstree-ocl"></i><a class="jstree-anchor" href="#"><i class="jstree-icon jstree-themeicon-hidden"></i>Loading ...</a></li>
    </ul>
</div>