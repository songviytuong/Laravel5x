@extends('cpanel::PortoAdmin.layouts.master')

@section('body-content')
<section class="body">
    @include('cpanel::PortoAdmin.layouts.header')

    <div class="inner-wrapper">
        @include('cpanel::PortoAdmin.layouts.sidebar')

        <section role="main" class="content-body pb-0">
            @include('cpanel::PortoAdmin.includes.breadcrumbs',[])

            <!-- start: page -->
            <section class="call-to-action call-to-action-primary call-to-action-top mb-4">
                <div class="container container-with-sidebar">
                    <div class="row">
                        <div class="col-xl-8">
                            <div class="call-to-action-content">
                                <h2 class="text-color-light mb-0 mt-4">Porto Admin is a <strong>complete package...</strong></h2>
                                <p class="lead">With everything you need to create your new administration system.</p>
                            </div>
                        </div>
                        <div class="col-xl-4">
                            <div class="call-to-action-btn float-right-xl mt-1 pt-1 mt-xl-4 pt-xl-4">
                                <a href="!@#" target="_blank" class="btn btn-primary-scale-2 btn-lg">0989 466 466</a>
                                <span class="d-none d-xl-inline-block">
                                    Only <strong>$3000</strong>
                                    <span class="arrow arrow-light hlb"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            
            <section class="call-to-action call-to-action-grey pb-4">
                <div class="container container-with-sidebar">
                    <div class="row">
                        <div class="col-xl-9 p-0">
                            <div class="call-to-action-content ml-4 pt-xl-5 pb-4">
                                <h2 class="mb-2">Start creating your new admin today with <strong>Porto Admin!</strong></h2>
                                <p class="lead">Now that you already know that Porto Admin is the best choice for your next project, do not hesitate,<br> purchase now for only $3000 and join many happy customers. Get started now.</p>
                            </div>
                        </div>
                        <div class="col-xl-3">
                            <div class="call-to-action-btn float-right-xl center mt-1 pt-1 mt-xl-5 pt-xl-4">
                                <a href="!@#" target="_blank" class="btn btn-primary btn-lg mb-3"><i class="fas fa-cark mr-1"></i>LEE PEACE</a>
                                <p><span class="alternative-font text-color-primary">Join The 4000+ Happy Customers :)</span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!-- end: page -->
        </section>
    </div>

    @include('cpanel::PortoAdmin.layouts.sidebar-right')

</section>
@stop
