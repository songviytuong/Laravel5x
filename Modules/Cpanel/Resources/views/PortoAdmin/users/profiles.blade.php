@extends('cpanel::PortoAdmin.layouts.master') @section('body-content')
<section class="body">
    @include('cpanel::PortoAdmin.layouts.header')

    <div class="inner-wrapper">
        @include('cpanel::PortoAdmin.layouts.sidebar')

        <section role="main" class="content-body pb-0">

            @include('cpanel::PortoAdmin.includes.breadcrumbs',$data['breadcrumbs'])

            <!-- start: page -->
            <div class="row">
                <div class="col-lg-4 col-xl-3 mb-4 mb-xl-0">
                    <section class="card">
                        <div class="card-body">
                            <div class="thumb-info mb-3">
                                <img src="{!!asset('/assets/cpanel/PortoAdmin/img/!logged-user.jpg')!!}" class="rounded img-fluid" alt="John Doe">
                                <div class="thumb-info-title">
                                    <span class="thumb-info-inner">{{(Auth::guard('cpanel')->user()->name) ?? 'Guest'}}</span>
                                    <span class="thumb-info-type">CEO</span>
                                </div>
                            </div>

                            <div class="widget-toggle-expand mb-3">
                                <div class="widget-header">
                                    <h5 class="mb-2">Profile Completion</h5>
                                    <div class="widget-toggle">+</div>
                                </div>
                                <div class="widget-content-collapsed">
                                    <div class="progress progress-xs light">
                                        @php
                                        $completed = 2;
                                        $total = 5;
                                        $percentage = ($completed*100)/$total;
                                        @endphp
                                        <div class="progress-bar" role="progressbar" aria-valuenow="{{$percentage}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$percentage}}%;">
                                            60%
                                        </div>
                                    </div>
                                </div>
                                <div class="widget-content-expanded">
                                    <ul class="simple-todo-list mt-3">
                                        <li class="completed">Update Profile Picture</li>
                                        <li class="completed">Change Personal Information</li>
                                        <li>Update Social Media</li>
                                        <li>Follow Someone</li>
                                    </ul>
                                </div>
                            </div>

                            <hr class="dotted short">

                            <h5 class="mb-2 mt-3">About</h5>
                            <p class="text-2">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam quis vulputate quam. Interdum et malesuada</p>
                            <div class="clearfix">
                                <a class="text-uppercase text-muted float-right" href="#">(View All)</a>
                            </div>

                            <hr class="dotted short">

                            <div class="social-icons-list">
                                <a rel="tooltip" data-placement="bottom" target="_blank" href="http://www.facebook.com" data-original-title="Facebook"><i class="fab fa-facebook-f"></i><span>Facebook</span></a>
                                <a rel="tooltip" data-placement="bottom" href="http://www.twitter.com" data-original-title="Twitter"><i class="fab fa-twitter"></i><span>Twitter</span></a>
                                <a rel="tooltip" data-placement="bottom" href="http://www.linkedin.com" data-original-title="Linkedin"><i class="fab fa-linkedin-in"></i><span>Linkedin</span></a>
                            </div>

                        </div>
                    </section>

                    <section class="card">
                        <header class="card-header">
                            <div class="card-actions">
                                <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                                <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                            </div>

                            <h2 class="card-title">
                                <span class="badge badge-primary label-sm font-weight-normal va-middle mr-3">198</span>
                                <span class="va-middle">Friends</span>
                            </h2>
                        </header>
                        <div class="card-body">
                            <div class="content">
                                <ul class="simple-user-list">
                                    <li>
                                        <figure class="image rounded">
                                            <img src="{!!asset('/assets/cpanel/PortoAdmin/img/!sample-user.jpg')!!}" alt="Joseph Doe Junior" class="rounded-circle">
                                        </figure>
                                        <span class="title">Joseph Doe Junior</span>
                                        <span class="message truncate">Lorem ipsum dolor sit.</span>
                                    </li>
                                    <li>
                                        <figure class="image rounded">
                                            <img src="{!!asset('/assets/cpanel/PortoAdmin/img/!sample-user.jpg')!!}" alt="Joseph Junior" class="rounded-circle">
                                        </figure>
                                        <span class="title">Joseph Junior</span>
                                        <span class="message truncate">Lorem ipsum dolor sit.</span>
                                    </li>
                                    <li>
                                        <figure class="image rounded">
                                            <img src="{!!asset('/assets/cpanel/PortoAdmin/img/!sample-user.jpg')!!}" alt="Joe Junior" class="rounded-circle">
                                        </figure>
                                        <span class="title">Joe Junior</span>
                                        <span class="message truncate">Lorem ipsum dolor sit.</span>
                                    </li>
                                    <li>
                                        <figure class="image rounded">
                                            <img src="{!!asset('/assets/cpanel/PortoAdmin/img/!sample-user.jpg')!!}" alt="Joseph Doe Junior" class="rounded-circle">
                                        </figure>
                                        <span class="title">Joseph Doe Junior</span>
                                        <span class="message truncate">Lorem ipsum dolor sit.</span>
                                    </li>
                                </ul>
                                <hr class="dotted short">
                                <div class="text-right">
                                    <a class="text-uppercase text-muted" href="#">(View All)</a>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="input-group">
                                <input type="text" class="form-control" name="q" id="q" placeholder="Search...">
                                <span class="input-group-append">
                                    <button class="btn btn-default" type="submit"><i class="fas fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </section>

                    <section class="card">
                        <header class="card-header">
                            <div class="card-actions">
                                <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                                <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                            </div>

                            <h2 class="card-title">Popular Posts</h2>
                        </header>
                        <div class="card-body">
                            <ul class="simple-post-list">
                                <li>
                                    <div class="post-image">
                                        <div class="img-thumbnail">
                                            <a href="#">
                                                <img src="{!!asset('/assets/cpanel/PortoAdmin/img/post-thumb-1.jpg')!!}" alt="">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="post-info">
                                        <a href="#">Nullam Vitae Nibh Un Odiosters</a>
                                        <div class="post-meta">
                                            Jan 10, 2017
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="post-image">
                                        <div class="img-thumbnail">
                                            <a href="#">
                                                <img src="{!!asset('/assets/cpanel/PortoAdmin/img/post-thumb-2.jpg')!!}" alt="">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="post-info">
                                        <a href="#">Vitae Nibh Un Odiosters</a>
                                        <div class="post-meta">
                                            Jan 10, 2017
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="post-image">
                                        <div class="img-thumbnail">
                                            <a href="#">
                                                <img src="{!!asset('/assets/cpanel/PortoAdmin/img/post-thumb-3.jpg')!!}" alt="">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="post-info">
                                        <a href="#">Odiosters Nullam Vitae</a>
                                        <div class="post-meta">
                                            Jan 10, 2017
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </section>

                </div>
                <div class="col-lg-8 col-xl-6">

                    <div class="tabs">
                        <ul class="nav nav-tabs tabs-primary">
                            <li class="nav-item active">
                                <a class="nav-link" href="#overview" data-toggle="tab">Overview</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#edit" data-toggle="tab">Edit</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div id="overview" class="tab-pane active">

                                <div class="p-3">

                                    <h4 class="mb-3">Update Status</h4>
                                    <!-- <style type="text/css">
                                        .message-text {
                                            height: 37px;
                                            -webkit-transition: all .5s ease;
                                            -moz-transition: all .5s ease;
                                            transition: all .5s ease;
                                            border: 1px solid #007bff;
                                        }

                                        .message-text:focus {
                                            height: 200px;
                                            -webkit-transition: all .5s ease;
                                            -moz-transition: all .5s ease;
                                            transition: all .5s ease;
                                            border: 1px solid #007bff;
                                        }
                                    </style> -->
                                    <section class="simple-compose-box mb-3">
                                        <form id="frmMessage" name="frmMessage" method="POST">
                                            {{ csrf_field() }}
                                            <textarea name="message_text" class="message-text" data-plugin-textarea-autosize required placeholder="What's on your mind?" rows="1"></textarea>
                                        </form>
                                        <div class="compose-box-footer">
                                            <ul class="compose-toolbar">
                                                <li>
                                                    <a href="#"><i class="fas fa-camera"></i></a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fas fa-map-marker-alt"></i></a>
                                                </li>
                                            </ul>
                                            <ul class="compose-btn">
                                                <li>
                                                    <a href="javascript:void(0);" id="btnPost" class="btn btn-primary btn-xs">Post</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </section>

                                    <h4 class="mb-3 pt-4">Timeline</h4>

                                    <div class="timeline timeline-simple mt-3 mb-3" id="ajaxGetTimelines"></div>
                                </div>

                            </div>
                            <div id="edit" class="tab-pane">

                                <form class="p-3">
                                    <h4 class="mb-3">Personal Information</h4>
                                    <div class="form-group">
                                        <label for="inputAddress">Address</label>
                                        <input type="text" class="form-control" id="inputAddress" placeholder="1234 Main St">
                                    </div>
                                    <div class="form-group">
                                        <label for="inputAddress2">Address 2</label>
                                        <input type="text" class="form-control" id="inputAddress2" placeholder="Apartment, studio, or floor">
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="inputCity">City</label>
                                            <input type="text" class="form-control" id="inputCity">
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="inputState">State</label>
                                            <select id="inputState" class="form-control">
                                                <option selected>Choose...</option>
                                                <option>...</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label for="inputZip">Zip</label>
                                            <input type="text" class="form-control" id="inputZip">
                                        </div>
                                    </div>

                                    <hr class="dotted tall">

                                    <h4 class="mb-3">Change Password</h4>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="inputPassword4">New Password</label>
                                            <input type="password" class="form-control" id="p" placeholder="Password">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="inputPassword4">Re New Password</label>
                                            <input type="password" class="form-control" id="pw" placeholder="Password">
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="col-md-12 text-right mt-3">
                                            <button class="btn btn-primary modal-c                                                                                       onfirm">Save</button>
                                        </div>
                                    </div>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3">

                    <h4 class="mb-3 mt-0">Sale Stats</h4>
                    <ul class="simple-card-list mb-3">
                        <li class="primary">
                            <h3>488</h3>
                            <p class="text-light">Nullam quris ris.</p>
                        </li>
                        <li class="primary">
                            <h3>$ 189,000.00</h3>
                            <p class="text-light">Nullam quris ris.</p>
                        </li>
                        <li class="primary">
                            <h3>16</h3>
                            <p class="text-light">Nullam quris ris.</p>
                        </li>
                    </ul>

                    <h4 class="mb-3 mt-4 pt-2">Projects</h4>
                    <ul class="simple-bullet-list mb-3">
                        <li class="red">
                            <span class="title">Porto Template</span>
                            <span class="description truncate">Lorem ipsom dolor sit.</span>
                        </li>
                        <li class="green">
                            <span class="title">Tucson HTML5 Template</span>
                            <span class="description truncate">Lorem ipsom dolor sit amet</span>
                        </li>
                        <li class="blue">
                            <span class="title">Porto HTML5 Template</span>
                            <span class="description truncate">Lorem ipsom dolor sit.</span>
                        </li>
                        <li class="orange">
                            <span class="title">Tucson Template</span>
                            <span class="description truncate">Lorem ipsom dolor sit.</span>
                            </ li>
                    </ul>

                    <h4 class="mb-3 mt-4 pt-2">Messages</h4>
                    <ul class="simple-user-list mb-3">
                        <li>
                            <figure class="image rounded">
                                <img src="{!!asset('/assets/cpanel/PortoAdmin/img/!sample-user.jpg')!!}" alt="Joseph Doe Junior" class="rounded-circle">
                            </figure>
                            <span class="title">Joseph Doe Junior</span>
                            <span class="message">Lorem ipsum dolor sit.</span>
                        </li>
                        <li>
                            <figure class="image rounded">
                                <img src="{!!asset('/assets/cpanel/PortoAdmin/img/!sample-user.jpg')!!}" alt="Joseph Junior" class="rounded-circle">
                            </figure>
                            <span class="title">Joseph Junior</span>
                            <span class="message">Lorem ipsum dolor sit.</span>
                        </li>
                        <li>
                            <figure class="image rounded">
                                <img src="{!!asset('/assets/cpanel/PortoAdmin/img/!sample-user.jpg')!!}" alt="Joe Junior" class="rounded-circle">
                            </figure>
                            <span class="title">Joe Junior</span>
                            <span class="message">Lorem ipsum dolor sit.</span>
                        </li>
                        <li>
                            <figure class="image rounded">
                                <img src="{!!asset('/assets/cpanel/PortoAdmin/img/!sample-user.jpg')!!}" alt="Joseph Doe Junior" class="rounded-circle">
                            </figure>
                            <span class="title">Joseph Doe Junior</span>
                            <span class="message">Lorem ipsum dolor sit.</span>
                        </li>
                    </ul>
                </div>

            </div>
            <!-- end: page -->
        </section>
    </div>
    @include('cpanel::PortoAdmin.layouts.sidebar-right')
</section>
@stop

@section('script')
<script>
    ajaxGetLoading();
    setTimeout(function() {
        ajaxGetTimelines();
    },1000);

    $("#frmMessage").validate({
        rules: {
            message_text: {
                required: true
            }
        },
        messages: {
            message_text: {
                required: "This field is required."
            }
        }
    });

    $('#btnPost').on("click", function (e) {
        e.preventDefault();
        if($("#frmMessage").valid()){
            var frmMessage = new FormData($('#frmMessage')[0]);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'POST',
                url: "{{route('cpanel.users.profiles')}}",
                data: $('#frmMessage').serialize(),
                dataType: "JSON",
                cache: false,
                beforeSend: function() {
                    // setting a timeout
                    ajaxGetLoading();
                },
                success: function (result) {
                    if(result.status){
                        new PNotify({
                            title: 'Message successfully sent :)',
                            text: 'Posted: a few seconds ago',
                            type: 'success'
                        });
                        $("#frmMessage").closest('form').find("input[type=text], textarea").val("");
                    }

                }
            }).done(function(c) {
                ajaxGetTimelines();
            });
        }
    });

    function ajaxGetTimelines(){
        $.ajax({
            url: "{{route('cpanel.users.profiles.ajaxGetTimelines')}}"
        }).done(function(c) {
            $("#ajaxGetTimelines").html(c);
        })
    }

    function ajaxGetLoading(){
        $.ajax({
            url: "{{route('cpanel.ajaxGetLoading')}}"
        }).done(function(c) {
            $("#ajaxGetTimelines").html(c);
        })
    }
</script>
@endsection