<div class="tm-body">
    <ol class="tm-items">
        @foreach ($list as $k=>$item)
        <div class="tm-title">
            <h5 class="m-0 pt-2 pb-2 text-uppercase">{{$k}}</h5>
        </div>
        @php
        rsort($item);
        @endphp
        @foreach($item as $it)
        <li>
            <div class="tm-box">
                <p class="text-muted mb-0">{{$it['time_elapsed']}}</p>
                <p>{{$it['message']}}
                    <span class="text-primary">
                        @if($it['tags'])
                            @foreach($it['tags'] as $tag)
                                #{{$tag}}
                                @if(!$loop->last),@endif
                            @endforeach
                        @endif
                    </span>
                </p>
                {{--
                <div class="thumbnail-gallery">
                    <a class="img-thumbnail lightbox" href="{!!asset('/assets/cpanel/PortoAdmin/img/projects/project-4.jpg')!!}" data-plugin-options='{ "type":"image" }'>
                        <img class="img-fluid" width="215" src="{!!asset('/assets/cpanel/PortoAdmin/img/projects/project-4.jpg')!!}">
                        <span class="zoom">
                                <i class="fas fa-search"></i>
                            </span>
                    </a>
                </div>
                --}}
            </div>
        </li>
        @endforeach @endforeach
    </ol>
</div>