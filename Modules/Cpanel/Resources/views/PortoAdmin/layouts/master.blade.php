﻿<!doctype html>
<html class="sidebar-left-big-icons">
    <head>

        <!-- Basic -->
        <meta charset="UTF-8">

        @section('head-title')
        <title>{!!SEO::getTitle()!!}</title>
        @stop
        @yield('head-title')

        @section('head-meta')
        {!!SEO::getMeta()!!}
        @stop

        @yield('head-meta')

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

        <!-- Web Fonts  -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

        <!-- Vendor CSS -->
        <link rel="stylesheet" href="{!!asset('/assets/cpanel/PortoAdmin/vendor/bootstrap/css/bootstrap.css')!!}" />
        <link rel="stylesheet" href="{!!asset('/assets/cpanel/PortoAdmin/vendor/animate/animate.css')!!}">

        <link rel="stylesheet" href="{!!asset('/assets/cpanel/PortoAdmin/vendor/font-awesome/css/fontawesome-all.min.css')!!}" />
        <link rel="stylesheet" href="{!!asset('/assets/cpanel/PortoAdmin/vendor/magnific-popup/magnific-popup.css')!!}" />
        <link rel="stylesheet" href="{!!asset('/assets/cpanel/PortoAdmin/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css')!!}" />

        <!-- Specific Page Vendor CSS -->
        @if (in_array(\Request::route()->getName(),['cpanel.users.profiles']))
        <link rel="stylesheet" href="{!!asset('/assets/cpanel/PortoAdmin/vendor/jstree/themes/default/style.css')!!}" />
        @elseif (in_array(\Request::route()->getName(),['cpanel.settings.siteprefs']))
        <link rel="stylesheet" href="{!!asset('/assets/cpanel/PortoAdmin/vendor/select2/css/select2.css')!!}" />
		<link rel="stylesheet" href="{!!asset('/assets/cpanel/PortoAdmin/vendor/select2-bootstrap-theme/select2-bootstrap.min.css')!!}" />
        @else
        <link rel="stylesheet" href="{!!asset('/assets/cpanel/PortoAdmin/vendor/owl.carousel/assets/owl.carousel.css')!!}" />
        <link rel="stylesheet" href="{!!asset('/assets/cpanel/PortoAdmin/vendor/owl.carousel/assets/owl.theme.default.css')!!}" />
        @endif			
        
        <link rel="stylesheet" href="{!!asset('/assets/cpanel/PortoAdmin/vendor/pnotify/pnotify.custom.css')!!}" />

        <!-- Theme CSS -->
        <link rel="stylesheet" href="{!!asset('/assets/cpanel/PortoAdmin/css/theme.css')!!}" />

        <!-- Theme Custom CSS -->
        <link rel="stylesheet" href="{!!asset('/assets/cpanel/PortoAdmin/css/custom.css')!!}">

        <!-- Head Libs -->
        <script src="{!!asset('/assets/cpanel/PortoAdmin/vendor/modernizr/modernizr.js')!!}"></script>	
        <script src="{!!asset('/assets/cpanel/PortoAdmin/vendor/style-switcher/style.switcher.localstorage.js')!!}"></script>

        <link rel="shortcut icon" href="{{ URL::asset('images/favicon.png') }}">

    </head>
    <body>
        @yield('body-content')

        <!-- Vendor -->
        <script src="{!!asset('/assets/cpanel/PortoAdmin/vendor/jquery/jquery.js')!!}"></script>
        <script src="{!!asset('/assets/cpanel/PortoAdmin/vendor/jquery-browser-mobile/jquery.browser.mobile.js')!!}"></script>
        <script src="{!!asset('/assets/cpanel/PortoAdmin/vendor/jquery-cookie/jquery-cookie.js')!!}"></script>
        <script src="{!!asset('/assets/cpanel/PortoAdmin/vendor/style-switcher/style.switcher.js')!!}"></script>
        <script src="{!!asset('/assets/cpanel/PortoAdmin/vendor/popper/umd/popper.min.js')!!}"></script>	
        <script src="{!!asset('/assets/cpanel/PortoAdmin/vendor/bootstrap/js/bootstrap.js')!!}"></script>
        <script src="{!!asset('/assets/cpanel/PortoAdmin/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js')!!}"></script>	
        <script src="{!!asset('/assets/cpanel/PortoAdmin/vendor/common/common.js')!!}"></script>		
        <script src="{!!asset('/assets/cpanel/PortoAdmin/vendor/nanoscroller/nanoscroller.js')!!}"></script>	
        <script src="{!!asset('/assets/cpanel/PortoAdmin/vendor/magnific-popup/jquery.magnific-popup.js')!!}"></script>
        <script src="{!!asset('/assets/cpanel/PortoAdmin/vendor/jquery-placeholder/jquery-placeholder.js')!!}"></script>

        <!-- Specific Page Vendor -->	
        @if (in_array(\Request::route()->getName(),['cpanel.users.profiles']))
        <script src="{!!asset('/assets/cpanel/PortoAdmin/vendor/jquery-validation/jquery.validate.js')!!}"></script>
        <script src="{!!asset('/assets/cpanel/PortoAdmin/vendor/autosize/autosize.js')!!}"></script>
        <script src="{!!asset('/assets/cpanel/PortoAdmin/vendor/jstree/jstree.js')!!}"></script>
        @elseif (in_array(\Request::route()->getName(),['cpanel.settings.siteprefs']))
        <script src="{!!asset('/assets/cpanel/PortoAdmin/vendor/jquery-validation/jquery.validate.js')!!}"></script>
        <script src="{!!asset('/assets/cpanel/PortoAdmin/vendor/select2/js/select2.js')!!}"></script>
        <script src="{!!asset('/assets/cpanel/PortoAdmin/js/examples/examples.validation.js')!!}"></script>
        @else
        <script src="{!!asset('/assets/cpanel/PortoAdmin/vendor/jquery-appear/jquery-appear.js')!!}"></script>	
        <script src="{!!asset('/assets/cpanel/PortoAdmin/vendor/owl.carousel/owl.carousel.js')!!}"></script>
        <script src="{!!asset('/assets/cpanel/PortoAdmin/vendor/isotope/isotope.js')!!}"></script>
        <!-- Examples -->
        <script src="{!!asset('/assets/cpanel/PortoAdmin/js/examples/examples.landing.dashboard.js')!!}"></script>
        @endif	
        
        <script src="{!!asset('/assets/cpanel/PortoAdmin/vendor/pnotify/pnotify.custom.js')!!}"></script>
        
        <!-- Theme Base, Components and Settings -->
        <script src="{!!asset('/assets/cpanel/PortoAdmin/js/theme.js')!!}"></script>

        <!-- Theme Custom -->
        <script src="{!!asset('/assets/cpanel/PortoAdmin/js/custom.js')!!}"></script>

        <!-- Theme Initialization Files -->
        <script src="{!!asset('/assets/cpanel/PortoAdmin/js/theme.init.js')!!}"></script>
{{-- 
        <script src="{!!asset('/assets/cpanel/PortoAdmin/vendor/jquery-idletimer/idle-timer.js')!!}"></script>
        <script src="{!!asset('/assets/cpanel/PortoAdmin/js/examples/examples.lockscreen.js')!!}"></script> --}}

        @yield('script')
        
    </body>
</html>
