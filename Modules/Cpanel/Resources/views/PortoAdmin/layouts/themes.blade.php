<?php
$theme_active = ($data['settings']['theme_active']) ? $data['settings']['theme_active'] : config('cpanel.theme_active');
$color_active = ($data['settings']['color_active']) ? $data['settings']['color_active'] : config('cpanel.color_active');
?>
<div class="account-box" style="margin-top:20px; background-color: transparent; border:0px">
    <div class="row">
        <div class="<?= ($color_active == '') ? 'col-md-12' : 'col-md-6'; ?>">
            <?php
            $themes = Business::cms_directories(app()->basePath() . '/Modules/Cpanel/Resources/views');
            ?>
            <select class="form-control" @change="onChangeThemes($event)">
                <option value='-1' selected disabled>Select Themes</option>
                <?php
                foreach ($themes as $theme) {
                    echo "<option value=" . $theme . " " . (($theme == $theme_active) ? 'selected' : '') . ">" . $theme . " " . (($theme == $theme_active) ? '( current )' : '') . "</option>";
                }
                ?>
            </select>
        </div>
        <div class="<?= ($color_active == '') ? 'hidden' : 'col-md-6'; ?>">
            <?php
            $colors = ($data['settings']['colors']) ? $data['settings']['colors'] : [];
            ?>
            <select class="form-control optColor <?= ($color_active) ? '' : 'hidden'; ?>" @change="onChangeColor($event)">
                <?php
                foreach ($colors as $color) {
                    echo "<option value=" . $color . " " . (($color == $color_active) ? 'selected' : '') . ">" . $color . " " . (($color == $color_active) ? '( current )' : '') . "</option>";
                }
                ?>
            </select>
        </div>
    </div>
</div>