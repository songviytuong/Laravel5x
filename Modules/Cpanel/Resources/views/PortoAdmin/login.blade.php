<!doctype html>
<html class="fixed">
    <head>

        <!-- Basic -->
        <meta charset="UTF-8">

        <meta name="keywords" content="HTML5 Admin Template" />
        <meta name="description" content="Porto Admin - Responsive HTML5 Template">
        <meta name="author" content="okler.net">

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

        <!-- Web Fonts  -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

        <!-- Vendor CSS -->
        <link rel="stylesheet" href="{!!asset('/assets/cpanel/PortoAdmin/vendor/bootstrap/css/bootstrap.css')!!}" />
        <link rel="stylesheet" href="{!!asset('/assets/cpanel/PortoAdmin/vendor/animate/animate.css')!!}">

        <link rel="stylesheet" href="{!!asset('/assets/cpanel/PortoAdmin/vendor/font-awesome/css/fontawesome-all.min.css')!!}" />
        <link rel="stylesheet" href="{!!asset('/assets/cpanel/PortoAdmin/vendor/magnific-popup/magnific-popup.css')!!}" />
        <link rel="stylesheet" href="{!!asset('/assets/cpanel/PortoAdmin/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css')!!}" />

        <!-- Theme CSS -->
        <link rel="stylesheet" href="{!!asset('/assets/cpanel/PortoAdmin/css/theme.css')!!}" />

        <!-- Theme Custom CSS -->
        <link rel="stylesheet" href="{!!asset('/assets/cpanel/PortoAdmin/css/custom.css')!!}">

        <!-- Head Libs -->
        <script src="{!!asset('/assets/cpanel/PortoAdmin/vendor/modernizr/modernizr.js')!!}"></script>
        <script src="{!!asset('/assets/cpanel/PortoAdmin/vendor/style-switcher/style.switcher.localstorage.js')!!}"></script>

    </head>
    <body>
        <!-- start: page -->
        <section class="body-sign">
            <div class="center-sign">
                <a href="/" class="logo float-left">
                    <img src="{!!asset('/assets/cpanel/PortoAdmin/img/logo.png')!!}" height="54" alt="Porto Admin" />
                </a>

                <div class="panel card-sign">
                    <div class="card-title-sign mt-3 text-right">
                        <h2 class="title text-uppercase font-weight-bold m-0"><i class="fas fa-user mr-1"></i> Sign In</h2>
                    </div>
                    <div class="card-body" id="login">
                        <form  action="{{ url('/cpanel/check-login2') }}" method="post">
                            {{ csrf_field() }}
                            <div class="form-group mb-3">
                                <label>Username</label>
                                <div class="input-group">
                                    <input id="email" type="email" name="email" class="form-control form-control-lg" />
                                    <span class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="fas fa-user"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>

                            <div class="form-group mb-3">
                                <div class="clearfix">
                                    <label class="float-left">Password</label>
                                    <a href="pages-recover-password.html" class="float-right">Lost Password?</a>
                                </div>
                                <div class="input-group">
                                    <input id="password" type="password" name="password" class="form-control form-control-lg" />
                                    <span class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="fas fa-lock"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-8">
                                    <div class="checkbox-custom checkbox-default">
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                        <label for="RememberMe">Remember Me</label>
                                    </div>
                                </div>
                                <div class="col-sm-4 text-right">
                                    <button type="submit" class="btn btn-primary mt-2">Sign In</button>
                                </div>
                            </div>

                            <span class="mt-3 mb-3 line-thru text-center text-uppercase">
                                <span>or</span>
                            </span>

                            <div class="mb-1 text-center">
                                <a class="btn btn-facebook mb-3 ml-1 mr-1" href="#">Connect with <i class="fab fa-facebook-f"></i></a>
                                <a class="btn btn-twitter mb-3 ml-1 mr-1" href="#">Connect with <i class="fab fa-twitter"></i></a>
                            </div>

                            <p class="text-center">Don't have an account yet? <a href="pages-signup.html">Sign Up!</a></p>
                            @include('cpanel::PortoAdmin.layouts.themes')
                        </form>
                    </div>
                </div>

                <p class="text-center text-muted mt-3 mb-3">&copy; Copyright 2018. All Rights Reserved.</p>
            </div>
        </section>
        <!-- end: page -->
        @include('cpanel::init')
        <!-- Vendor -->
        <script src="{!!asset('/assets/cpanel/PortoAdmin/vendor/jquery/jquery.js')!!}"></script>
        <script src="{!!asset('/assets/cpanel/PortoAdmin/vendor/jquery-browser-mobile/jquery.browser.mobile.js')!!}"></script>
        <script src="{!!asset('/assets/cpanel/PortoAdmin/vendor/jquery-cookie/jquery-cookie.js')!!}"></script>
        <script src="{!!asset('/assets/cpanel/PortoAdmin/vendor/style-switcher/style.switcher.js')!!}"></script>
        <script src="{!!asset('/assets/cpanel/PortoAdmin/vendor/popper/umd/popper.min.js')!!}"></script>
        <script src="{!!asset('/assets/cpanel/PortoAdmin/vendor/bootstrap/js/bootstrap.js')!!}"></script>
        <script src="{!!asset('/assets/cpanel/PortoAdmin/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js')!!}"></script>	
        <script src="{!!asset('/assets/cpanel/PortoAdmin/vendor/common/common.js')!!}"></script>		
        <script src="{!!asset('/assets/cpanel/PortoAdmin/vendor/nanoscroller/nanoscroller.js')!!}"></script>
        <script src="{!!asset('/assets/cpanel/PortoAdmin/vendor/magnific-popup/jquery.magnific-popup.js')!!}"></script>	
        <script src="{!!asset('/assets/cpanel/PortoAdmin/vendor/jquery-placeholder/jquery-placeholder.js')!!}"></script>

        <!-- Theme Base, Components and Settings -->
        <script src="{!!asset('/assets/cpanel/PortoAdmin/js/theme.js')!!}"></script>

        <!-- Theme Custom -->
        <script src="{!!asset('/assets/cpanel/PortoAdmin/js/custom.js')!!}"></script>

        <!-- Theme Initialization Files -->
        <script src="{!!asset('/assets/cpanel/PortoAdmin/js/theme.init.js')!!}"></script>
        <!-- Analytics to Track Preview Website -->
        @include('cpanel::global')
    </body>
</html>
