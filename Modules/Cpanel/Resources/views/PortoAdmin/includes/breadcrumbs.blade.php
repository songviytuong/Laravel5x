<header class="page-header">
    <h2>{!!isset($page) ? $page : 'Dashboard'!!}</h2>
    <div class="right-wrapper text-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{url('/cpanel')}}">
                    <i class="fas fa-home"></i>
                </a>
            </li>
            @if (isset($page_link))
            @foreach($page_link as $item)
            <li><span>{!!$item!!}</span></li>
            @endforeach
            @else
            <li><span>{!!isset($page) ? $page : 'Dashboard'!!}</span></li>
            @endif
        </ol>

        <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
    </div>
</header>