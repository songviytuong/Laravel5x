<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="{!! route('cpanel.index') !!}" class="site_title"><i class="fa fa-paw"></i> <span>{!! config('cpanel.name') !!}</span></a>
        </div>

        <div class="clearfix"></div>

        <!-- menu profile quick info -->
        <div class="profile clearfix">
            <div class="profile_pic">
                <script type="text/javascript">
//<![CDATA[
                    window.__mirage2 = {petok: "1a8b161f2353c28850a9c39a223039e92410d670-1525917107-1800"};
//]]>
                </script>
                <script type="text/javascript" src="https://ajax.cloudflare.com/cdn-cgi/scripts/04b3eb47/cloudflare-static/mirage2.min.js"></script>
                <img data-cfsrc="{!!asset('/assets/cpanel/OneEleven/gentelella/images/img.jpg')!!}" alt="..." class="img-circle profile_img" style="display:none;visibility:hidden;"><noscript><img src="{!!asset('/assets/cpanel/OneEleven/gentelella/images/img.jpg')!!}" alt="..." class="img-circle profile_img"></noscript>
            </div>
            <div class="profile_info">
                <span>Welcome,</span>
                <h2>{{(Auth::guest()) ? 'Guest':Auth::user()->name}}</h2>
            </div>
        </div>
        <!-- /menu profile quick info -->
        <br />
        <!-- sidebar menu -->
        @include('cpanel::OneEleven.layouts.sidebar');
        <!-- /sidebar menu -->
    </div>
</div>

<!-- top navigation -->
<div class="top_nav">
    <div class="nav_menu">
        <nav>
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>

            <ul class="nav navbar-nav navbar-right">
                <li class="">
                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <img data-cfsrc="{!!asset('/assets/cpanel/OneEleven/gentelella/images/img.jpg')!!}" alt="" style="display:none;visibility:hidden;"><noscript><img src="{!!asset('/assets/cpanel/OneEleven/gentelella/images/img.jpg')!!}" alt=""></noscript>John Doe
                        <span class=" fa fa-angle-down"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                        <li><a href="javascript:;"> Profile</a></li>
                        <li>
                            <a href="javascript:;">
                                <span class="badge bg-red pull-right">50%</span>
                                <span>Settings</span>
                            </a>
                        </li>
                        <li><a href="javascript:;">Help</a></li>
                        <li><a href="{{ url('/cpanel/logout') }}"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                    </ul>
                </li>

                <li role="presentation" class="dropdown">
                    <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                        <i class="fa fa-envelope-o"></i>
                        <span class="badge bg-green">6</span>
                    </a>
                    <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                        <li>
                            <a>
                                <span class="image"><img data-cfsrc="{!!asset('/assets/cpanel/OneEleven/gentelella/images/img.jpg')!!}" alt="Profile Image" style="display:none;visibility:hidden;" /><noscript><img src="{!!asset('/assets/cpanel/OneEleven/gentelella/images/img.jpg')!!}" alt="Profile Image" /></noscript></span>
                                <span>
                                    <span>John Smith</span>
                                    <span class="time">3 mins ago</span>
                                </span>
                                <span class="message">
                                    Film festivals used to be do-or-die moments for movie makers. They were where...
                                </span>
                            </a>
                        </li>
                        <li>
                            <a>
                                <span class="image"><img data-cfsrc="{!!asset('/assets/cpanel/OneEleven/gentelella/images/img.jpg')!!}" alt="Profile Image" style="display:none;visibility:hidden;" /><noscript><img src="{!!asset('/assets/cpanel/OneEleven/gentelella/images/img.jpg')!!}" alt="Profile Image" /></noscript></span>
                                <span>
                                    <span>John Smith</span>
                                    <span class="time">3 mins ago</span>
                                </span>
                                <span class="message">
                                    Film festivals used to be do-or-die moments for movie makers. They were where...
                                </span>
                            </a>
                        </li>
                        <li>
                            <a>
                                <span class="image"><img data-cfsrc="{!!asset('/assets/cpanel/OneEleven/gentelella/images/img.jpg')!!}" alt="Profile Image" style="display:none;visibility:hidden;" /><noscript><img src="{!!asset('/assets/cpanel/OneEleven/gentelella/images/img.jpg')!!}" alt="Profile Image" /></noscript></span>
                                <span>
                                    <span>John Smith</span>
                                    <span class="time">3 mins ago</span>
                                </span>
                                <span class="message">
                                    Film festivals used to be do-or-die moments for movie makers. They were where...
                                </span>
                            </a>
                        </li>
                        <li>
                            <a>
                                <span class="image"><img data-cfsrc="{!!asset('/assets/cpanel/OneEleven/gentelella/images/img.jpg')!!}" alt="Profile Image" style="display:none;visibility:hidden;" /><noscript><img src="{!!asset('/assets/cpanel/OneEleven/gentelella/images/img.jpg')!!}" alt="Profile Image" /></noscript></span>
                                <span>
                                    <span>John Smith</span>
                                    <span class="time">3 mins ago</span>
                                </span>
                                <span class="message">
                                    Film festivals used to be do-or-die moments for movie makers. They were where...
                                </span>
                            </a>
                        </li>
                        <li>
                            <div class="text-center">
                                <a>
                                    <strong>See All Alerts</strong>
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
</div>
<!-- /top navigation -->