{{-- file: /app/views/layouts/master.blade.php --}}
<!DOCTYPE html>
<html lang="{{App::getLocale()}}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        {{-- Part: site title with default value in parent --}}
        @section('head-title')
        <title>{!!SEO::getTitle()!!}</title>
        
        @stop
        @yield('head-title')

        @section('head-meta')
        {!!SEO::getMeta()!!}
        @stop

        {{-- Part: all meta-related contents --}}
        @yield('head-meta')
        <meta name="csrf-token" content="{{ csrf_token() }}" />

        {{-- Part: load fonts --}}
        @yield('head-fonts')
        {{-- Part: load styles for the page --}}
        @section('head-styles')
        {!!SEO::addCss([
            '/assets/cpanel/OneEleven/vendors/bootstrap/dist/css/bootstrap.min.css',
            '/assets/cpanel/OneEleven/vendors/font-awesome/css/font-awesome.min.css',
            '/assets/cpanel/OneEleven/vendors/nprogress/nprogress.css',
            '/assets/cpanel/OneEleven/vendors/iCheck/skins/flat/green.css',
            '/assets/cpanel/OneEleven/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css',
            '/assets/cpanel/OneEleven/vendors/jqvmap/dist/jqvmap.min.css',
            '/assets/cpanel/OneEleven/vendors/bootstrap-daterangepicker/daterangepicker.css',
            '/assets/cpanel/OneEleven/build/css/custom.min.css',
                ], true)!!}
        {!!SEO::getCss()!!}
        @stop
        @yield('head-styles')
        {{-- Part: load scripts needed --}}
        @section('head-scripts')

        @stop
        @yield('head-scripts')
        {{-- Part: anything else in head --}}
        @yield('head-extra')

        <link rel="shortcut icon" href="{{ URL::asset('images/favicon.png') }}">

    </head>
    <body class="nav-md">
        {{-- Part: something at start of body --}}
        @yield('body-start')
        
        {{-- Part: create main content of the page --}}
        @yield('body-content')

        <script type="text/javascript">
            var BASEPATH = '/cpanel/';
        </script>
        
        {{-- Part: load scripts --}}
        @section('body-scripts')
        {!!SEO::addJs([
            '/assets/cpanel/OneEleven/vendors/jquery/dist/jquery.min.js',
            '/assets/cpanel/OneEleven/vendors/bootstrap/dist/js/bootstrap.min.js',
            '/assets/cpanel/OneEleven/vendors/fastclick/lib/fastclick.js',
            '/assets/cpanel/OneEleven/vendors/nprogress/nprogress.js',
            '/assets/cpanel/OneEleven/vendors/Chart.js/dist/Chart.min.js',
            '/assets/cpanel/OneEleven/vendors/gauge.js/dist/gauge.min.js',
            '/assets/cpanel/OneEleven/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js',
            '/assets/cpanel/OneEleven/vendors/iCheck/icheck.min.js',
            '/assets/cpanel/OneEleven/vendors/skycons/skycons.js',
            '/assets/cpanel/OneEleven/vendors/Flot/jquery.flot.js',
            '/assets/cpanel/OneEleven/vendors/Flot/jquery.flot.pie.js',
            '/assets/cpanel/OneEleven/vendors/Flot/jquery.flot.time.js',
            '/assets/cpanel/OneEleven/vendors/Flot/jquery.flot.stack.js',
            '/assets/cpanel/OneEleven/vendors/Flot/jquery.flot.resize.js',
            '/assets/cpanel/OneEleven/vendors/flot.orderbars/js/jquery.flot.orderBars.js',
            '/assets/cpanel/OneEleven/vendors/flot-spline/js/jquery.flot.spline.min.js',
            '/assets/cpanel/OneEleven/vendors/flot.curvedlines/curvedLines.js',
            '/assets/cpanel/OneEleven/vendors/DateJS/build/date.js',
            '/assets/cpanel/OneEleven/vendors/jqvmap/dist/jquery.vmap.js',
            '/assets/cpanel/OneEleven/vendors/jqvmap/dist/maps/jquery.vmap.world.js',
            '/assets/cpanel/OneEleven/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js',
            '/assets/cpanel/OneEleven/vendors/moment/min/moment.min.js',
            '/assets/cpanel/OneEleven/vendors/bootstrap-daterangepicker/daterangepicker.js',
            '/assets/cpanel/OneEleven/build/js/custom.min.js',
                ], true)!!}
        {!!SEO::getJs()!!}
        @stop
        @yield('body-scripts')
        {{-- Part: something else to do --}}
        @yield('body-others')
        {{-- Part: finalize stuffs if there is --}}
        {{-- Part: footer --}}
        @yield('body-end')
    </body>
</html>