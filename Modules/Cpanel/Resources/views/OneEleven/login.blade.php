<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{!!SEO::getTitle()!!}</title>
        <!-- Bootstrap -->
        <link href="{!!asset('/assets/cpanel/OneEleven/vendors/bootstrap/dist/css/bootstrap.min.css')!!}" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="{!!asset('/assets/cpanel/OneEleven/vendors/font-awesome/css/font-awesome.min.css')!!}" rel="stylesheet">
        <!-- NProgress -->
        <link href="{!!asset('/assets/cpanel/OneEleven/vendors/nprogress/nprogress.css')!!}" rel="stylesheet">
        <!-- Animate.css -->
        <link href="{!!asset('/assets/cpanel/OneEleven/vendors/animate/animate.min.css')!!}" rel="stylesheet">
        <!-- Custom Theme Style -->
        <link href="{!!asset('/assets/cpanel/OneEleven/build/css/custom.min.css')!!}" rel="stylesheet">
    </head>

    <body class="login">
        <div>
            <a class="hiddenanchor" id="signup"></a>
            <a class="hiddenanchor" id="signin"></a>

            <div class="login_wrapper" id="login">
                <div class="animate form login_form">
                    <section class="login_content">
                        <form method="POST" action="{{ url('/cpanel/check-login2') }}">
                            {{ csrf_field() }}
                            <h1>Login {!!config('cpanel.name')!!}</h1>
                            @if($errors->has('error'))
                            @foreach ($errors->all() as $error)
                            <div>{{ $error }}</div>
                            <br/>
                            @endforeach
                            @endif 
                            <div>
                                <input id="email" type="email" class="form-control" name="email" placeholder="E-mail" value="{{ old('email') }}" required autofocus/>
                            </div>
                            <div>
                                <input id="password" type="password" class="form-control" name="password" required data-eye placeholder="Password"/>
                            </div>
                            <div>
                                <button type="submit" class="btn btn-default">Log in</button>
                                <a class="reset_pass" href="#">Lost your password?</a>
                            </div>

                            <div class="clearfix"></div>


                            <div class="separator">
                                <p class="change_link">New to site?
                                    <a href="#signup" class="to_register"> Create Account </a>
                                </p>

                                <div class="clearfix"></div>
                                <br />

                                <div>
                                    <h1><i class="fa fa-paw"></i></h1>
                                    <p>©2018 All Rights Reserved.</p>
                                </div>
                            </div>
                        </form>
                    </section>
                    @include('cpanel::OneEleven.layouts.themes')
                </div>

                <div id="register" class="animate form registration_form">
                    <section class="login_content">
                        <form>
                            <h1>Create Account</h1>
                            <div>
                                <input type="text" class="form-control" placeholder="Username" required="" />
                            </div>
                            <div>
                                <input type="email" class="form-control" placeholder="Email" required="" />
                            </div>
                            <div>
                                <input type="password" class="form-control" placeholder="Password" required="" />
                            </div>
                            <div>
                                <a class="btn btn-default submit" href="index.html">Submit</a>
                            </div>

                            <div class="clearfix"></div>

                            <div class="separator">
                                <p class="change_link">Already a member ?
                                    <a href="#signin" class="to_register"> Log in </a>
                                </p>

                                <div class="clearfix"></div>
                                <br />

                                <div>
                                    <h1><i class="fa fa-paw"></i></h1>
                                    <p>©2018 All Rights Reserved.</p>
                                </div>
                            </div>
                        </form>
                    </section>
                    @include('cpanel::OneEleven.layouts.themes')
                </div>
            </div>
        </div>
        @include('cpanel::init')
    </body>
</html>

