<!--script
            src="https://code.jquery.com/jquery-3.2.1.min.js"
            integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
        crossorigin="anonymous"></script>
        <script src="https://unpkg.com/vue"></>
        <script src="https://cdn.jsdelivr.net/vue.resource/1.3.1/vue-resource.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>

<script>
(function ($) {
    var _token = '<?php echo csrf_token() ?>';
    $(document).ready(function () {
            var login = new Vue({
                el: '#login',
                data: {
                    item: {
                        email: '',
                        password: '',
                    }
                },
                mounted: function () {
                    this.doLogin();
                },
                methods: {
                    doLogin: function () {
                        var _this = this;
                        this.$http.post("{{route('check.login.withajax')}}", {
                            _token: _token,
                            email: _this.item.email,
                            password: _this.item.password,
                        }).then(function (success) {
                            if (success.body.status) {
                                window.location.href = "{{route('cpanel.index')}}";
                            }
//                        _this.loadItems();
                        }, function (error) {
                            console.log(error);
                        });
                    },
                    onChangeColor: function (event) {
                        this.$http.post("{{route('cpanel.switch.colors')}}", {
                            color: event.target.value
                        }).then(function (success) {
                            if (success.body.status) {
                                location.reload();
                            }
                        }, function (error) {
                            console.log(error);
                        });
                    },
                    onChangeThemes: function (event) {

                        this.$http.post("{{route('cpanel.switch.themes')}}", {
                            _token: _token,
                            theme: event.target.value
                        }).then(function (success) {
                            console.log(success);
                            if (success.body.status) {
                                location.reload();
                            }
                        }, function (error) {
                            console.log(error);
                        });
                    }
                }
            });

        });

    })(jQuery);
</script-->
<script>
    window.Laravel = @php echo json_encode(['csrfToken' => csrf_token(),]); @endphp;
</script>
<script type="text/javascript" src="{!!asset('js/app.js')!!}"></script>
<script type="text/javascript" src="{!!asset('js/app.custom.js')!!}"></script>