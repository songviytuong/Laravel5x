<?php
    $themes = config('cpanel.themes');
    $verify = Hash::make(serialize($themes));

    $checksum = Modules\Cpanel\Entities\SitePrefs::select('sitepref_value')->where('sitepref_name','checksum')->get()->first();
    $checksum = ($checksum) ? $checksum->sitepref_value : '';

    $isOK = Hash::check(serialize($themes),$checksum);
    if(!$isOK){
?>
<div class="text-center" style="padding-top:20px;"><a href='{!!route("cpanel.settings.reset_siteprefs")!!}'>Need Upgrade</a></div>
<?php } ?>