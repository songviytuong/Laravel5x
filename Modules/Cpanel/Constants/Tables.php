<?php

namespace Modules\Cpanel\Constants;

class Tables {

    const Contacts = '5x_contacts';
    const ContactsGroups = '5x_contacts_groups';
    const Bookings = '5x_bookings';

}
