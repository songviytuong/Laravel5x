<?php

//Cpanel::Home
Route::group(['prefix' => 'cpanel', 'namespace' => 'Modules\Cpanel\Http\Controllers'], function () {
    Route::get('/', 'CpanelController@index')->name('cpanel.index');
    Route::get('/login', 'LoginController@login')->name('cpanel.login');
    Route::post('/check-login', 'LoginController@checkLogin')->name('check.login.withajax');
    Route::post('/check-login2', 'LoginController@checkLogin2')->name('check.login');
    Route::get('/logout', 'LoginController@logout')->name('cpanel.logout');

    Route::post('/switch-themes', 'CpanelController@switchThemes')->name('cpanel.switch.themes');
    Route::post('/switch-colors', 'CpanelController@switchColors')->name('cpanel.switch.colors');

    Route::get('/ajax-get-loading', 'CpanelController@ajaxGetLoading')->name('cpanel.ajaxGetLoading');
});

//Cpanel::Projects
Route::group(['prefix' => 'cpanel/projects', 'namespace' => 'Modules\Cpanel\Http\Controllers'], function () {
    Route::post('/ajax-load-projects', 'ProjectController@loadProjects')->name('cpanel.projects.ajax.load_projects');
    Route::get('/ajax-load-edit-project', 'ProjectController@loadEditProject')->name('cpanel.projects.ajax.load_edit_project');
    Route::post('/add-project', 'ProjectController@addProject')->name('cpanel.projects.add.project');
    Route::post('/save-project', 'ProjectController@saveProject')->name('cpanel.projects.save.project');
    Route::post('/remove-project', 'ProjectController@removeProject')->name('cpanel.projects.remove.project');
});

//Cpanel::Tasks
Route::group(['prefix' => 'cpanel/tasks', 'namespace' => 'Modules\Cpanel\Http\Controllers'], function () {
    Route::get('/ajax-load-tasks', 'TaskController@loadTasks')->name('cpanel.tasks.ajax.load_tasks');
    Route::get('/{alias?}', 'TaskController@index')->name('cpanel.tasks.index');
    Route::post('/new-task', 'TaskController@newTask')->name('cpanel.tasks.new.task');
    Route::post('/update-task', 'TaskController@updateTask')->name('cpanel.tasks.update.task');
});

//Cpanel::Activities
Route::group(['prefix' => 'cpanel/activities', 'namespace' => 'Modules\Cpanel\Http\Controllers'], function () {
    Route::get('/', 'ActivitiesController@index')->name('cpanel.activities.index');
});

//Cpanel::Contacts
Route::group(['prefix' => 'cpanel/contacts', 'namespace' => 'Modules\Cpanel\Http\Controllers'], function () {
    Route::get('/', 'ContactController@index')->name('cpanel.contacts.index');
    Route::post('/ajax-load-contacts', 'ContactController@index')->name('cpanel.contacts.ajax.load_contacts');
    Route::get('/add', 'ContactController@addContact')->name('cpanel.contacts.add');
    Route::post('/add', 'ContactController@addContact')->name('cpanel.contacts.add');
    Route::get('/remove-contact', 'ContactController@removeContact')->name('cpanel.contacts.remove.contact');
    Route::post('/remove-contact', 'ContactController@removeContact')->name('cpanel.contacts.remove.contact');
});

//Cpanel::Bookings
Route::group(['prefix' => 'cpanel/bookings', 'namespace' => 'Modules\Cpanel\Http\Controllers'], function () {
    Route::get('/', 'BookingController@index')->name('cpanel.bookings.index');
    Route::post('/load-booking/{bid?}', 'BookingController@loadBooking')->name('cpanel.bookings.ajax.load_bookings');
    Route::post('/syncAllData', 'BookingController@syncAllData')->name('cpanel.bookings.ajax.syncAllData');
});

//Cpanel::Settings
Route::group(['prefix' => 'cpanel/settings', 'namespace' => 'Modules\Cpanel\Http\Controllers'], function () {
    Route::get('/', 'SettingController@siteprefs')->name('cpanel.settings.siteprefs');
    Route::get('/site-prefs-reset', 'SettingController@sitePrefsReset')->name('cpanel.settings.reset_siteprefs');
});

//Cpanel::Users
Route::group(['prefix' => 'cpanel/users', 'namespace' => 'Modules\Cpanel\Http\Controllers'], function () {
    Route::get('/', 'UserController@index')->name('cpanel.users.profiles');
    Route::get('/ajax-get-timelines', 'UserController@ajaxGetTimelines')->name('cpanel.users.profiles.ajaxGetTimelines');
    Route::post('/', 'UserController@index')->name('cpanel.users.profiles');
    Route::post('/avatar', 'UserController@avatar')->name('cpanel.users.avatar');
});

Route::get('/assets/{module}/{type}/{file}', [
    function ($module, $type, $file) {
        $module = ucfirst($module);
        $path = app_path("../Modules/$module/Resources/assets/$type/$file");
        if (\File::exists($path)) {
            if ($type == 'js') {
                return response()->file($path, array('Content-Type' => 'application/javascript'));
            } else {
                return response()->file($path, array('Content-Type' => 'text/css'));
            }
        }

        return response()->json([], 404);
    },
]);

Route::get('/assets/{module}/{themes}/{color}/{filepath}', [
    function ($module, $themes, $color, $filepath) {
        $module = ucfirst($module);
        $path = app_path("../Modules/$module/Resources/themes/$themes/$color/$filepath");
        $ext = last(explode('.', $path));
        if (\File::exists($path)) {
            if ($ext == 'js') {
                return response()->file($path, array('Content-Type' => 'application/javascript'));
            } else {
                return response()->file($path, array('Content-Type' => 'text/css'));
            }
        }

        return response()->json([], 404);
    },
])->where('filepath', '(.*)');
