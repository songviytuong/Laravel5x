<?php

namespace Modules\Cpanel\Http\Controllers;

use View;
use Route;
use App\Helpers\Business;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class BaseController extends Controller
{
    //------------ GLOBAL START ---------------------

    public $data = [];
    protected $theme_active;
    protected $color_active;

    //------------- GLOBAL END ----------------------
    protected $guard = 'cpanel';
    protected $administrator = [];
    protected $uid = null;

    public function __construct()
    {
        $route_name = Route::currentRouteName();
        $except = ['cpanel.switch.themes', 'cpanel.switch.colors'];
        if (Auth::guard($this->guard)->check() || in_array($route_name, $except)) {
            $this->administrator = Auth::guard($this->guard)->user();
            $this->uid = $this->administrator['id'];
            View::share(['administrator' => $this->administrator]);
        } else {
            Redirect::route('cpanel.login')->send();
        }

        //------------ GLOBAL START ---------------------
        $this->theme_active = (Business::cms_getSitePrefs('theme_active')) ? Business::cms_getSitePrefs('theme_active') : ((session('theme_active')) ? session('theme_active') : config('cpanel.theme_active'));
        $this->color_active = (Business::cms_getSitePrefs('color_active')) ? Business::cms_getSitePrefs('color_active') : ((session('color_active')) ? session('color_active') : config('cpanel.color_active'));
        $this->data['settings']['color_active'] = $this->color_active;
        $this->data['settings']['theme_active'] = $this->theme_active;

        return View::share(array('data' => $this->data));
        //------------- GLOBAL END ----------------------
    }
}
