<?php

namespace Modules\Cpanel\Http\Controllers;

use View;
use Carbon\Carbon;
use App\Helpers\Business;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Cpanel\Entities\Tasks;
use Illuminate\Support\Facades\Auth;
use Modules\Cpanel\Entities\Projects;
use Illuminate\Support\Facades\Validator;

class ProjectController extends BaseController
{
    //------------ GLOBAL START ---------------------

    protected $theme_active;
    protected $color_active;
    public $data = [];

    //------------- GLOBAL END ----------------------

    public function __construct()
    {
        parent::__construct();
        //------------ GLOBAL START ---------------------
        $this->theme_active = (Business::cms_getSitePrefs('theme_active')) ? Business::cms_getSitePrefs('theme_active') : config('cpanel.theme_active');
        $this->color_active = (Business::cms_getSitePrefs('color_active')) ? Business::cms_getSitePrefs('color_active') : config('cpanel.color_active');
        $this->data['settings']['color_active'] = $this->color_active;
        $this->data['settings']['theme_active'] = $this->theme_active;

        return View::share(array('data' => $this->data));
        //------------- GLOBAL END ----------------------
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function loadProjects(Request $request)
    {
        //---Show SideBar
        $projects = Projects::listProjects();
        $this->data['sidebar']['projects'] = $projects;

        $data = $this->data;

        return view('cpanel::'.$this->theme_active.'.projects.ajax_load_projects', compact('data'));
    }

    public function loadEditProject(Request $request)
    {
        $project = Projects::getProjectById($request->project_id);
        if ($project) {
            $response['status'] = true;
            $response['data'] = [
                'project_name' => $project->project_name,
            ];
        } else {
            $response['status'] = false;
        }

        return response()->json($response);
    }

    public function addProject(Request $request)
    {
        $user_id = Auth::guard('cpanel')->user()->id;

        if ($request->ajax()) {
            $module = $request->module;

            switch ($module) {
                case 'projects':
                    $validator = Validator::make($request->all(), [
                                'project_name' => 'required',
                    ]);
                    if (!empty($validator) && $validator->fails()) {
                        $response['status'] = false;
                    } else {
                        $inserted = [
                            'project_name' => $request->project_name,
                            'alias' => Business::munge_string_to_url($request->project_name),
                            'description' => $request->description,
                            'created_by' => $user_id,
                            'updated_by' => $user_id,
                        ];
                        Projects::insert($inserted);
                        $response['status'] = true;
                    }
                    break;
            }

            return response()->json($response);
        }
    }

    public function saveProject(Request $request)
    {
        $user_id = Auth::guard('cpanel')->user()->id;

        if ($request->ajax()) {
            $module = $request->module;

            switch ($module) {
                case 'projects':
                    $validator = Validator::make($request->all(), [
                                'project_name' => 'required',
                    ]);
                    if (!empty($validator) && $validator->fails()) {
                        $response['status'] = false;
                    } else {
                        $updated = [
                            'project_name' => $request->project_name,
                            'alias' => Business::munge_string_to_url($request->project_name),
                            'description' => $request->description,
                            'updated_by' => $user_id,
                        ];
                        Projects::where(['id' => $request->project_id])->update($updated);
                        $response['status'] = true;
                        $response['alias'] = Business::munge_string_to_url($request->project_name);
                    }
                    break;
            }

            return response()->json($response);
        }
    }

    public function removeProject(Request $request)
    {
        $user_id = Auth::guard('cpanel')->user()->id;
        if ($request->ajax()) {
            // $res=Projects::where('id', $request->project_id)->delete();
            $updated = [
                'updated_by' => $user_id,
                'updated_at' => Carbon::now()->toDateTimeString(),
                'deleted_at' => Carbon::now()->toDateTimeString(),
            ];
            $res = Projects::where('id', $request->project_id)->update($updated);
            if ($res) {
                $response['status'] = true;
                Tasks::where('project_id', $request->project_id)->update(['deleted_at' => Carbon::now()->toDateTimeString()]);
            } else {
                $response['status'] = false;
            }

            return response()->json($response);
        }
    }
}
