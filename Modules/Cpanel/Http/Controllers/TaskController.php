<?php

namespace Modules\Cpanel\Http\Controllers;

use SEO;
use View;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Cpanel\Jobs\JTasks;
use Modules\Cpanel\Constants\Jobs;
use Modules\Cpanel\Entities\Tasks;
use Modules\Cpanel\Entities\Projects;

class TaskController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        SEO::setTitle('::Tasks Manager::');

        //---Show Tasks List
        $tasks = Tasks::listTasks($request);
        $this->data['screen']['tasks'] = $tasks;

        if ($request->alias) {
            $except = ['ajax-load-projects'];
            $valid = Projects::alias_exists($request->alias, $except);
            if (!$valid) {
                return redirect()->route('cpanel.tasks.index');
            }
            SEO::setTitle('::Tasks Manager::Project::');

            //---Show Project_ID
            $project_id = Projects::getIDByAlias($request->alias);
            $this->data['screen']['project_id'] = $project_id;
        }

        $data = $this->data;

        return view('cpanel::'.$this->theme_active.'.tasks.index', compact('data'));
    }

    public function newTask(Request $request)
    {
        if ($request->ajax() && $request->project_id != 0) {
            $inserted = [
                'task_name' => $request->task_name,
                'project_id' => $request->project_id,
            ];
            Tasks::insert($inserted);
            $response['status'] = true;
        } else {
            $response['status'] = false;
        }

        return response()->json($response);
    }

    public function updateTask(Request $request)
    {
        if ($request->ajax()) {
            $tasks = Tasks::where(['task_name' => $request->task_name])->get()->first();
            $before_status = ($tasks) ? $tasks->status : 0;
            if ($before_status == 1) {
                $after_status = 0;
            } else {
                $after_status = 1;
            }

//            switch ($request->type) {
//                case 'status':
//                    $updated = [
//                        'status' => $after_status,
//                    ];
//                    Tasks::where(['task_name' => $request->task_name])->update($updated);
//                    break;
//                case 'task_name':
//                    $updated = [
//                        'task_name' => $request->task_name,
//                    ];
//                    Tasks::where(['id' => $request->task_id])->update($updated);
//                    break;
//            }

            $DataRequest = [
                'task_id' => ($request->task_id) ? $request->task_id : '',
                'type' => ($request->type) ? $request->type : '',
                'task_name' => ($request->task_name) ? $request->task_name : '',
                'status' => $after_status,
            ];

            $job = (new JTasks($DataRequest))->onConnection('sync')->onQueue(Jobs::JTasks);
            dispatch($job);

            $response['status'] = true;
        } else {
            $response['status'] = false;
        }

        return response()->json($response);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function loadTasks(Request $request)
    {
        $tasks = Tasks::listTasks($request);
        $this->data['screen']['tasks'] = $tasks;

        $data = $this->data;

        return view('cpanel::'.$this->theme_active.'.tasks.ajax_load_tasks', compact('data'));
    }
}
