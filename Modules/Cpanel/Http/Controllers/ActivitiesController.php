<?php

namespace Modules\Cpanel\Http\Controllers;

use View;
use App\Helpers\Business;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Cpanel\Entities\Activities;

class ActivitiesController extends BaseController
{
    //------------ GLOBAL START ---------------------

    protected $theme_active;
    protected $color_active;
    public $data = [];

    //------------- GLOBAL END ----------------------

    public function __construct()
    {
        parent::__construct();
        //------------ GLOBAL START ---------------------
        $this->theme_active = (Business::cms_getSitePrefs('theme_active')) ? Business::cms_getSitePrefs('theme_active') : config('cpanel.theme_active');
        $this->color_active = (Business::cms_getSitePrefs('color_active')) ? Business::cms_getSitePrefs('color_active') : config('cpanel.color_active');
        $this->data['settings']['color_active'] = $this->color_active;
        $this->data['settings']['theme_active'] = $this->theme_active;

        return View::share(array('data' => $this->data));
        //------------- GLOBAL END ----------------------
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        //---Show Activities
        $activities = Activities::listActivities();
        $this->data['screen']['activities'] = $activities;

        $data = $this->data;

        return view('cpanel::'.$this->theme_active.'.activities.index', compact('data'));
    }
}
