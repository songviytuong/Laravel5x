<?php

namespace Modules\Cpanel\Http\Controllers;

use SEO;
use View;
use App\Helpers\Business;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Modules\Cpanel\Entities\Contacts;
use Modules\Cpanel\Entities\ContactsGroups;
use Modules\Cpanel\Constants\Tables;

class ContactController extends BaseController
{
    public $contacts_groups = [];
    public $contacts = [];

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $user_id = $this->uid;
        $where = ['user_id' => $user_id];

        SEO::setTitle('::Contacts Manager::');

        //--Show Groups
        $contacts_groups = ContactsGroups::listContactsGroups();
//        echo "<pre>";
//        var_dump($contacts_groups); exit();
        $this->data['screen']['contacts_groups'] = $contacts_groups;

        //--List Contacts
//        $contacts = Contacts::listContacts(['user_id' => 1, 'group_id' => 1]);
//        $this->data['screen']['contacts'] = $contacts;

        if ($request->ajax()) {
            $type = $request->type;
            switch ($type) {
                case 'category':
                    $where['group_id'] = $request->id;
                    break;
                case 'all':
                    $where['all'] = true;
                    break;
            }
            $contacts = Contacts::listContacts($where);
            
            $page = $request->page;
            if ($page > 1 && !count($contacts)) {
                $contacts = $contacts->paginate(config('cpanel.records_per_page'), ['*'], 'page', $page - 1);
            }
            $paging = $contacts->toArray();
            $request->session()->put('ajaxSearchFromData', ['type' => $type, 'group_id' => $request->id]);
            $data = $this->data;

            return view('cpanel::'.$this->theme_active.'.contacts.ajax_load_contacts', compact('data', 'contacts', 'paging'));
        }

        $data = $this->data;

        return view('cpanel::'.$this->theme_active.'.contacts.index', compact('data', 'contacts_groups'));
    }

    public function addContact(Request $request)
    {
        $user_id = $this->uid;

        //--Show Groups
        $contacts_groups = ContactsGroups::listContactsGroups();
        $this->data['screen']['contacts_groups'] = $contacts_groups;

        if ($request->ajax()) {
            DB::beginTransaction();
            try {
                $group_txt = ($request->group_text) ? $request->group_text : '';
                if ($group_txt) {
                    //Exist valid
                    $is_exists = Business::cms_existsFieldWithValue('group_name', $group_txt, Tables::ContactsGroups);
                    if ($is_exists) {
                        $group_id = ContactsGroups::getIDFromGroupName($group_txt);
                    } else {
                        $group_id = ContactsGroups::insertGetId(['group_name' => $group_txt]);
                    }
                }

                $dataInsert = [
                    'firstname' => ($request->firstname) ? $request->firstname : '',
                    'lastname' => ($request->lastname) ? $request->lastname : '',
                    'phone' => ($request->phone) ? $request->phone : '',
                    'email' => ($request->email) ? $request->email : '',
                    'address' => ($request->address) ? $request->address : '',
                    'gender' => ($request->gender) ? $request->gender : 0,
                    'group_id' => ($group_id) ? $group_id : 0,
                    'status' => ($request->status) ? $request->status : 0,
                ];

                $dataInsert['user_id'] = $user_id;

                if (Contacts::insert($dataInsert)) {
                    $response['status'] = true;
                }
                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();
                $response['status'] = $e->getMessage();
            }

            return response()->json($response);
        }

        $data = $this->data;

        return view('cpanel::'.$this->theme_active.'.contacts.add', compact('data', 'contacts_groups'));
    }

    public function removeContact(Request $request)
    {
        if ($request->ajax()) {
            $data = $request->all();
            $mytoken = md5('REMOVEGROUP'.$request->id);
            if ($mytoken == $request->token) {
                $group = ContactsGroups::find($request->id);
                if ($group->delete()) {
                    $response['status'] = true;
                }
            } else {
                $response['status'] = false;
            }

            return response()->json($response);
        } else {
            return redirect()->route('cpanel.contacts.index');
        }
    }
}
