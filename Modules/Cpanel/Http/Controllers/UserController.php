<?php

namespace Modules\Cpanel\Http\Controllers;

use Auth;
use View;
use Carbon\Carbon;
use App\Helpers\Business;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Modules\Cpanel\Entities\Timelines;

class UserController extends BaseController
{
    //------------ GLOBAL START ---------------------

    protected $theme_active;
    protected $color_active;
    public $data = [];

    //------------- GLOBAL END ----------------------

    public function __construct()
    {
        parent::__construct();
        //------------ GLOBAL START ---------------------
        $this->theme_active = (Business::cms_getSitePrefs('theme_active')) ? Business::cms_getSitePrefs('theme_active') : config('cpanel.theme_active');
        $this->color_active = (Business::cms_getSitePrefs('color_active')) ? Business::cms_getSitePrefs('color_active') : config('cpanel.color_active');
        $this->data['settings']['color_active'] = $this->color_active;
        $this->data['settings']['theme_active'] = $this->theme_active;

        return View::share(array('data' => $this->data));
        //------------- GLOBAL END ----------------------
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
//         $x = 100;
        // $total = 500;
        // $percentage = ($x*100)/$total;
        // echo $percentage; exit();

        if ($request->isMethod('post')) {
            $message = $request->message_text;
            $data_insert = [
                'user_id' => Auth::guard('cpanel')->user()->id,
                'message' => $message,
                'tags' => '',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ];
            if (Timelines::insert($data_insert)) {
                $response['status'] = true;
            } else {
                $response['status'] = false;
            }

            return response()->json($response);
        }

        //------------ GLOBAL START ---------------------
        $theme_active = Business::cms_getSitePrefs('theme_active');
        $themes = (Business::cms_getSitePrefs('themes')) ? unserialize(Business::cms_getSitePrefs('themes')) : config('cpanel.themes');
        $colors = $themes[$theme_active]['color'];
        $this->data['settings']['colors'] = $colors;
        //------------- GLOBAL END ----------------------

        $this->data['breadcrumbs']['page'] = 'Profiles';
        $this->data['breadcrumbs']['page_link'] = ['Users', 'Profiles'];

//        echo "<pre>";
//        var_dump($list);
//        exit();

        $data = $this->data;

        return View::make('cpanel::'.$this->theme_active.'.users.profiles', compact('data'));
    }

    public function ajaxGetTimelines()
    {
        $timelines = Timelines::where('user_id', Auth::guard('cpanel')->user()->id)->orderBy('month_year', 'desc')->select(
            'id', 'message', 'created_at', 'tags', DB::raw('DATE_FORMAT(created_at, "%Y-%m") as month_year'))->get();
        $month_year = [];
        $list = [];
        if ($timelines) {
            foreach ($timelines as $key => $timeline) {
                if (!isset($month_year[$timeline->month_year])) {
                    $month_year[$timeline->month_year] = $timeline->month_year;
                }
                $my = date('F Y', strtotime($timeline->month_year));
                $temp = $timeline->toArray();
                $temp['time_elapsed'] = Business::cms_timeElapsed($timeline['created_at']);
                $temp['tags'] = ($timeline['tags']) ? explode(',', $timeline['tags']) : [];
                $list[$my][$key] = $temp;
            }
        }

        return View::make('cpanel::'.$this->theme_active.'.users.profiles_ajaxGetTimelines', compact('timelines', 'list'));
    }
}
