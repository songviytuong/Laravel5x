<?php

namespace Modules\Cpanel\Http\Controllers;

use View;
use Carbon\Carbon;
use App\Helpers\Business;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Cpanel\Entities\SitePrefs;

class CpanelController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //------------ GLOBAL START ---------------------
        $theme_active = Business::cms_getSitePrefs('theme_active');
        $themes = (Business::cms_getSitePrefs('themes')) ? unserialize(Business::cms_getSitePrefs('themes')) : config('cpanel.themes');
        $colors = $themes[$theme_active]['color'];
        $this->data['settings']['colors'] = $colors;
        //------------- GLOBAL END ----------------------

        return View::make('cpanel::' . $this->theme_active . '.' . $this->theme_active, array('data' => $this->data));
    }

    public function switchThemes()
    {
        if (request()->ajax()) {
            $theme_active = request()->theme;
            $themes = (Business::cms_getSitePrefs('themes')) ? unserialize(Business::cms_getSitePrefs('themes')) : config('cpanel.themes');
            $colors = $themes[$theme_active]['color'];
            $UpdateTheme = [
                'sitepref_value' => $theme_active,
                'updated_at' => Carbon::now()->toDateTimeString(),
            ];
            SitePrefs::where('sitepref_name', 'theme_active')->update($UpdateTheme);
            $UpdateColor = [
                'sitepref_value' => (count($colors) > 0) ? $colors[0] : '',
                'updated_at' => Carbon::now()->toDateTimeString(),
            ];
            SitePrefs::where('sitepref_name', 'color_active')->update($UpdateColor);

            session()->put('theme_active', $theme_active);
            session()->put('color_active', ($colors) ? $colors[0] : '');

            return response(array('status' => true), 200, []);
        }
    }

    public function switchColors()
    {
        if (request()->ajax()) {
            $theme_active = Business::cms_getSitePrefs('theme_active');
            $themes = (Business::cms_getSitePrefs('themes')) ? unserialize(Business::cms_getSitePrefs('themes')) : config('cpanel.themes');
            $colors = $themes[$theme_active]['color'];
            if (count($colors) > 0) {
                SitePrefs::where('sitepref_name', 'color_active')->update(array(
                    'sitepref_value' => request()->color,
                    'updated_at' => Carbon::now()->toDateTimeString(),
                ));

                return response(array('status' => true), 200, []);
            }
        }

        return response(array('status' => false), 200, []);
    }

    public function ajaxGetLoading()
    {
        return View::make('cpanel::ajaxGetLoading');
    }
}
