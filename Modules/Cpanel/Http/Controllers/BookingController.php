<?php

namespace Modules\Cpanel\Http\Controllers;

use SEO;
use View;
use App\Helpers\Business;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class BookingController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        SEO::setTitle('::Booking Manager::');

        $bookings = Business::cms_loginGetContent(env('API_MUSLIM_URL'), ['method' => 'post', 'token' => '27061987'], $fields = [], 'json');
        $data = $this->data;

        return view('cpanel::'.$this->theme_active.'.bookings.index', compact('data', 'bookings'));
    }

    public function loadBooking(Request $request)
    {
        $extra1 = [];
        // $extra1[] = ($request->bid) ? 'A.item_id=' . $request->bid : '';
        // $extra1[] = "(A.create_time >= '2018-02-01' and A.create_time <= '2018-08-31')";
        $extra = serialize($extra1);

        $bookings = Business::cms_loginGetContent(env('API_MUSLIM_URL'), ['method' => 'post', 'token' => '27061987'], $fields = ['extra' => $extra], 'json');
        $i = 0;
        $arr = [];

        if (!empty($bookings) && is_array($bookings)) {
            foreach ($bookings as $k => $booking) {
                foreach ($booking as $key => $item) {
                    $arr[$i][$key] = $item;
                    $arr[$i]['DT_RowId'] = 'row_'.$bookings[$k]['booking_id'];
                    $arr[$i]['DT_RowClass'] = 'booking-item';
                }
                ++$i;
            }
        }
        $responese['data'] = $arr;

        return json_encode($responese);
    }

    public function syncAllData()
    {
        $res = array();
        $res['Updated'] = 'True';
        $res['CurrentStatus'] = 1;
        sleep(1);
        echo json_encode($res);
    }
}
