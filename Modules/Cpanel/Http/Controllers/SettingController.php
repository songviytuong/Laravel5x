<?php

namespace Modules\Cpanel\Http\Controllers;

use Carbon\Carbon;
use App\Helpers\Business;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Hash;
use Modules\Cpanel\Entities\SitePrefs;
use View;

class SettingController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('cpanel::settings');
    }

    /**
     * Settings.
     *
     * @return Pages
     */
    public function siteprefs()
    {
        //Data for breadcrumbs
        $this->data['breadcrumbs']['page'] = 'Site Prefs';
        $this->data['breadcrumbs']['page_link'] = ['Settings', 'Site Prefs'];
        //Data for siteprefs
        $siteprefs = SitePrefs::get()->toArray();
        $siteprefs_arr = [];
        foreach ($siteprefs as $sitepref) {
            $siteprefs_arr[$sitepref['sitepref_name']] = $sitepref['sitepref_value'];
        }
        $this->data['siteprefs'] = $siteprefs_arr;
        //Data for global
        $data = $this->data;
        //View
        return View::make('cpanel::'.$this->theme_active.'.settings.siteprefs', compact('data'));
    }

    /**
     * Reset SitePrefs from File: config to Databases.
     */
    public function sitePrefsReset()
    {
        $allows = ['themes', 'theme_active', 'color_active'];

        $config = config('cpanel');
        foreach ($config as $key => $item) {
            if (!in_array($key, $allows)) {
                continue;
            }
            if (is_array($item)) {
                $item = serialize($item);
            }

            if (Business::cms_existsFieldWithValue('sitepref_name', $key, '5x_siteprefs')) {
                SitePrefs::where('sitepref_name', $key)->update(['sitepref_value' => $item, 'updated_at' => Carbon::now()->toDateTimeString()]);
            } else {
                SitePrefs::insert(['sitepref_name' => $key, 'sitepref_value' => $item, 'created_at' => Carbon::now()->toDateTimeString(), 'updated_at' => Carbon::now()->toDateTimeString()]);
            }
        }

        //CHECKSUM SitePrefs
        $themes = config('cpanel.themes');
        $verify = Hash::make(serialize($themes));

        if (Business::cms_existsFieldWithValue('sitepref_name', 'checksum', '5x_siteprefs')) {
            SitePrefs::where('sitepref_name', 'checksum')->update(['sitepref_value' => $verify, 'updated_at' => Carbon::now()->toDateTimeString()]);
        } else {
            SitePrefs::insert(['sitepref_name' => 'checksum', 'sitepref_value' => $verify, 'created_at' => Carbon::now()->toDateTimeString(), 'updated_at' => Carbon::now()->toDateTimeString()]);
        }

        return redirect(URL::previous());
    }
}
