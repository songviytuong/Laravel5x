<?php

namespace Modules\Cpanel\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use SEO;
use View;
use App\Helpers\Business;

class LoginController extends BaseController
{
    //------------ GLOBAL START ---------------------

    protected $theme_active;
    protected $color_active;
    public $data = [];

    //------------- GLOBAL END ----------------------

    public function __construct()
    {
        //------------ GLOBAL START ---------------------
        $this->theme_active = (Business::cms_getSitePrefs('theme_active')) ? Business::cms_getSitePrefs('theme_active') : config('cpanel.theme_active');
        $this->color_active = (Business::cms_getSitePrefs('color_active')) ? Business::cms_getSitePrefs('color_active') : config('cpanel.color_active');
        $this->data['settings']['color_active'] = $this->color_active;
        $this->data['settings']['theme_active'] = $this->theme_active;

        return View::share(array('data' => $this->data));
        //------------- GLOBAL END ----------------------
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function login()
    {
        SEO::setTitle('Login Cpanel - CMS&trade; Admin Console');

        //------------ GLOBAL START ---------------------
        $theme_active = Business::cms_getSitePrefs('theme_active');
        $themes = (Business::cms_getSitePrefs('themes')) ? unserialize(Business::cms_getSitePrefs('themes')) : config('cpanel.themes');
        $colors = $themes[$theme_active]['color'];
        $this->data['settings']['colors'] = $colors;
        //------------- GLOBAL END ----------------------

        return View::make('cpanel::'.$this->theme_active.'.login', array('data' => $this->data));
    }

    public function checkLogin()
    {
        if (request()->ajax()) {
            $items = [];
            $rules = array(
                'email' => 'required|email',
                'password' => 'required|alphaNum|min:3',
            );
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                $items['validator'] = $validator;

                return response(array(
                    'status' => false,
                    'data' => $items,
                        ), 200, []);
            } else {
                $userdata = array(
                    'email' => Input::get('email'),
                    'password' => Input::get('password'),
                );

                if (Auth::guard($this->guard)->attempt($userdata)) {
                    $items['userdata'] = $userdata;
                }

                return response(array(
                    'status' => true,
                    'data' => $items,
                        ), 200, []);
            }
        }
    }

    public function checkLogin2()
    {
        $rules = array(
            'email' => 'required|email',
            'password' => 'required|alphaNum|min:3',
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('/cpanel/login')
                            ->withErrors($validator)
                            ->withInput();
        } else {
            $userdata = array(
                'email' => Input::get('email'),
                'password' => Input::get('password'),
            );

            if (Auth::guard($this->guard)->attempt($userdata)) {
//                die('a');
                return Redirect::to('/cpanel');
            } else {
                return Redirect::to('/cpanel/login')->withErrors(['error' => 'loginid & password is not correct']);
            }
        }
    }

    public function logout()
    {
        Auth::guard($this->guard)->logout();

        return Redirect::to('/cpanel');
    }
}
