<?php

namespace Modules\Cpanel\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * @var array
     */
    protected $middleware = [
    ];

    /**
     * The application's route middleware.
     *
     * @var array
     */
    protected $routeMiddleware = [
    ];

    public function handle($request)
    {
        try {
            return parent::handle($request);
        } catch (\Symfony\Component\HttpKernel\Exception\NotFoundHttpException $e) {
            return response()->view('errors.custom', ['exception' => $e], 404);
        } catch (Exception $e) {
            $this->reportException($e);

            return $this->renderException($request, $e);
        }
    }
}
