<?php

return [
    #-- MAIN
    'name' => 'Cpanel',
    #-- SITE_PREFS
    'themes' => [
        'PreAdmin' => [
            'color' => [
                'light', 'orange', 'purple', 'blue', 'dark'
            ]
        ],
        'OneEleven' => [
            'color' => [
            ]
        ],
        'PortoAdmin' => [
            'color' => [
            ]
        ],
    ],
    'theme_active' => 'OneEleven',
    'color_active' => '',
    #-- PRIORITY_TYPE
    'priorities' => [
        0 => 'Low',
        1 => 'Medium',
        2 => 'High',
    ],
    #-- METADATA_SEOS
    'seos' => [
        'cpanel/users' => [
            'cpanel.users.profiles' => [
                'meta_title' => '',
                'meta_description' => ''
            ]
        ]
    ],
    'gender' => [
        0 => 'Male',
        1 => 'Female',
        2 => 'Other',
    ],
    'records_per_page' => 10
];
