<?php

Route::group(['prefix' => 'partners', 'namespace' => 'Modules\Partners\Http\Controllers'], function()
{
    Route::get('/', 'PartnersController@index');
});
