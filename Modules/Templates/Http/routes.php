<?php

Route::group(['prefix' => 'templates', 'namespace' => 'Modules\Templates\Http\Controllers'], function()
{
    Route::get('/', 'TemplatesController@index');
});

Route::get('/assets-templates/{ext}/{filepath}', [
    function($ext, $filepath) {
        $ext = ucfirst($ext);
        $path = app_path("../Modules/Templates/Resources/$ext/$filepath");
        $last = last(explode('.', $path));
        if (\File::exists($path)) {
            if ($last == 'js') {
                return response()->file($path, array('Content-Type' => 'application/javascript'));
            } else {
                return response()->file($path, array('Content-Type' => 'text/css'));
            }
        }
        return response()->json([], 404);
    }
])->where('filepath', '(.*)');