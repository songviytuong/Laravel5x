<!DOCTYPE html>
<html>
	<head>
		<!-- Basic -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">	
		<title>HTML5 Template</title>
		<!-- Favicon -->
		<link rel="shortcut icon" href="{{ Asset::get('/assets-templates/assets/img/favicon.ico') }}" type="image/x-icon" />
		<link rel="apple-touch-icon" href="{{ Asset::get('/assets-templates/assets/img/apple-touch-icon.png') }}">
		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">
		<!-- Web Fonts  -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">
		<!-- Vendor CSS -->
		<link rel="stylesheet" href="{{ Asset::get('/assets-templates/assets/vendor/bootstrap/css/bootstrap.min.css') }}">
		<link rel="stylesheet" href="{{ Asset::get('/assets-templates/assets/vendor/font-awesome/css/fontawesome-all.min.css') }}">
		<link rel="stylesheet" href="{{ Asset::get('/assets-templates/assets/vendor/animate/animate.min.css') }}">
		<link rel="stylesheet" href="{{ Asset::get('/assets-templates/assets/vendor/simple-line-icons/css/simple-line-icons.min.css') }}">
		<link rel="stylesheet" href="{{ Asset::get('/assets-templates/assets/vendor/owl.carousel/assets/owl.carousel.min.css') }}">
		<link rel="stylesheet" href="{{ Asset::get('/assets-templates/assets/vendor/owl.carousel/assets/owl.theme.default.min.css') }}">
		<link rel="stylesheet" href="{{ Asset::get('/assets-templates/assets/vendor/magnific-popup/magnific-popup.min.css') }}">
		<!-- Theme CSS -->
		<link rel="stylesheet" href="{{ Asset::get('/assets-templates/assets/css/theme.css') }}">
		<link rel="stylesheet" href="{{ Asset::get('/assets-templates/assets/css/theme-elements.css') }}">
		<link rel="stylesheet" href="{{ Asset::get('/assets-templates/assets/css/theme-blog.css') }}">
		<link rel="stylesheet" href="{{ Asset::get('/assets-templates/assets/css/theme-shop.css') }}">
		<!-- Current Page CSS -->
		<link rel="stylesheet" href="{{ Asset::get('/assets-templates/assets/vendor/rs-plugin/css/settings.css') }}">
		<link rel="stylesheet" href="{{ Asset::get('/assets-templates/assets/vendor/rs-plugin/css/layers.css') }}">
		<link rel="stylesheet" href="{{ Asset::get('/assets-templates/assets/vendor/rs-plugin/css/navigation.css') }}">
		<link rel="stylesheet" href="{{ Asset::get('/assets-templates/assets/vendor/circle-flip-slideshow/css/component.css') }}">
		<!-- Skin CSS -->
		<link rel="stylesheet" href="{{ Asset::get('/assets-templates/assets/css/skins/default.css') }}">
		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="{{ Asset::get('/assets-templates/assets/css/custom.css') }}">

		<!-- Head Libs -->
		<script src="{{ Asset::get('/assets-templates/assets/vendor/modernizr/modernizr.min.js') }}"></script>

	</head>
	<body>
        @yield('content')
		<!-- Vendor -->
		<script src="{{ Asset::get('/assets-templates/assets/vendor/jquery/jquery.min.js') }}"></script>
		<script src="{{ Asset::get('/assets-templates/assets/vendor/jquery.appear/jquery.appear.min.js') }}"></script>
		<script src="{{ Asset::get('/assets-templates/assets/vendor/jquery.easing/jquery.easing.min.js') }}"></script>
		<script src="{{ Asset::get('/assets-templates/assets/vendor/jquery-cookie/jquery-cookie.min.js') }}"></script>
		<script src="{{ Asset::get('/assets-templates/assets/vendor/popper/umd/popper.min.js') }}"></script>
		<script src="{{ Asset::get('/assets-templates/assets/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
		<script src="{{ Asset::get('/assets-templates/assets/vendor/common/common.min.js') }}"></script>
		<script src="{{ Asset::get('/assets-templates/assets/vendor/jquery.validation/jquery.validation.min.js') }}"></script>
		<script src="{{ Asset::get('/assets-templates/assets/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js') }}"></script>
		<script src="{{ Asset::get('/assets-templates/assets/vendor/jquery.gmap/jquery.gmap.min.js') }}"></script>
		<script src="{{ Asset::get('/assets-templates/assets/vendor/jquery.lazyload/jquery.lazyload.min.js') }}"></script>
		<script src="{{ Asset::get('/assets-templates/assets/vendor/isotope/jquery.isotope.min.js') }}"></script>
		<script src="{{ Asset::get('/assets-templates/assets/vendor/owl.carousel/owl.carousel.min.js') }}"></script>
		<script src="{{ Asset::get('/assets-templates/assets/vendor/magnific-popup/jquery.magnific-popup.min.js') }}"></script>
		<script src="{{ Asset::get('/assets-templates/assets/vendor/vide/vide.min.js') }}"></script>
		<!-- Theme Base, Components and Settings -->
		<script src="{{ Asset::get('/assets-templates/assets/js/theme.js') }}"></script>
		<!-- Current Page Vendor and Views -->
		<script src="{{ Asset::get('/assets-templates/assets/vendor/rs-plugin/js/jquery.themepunch.tools.min.js') }}"></script>
		<script src="{{ Asset::get('/assets-templates/assets/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js') }}"></script>
		<script src="{{ Asset::get('/assets-templates/assets/vendor/circle-flip-slideshow/js/jquery.flipshow.min.js') }}"></script>
		<script src="{{ Asset::get('/assets-templates/assets/js/views/view.home.js') }}"></script>
		<!-- Theme Custom -->
		<script src="{{ Asset::get('/assets-templates/assets/js/custom.js') }}"></script>
		<!-- Theme Initialization Files -->
		<script src="{{ Asset::get('/assets-templates/assets/js/theme.init.js') }}"></script>
	</body>
</html>
