$('.message a').click(function(){
   $('form').animate({height: "toggle", opacity: "toggle"}, "slow");
});

$().ready(function(){
    setTimeout(function(){
        $('#GroupName').focus();
        /*Live*/
        $('.sGroup').attr("disabled",false);
        $('.uGroup').attr("disabled",false);
        $('.addMember').attr("disabled",false);
//        $('.fa-trash-o').removeClass("delGroup");
        /*End Live*/
        loadPlayerData();
    },100);
});

$('.sGroup').click(function(){
    $.ajax({
        url: "/insertData",
        dataType: "html",
        type: "POST",
        data: "GroupName="+$('#GroupName').val(),
        context: $(this),
        success: function(result){
            if(result == "OK"){
                alert('Tạo nhóm thành công !!!');
                window.location = "/";
            }else{
                alert('Yêu cầu nhập tên nhóm !!!');
                $('#GroupName').focus();
            }
        }
    });
});

$('.uGroup').click(function(){
    $.ajax({
        url: "/updateData",
        dataType: "html",
        type: "POST",
        data: "GroupName="+$('#GroupName').val()+"&ID="+$('#idEdit').val(),
        context: $(this),
        success: function(result){
            if(result == "OK"){
                alert('Cập nhật thành công !!!');
                window.location = "../../";
            }
        }
    });
});

$('.group').change(function(){
    var id = $(this).val();
    if($(this).val() == -1){
        $('.showMember').html("");
    }else{
        $.ajax({
            url: "/listMember",
            dataType: "html",
            type: "GET",
            data: "group_id="+id,
            context: $(this),
            success: function(result){
                $('.showMember').html(result);
                /*Live*/
                //$('.fa-trash-o').removeClass("delMember");
                /*End Live*/
            }
        });
    }
});
$(document).on("click", ".activeMember", function(){
    var ID = $(this).attr("rel");
    var Active = $(this).attr("data-source");
    $.ajax({
        url: "/activeMember",
        dataType: "html",
        type: "POST",
        data: "ID="+ID+"&Active="+Active,
        context: $(this),
        success: function(result){
            if(result == "OK"){
                $('.group').change();
            }
        }
    });
});

$(document).on("click", ".onClass", function(){
    var ID = $(this).attr("rel");
    var Status = $(this).attr("data-source");
    var Class = $(this).attr("data-class");
    $.ajax({
        url: "/onClass",
        dataType: "html",
        type: "POST",
        data: "ID="+ID+"&Status="+Status+"&Class="+Class,
        context: $(this),
        success: function(result){
            if(result == "OK"){
                loadPlayerData();
            }
        }
    });
});

$(document).on("click", ".fa-undo", function(){
    var rel = $(this).attr("rel");
    $(this).addClass("fa-spin");
    if (!confirm('Bạn muốn Reset?\nLưu ý: Toàn bộ trạng thái các đội được Reset :(')) {
        return false;
    }else{
    $.ajax({
            url: "/resetActive",
            dataType: "html",
            type: "POST",
            data: "Class="+rel,
            context: $(this),
            beforeSend: function () {
                
            },
            success: function(result){
                if(result == "OK"){
                    if(rel != ""){
                        loadPlayerData();
                    }else{
                        $('.group').change();
                        $(this).removeClass("fa-spin");
                    }
                }
            }
        });
    }
});

$('.addMember').click(function(){
    $(this).addClass("disabled");
    var name = $('#Name').val();
    var gid = $('.group').val();
    if(name == ""){
        alert('Nhập vào tên Thành Viên !!!');
        $('#Name').focus();
        $(this).removeClass("disabled");
    }else if(gid == -1){
        alert('Chọn nhóm để thêm Thành Viên !!!');
        $('.group').focus();
        $(this).removeClass("disabled");
    }else{
        $.ajax({
            url: "/addMember",
            dataType: "html",
            type: "POST",
            data: $('#frmCreateMember').serialize(),
            context: $(this),
            success: function(result){
                if(result == "OK"){
                    $('.group').change();
                    $('#Name').val("");
                    $('#Phone').val("");
                }
                $(this).removeClass("disabled");
            }
        });
    }
});

$('.goPlay').click(function(){
    window.location = "/play";
});

$('.goRandom').click(function(){
    window.location = "/random";
});

$('#Number').keypress(function(e){
    if(e.which == 13) {
        $('.sCreate').click();
    }
});

$('#GroupName').keypress(function(e){
    if(e.which == 13) {
        $('.sGroup').click();
    }
});

$('.sCreate').click(function(){
    $.ajax({
        url: "/screate",
        dataType: "html",
        type: "POST",
        data: "Number="+$('#Number').val(),
        context: $(this),
        success: function(result){
            if(result == "False"){
                $('#Number').focus();
            }else{
                $('#showResult').html(result);
                $('#Number').attr("disabled",true);
                $(this).attr("disabled",true);
                $('.sPlay').removeClass("hidden");
                $('.sReset').removeClass("hidden");
            }
        }
    });
});

$('.sReset').click(function(){
    location.reload();
});

$('.sPlay').click(function(){
    $.ajax({
        url: "/splay",
        dataType: "html",
        type: "POST",
        data: $('#frmCreatePlay').serialize(),
        context: $(this),
        success: function(result){
            window.location = "/iplayer/"+result;
        }
    });
});

$('.sPrint').click(function(){
    $('.prt').remove();
    $('.showClass').remove();
    window.print();
});

$('#Number').keypress(function(e){
    if(e.which == 13) {
        $('.sPlays').click();
    }
});

$('.chk').click(function(){
    var rel = $(this).attr("rel");
    //alert('Bạn đã chọn danh sách cho Class'+rel+"?\nNếu chưa vui lòng quay ra chọn danh sách trước.");
    $('#chkClass').val(rel);
    
    $.ajax({
        url: "/getCount",
        dataType: "html",
        type: "GET",
        data: "Class="+rel,
        context: $(this),
        beforeSend: function () {
            $('.chk').removeClass("active");
            $(this).addClass("active");
        },
        success: function(result){
            $('#Number').attr("placeholder","Nhập số bảng thi đấu...("+result+" người)");
        }
    });
    
});

$('.sPlays').click(function(){
    var iclass = $('#chkClass').val();
    var number = $('#Number').val();
    if(iclass == ""){
        alert("Class A | E | F | G | H | J | ?");
    }else if(number == ""){
        alert("Nhập số bảng thi đấu!!!");
        $('#Number').focus();
    }else if(parseInt(number) > 7){
        alert("Không nhập quá 7 bảng!!!");
        $('#Number').focus();
        $('#Number').select();
    }else{
        $.ajax({
            url: "/splays",
            dataType: "html",
            type: "POST",
            data: "Number="+number+"&Class="+$('#chkClass').val(),
            context: $(this),
            beforeSend: function () {
                $(this).addClass("disabled",true);
                $('.sPrint').removeClass("disabled");
                $('#Number').attr("readonly",true);
                $(this).html("<i class='fa fa-random'></i> Random");
            },
            success: function(result){
                $('#showResult').html(result);
            }
        });
    }
});


$(document).on("click", ".delGroup", function(){
    if (!confirm('Bạn muốn xóa Group?\nLưu ý: Các Thành Viên trong nhóm bị xóa :(')) {
        return false;
    }else{
        $.ajax({
            url: "/deleteGroup",
            dataType: "html",
            type: "POST",
            data: "ID=" + $(this).attr("rel"),
            context: $(this),
            success: function () {
                location.reload();
            }
        });
    }
});

$(document).on("click", ".delMember", function(){
    if (!confirm('Bạn muốn xóa Thành Viên ??')) {
        return false;
    }else{
        $.ajax({
            type: "GET",
            url: "/deleteMember",
            data: "ID=" + $(this).attr("rel"),
            beforeSend: function () {
            },
            success: function () {
                $('.group').change();
                $('#Name').val("");
                $('#Phone').val("");
            }
        });
    }
});

$(document).on("click", ".listPlayer", function(){
    window.location = "/listPlayer";
});

function loadPlayerData(){
    $.ajax({
        type: "GET",
        url: "/listPlayerData",
        data: "",
        beforeSend: function () {
        },
        success: function (result) {
            $('#showPlayer').html(result);
        }
    });
}