<?php
namespace Tests;
use Illuminate\Support\Facades\Hash;
use Illuminate\Contracts\Console\Kernel;
trait CreatesApplication
{
    public $baseUrl;
    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../bootstrap/app.php';
        $app->make(Kernel::class)->bootstrap();
        $app->loadEnvironmentFrom('.env');
        $this->baseUrl = env('APP_URL', $this->baseUrl);
        Hash::setRounds(4);
        return $app;
    }
}
