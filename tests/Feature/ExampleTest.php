<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ExampleTest extends TestCase {

    /**
     * @Description: This method test Login Return True
     * @Params:
     * @Author:
     * @Date: 2018-05-18
     * @Notes:
     * @Action:
     */
    public final function test_login_return_true() {
        $this->assertTrue(true);
    }

    /**
     * @Description: This method test Test login return True When 1
     * @Params:
     * @Author:
     * @Date: 2018-05-18
     * @Notes:
     * @Action:
     */
    public final function test_login_return_true_when_1() {
        $this->assertTrue(true);
    }

    /**
     * @Description: This method test login return True When 2
     * @Params:
     * @Author:
     * @Date: 2018-05-18
     * @Notes:
     * @Action:
     */
    public final function test_login_return_true_when_2() {
        $this->assertTrue(true);
    }

}
