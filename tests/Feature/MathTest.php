<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MathTest extends TestCase {

    public $value1;
    public $value2;

    protected function setUp() {
        $this->value1 = 2;
        $this->value2 = 3;
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public final function test_math_example() {
        $this->assertTrue($this->value1 + $this->value2 == 5);
    }

}
