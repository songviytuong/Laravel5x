<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ModuleCpanelTest extends TestCase {

    /**
     * @Description: This method test Go to Dashboard
     * @Params:
     * @Author: Lee Peace
     * @Date: 16-05-2018
     * @Notes:
     * @Action: Visit to dashboard and see text "Login Cpanel"
     */
    public final function test_example_module_cpanel() {
        $this
                ->visit('/cpanel/login')
                ->see('Login Cpanel');
    }
}
