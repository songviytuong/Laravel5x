<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Box;

class BoxTest extends TestCase {

    /**
     * A basic test example.
     *
     * @return void
     */
    public final function test_has_item_in_box() {
        $box = new Box(['cat', 'toy', 'torch']);
        $this->assertTrue($box->has('toy'));
        $this->assertFalse($box->has('ball'));
    }

}
