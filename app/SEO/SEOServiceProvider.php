<?php

namespace App\SEO;

use Illuminate\Support\ServiceProvider;

class SEOServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Register the service provider.
     */
    public function register()
    {
        $this->registerSeoBuilder();
        $this->app->alias('SEO', 'App\SEO\SEOBuilder');
    }

    /**
     * Register the Seo builder instance.
     */
    protected function registerSeoBuilder()
    {
        $this->app->singleton('SEO', function ($app) {
            return new SEOBuilder($app['url'], $app['view']);
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['SEO', 'App\SEO\SEOBuilder'];
    }
}
