<?php
namespace App\SEO;
use Illuminate\Support\Facades\Facade;
/**
 * @see \App\Seo
 */
class SEOFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'SEO';
    }
}