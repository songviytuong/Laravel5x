<?php

namespace App\Plugins;

use Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class SitePrefs
{
    public static $table = 'cms_siteprefs';

    public function __construct()
    {
    }

    /**
     * setup init.
     */
    public static function setup()
    {
        if (!Schema::hasTable(self::$table)) {
            Schema::create(self::$table, function ($table) {
                $table->char('sitepref_name');
                $table->text('sitepref_value');
                $table->timestamp('create_date')->useCurrent();
                $table->timestamp('modified_date')->useCurrent();
            });
            Cache::forget(__CLASS__);

            return;
        }

        $value = Cache::rememberForever(__CLASS__, function () {
            return self::_read();
        });

        return $value;
    }

    /**
     * _read.
     */
    public static function _read()
    {
        $result = DB::table(self::$table)->select('sitepref_name', 'sitepref_value');
        $dbr = $result->get()->toArray();
        if (is_array($dbr)) {
            $_prefs = array();
            for ($i = 0, $n = count($dbr); $i < $n; ++$i) {
                $row = $dbr[$i];
                $_prefs[$row->sitepref_name] = $row->sitepref_value;
            }

            return $_prefs;
        }
    }

    /**
     * @Function exists.
     *
     * @param string $key This is a the key
     *
     * @return bool @The key has exist
     */
    public static function exists($key)
    {
        $prefs = Cache::get(__CLASS__);

        if (is_array($prefs) && in_array($key, array_keys($prefs))) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * set.
     *
     * @param mixed $key
     * @param mixed $value
     */
    public function set($key, $value)
    {
        $inserted = [
            'sitepref_name' => $key,
            'sitepref_value' => $value,
        ];
        if (!self::exists($key)) {
            DB::table(self::$table)->insert($inserted);
        } else {
            DB::statement('UPDATE '.self::$table.' SET sitepref_value = ? where sitepref_name = ?', [$value, $key]);
        }

        Cache::forget(__CLASS__);
    }

    /**
     * get.
     *
     * @param mixed $key
     * @param mixed $dflt
     */
    public function get($key, $dflt = '')
    {
        $prefs = Cache::get(__CLASS__);
        if (isset($prefs[$key])) {
            return $prefs[$key];
        }

        return $dflt;
    }

    /**
     * remove.
     *
     * @param mixed $key
     * @param mixed $like
     */
    public function remove($key, $like = false)
    {
        DB::statement('DELETE FROM '.self::$table.' sitepref_name = ?', [$key]);
        if ($like) {
            DB::statement('DELETE FROM '.self::$table.' sitepref_name LIKE ?', [$key.'%']);
        }
        Cache::forget(__CLASS__);
    }

    /**
     * list_by_prefix.
     *
     * @param mixed $prefix
     */
    public function list_by_prefix($prefix)
    {
        if (!$prefix) {
            return;
        }

        $dbr = DB::select(DB::raw('SELECT sitepref_name FROM '.self::$table.' WHERE sitepref_name LIKE :prefix'), array(
            'prefix' => $prefix.'%',
          ));

        if (is_array($dbr) && count($dbr)) {
            return $dbr;
        }
    }
}
