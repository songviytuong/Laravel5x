<?php

namespace App\Plugins\ext;

class get_param
{
    protected $config = [];

    public function run($params = [], $key, $dflt = null)
    {
        return $this->doAction($params, $key, $dflt);
    }

    /**
     * Given an associative array, extract the value of one key, with a default.
     * If the key does not exist in the array, or it's value is empty, then the default is used.
     *
     * @param hash   $params The input associative array
     * @param string $key    The input key to search for
     * @param mixed  $dflt   The default value
     *
     * @return mixed The value of the element in the array, or the default
     */
    public static function doAction($params = [], $key, $dflt = null)
    {
        if (isset($params[$key])) {
            $tmp = $params[$key];
            if (is_string($tmp)) {
                $tmp = trim($tmp);
            }

            return $tmp;
        }

        return $dflt;
    }
}
