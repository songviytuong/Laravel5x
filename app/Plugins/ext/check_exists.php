<?php

namespace App\Plugins\ext;

use Illuminate\Support\Facades\DB;

class check_exists
{
    protected $config = [];

    public function run(string $fieldid, string $fieldval, string $table)
    {
        return $this->doAction($fieldid, $fieldval, $table);
    }

    /**
     * doAction.
     *
     * @param string $fieldid
     * @param string $fieldval
     * @param string $table
     *
     * @see Plugin::check_exists('alias', 'home', 'routes')
     */
    public static function doAction(string $fieldid, string $fieldval, string $table)
    {
        $obj = DB::selectOne(DB::raw('select exists(select 1 from '.$table.' where `'.$fieldid.'`=:fieldval) as `exists`'), [
            'fieldval' => $fieldval,
        ]);
        if ($obj->exists == 1) {
            return true;
        } else {
            return false;
        }
    }
}
