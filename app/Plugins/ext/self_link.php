<?php

namespace App\Plugins\ext;

class self_link
{
    protected $config = [];

    public function run($params = array())
    {
        echo $this->defineHTML($params);
    }

    public static function defineHTML($params)
    {
        $url = isset($params['url']) ? $params['url'] : '';
        $title = isset($params['title']) ? $params['title'] : '';
        $text = isset($params['text']) ? $params['text'] : $title;
        $href = isset($params['href']) ? $params['href'] : '';
        if ($href) {
            $result = $href;
        } else {
            $result = '<a href="'.$url.'"';
            $result .= ' title="'.$title.'" ';
            if (isset($params['target'])) {
                $result .= ' target="'.$params['target'].'"';
            }
            if (isset($params['id'])) {
                $result .= ' id="'.$params['id'].'"';
            }
            if (isset($params['class'])) {
                $result .= ' class="'.$params['class'].'"';
            }
            if (isset($params['tabindex'])) {
                $result .= ' tabindex="'.$params['tabindex'].'"';
            }
            if (isset($params['more'])) {
                $result .= ' '.$params['more'];
            }
            $result .= '>';
            $result .= $text;
            $result .= '</a>';
        }
        $result = trim($result);

        return $result;
    }
}
