<?php

namespace App\Plugins\ext;

class copy_right
{
    protected $config = [];

    public function run(string $start)
    {
        echo $this->defineHTML($start);
    }

    public static function defineHTML($start = '2004')
    {
        if (date('Y') <= $start) {
            return date('Y');
        } else {
            return $start.'-'.date('Y');
        }
    }
}
