<?php

namespace App\Plugins\ext;

class cms_get_youtubeid
{
    protected $config = [];

    public function run($params)
    {
        echo $this->getYoutubeId($params);
    }

    /**
     * getYoutubeId.
     *
     * @param string $Youtube_Url
     */
    public static function getYoutubeId($Youtube_Url)
    {
        /**
         * Pattern matches
         * http://youtu.be/ID
         * http://www.youtube.com/embed/ID
         * http://www.youtube.com/watch?v=ID
         * http://www.youtube.com/?v=ID
         * http://www.youtube.com/v/ID
         * http://www.youtube.com/e/ID
         * http://www.youtube.com/user/username#p/u/11/ID
         * http://www.youtube.com/leogopal#p/c/playlistID/0/ID
         * http://www.youtube.com/watch?feature=player_embedded&v=ID
         * http://www.youtube.com/?feature=player_embedded&v=ID.
         */
        $YoutubeId = '';
        if (!empty($Youtube_Url)) {
            if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $Youtube_Url, $match)) {
                $YoutubeId = $match[1];
            }
        }

        return $YoutubeId;
    }
}
