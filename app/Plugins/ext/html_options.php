<?php

namespace App\Plugins\ext;

use App\Facades\Plugin;

class html_options
{
    protected $config = [];

    public function run($params = [])
    {
        echo $this->doAction($params);
    }

    /**
     * doAction.
     *
     * @param array $tmp = array('label'=>'myoptgroup','value'=>array( array('label'=>'opt1','value'=>'value1'), array('label'=>'opt2','value'=>'value2') ) );
     *
     * @see Plugin::html_options(['options'=> $tmp, 'selected'=> 'value1,value2'])
     */
    public static function doAction($params = [])
    {
        $options = null;
        if (!isset($params['options'])) {
            if (isset($params['value']) && isset($params['label'])) {
                $opt = array();
                $opt['label'] = $params['label'];
                $opt['value'] = $params['value'];
                if (isset($params['title'])) {
                    $opt['title'] = $params['title'];
                }
                if (isset($params['class'])) {
                    $opt['class'] = $params['class'];
                }
                $options = $opt;
            } else {
                return;
            }
        } else {
            $options = $params['options'];
        }

        $out = null;
        if (is_array($options) && count($options)) {
            $selected = null;
            if (isset($params['selected'])) {
                $selected = $params['selected'];
                if (!is_array($selected)) {
                    $selected = explode(',', $selected);
                }
            }
            $out = Plugin::create_option($params['options'], $selected);
        }

        return $out;
    }
}
