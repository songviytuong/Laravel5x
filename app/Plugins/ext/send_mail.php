<?php

namespace App\Plugins\ext;

use Illuminate\Support\Facades\Mail;

class send_mail
{
    protected $config = [];

    /**
     * run.
     *
     * @param array $config_email
     *
     * @return json_encode
     */
    public function run($config_email = [])
    {
        echo json_encode($this->doAction($config_email));
    }

    /**
     * doAction.
     *
     * @param array $config_email
     */
    public static function doAction($config_email = [])
    {
        $errors = '';
        if (empty($config_email['from']) || empty($config_email['to'])) {
            return response()->json(['msg' => 'Required From-To Email']);
        }

        try {
            $from = isset($config_email['from']) ? $config_email['from'] : '';
            $from_name = isset($config_email['from_name']) ? $config_email['from_name'] : '';
            $to = isset($config_email['to']) ? $config_email['to'] : '';
            $subject = isset($config_email['subject']) ? $config_email['subject'] : '';
            $msg = isset($config_email['msg']) ? $config_email['msg'] : '';
            $template = isset($config_email['template']) ? $config_email['template'] : '';
            $cc = isset($config_email['cc']) ? $config_email['cc'] : null;
            $bcc = isset($config_email['bcc']) ? $config_email['bcc'] : null;
            $pathToFile = isset($config_email['pathToFile']) ? $config_email['pathToFile'] : null;
            Mail::send($template, $msg, function ($email) use ($from, $from_name, $to, $subject, $cc, $bcc, $pathToFile) {
                if ($from_name != '') {
                    $email->from($from, $from_name);
                } else {
                    $email->from($from, $from);
                }
                $email->to($to);
                if ($cc !== null) {
                    $email->cc($cc);
                }
                if ($bcc !== null) {
                    $email->bcc($bcc);
                }
                if ($pathToFile !== null) {
                    $email->attach($pathToFile);
                }
                $email->subject($subject);
            });
        } catch (\Exception $e) {
            $errors = $e->getMessage();
        }

        if ($errors == '') {
            return response([
                'success' => true,
                'msg' => 'Transaction has been updated successfully',
            ], 200, []);
        } else {
            return response()->json(['msg' => $errors]);
        }
    }
}
