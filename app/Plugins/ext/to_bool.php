<?php

namespace App\Plugins\ext;

class to_bool
{
    protected $config = [];

    public function run($in, $strict = false)
    {
        return $this->doAction($in, $strict);
    }

    /**
     * Given a string input that theoretically represents a boolean value
     * return either true or false.
     *
     * @param mixed $in     input value
     * @param bool  $strict whether strict testing should be used
     *
     * @return bool
     */
    public static function doAction($in, $strict = false)
    {
        if (is_bool($in) && $in === true) {
            return true;
        }
        if (is_bool($in) && $in === false) {
            return false;
        }
        $in = strtolower($in);
        if (in_array($in, array('1', 'y', 'yes', 'true', 't', 'on'))) {
            return true;
        }
        if (in_array($in, array('0', 'n', 'no', 'false', 'f', 'off'))) {
            return false;
        }
        if ($strict) {
            return null;
        }

        return $in ? true : false;
    }
}
