<?php

namespace App\Plugins\ext;

class get_protocol
{
    protected $config = [];

    public function run()
    {
        return $this->doAction();
    }

    public static function doAction()
    {
        if (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
            $protocol = 'https://'.$_SERVER['HTTP_HOST'];
        } else {
            $protocol = 'http://'.$_SERVER['HTTP_HOST'];
        }

        return $protocol;
    }
}
