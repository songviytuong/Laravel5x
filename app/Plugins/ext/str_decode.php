<?php

namespace App\Plugins\ext;

use App\Facades\Plugin;

class str_decode
{
    protected $config = [];

    public function run($str, $n = 3, $max_n = 5, $str_s = null, $str_e = null)
    {
        return $this->doAction($str, $n, $max_n, $str_s, $str_e);
    }

    public static function doAction($str, $n = 3, $max_n = 5, $str_s = 'XXXXX', $str_e = 'YYYYY')
    {
        $str = Plugin::num_decode($str, $n);
        $str = substr($str, $n + $max_n, strlen($str) - $n - $max_n);
        // if (strpos($str, $str_s) !== false && strpos($str, $str_e) !== false) {
        //     $str = str_replace(array($str_s, $str_e), array('', ''), $str);
        return $str;
        // } else {
        //     return '';
        // }
    }
}
