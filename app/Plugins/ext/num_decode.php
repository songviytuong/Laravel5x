<?php

namespace App\Plugins\ext;

class num_decode
{
    protected $config = [];

    public function run($item, $n = 3)
    {
        return $this->doAction($item, $n);
    }

    public static function doAction($item, $n)
    {
        for ($i = 0; $i < $n; ++$i) {
            $item = base64_decode($item);
        }

        return $item;
    }
}
