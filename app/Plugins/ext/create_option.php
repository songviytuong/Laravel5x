<?php

namespace App\Plugins\ext;

use App\Facades\Plugin;

class create_option
{
    protected $config = [];

    public function run($data = [], $selected = null)
    {
        return $this->doAction($data, $selected);
    }

    /**
     * A simple recursive utility function to create an option, or a set of options for a select list or multiselect list.
     *
     * Accepts an associative 'option' array with at least two populated keys: 'label' and 'value'.
     * If 'value' is not an array then a single '<option>' is created.  However, if 'value' is itself
     * an array then an 'optgroup' will be created with it's values.
     *
     * i.e: $tmp = array('label'=>'myoptgroup','value'=>array( array('label'=>'opt1','value'=>'value1'), array('label'=>'opt2','value'=>'value2') ) );
     *
     * The 'option' array can have additional keys for 'title' and 'class'
     *
     * i.e: $tmp = array('label'=>'opt1','value'=>'value1','title'=>'My title','class'=>'foo');
     *
     * @param array           $data     The option data
     * @param string[]|string $selected The selected elements
     *
     * @return string the generated <option> element(s)
     *
     * @see self::create_option()
     */
    public static function doAction($data = [], $selected = null)
    {
        $out = '';
        if (!is_array($data)) {
            return;
        }

        if (isset($data['label']) && isset($data['value'])) {
            if (!is_array($data['value'])) {
                $out .= '<option value="'.trim($data['value']).'"';
                if ($selected == $data['value'] || is_array($selected) && in_array($data['value'], $selected)) {
                    $out .= ' selected="selected"';
                }
                if (isset($data['title']) && $data['title']) {
                    $out .= ' title="'.trim($data['title']).'"';
                }
                if (isset($data['class']) && $data['class']) {
                    $out .= ' class="'.trim($data['class']).'"';
                }
                $out .= '>'.$data['label'].'</option>';
            } else {
                $out .= '<optgroup label="'.$data['label'].'">';
                foreach ($data['value'] as $one) {
                    $out .= Plugin::create_option($one, $selected);
                }
                $out .= '</optgroup>';
            }
        } else {
            foreach ($data as $rec) {
                $out .= Plugin::create_option($rec, $selected);
            }
        }

        return $out;
    }
}
