<?php

namespace App\Plugins\ext;

use Illuminate\Support\Facades\Config;

class get_languages
{
    protected $config = [];

    public function run($params = array())
    {
        echo $this->defineHTML($params);
    }

    public function defineHTML($params)
    {
        $result = '';
        switch (isset($params['type'])) {
            case 'list':
                $result = $this->getLanguagesList($params);
                break;
            default:
                break;
        }

        return $result;
    }

    /**
     * Params: Array('label','').
     */
    public function getLanguagesList($params)
    {
        $label = isset($params['label']) ? $params['label'].': ' : '';

        $html = [];
        $locales = (config::has('app.locales') ? config('app.locales') : []);
        foreach ($locales as $alias => $locale) {
            $html[] = "<a href='/".$alias."/set-language/'>".$locale[1].'</a>';
        }
        echo $label.implode('//', $html);
    }
}
