<?php

namespace App\Plugins\ext;

class file_format
{
    protected $config = [];

    public function run($inputFileName, $type = null, $extension = null)
    {
        return $this->doAction($inputFileName, $type, $extension);
    }

    public static function doAction($inputFileName, $type = 'image', $extension = null)
    {
        $mimetypes = [];
        switch ($type) {
            case 'image':
                $mimetypes = ['image/jpeg', 'image/jpg', 'image/png', 'image/gif'];
                break;
            case 'file':
                $mimetypes = ['image/jpeg', 'image/jpg', 'image/png', 'image/gif'];
                break;
            default:
                $mimetypes = ['image/jpeg', 'image/jpg', 'image/png', 'image/gif'];
                break;
        }
        if (!empty($inputFileName) && !empty($mimetypes) && in_array($extension, $mimetypes)) {
            return true;
        }

        return false;
    }
}
