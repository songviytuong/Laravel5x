<?php

namespace App\Plugins\ext;

class cms_get_youtube_iframe
{
    protected $config = [];

    public function run($params = array())
    {
        echo $this->getYoutubeIframe($params);
    }

    /**
     * getYoutubeIframe.
     *
     * @param array $params['Youtube_url'=>'url', 'Width' => 200, 'Height' => 200]
     */
    public static function getYoutubeIframe($params)
    {
        $iframe = '';
        $_params = [];
        if (!empty($params['Youtube_Url'])) {
            if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $params['Youtube_Url'], $match)) {
                $YoutubeId = $match[1];
            }
            $_params[] = 'src="https://www.youtube.com/embed/'.$YoutubeId.'"';
        }

        if (!empty($params['Width'])) {
            $_params[] = 'width="'.$params['Width'].'"';
        }

        if (!empty($params['Height'])) {
            $_params[] = 'height="'.$params['Height'].'"';
        }

        $iframe = '<iframe '.implode(' ', $_params).' frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';

        return $iframe;
    }
}
