<?php

namespace App\Plugins\ext;

use DateTime;

class time_elapsed_2
{
    protected $config = [];

    public function run($start, $end = null, $limit = null, $filter = true, $suffix = 'ago', $format = 'Y-m-d', $separator = ' ', $minimum = 1)
    {
        echo $this->doAction($start, $end, $limit, $filter, $suffix, $format, $separator, $minimum);
    }

    /**
     * doAction
     *
     * @param  mixed $start
     * @param  mixed $end
     * @param  mixed $limit
     * @param  mixed $filter
     * @param  mixed $suffix
     * @param  mixed $format
     * @param  mixed $separator
     * @param  mixed $minimum
     *
     * @return void
     * elapsedTimeString('2010-04-26'); 
     * elapsedTimeString('1920-01-01', '2500-02-24', null, false);
     * elapsedTimeString('2010-05-26', '2012-02-24', null, ['month', 'year']);
     * elapsedTimeString('2010-05-26', '2012-02-24', '1 year');
     */
    public static function doAction($start, $end = null, $limit = null, $filter = true, $suffix = 'ago', $format = 'Y-m-d', $separator = ' ', $minimum = 1)
    {
        $dates = (object) array(
            'start' => new DateTime($start ? : 'now'),
            'end' => new DateTime($end ? : 'now'),
            'intervals' => array('y' => 'year', 'm' => 'month', 'd' => 'day', 'h' => 'hour', 'i' => 'minute', 's' => 'second'),
            'periods' => array()
        );
        $elapsed = (object) array(
            'interval' => $dates->start->diff($dates->end),
            'unknown' => 'unknown'
        );
        if ($elapsed->interval->invert === 1) {
            return trim('0 seconds ' . $suffix);
        }
        if (false === empty($limit)) {
            $dates->limit = new DateTime($limit);
            if (date_create()->add($elapsed->interval) > $dates->limit) {
                return $dates->start->format($format) ? : $elapsed->unknown;
            }
        }
        if (true === is_array($filter)) {
            $dates->intervals = array_intersect($dates->intervals, $filter);
            $filter = false;
        }
        foreach ($dates->intervals as $period => $name) {
            $value = $elapsed->interval->$period;
            if ($value >= $minimum) {
                $dates->periods[] = vsprintf('%1$s %2$s%3$s', array($value, $name, ($value !== 1 ? 's' : '')));
                if (true === $filter) {
                    break;
                }
            }
        }
        if (false === empty($dates->periods)) {
            return trim(vsprintf('%1$s %2$s', array(implode($separator, $dates->periods), $suffix)));
        }
    
        return $dates->start->format($format) ? : $elapsed->unknown;
    }
}
