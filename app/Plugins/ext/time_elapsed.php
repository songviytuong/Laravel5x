<?php

namespace App\Plugins\ext;

use Carbon\Carbon;

class time_elapsed
{
    protected $config = [];

    public function run($datetime = '', $full = false)
    {
        echo $this->doAction($datetime, $full);
    }

    /**
     * doAction
     * 1. Plugin::time_elapsed("2018-10-20 17:25:43");
     * Return: 4 weeks ago
     * 2. Plugin::time_elapsed("2018-10-20 17:25:43", true);
     * Return: 4 weeks, 1 day, 22 hours, 26 minutes, 32 seconds ago
     * 
     * @param  mixed $datetime
     * @param  mixed $full
     *
     * @return void
     */
    public static function doAction($datetime = '', $full = false)
    {
        if($datetime){
            $now = new Carbon();
            $ago = new Carbon($datetime);
            $diff = $now->diff($ago);
    
            $diff->w = floor($diff->d / 7);
            $diff->d -= $diff->w * 7;
    
            $string = array(
                'y' => 'year',
                'm' => 'month',
                'w' => 'week',
                'd' => 'day',
                'h' => 'hour',
                'i' => 'minute',
                's' => 'second',
            );
            foreach ($string as $k => &$v) {
                if ($diff->$k) {
                    $v = $diff->$k.' '.$v.($diff->$k > 1 ? 's' : '');
                } else {
                    unset($string[$k]);
                }
            }
    
            if (!$full) {
                $string = array_slice($string, 0, 1);
            }
    
            return $string ? implode(', ', $string).' ago' : 'just now';
        }
        return "--missing DateTime--";
    }
}
