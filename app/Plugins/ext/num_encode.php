<?php

namespace App\Plugins\ext;

class num_encode
{
    protected $config = [];

    public function run($item, $n = 3)
    {
        return $this->doAction($item, $n);
    }

    public static function doAction($item, $n)
    {
        for ($i = 0; $i < $n; ++$i) {
            $item = base64_encode($item);
        }
        //return $item;
        return str_replace('=', '', $item);
    }
}
