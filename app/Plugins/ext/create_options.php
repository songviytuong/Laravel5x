<?php

namespace App\Plugins\ext;

use App\Facades\Plugin;

class create_options
{
    protected $config = [];

    public function run($options = [], $selected = null)
    {
        return $this->doAction($options, $selected);
    }

    public static function doAction($options = [], $selected = null)
    {
        if (!is_array($options) || count($options) == 0) {
            return;
        }

        $out = '';
        foreach ($options as $key => $value) {
            if (!is_array($value)) {
                $out .= Plugin::create_option(array('label' => $value, 'value' => $key), $selected);
            } else {
                $out .= Plugin::create_option($value, $selected);
            }
        }

        return $out;
    }
}
