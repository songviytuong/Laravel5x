<?php

namespace App\Plugins\ext;

use App\Facades\Plugin;

class check_exists_and_keyunique
{
    protected $config = [];

    public function run(string $fieldid, string $fieldval, string $table, $hash_type = 'openssl_random', $length = 20)
    {
        return $this->doAction($fieldid, $fieldval, $table, $hash_type, $length);
    }

    /**
     * @method: check_exists_and_keyunique.
     *
     * @param string $fieldid
     * @param string $fieldval
     * @param string $table
     * @param string $hash_type
     * @param string $length
     *
     * @see Plugin::check_exists_and_keyunique('alias', 'home', 'routes')
     * @see Plugin::check_exists_and_keyunique('alias', 'home', 'routes', 'guid')
     * @see Plugin::check_exists_and_keyunique('alias', 'home', 'routes', 'random | openssl_random', 20)
     *
     * @return random string unique or null
     */
    public static function doAction(string $fieldid, string $fieldval, string $table, $hash_type = 'openssl_random', $length = 20)
    {
        $token = '';
        if (Plugin::check_exists($fieldid, $fieldval, $table)) {
            switch ($hash_type) {
                case 'guid':
                    $token = Plugin::create_guid();
                    break;
                case 'random':
                    $token = Plugin::str_random($length);
                    break;
                case 'openssl_random':
                default:
                    $token = bin2hex(openssl_random_pseudo_bytes($length));
                    break;
            }
        }

        return $token;
    }
}
