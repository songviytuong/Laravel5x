<?php

namespace App\Plugins\ext;

use Illuminate\Support\Facades\DB;
use App\Facades\Plugin;

class check_exists_and_getdata
{
    protected $config = [];

    public function run(string $fieldid, string $fieldval, string $table, $fullrow = false, $selects = [])
    {
        return $this->doAction($fieldid, $fieldval, $table, $fullrow, $selects);
    }

    /**
     * doAction.
     *
     * @param string $fieldid
     * @param string $fieldval
     * @param string $table
     * @param bool   $fullrow
     * @param array  $selects
     *
     * @see Plugin::check_exists_and_getdata('alias', 'home', 'routes', true)
     * @see Plugin::check_exists_and_getdata('alias', 'home', 'routes', false, ['id'])
     * @see Plugin::check_exists_and_getdata('alias', 'home', 'routes', false, ['id as ID', 'name as Name']))
     */
    public static function doAction(string $fieldid, string $fieldval, string $table, $fullrow = false, $selects = [])
    {
        $result = $data = [];
        if (Plugin::check_exists($fieldid, $fieldval, $table)) {
            if ($fullrow) {
                $row = DB::table($table)->where($fieldid, $fieldval)->get()->first();
                $data = $row;
            } else {
                if ($selects) {
                    $row = DB::table($table)->select($selects)->where($fieldid, $fieldval)->get()->first();
                    foreach ($selects as $select) {
                        if (strpos($select, 'as') !== false) {
                            $after = trim(explode('as', $select, 2)[1]);
                            $data[$after] = $row->$after;
                        } else {
                            $data[$select] = $row->$select;
                        }
                    }
                }
            }
            $result['status'] = true;
            if ($fullrow || $selects) {
                $result['data'] = $data;
            }
        } else {
            $result['status'] = false;
        }

        return $result;
    }
}
