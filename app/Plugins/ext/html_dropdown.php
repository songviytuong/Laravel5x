<?php

namespace App\Plugins\ext;

use App\Facades\Plugin;

class html_dropdown
{
    protected $config = [];

    public function run($name = '', $list_options = [], $selected = '', $params = [])
    {
        echo $this->doAction($name, $list_options, $selected, $params);
    }

    /**
     * doAction.
     *
     * @param array $tmp = array(array('label'=>'opt1','value'=>'value1'), array('label'=>'opt2','value'=>'value2'));
     *
     * @see Plugin::html_dropdown('chk',$tmp,'value2',['multiple'=>true, 'size'=> 4,'id'=>'aaaa', 'class'=> 'chkClass'])
     */
    public static function doAction($name, $list_options, $selected, $params = [])
    {
        if ($name == '') {
            return;
        }
        if (!is_array($list_options) || count($list_options) == 0) {
            return;
        }

        $options = Plugin::create_options($list_options, $selected);
        $elem_id = $name;

        if (in_array('multiple', $params) && !ends_with($name, '[]')) {
            // auto adjust dropdown name if it allows multiple selections.
            $name .= '[]';
        }

        $out = "<select name=\"{$name}\"";
        foreach ($params as $key => $value) {
            switch ($key) {
                case 'id':
                    $out .= " id=\"{$value}\"";
                    $elem_id = $value;
                    break;

                case 'multiple':
                    $out .= ' multiple="multiple"';
                    break;

                case 'class':
                    $out .= " class=\"{$value}\"";
                    break;

                case 'title':
                    $out .= " title=\"{$value}\"";
                    break;

                case 'size':
                    if (!in_array('multiple', $params)) {
                        $value = 1;
                    } else {
                        $value = (int) $value;
                    }
                    $out .= " size=\"{$value}\"";
                    break;
            }
        }
        $out .= '>'.$options."</select>\n";

        return $out;
    }
}
