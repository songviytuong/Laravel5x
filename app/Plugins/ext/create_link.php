<?php

namespace App\Plugins\ext;

use Plugin;

class create_link
{
    protected $config = [];

    /**
     * run
     * Plugin::create_link(['contents' => 'Hello', 'class' => 'delete_ajax', 'target' => '_blank', 'title' => 'AirBridal 2', 'onlyhref' => false]);.
     */
    public function run($params = [])
    {
        echo $this->doAction($params);
    }

    /**
     * doAction.
     *
     * @param mixed $params
     */
    public static function doAction($params = [])
    {
        $action = isset($params['action']) ? $params['action'] : '';
        $class = isset($params['class']) ? $params['class'] : '';
        $contents = isset($params['contents']) ? $params['contents'] : '';
        $warn_message = isset($params['warn_message']) ? $params['warn_message'] : '';
        $onlyhref = isset($params['onlyhref']) ? $params['onlyhref'] : false;
        $target_url = isset($params['target_url']) ? $params['target_url'] : '';
        $back_url = isset($params['back_url']) ? $params['back_url'] : '';
        $is_route = isset($params['is_route']) ? $params['is_route'] : false;

        $attributes['class'] = isset($params['class']) ? $params['class'] : '';
        $attributes['title'] = isset($params['title']) ? $params['title'] : '';

        // create url....
        $text = Plugin::create_url($params);

        if (!$onlyhref) {
            $beginning = '<a';
            foreach ($attributes as $key => $attr) {
                if ($attr != '') {
                    $beginning .= ' '.$key.'="'.$attr.'"';
                }
            }
            $beginning .= ' href="';
            $text = $beginning.$text.'"';
            if ($warn_message != '') {
                $text .= ' onclick="return confirm(\''.$warn_message.'\');"';
            }
            $text .= '>'.$contents.'</a>';
        }

        return $text;
    }
}
