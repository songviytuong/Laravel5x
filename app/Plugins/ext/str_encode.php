<?php

namespace App\Plugins\ext;

use App\Facades\Plugin;

class str_encode
{
    protected $config = [];

    public function run($str, $n = 3, $max_n = 5, $str_s = null, $str_e = null)
    {
        return $this->doAction($str, $n, $max_n, $str_s, $str_e);
    }

    public static function doAction($str, $n = 3, $max_n = 5, $str_s = 'XXXXX', $str_e = 'YYYYY')
    {
        $str_rand1 = strtoupper(Plugin::str_random($n + $max_n));
        $str_ = $str_rand1.$str.$str_s.$str_e;
        $str_ = Plugin::num_encode($str_, $n);

        return $str_;
    }
}
