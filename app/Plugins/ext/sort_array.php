<?php

namespace App\Plugins\ext;

class sort_array
{
    protected $config = [];

    public function run($array, $index = 0, $order = 'asc', $natsort = false, $case_sensitive = false)
    {
        return $this->doAction($array, $index, $order, $natsort, $case_sensitive);
    }

    /**
     * doAction.
     *
     * @param mixed $array
     * @param mixed $index
     * @param mixed $order
     * @param mixed $natsort
     * @param mixed $case_sensitive
     *
     * @see Plugin::sort_array($arr, 0, 'desc', true);
     */
    public static function doAction($array = [], $index = 0, $order = 'asc', $natsort = false, $case_sensitive = false)
    {
        $res = [];
        if (is_array($array) && count($array) > 0) {
            foreach (array_keys($array) as $key) {
                $temp[$key] = $array[$key][$index];
            }
            if (!$natsort) {
                if ($order == 'asc') {
                    asort($temp);
                } else {
                    arsort($temp);
                }
            } else {
                if ($case_sensitive === true) {
                    natsort($temp);
                } else {
                    natcasesort($temp);
                }
                if ($order != 'asc') {
                    $temp = array_reverse($temp, true);
                }
            }
            foreach (array_keys($temp) as $key) {
                if (is_numeric($key)) {
                    $res[] = $array[$key];
                } else {
                    $res[$key] = $array[$key];
                }
            }
        }

        return $res;
    }
}
