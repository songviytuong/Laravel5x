<?php

namespace App\Plugins\ext;

use Illuminate\Support\Facades\URL;

class create_url
{
    protected $config = [];

    public function run($params = [])
    {
        echo $this->doAction($params);
    }

    public static function doAction($params = [])
    {
        $text = '';

        $action = isset($params['action']) ? $params['action'] : 'Business';
        $contents = isset($params['contents']) ? $params['contents'] : '';
        $onlyhref = isset($params['onlyhref']) ? $params['onlyhref'] : false;
        $target_url = isset($params['target_url']) ? $params['target_url'] : '';
        $back_url = isset($params['back_url']) ? $params['back_url'] : '';
        $is_route = isset($params['is_route']) ? $params['is_route'] : false;

        if ($target_url != '') {
            $text = ($is_route) ? route($target_url) : $target_url;
        } else {
            $text = URL::to('/');
        }

        $text .= '/?action='.$action;

        foreach ($params as $key => $value) {
            if (!$onlyhref) {
                if (in_array($key, ['back_url'])) {
                    $text .= '&amp;'.$key.'='.rawurlencode($value);
                } else {
                    continue;
                }
            }
        }

        if ($back_url != '') {
            $text .= '&amp;'.'back_url='.(($is_route) ? route($back_url) : $back_url);
        }

        return $text;
    }
}
