<?php

namespace App\Plugins\ext;

class html_entities
{
    protected $config = [];

    public function run($val, $param = ENT_QUOTES, $charset = 'UTF-8', $convert_single_quotes = false)
    {
        echo $this->doAction($val, $param, $charset, $convert_single_quotes);
    }

    public static function doAction($val, $param = ENT_QUOTES, $charset = 'UTF-8', $convert_single_quotes = false)
    {
        if ($val == '') {
            return '';
        }

        $val = str_replace('&#032;', ' ', $val);
        $val = str_replace('&', '&amp;', $val);
        $val = str_replace('<!--', '&#60;&#33;--', $val);
        $val = str_replace('-->', '--&#62;', $val);
        $val = str_ireplace('<script', '&#60;script', $val);
        $val = str_replace('>', '&gt;', $val);
        $val = str_replace('<', '&lt;', $val);
        $val = str_replace('"', '&quot;', $val);
        $val = preg_replace('/\$/', '&#036;', $val);
        $val = str_replace('!', '&#33;', $val);
        $val = str_replace("'", '&#39;', $val);

        if ($convert_single_quotes) {
            $val = str_replace("\\'", '&apos;', $val);
            $val = str_replace("'", '&apos;', $val);
        }

        return $val;
    }
}
