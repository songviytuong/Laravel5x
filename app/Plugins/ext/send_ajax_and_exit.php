<?php

namespace App\Plugins\ext;

class send_ajax_and_exit
{
    protected $config = [];

    public function run($output = [], $content_type = null, $filename = null)
    {
        $this->doAction($output, $content_type, $filename);
    }

    public static function doAction($output = [], $content_type = 'application/json', $filename = 'report.txt')
    {
        $handlers = ob_list_handlers();
        for ($cnt = 0; $cnt < sizeof($handlers); ++$cnt) {
            ob_end_clean();
        }

        header('Pragma: public');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Cache-Control: private', false);
        if (in_array($content_type, ['application/json', 'text/plain'])) {
            header('Content-Type: '.$content_type);
        }
        if ($filename) {
            header("Content-Disposition: attachment; filename=\"$filename\"");
            header('Content-Transfer-Encoding: binary');
        }

        $result = json_encode($output);
        echo $result;
        exit();
    }
}
