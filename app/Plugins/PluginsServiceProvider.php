<?php

namespace App\Plugins;

use Illuminate\Support\ServiceProvider;
use App\Facades\Plugin;
use File;

class PluginsServiceProvider extends ServiceProvider {

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return null
     */
    public function register() {
        $this->registerServices();
        $this->configureSapling();
        foreach (File::allFiles(app_path() . '/Plugins/ext') as $partial) {
            $filename = explode(".", $partial->getRelativePathname())[0];
            Plugin::register($filename, 'App\\Plugins\\ext\\' . $filename);
        }
        //Plugin::register('cms_selflink', 'App\Plugins\ext\cms_selflink');
    }

    /**
     * Register the package services.
     *
     * @return null
     */
    protected function registerServices() {
        $this->app->singleton('plugins', function($app) {
            $blade = $app['view']->getEngineResolver()->resolve('blade')->getCompiler();
            return new Plugins($app, $blade);
        });
    }

    /**
     * Configure Sapling
     *
     * Configures Sapling (Twig) extensions if the Sapling package
     * is found to be installed.
     *
     * @return void
     */
    protected function configureSapling() {
        if ($this->app['config']->has('sapling')) {
            $this->app['config']->push(
                    'sapling.extensions', 'App\Twig\Extensions\Plugin'
            );
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides() {
        return ['plugins'];
    }

}
