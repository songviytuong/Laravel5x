<?php

namespace App\Listeners;

use App\Events\UserLogon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class LastLoginUpdated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserLogon  $event
     * @return void
     */
    public function handle(UserLogon $event)
    {
        $logon = [
            'msg' => 'Lastest Login',
            'timer' => \Carbon\Carbon::now()->toDateTimeString()
        ];
        \Log::info('logon', ['message'=> $logon]);
    }
}
