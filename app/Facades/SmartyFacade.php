<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class SmartyFacade extends Facade {

    protected static function getFacadeAccessor() {
        return 'SmartyService';
    }

}
