<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @method static \App\Facades\Plugin cms_to_bool(string $in, boolean $strict)
 * @method static \App\Facades\Plugin cms_get_param(array $params, string $key, string $dflt)
 *
 * @see \App\Facades\Plugin
 */
class Plugin extends Facade
{
    /**
     * Get the registered name of the plugin.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'plugins';
    }
}
