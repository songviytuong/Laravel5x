<?php

namespace App\Models;

use Cache;
use Illuminate\Support\Facades\Route;
use Illuminate\Database\Eloquent\Model;
use App\Helpers\Business;

class SEOs extends Model
{
    public $timestamps = false;
    protected $dateFormat = 'U';
    protected $table = '5x_seos';

    const CREATED_AT = 'creation_date';
    const UPDATED_AT = 'last_update';

    /**
     * getDefaultSEO.
     *
     * @param mixed $sku
     *
     * @author $TBinh
     */
    public static function getDefaultSEO($sku = '')
    {
        $route_name = Route::currentRouteName();
        if ($sku) {
            self::where('sku', $sku);
        } else {
            if ($route_name && Business::cms_existsFieldWithValue('sku', $route_name, '5x_seos')) {
                self::where('sku', $route_name);
            } else {
                self::where('id', 1);
            }
        }
        return $value = Cache::rememberForever(__FUNCTION__, function () {
            return self::get()->first();
        });
    }
}
