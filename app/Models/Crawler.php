<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Crawler extends Model {

    protected $table = '5x_crawlers';
    
    public static function getCrawlers(){
    	$obj = new Crawler();
        $refObj = new ReflectionObject($obj);
        $refProp1 = $refObj->getProperty('table');
        $refProp1->setAccessible(true);
        $table = $refProp1->getValue($obj);
	return $table;
    }

    public function getQueueableConnection() {
        
    }

    public function resolveRouteBinding($value) {
        
    }

}
