<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Doodles extends Model
{
    public $timestamps = false;
    protected $table = '5x_doodles';
}
