<?php

namespace App\Models;

use App\Languages\HasTranslations;
use Illuminate\Database\Eloquent\Model;

class Fragment extends Model
{
    use HasTranslations;

    protected $translatable = ['text'];
    protected $table = '5x_fragments';

    /**
     * getGroup.
     *
     * @param mixed $group
     * @param mixed $locale
     *
     * @return array
     *
     * @author $TBinh
     */
    public static function getGroup(string $group, string $locale): array
    {
        return static::query()->where('key', 'LIKE', "{$group}.%")
            ->where('locale', $locale)
            ->get()
            ->map(function (Fragment $fragment) use ($locale, $group) {
                $key = preg_replace("/{$group}\\./", '', $fragment->key, 1);
                $text = $fragment->translate('text', $locale);

                return compact('key', 'text');
            })
            ->pluck('text', 'key')
            ->toArray();
    }
}
