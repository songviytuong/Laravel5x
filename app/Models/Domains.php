<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Session;
use SEO;
use App\Models\SEOs;
use Illuminate\Support\Facades\Cache;
use DB;
use File;

class Domains extends Model {

    protected $table = "5x_domains";

    public static function validDomain($domain) {
        if (!Domains::where('domain', $domain)->first()) {
            return abort(404); // or redirect to your homepage route.
        }
    }

    public static function getThemes($status = 1, $domain = '', $deleted_flg = 0) {
        $domain = session()->get('domain');
        $data = Domains::select('id', 'themes', 'isset_url')->where(['status' => 1, 'domain' => $domain, 'deleted_flg' => $deleted_flg])->first();
        $data = ($data['themes']) ? $data->toArray() : null;
        if ($data && self::validThemes($data['themes'])) {
            return $data;
        } else {
            return false;
        }
    }

    public static function validThemes($themes_name) {
        if (File::exists(resource_path('views/themes/' . $themes_name . '/index.blade.php'))) {
            return true;
        } else {
            return false;
        }
    }

    public static function getMetaData() {
        return Cache::rememberForever('MetaData', function() {
                    $data = static::getThemes();
                    $SEO = SEOs::where(['sku' => $data['id']])->first();
                    $SEO = ($SEO) ? $SEO->toArray() : null;
                    return $SEO;
                });
    }

    public static function generalSlug($length = 10) {
        $slug = bin2hex(openssl_random_pseudo_bytes($length));
        return $slug;
    }

    public static function getFieldByParams($fields = [], $where = []) {
        if ($fields && $where) {
            $result = Domains::where($where)->select($fields)->get()->toArray();
            return $result;
        } else {
            return false;
        }
    }

    public static function updateSlug($id) {
        if ($id) {
            DB::table('5x_domains')->whereId($id)->update(['slug' => self::generalSlug()]);
            return true;
        } else {
            return false;
        }
    }

}
