<?php

namespace App\Providers;

use App\Plugins\SitePrefs;
use Illuminate\Support\ServiceProvider;

class SitePrefsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        $this->app->singleton('siteprefs', function () {
            return new SitePrefs();
        });
    }
}
