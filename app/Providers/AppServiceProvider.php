<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     */
    public function boot()
    {
    }

    /**
     * Register any application services.
     */
    public function register()
    {
        if ($this->app->environment('production')) {
            $this->app->register('App\Services\TranslationServiceProvider');
        }
        $this->app->register('App\SEO\SEOServiceProvider');
        $this->app->register('App\Plugins\PluginsServiceProvider');
    }
}
