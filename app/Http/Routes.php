<?php

/**
 * Load all routes
 */
foreach(File::allFiles(__DIR__.'/Routes') as $partial)
{
    require_once $partial->getPathname();
}

Auth::routes();

