<?php

namespace App\Helpers;

use File;
use Cache;
use Request;
use Swift_Mailer;
use Carbon\Carbon;
use ReflectionClass;
use ReflectionMethod;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Mail;
use Modules\Cpanel\Entities\SitePrefs;
use Illuminate\Support\Facades\Session;
use Swift_SmtpTransport as SmtpTransport;

abstract class Business
{
    public static function cms_isTestFirst($string)
    {
        $string = strtolower($string);
        $string = str_replace(' ', '_', $string);
        if (preg_match('/^test_/', $string)) {
            $string = str_replace('test_', '', $string);
        }

        return 'test_'.$string;
    }

    public static function cms_validTestFirst($string = '')
    {
        $new_string = self::cms_isTestFirst($string);

        return $new_string;
    }

    /**
     * @Function: cms_listAllFunctionsTest
     * @Params: List all function test in tests
     * @Result: Array
     * @Date: 20-05-18
     */
    public static function cms_listAllFunctionsTest()
    {
        $arrayMaster = array();
        $result = array();
        $documents = self::cms_getDocuments();
        foreach ($documents as $key => $folders) {
            foreach ($documents[$key]['file'] as $f => $class) {
                array_push($arrayMaster, $class['funs']);
            }
        }
        foreach ($arrayMaster as $master) {
            foreach ($master as $item) {
                $result[] = $item;
            }
        }

        return $result;
    }

    /**
     * @Function: cms_getDocuments
     * @Params: Read folder tests
     * @Result: Array
     * @Date: 16-05-18
     */
    public static function cms_getDocuments()
    {
        $path = app_path().'/../tests';
        $arr = array();
        $dir = File::directories($path);
        $clx = '';
        foreach ($dir as $key => $folder) {
            $arr[$key]['folder'] = explode('tests\\', $folder)[1];
            $files = File::allFiles($folder);
            foreach ($files as $k => $file) {
                //ClassName
                $clx = explode('.', ucfirst(explode('/../', $file->getPathName())[1]))[0];
                $arr[$key]['file'][$k]['file'] = $file->getFileName();
                $arr[$key]['file'][$k]['source'] = $file->getPathName();
                $arr[$key]['file'][$k]['class'] = $clx;
                $class = new ReflectionClass($clx);
                $methods = $class->getMethods(ReflectionMethod::IS_FINAL);
                if ($methods) {
                    foreach ((array) $methods as $t => $method) {
                        //GET::MethodName
                        if (!preg_match('/^test_/', $method->name)) {
                            $arr[$key]['file'][$k]['funs'] = [];
                            continue;
                        } else {
                            $arr[$key]['file'][$k]['funs'][$t]['folder'] = explode('tests\\', $folder)[1];
                            $arr[$key]['file'][$k]['funs'][$t]['class'] = $clx;
                            $arr[$key]['file'][$k]['funs'][$t]['function_name'] = $method->name;
                            //GET::Document
                            $comment_string = $class->getMethod($method->name)->getdoccomment();
                            preg_match_all('#@(.*?)\n#s', $comment_string, $matches);
                            $arr[$key]['file'][$k]['funs'][$t]['function_desc'] = json_encode($matches[1]);
                            //GET::Code
                            $func = new ReflectionMethod($clx, $method->name);
                            $filename = $func->getFileName();
                            $start_line = $func->getStartLine() - 1; // it's actually - 1, otherwise you wont get the function() block
                            $end_line = $func->getEndLine();
                            $length = $end_line - $start_line;
                            $source = file($filename);
                            $body = implode('', array_slice($source, $start_line, $length));
                            $arr[$key]['file'][$k]['funs'][$t]['function_detail'] = serialize($body);
                        }
                    }
                } else {
                    $arr[$key]['file'][$k]['funs'] = [];
                }
            }
        }

        return Cache::rememberForever(__FUNCTION__, function () use ($arr) {
            return $arr;
        });
    }

    /**
     * @Function: cms_getSitePrefs
     * @Params:
     * @Result: Array
     * @Date: 10-05-18
     */
    public static function cms_getSitePrefs($key_target = '')
    {
        $arr = array();
        $SitePrefs = SitePrefs::get()->toArray();
        foreach ($SitePrefs as $key => $item) {
            $arr[$item['sitepref_name']] = $item['sitepref_value'];
        }
        if ($key_target) {
            return $arr[$key_target];
        }

        return Cache::rememberForever(__FUNCTION__, function () {
            return SitePrefs::get()->toArray();
        });
    }

    /**
     * @Function: cms_directories
     * @Params:
     * @Result: Array
     * @Date: 10-05-18
     */
    public static function cms_directories($path)
    {
        $arr = array();
        $list = Cache::rememberForever(__FUNCTION__.$path, function () use ($path) {
            return File::directories($path);
        });
        foreach ($list as $partial) {
            $arr[] = last(explode('\\', $partial));
        }

        return $arr;
    }

    /**
     * @Function: cms_getProtocol
     * @Params:
     * @Result: (string)
     * @Date: 02-05-18
     */
    public static function cms_getProtocol()
    {
        if (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
            $protocol = 'https://'.Request::server('HTTP_HOST');
        } else {
            $protocol = 'http://'.Request::server('HTTP_HOST');
        }

        return $protocol;
    }

    /**
     * @Function: cms_existsFieldWithValue
     * @Result: (boolean) true or false and [id]
     * @Date: 21-04-18
     */
    public static function cms_existsFieldWithValue(string $field, string $value, string $table, $id = null)
    {
        $resultObj = DB::selectOne(DB::raw('select exists(select 1 from '.$table.' where `'.$field.'`=:value) as `exists`'), [
            'value' => $value,
        ]);
        $result = [];
        if ($resultObj->exists == 1) {
            if ($id) {
                $ids = DB::table($table)->select($id.' as id')->where($field, $value)->get()->first();
                $result['id'] = $ids->id;
            }
            $result['status'] = true;
        }

        return $result;
    }

    /**
     * @Function: cms_keyUnique
     * @Date: 21-04-18
     */
    public static function cms_keyUnique($token, $length, $table, $field, $type = 'openssl_random')
    {
        $resultObj = DB::selectOne(DB::raw('select exists(select 1 from `auth_keys` where `auth_key`=:token) as `exists`'), [
                    'token' => $token,
        ]);
        if ($resultObj->exists == 1) {
            $token = bin2hex(openssl_random_pseudo_bytes($length));
        }

        return $token;
    }

    /**
     * @Function: cms_joinPath
     * @Date: 21-04-18
     */
    public static function cms_joinPath()
    {
        $args = func_get_args();

        return implode(DIRECTORY_SEPARATOR, $args);
    }

    /**
     * @Function: cms_betterStripTags
     * @Date: 02-05-18
     */
    public static function cms_betterStripTags($str, $allowable_tags = '', $strip_attrs = false, $preserve_comments = false, callable $callback = null)
    {
        $allowable_tags = array_map('strtolower', array_filter( // lowercase
            preg_split('/(?:>|^)\\s*(?:<|$)/', $allowable_tags, -1, PREG_SPLIT_NO_EMPTY), // get tag names
            function ($tag) {
                return preg_match('/^[a-z][a-z0-9_]*$/i', $tag);
            } // filter broken
        ));
        $comments_and_stuff = preg_split('/(<!--.*?(?:-->|$))/', $str, -1, PREG_SPLIT_DELIM_CAPTURE);
        foreach ($comments_and_stuff as $i => $comment_or_stuff) {
            if ($i % 2) { // html comment
                if (!($preserve_comments && preg_match('/<!--.*?-->/', $comment_or_stuff))) {
                    $comments_and_stuff[$i] = '';
                }
            } else { // stuff between comments
                $tags_and_text = preg_split("/(<(?:[^>\"']++|\"[^\"]*+(?:\"|$)|'[^']*+(?:'|$))*(?:>|$))/", $comment_or_stuff, -1, PREG_SPLIT_DELIM_CAPTURE);
                foreach ($tags_and_text as $j => $tag_or_text) {
                    $is_broken = false;
                    $is_allowable = true;
                    $result = $tag_or_text;
                    if ($j % 2) { // tag
                        if (preg_match("%^(</?)([a-z][a-z0-9_]*)\\b(?:[^>\"'/]++|/+?|\"[^\"]*\"|'[^']*')*?(/?>)%i", $tag_or_text, $matches)) {
                            $tag = strtolower($matches[2]);
                            if (in_array($tag, $allowable_tags)) {
                                if ($strip_attrs) {
                                    $opening = $matches[1];
                                    $closing = ($opening === '</') ? '>' : $closing;
                                    $result = $opening.$tag.$closing;
                                }
                            } else {
                                $is_allowable = false;
                                $result = '';
                            }
                        } else {
                            $is_broken = true;
                            $result = '';
                        }
                    } else { // text
                        $tag = false;
                    }
                    if (!$is_broken && isset($callback)) {
                        // allow result modification
                        call_user_func_array($callback, array(&$result, $tag_or_text, $tag, $is_allowable));
                    }
                    $tags_and_text[$j] = $result;
                }
                $comments_and_stuff[$i] = implode('', $tags_and_text);
            }
        }
        $str = implode('', $comments_and_stuff);
        $str = str_replace(' ', '', $str);

        return $str;
    }

    public static function cms_isAvailable($url, $timeout = 30)
    {
        $ch = curl_init(); // get cURL handle
        // set cURL options
        $opts = array(
            CURLOPT_RETURNTRANSFER => true, // do not output to browser
            CURLOPT_URL => $url, // set URL
            CURLOPT_NOBODY => true, // do a HEAD request only
            CURLOPT_TIMEOUT => $timeout,
        );   // set timeout
        curl_setopt_array($ch, $opts);
        curl_exec($ch); // do it!
        $retval = curl_getinfo($ch, CURLINFO_HTTP_CODE) == 200; // check if HTTP OK
        curl_close($ch); // close handle
        return $retval;
    }

    public static function cms_loginGetContent($url = '', $params = ['method' => 'get'], $fields = [], $return = 'json', $headers = [])
    {
//        $headers = array(
//            "Content-type: multipart/form-data",
//            "Accept: multipart/form-data",
//            "guid: 065826d0-d053-4c13-b2d1-91ad605383a3"
//        );

        if (!$url) {
            echo 'Link empty !';

            return;
        }

        if (!empty($params['token'])) {
            $fields['token'] = $params['token'];
        }

        $postinfo = '';
        foreach ($fields as $key => $field) {
            $postinfo .= '&'.$key.'='.$field;
        }
        $postinfo = substr($postinfo, 1);
//        var_dump($fields); exit();

        $cookie_file_path = '/cookie.txt';

        $ch = curl_init();
        //page with the content I want to grab
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_NOBODY, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

        curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file_path);
        //set the cookie the site has for certain features, this is optional
        curl_setopt($ch, CURLOPT_COOKIE, 'cookiename=0');
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US; rv:1.7.12) Gecko/20050915 Firefox/1.0.7');

        curl_setopt($ch, CURLOPT_REFERER, $_SERVER['REQUEST_URI']);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, strtoupper($params['method']));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postinfo);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        //do stuff with the info with DomDocument() etc
        $result = curl_exec($ch);
        curl_close($ch);
        if (strtoupper($return) == 'JSON') {
            return json_decode($result, true);
        }

        return $result;
    }

    /**
     * cms_timeElapsed.
     *
     * @param string $datetime
     * @param bool   $full
     */
    public static function cms_timeElapsed($datetime, $full = false)
    {
        $now = new Carbon();
        $ago = new Carbon($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k.' '.$v.($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) {
            $string = array_slice($string, 0, 1);
        }

        return $string ? implode(', ', $string).' ago' : 'just now';
    }

    /**
     * munge_string_to_url.
     *
     * @param mixed $alias
     * @param mixed $tolower
     * @param mixed $withslash
     * @param mixed $gallery
     * @param bool  $google
     */
    public static function munge_string_to_url($alias, $tolower = true, $withslash = false, $gallery = false, $google = false)
    {
        // replacement.php is encoded utf-8 and must be the first modification of alias
        $toreplace = array(
            'Ä', 'ä', 'Æ', 'æ', 'Ǽ', 'ǽ', 'Å', 'å', 'Ǻ', 'ǻ', 'À', 'Á', 'Â', 'Ã', 'à', 'á', 'â', 'ã', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ǎ', 'ǎ', 'Ạ', 'Ạ', 'ạ', 'Ả', 'ả', 'Ấ', 'ấ', 'Ầ', 'ầ', 'Ẩ', 'ẩ', 'Ẫ', 'ẫ', 'Ậ', 'ậ', 'Ắ', 'ắ', 'Ằ', 'ằ', 'Ẳ', 'ẳ', 'Ẵ', 'ẵ', 'Ặ', 'ặ',
            'Ç', 'ç', 'Ć', 'ć', 'Ĉ', 'ĉ', 'Ċ', 'ċ', 'Č', 'č',
            'Ð', 'ð', 'Ď', 'ď', 'Đ', 'đ',
            'È', 'É', 'Ê', 'Ë', 'è', 'é', 'ê', 'ë', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę', 'Ě', 'ě', 'Ẹ', 'ẹ', 'Ẻ', 'ẻ', 'Ẽ', 'Ế', 'ế', 'Ề', 'ề', 'Ể', 'ể', 'ễ', 'Ệ', 'ệ', 'Ə', 'ə',
            'ſ', 'ſ',
            'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ',
            'Ĥ', 'ĥ', 'Ħ', 'ħ',
            'Ì', 'Í', 'Î', 'Ï', 'ì', 'í', 'î', 'ï', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ǐ', 'ǐ', 'Ỉ', 'ỉ', 'Ị', 'ị',
            'Ĳ', 'ĳ',
            'ﬁ', 'ﬂ',
            'Ĵ', 'ĵ',
            'Ķ', 'ķ', 'ĸ',
            'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł',
            'Ñ', 'ñ', 'Ń', 'ń', 'Ņ', 'Ň', 'ň', 'ŉ', 'Ŋ', 'ŋ',
            'Ö', 'ö', 'Ø', 'ø', 'Ǿ', 'ǿ', 'Ò', 'Ó', 'Ô', 'Õ', 'ò', 'ó', 'ô', 'õ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Ǒ', 'ǒ', 'Ọ', 'ọ', 'Ỏ', 'ỏ', 'Ố', 'ố', 'Ồ', 'ồ', 'Ổ', 'ổ', 'Ỗ', 'ỗ', 'Ộ', 'ộ', 'Ớ', 'ớ', 'Ờ', 'ờ', 'Ở', 'ở', 'Ỡ', 'ỡ', 'Ợ', 'ợ', 'Ơ', 'ơ',
            'Œ', 'œ',
            'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř',
            'Ś', 'ś', 'Ŝ', 'Ş', 'ş', 'Š', 'š',
            'Ţ', 'ţ', 'Ť', 'ť', 'Ŧ', 'ŧ',
            'Ü', 'ü', 'Ù', 'Ú', 'Û', 'ù', 'ú', 'û', 'Ụ', 'ụ', 'Ủ', 'ủ', 'Ứ', 'ứ', 'Ừ', 'ừ', 'Ữ', 'ữ', 'Ự', 'ự', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ǔ', 'ǔ', 'ǖ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ư', 'ư',
            'Ŵ', 'ŵ', 'Ẁ', 'ẁ', 'Ẃ', 'ẃ', 'Ẅ', 'ẅ',
            'Ý', 'ý', 'ÿ', 'Ŷ', 'ŷ', 'Ÿ', 'Ỳ', 'ỳ', 'Ỵ', 'ỵ', 'Ỷ', 'ỷ', 'Ỹ', 'ỹ',
            'Þ', 'þ', 'ß',
            'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž',
            'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С',
            'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я',
            'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с',
            'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я',
        );
        $replacement = array(
            'ae', 'ae', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a',
            'c', 'c', 'c', 'c', 'c', 'c', 'c', 'c', 'c', 'c',
            'd', 'd', 'd', 'd', 'd', 'd',
            'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e',
            'f', 'f',
            'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g',
            'h', 'h', 'h', 'h',
            'i', 'i', 'i', 'i', 'i', 'i', 'i', 'i', 'i', 'i', 'i', 'i', 'i', 'i', 'i', 'i', 'i', 'i', 'i', 'i', 'i', 'i', 'i', 'i',
            'ij', 'ij',
            'fi', 'fl',
            'j', 'j',
            'k', 'k', 'k',
            'l', 'l', 'l', 'l', 'l', 'l', 'l', 'l', 'l', 'l',
            'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n',
            'oe', 'oe', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o',
            'oe', 'oe',
            'r', 'r', 'r', 'r', 'r', 'r',
            's', 's', 's', 's', 's', 's', 's',
            't', 't', 't', 't', 't', 't',
            'ue', 'ue', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u',
            'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w',
            'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y',
            'th', 'th', 'ss',
            'z', 'z', 'z', 'z', 'z', 'z',
            'a', 'b', 'v', 'g', 'd', 'e', 'e', 'zh', 'z', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's',
            't', 'u', 'f', 'h', 'ts', 'ch', 'sh', 'sch', '', 'y', '', 'e', 'yu', 'ya',
            'a', 'b', 'v', 'g', 'd', 'e', 'e', 'zh', 'z', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's',
            't', 'u', 'f', 'h', 'ts', 'ch', 'sh', 'sch', '', 'y', '', 'e', 'yu', 'ya',
        );

        $alias = str_replace($toreplace, $replacement, $alias);

        // lowercase only on empty aliases
        if ($tolower == true) {
            $alias = strtolower($alias);
        }
        $expr = '/[^a-z0-9-_]+/i';
        if ($withslash) {
            $expr = '/[^\p{L}_\.\-\ \d\/\.]/u';
        }

        if ($gallery) {
            $expr = '/[^\p{L}_\.\-\ \d\.]/u';
        }
        $alias = preg_replace($expr, '-', $alias);

        for ($i = 0; $i < 5; ++$i) {
            $tmp = str_replace('--', '-', $alias);
            if ($tmp == $alias) {
                break;
            }
            $alias = $tmp;
        }
        $alias = trim($alias, '-');
        $alias = trim($alias);

        if ($google) {
            $alias = str_replace('-', '+', $alias);
        }

        return $alias;
    }

    /**
     * setEnvironmentValue.
     *
     * @param mixed $envKey
     * @param mixed $envValue
     */
    public static function setEnvironmentValue($envKey, $envValue)
    {
        $envFile = app()->environmentFilePath();
        $str = file_get_contents($envFile);
        $oldValue = env($envKey);
        if ($envValue != config('master.SUPPORTED_TEMPLATES')[0]) {
            $envValue == '';
            $str = str_replace("{$envKey}={$oldValue}", "{$envKey}=\n", $str);
        } else {
            $str = str_replace("{$envKey}={$oldValue}", "{$envKey}={$envValue}\n", $str);
        }
        $str = trim($str);
        if (env('APP_ENV') == 'developement') {
            $fp = fopen($envFile, 'w');
            fwrite($fp, $str);
            fclose($fp);
        }
    }

    /**
     * cms_htmlentities.
     *
     * @param mixed $val
     * @param mixed $param
     * @param mixed $charset
     * @param mixed $convert_single_quotes
     */
    public static function cms_htmlentities($val, $param = ENT_QUOTES, $charset = 'UTF-8', $convert_single_quotes = false)
    {
        if ($val == '') {
            return '';
        }

        $val = str_replace('&#032;', ' ', $val);
        $val = str_replace('&', '&amp;', $val);
        $val = str_replace('<!--', '&#60;&#33;--', $val);
        $val = str_replace('-->', '--&#62;', $val);
        $val = str_ireplace('<script', '&#60;script', $val);
        $val = str_replace('>', '&gt;', $val);
        $val = str_replace('<', '&lt;', $val);
        $val = str_replace('"', '&quot;', $val);
        $val = preg_replace('/\$/', '&#036;', $val);
        $val = str_replace('!', '&#33;', $val);
        $val = str_replace("'", '&#39;', $val);

        if ($convert_single_quotes) {
            $val = str_replace("\\'", '&apos;', $val);
            $val = str_replace("'", '&apos;', $val);
        }

        return $val;
    }

    /**
     * cms_CreateUrl.
     *
     * @param mixed $modinstance
     * @param mixed $action
     * @param mixed $params
     * @param mixed $target_url
     * @param mixed $back_url
     * @param mixed $is_route
     *
     * @return string
     */
    public static function cms_CreateUrl(&$modinstance, $action, $params = array(), $target_url = '', $back_url = '', $is_route)
    {
        $text = '';

        if ($target_url != '') {
            $text = ($is_route) ? route($target_url) : $target_url;
        } else {
            $text = URL::to('/');
        }

        $action = (isset($action) ? $action : 'Business');

        $text .= '/?action='.$action;

        foreach ($params as $key => $value) {
            $key = self::cms_htmlentities($key);
            if (in_array($key, array('target_url', 'back_url', 'action', 'class'))) {
                continue;
            }
            $value = self::cms_htmlentities($value);
            $text .= '&amp;'.$key.'='.rawurlencode($value);
        }

        if ($back_url != '') {
            $text .= '&amp;'.'back_url='.(($is_route) ? route($back_url) : $back_url);
        }

        return $text;
    }

    /**
     * cms_CreateLink.
     *
     * @param mixed $modinstance
     * @param mixed $action
     * @param mixed $contents
     * @param mixed $params
     * @param mixed $warn_message
     *
     * @return string $text
     */
    public static function cms_CreateLink(&$modinstance, $action, $contents = '', $params = array(), $warn_message = '',
    $onlyhref = false, $target_url = '', $back_url = '', $is_route = false)
    {
        if (!is_array($params) && $params == '') {
            $params = array();
        }

        $action = self::cms_htmlentities($action);
        $target_url = self::cms_htmlentities($target_url);
        $back_url = self::cms_htmlentities($back_url);

        $class = (isset($params['class']) ? self::cms_htmlentities($params['class']) : '');
        // create url....
        $text = self::cms_CreateUrl($modinstance, $action, $params, $target_url, $back_url, $is_route);

        if (!$onlyhref) {
            $beginning = '<a';
            if ($class != '') {
                $beginning .= ' class="'.$class.'"';
            }
            $beginning .= ' href="';
            $text = $beginning.$text.'"';
            if ($warn_message != '') {
                $text .= ' onclick="return confirm(\''.$warn_message.'\');"';
            }
            $text .= '>'.$contents.'</a>';
        }

        return $text;
    }

    /**
     * Given an output array or object, encode it to json, and exit.
     * This is a convenience method.  It also handles clearing any data that has already been sent to output buffers.
     *
     * @param mixed $output
     */
    public static function send_ajax_and_exit($output)
    {
        $handlers = ob_list_handlers();
        for ($cnt = 0; $cnt < sizeof($handlers); ++$cnt) {
            ob_end_clean();
        }

        header('Pragma: public');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Cache-Control: private', false);
        header('Content-Type: application/json');
        $output = json_encode($output);
        echo $output;
        exit;
    }

    /**
     * sendEmail.
     *
     * @param mixed $config_email
     * @param mixed $settings
     */
    public static function sendEmail($config_email, $settings = [])
    {
        try {
            if (!empty($settings)) {
                $transport = new SmtpTransport($settings['Host'], $settings['Port'], $settings['Encryption']);
                $transport->setUsername($settings['Username']);
                $transport->setPassword($settings['Password']);
                $mail_instanse = new Swift_Mailer($transport);
                Mail::setSwiftMailer($mail_instanse);
            }

            $from = isset($config_email['from']) ? $config_email['from'] : '';
            $from_name = isset($config_email['from_name']) ? $config_email['from_name'] : '';
            $to = isset($config_email['to']) ? $config_email['to'] : '';
            $subject = isset($config_email['subject']) ? $config_email['subject'] : '';
            $params = isset($config_email['params']) ? $config_email['params'] : [];
            $template = isset($config_email['template']) ? $config_email['template'] : '';
            $cc = isset($config_email['cc']) ? $config_email['cc'] : null;
            $bcc = isset($config_email['bcc']) ? $config_email['bcc'] : null;
            $pathToFile = isset($config_email['pathToFile']) ? $config_email['pathToFile'] : null;
            Mail::send($template, ['params' => $params], function ($email) use ($from, $from_name, $to, $subject, $cc, $bcc, $pathToFile) {
                if ($from_name != '') {
                    $email->from($from, $from_name);
                } else {
                    $email->from($from, $from);
                }
                $email->to($to);
                if ($cc !== null) {
                    $email->cc($cc);
                }
                if ($bcc !== null) {
                    $email->bcc($bcc);
                }
                if ($pathToFile !== null) {
                    if (is_array($pathToFile)) {
                        foreach ($pathToFile as $path_file) {
                            $email->attach($path_file);
                        }
                    } else {
                        $email->attach($pathToFile);
                    }
                }
                $email->subject($subject);
            });

            return true;
        } catch (\Exception $e) {
            echo $e->getMessage();
            Session::flash('flash_errors', $e->getMessage());

            return false;
        }
    }

    /**
     * Makes translation fall back to specified value if definition does not exist.
     *
     * @param string      $key
     * @param null|string $fallback
     * @param null|string $locale
     * @param array|null  $replace
     *
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public static function trans_fb(string $key, ?string $fallback = null, ?string $locale = null, ?array $replace = [])
    {
        if (\Illuminate\Support\Facades\Lang::has($key, $locale)) {
            return trans($key, $replace, $locale);
        }

        return $fallback;
    }
}
