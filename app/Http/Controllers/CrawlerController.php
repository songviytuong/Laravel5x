<?php

namespace App\Http\Controllers;

use DB;
use App;
use URL;
use File;
use Carbon\Carbon;
use ErrorException;
use App\Helpers\Business;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Jobs\CrawlerJobUpdateDetails;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redirect;
use Symfony\Component\DomCrawler\Crawler;
use App\Jobs\CrawlerJobDoodlesUpdateDetails;
use App\Models\Doodles;

class CrawlerController extends Controller
{
    public $num = 0;

    public function index(Request $request)
    {
        //        echo URL::previous(); exit();
        //        $crawler = $client->request('GET', 'https://github.com/');
        //        $crawler = $client->click($crawler->selectLink('Sign in')->link());
        //        $form = $crawler->selectButton('Sign in')->form();
        //        $crawler = $client->submit($form, array('login' => 'songviytuong', 'password' => '1cAdillac12'));
        //        $crawler->filter('.flash-error')->each(function ($node) {
        //            print $node->text() . "\n";
        //        });

        $arr = [
            'cho-thue-phong-tro',
            'nha-cho-thue',
            'cho-thue-can-ho',
            'tim-nguoi-o-ghep',
        ];

        $alias = ($request->alias == 0) ? $arr[0] : isset($arr[$request->alias]);
        $link = 'https://thuephongtro123.com/' . $alias . '/page/' . $request->id;

        $page = isset($request->id) ? $request->id : 1;
        if ($page == -1 || ($request->alias >= count($arr))) {
            echo "<div style='width:100%; text-align:center; padding-top:20%; font-size:24px;'>Record Not Found</div>";

            return;
        } else {
            $PATH = resource_path('/crawler/' . $alias . '/');
            $FILE = $PATH . $alias . '-p' . $request->id . '.html';

            if (!file_exists($PATH)) {
                mkdir($PATH, 0777, true);
            }

            if (!File::exists($FILE)) {
                $str = Business::cms_loginGetContent($link);
                File::put($FILE, $str);
            }

            /*
            $client = new Client();
            $crawler = $client->request('GET', $link);
             */
            $crawler = new Crawler();
            $content = File::get($FILE);
            $crawler->addHtmlContent($content);
            $data_source = $crawler->filterXPath('//a[@class="post-link"]');
            $insert = array();
            foreach ($data_source as $key => $item) {
                if ($item->nodeValue === '') {
                    continue;
                }

                //            $value = '';
                //            foreach ($item->childNodes as $child) {
                //                $value .= $item->ownerDocument->saveHTML($child);
                //            }
                $insert[$key]['title'] = $item->nodeValue;
                $insert[$key]['postid'] = str_replace('-', '', substr(trim($item->getAttribute('href')), -10, 5));
                $insert[$key]['link'] = str_replace('/', '', $item->getAttribute('href'));
                //                  $insert[$key]['href'] = $child->getAttribute('href');
                //                  $insert[$key]['value'] = $value;
            }
            //          echo "<pre>";
            //          var_dump($insert);
            //          exit();
            $uri = URL::previous();
            $uri1 = explode('/', $uri);
            $uri = Business::cms_joinPath($uri1[0], $uri1[1], $uri1[2], $uri1[3]);
            if ($insert == null) {
                File::delete($FILE);

                return Redirect::to(Business::cms_joinPath($uri1[3], ($request->alias + 1), 1));
                echo 'Not Found: ';
                for ($i = 0; $i < count($arr); ++$i) {
                    echo "<a href='" . Business::cms_joinPath($uri, $i, 1) . "'>Link " . $i . '</a> | ';
                }
            } else {
                foreach ($insert as $row) {
                    $Insert = [
                        'title' => $row['title'],
                        'postid' => $row['postid'],
                        'link' => $row['link'],
                    ];
                    $job = (new CrawlerJob($Insert))->onQueue('crawler')->delay(Carbon::now()->addSeconds(3));
                    dispatch($job);
                }
            }

            //            File::delete($FILE);
            //        $class = $crawler->filterXPath('//span[contains(@id, "-10")]')->evaluate('substring-before(@id, "-")');
            //$crawler->filterXPath('//span[@class="article"]')->each(function (Crawler $node, $i) {
            //    $class1[] = $node->text();
            //});
            //        echo "<pre>";
            //        var_dump($insert);
            //        exit();
            //            $job = (new CrawlerJob($insert))->onQueue('crawler')->delay(Carbon::now()->addSeconds(3));
            //            dispatch($job);
            //        die($request->id);
            ++$page;

            echo "<meta http-equiv='refresh' content='0;URL=\"" . Business::cms_joinPath($uri, $request->alias, $page) . "\"' />";
        }
    }

    public function doodles(Request $request)
    {
        $resultObj = DB::selectOne(DB::raw('SELECT MAX(run_date_array) as `maxDate` FROM 5x_doodles'));
        $start = Carbon::parse($resultObj->maxDate);

        $nowYear = Carbon::now()->year;
        $nowMonth = Carbon::now()->month;

        $year = $start->year;
        $month = $start->month;

        //         var_dump($year,$month);
        //  exit();
        if ($month > 12) {
            $year = $year + 1;
            $month = 1;
        }

        $url = 'https://www.google.com/doodles/json/' . $year . '/' . $month;
        Log::info('++++++ Url: ' . $url);

        $output = Business::cms_loginGetContent($url, ['method' => 'get'], [], 'html', []);
        $result = json_decode($output);

        $this->ExecuteData($result, $url);

        $uri = URL::previous();
        $uri = explode('/', $uri);

        Log::info('Year: ' . $year . ' - NowYear: ' . $nowYear . ' - request->year: ' . $request->year . ' - request->month: ' . $request->month . ' - nowMonth: ' . $nowMonth);
        if ($year > $nowYear || ($request->year > $year) || ($year == $nowYear && $request->month > $month)) {
            $uri = Business::cms_joinPath($uri[0], $uri[1], $uri[2], $uri[3], 'doodlesDetail');
            echo "<a href='" . $uri . '?cc' . "'>Link</a><br/>";
            echo '$ php artisan queue:listen --queue=doodles_details';
            exit();
        }

        ++$month;

        $uri = Business::cms_joinPath($uri[0], $uri[1], $uri[2], $uri[3], 'doodles');
        echo "<meta http-equiv='refresh' content='0;URL=\"" . Business::cms_joinPath($uri, $year, $month) . "\"' />";
    }

    public function ExecuteData($result = array(), $url = '')
    {
        $url_vi = $url . '?hl=vi';

        if (count($result) > 0) {
            for ($j = 0; $j < count($result); ++$j) {
                if (!Business::cms_existsFieldWithValue('alias', $result[$j]->name, '5x_doodles')) {
                    Log::info('Alias: ' . $result[$j]->name);

                    $output_vi = Business::cms_loginGetContent($url_vi, ['method' => 'get'], [], 'html', []);
                    $result_vi = json_decode($output_vi);

                    $title_vi = $result_vi[$j]->title;
                    $title_vi = str_replace('"', '\"', $title_vi);
                    $title_en = $result[$j]->title;
                    $title_en = str_replace('"', '\"', $title_en);
                    $alias = $result[$j]->name;
                    $image_url = $result[$j]->url;
                    $image_big_url = isset($result[$j]->high_res_url) ? $result[$j]->high_res_url : '';
                    $run_date_array = implode('-', $result[$j]->run_date_array);

                    $insert = [
                        'title_en' => $title_en,
                        'title_vi' => $title_vi,
                        'alias' => $alias,
                        'image_url' => $image_url,
                        'image_big_url' => $image_big_url,
                        'run_date_array' => $run_date_array,
                    ];

                    $last_id = DB::table('5x_doodles')->insertGetId($insert);

                    if ($last_id) {
                        $SOURCE = isset($result[$j]->high_res_url) ? $result[$j]->high_res_url : '';
                        $PATH = 'D:\GitLab\Laravel5x\resources\crawler/google';
                        if (in_array(explode('.', $SOURCE)[count(explode('.', $SOURCE)) - 1], ['jpg', 'gif', 'png'])) {
                            $SOURCE = 'https:' . $SOURCE;
                        }

                        $file_headers = @get_headers($SOURCE);

                        $ext = (explode('.', $SOURCE)[count(explode('.', $SOURCE)) - 1] != 'gif') ? ((explode('.', $SOURCE)[count(explode('.', $SOURCE)) - 1] == 'png') ? '.png' : '.jpg') : '.gif';
                        $FILENAME = 'google-' . $last_id . $ext;
                        if ($file_headers && $file_headers[0] === 'HTTP/1.0 200 OK') {
                            try {
                                file_put_contents($PATH . '/' . $FILENAME, file_get_contents($SOURCE));
                            } catch (ErrorException $e) {
                                continue;
                            }
                        }
                    } else {
                        echo 'Insert Error: ';
                        die();
                    }
                } else {
                    Log::info('Ignore Alias: ' . $result[$j]->name);
                }
            }
        }
    }

    public function doodlesRemoveIMG()
    {
        $SOURCE = 'D:/GitLab/Laravel5x/resources/crawler/google';
        $allFiles = scandir($SOURCE, 1);

        // filter the ones that match the filename.*
        $matchingFiles = preg_grep('/^google-/', $allFiles);
        $res = [];
        // iterate through files and echo their content
        foreach ($matchingFiles as $path) {
            // $arr[] = $path;
            $id = str_replace('google-', '', explode('.', $path)[0]);

            if (!Doodles::find($id)) {
                $res[] = $path;
                unlink($SOURCE . '/' . $path);
            }
        }
        if ($res) {
            echo 'Remove IMGs not in Database:<br/>';
            echo $res;
        } else {
            echo 'Files has been cleaned.';
        }
        exit();
    }

    public function doodlesGetItem(Request $request)
    {
        $year = $request->year ? $request->year : '1998';
        $month = $request->month ? $request->month : '01';

        if ($month > 12) {
            $year = $year + 1;
            $month = 1;
        }

        $next_month = Carbon::now();
        $next_month = $next_month->addMonth(1)->month;

        if ($year > Carbon::now()->year) {
            die('Done');
        }

        $url = 'https://www.google.com/doodles/json/' . $year . '/' . $month;

        $output = Business::cms_loginGetContent($url, ['method' => 'get'], [], 'html', []);
        $result = json_decode($output);

        $this->ExecuteData($result, $url);

        ++$month;

        $uri = URL::previous();
        $uri = explode('/', $uri);
        $uri = Business::cms_joinPath($uri[0], $uri[1], $uri[2], $uri[3], 'doodlesGetItem');
        echo "<meta http-equiv='refresh' content='0;URL=\"" . Business::cms_joinPath($uri, $year, $month) . "\"' />";
    }

    public function doodlesDetail(Request $request)
    {
        if (isset($request['cc'])) {
            Cache::flush();
        }

        $Doodles_List = Cache::rememberForever('Doodles_List', function () {
            return DB::table('5x_doodles')->select('id', 'alias')->whereNull('description')->where('flg', 0)->get()->toArray();
        });

        $tmp = array();
        foreach ($Doodles_List as $key => $item) {
            $tmp[$key][] = $item->id;
            $tmp[$key][] = $item->alias;
        }
        $key_data = isset($request->id) ? $request->id : -1;

        if ($key_data == -1 || $key_data > count($Doodles_List)) {
            echo "<div style='width:100%; text-align:center; padding-top:20%; font-size:24px;'>Record Not Found</div>";

            return;
        }

        $key = $key_data;
        $item = isset($tmp[$key_data]) ? $tmp[$key_data] : '';

        try {
            if (!empty($item[0]) && $item[0] != '') {
                $link = 'https://www.google.com/doodles/' . $item[1];

                $PATH = resource_path('/crawler/doodlesDetail/');
                $FILE = $PATH . 'google-' . $item[0] . '.html';
                if (!File::exists($PATH)) {
                    mkdir($PATH, 0777, true);
                }

                if (Business::cms_isAvailable($link)) {
                } else {
                    if (!File::exists($FILE)) {
                        $str = Business::cms_loginGetContent($link, ['method' => 'get'], [], 'html', []);
                        File::put($FILE, $str);
                        $refresh = URL::previous();

                        return Redirect::to($refresh);
                    } else {
                        echo "<div style='width:100%; text-align:center; padding-top:20%; font-size:24px;'>Baby Crawling... " . $key_data . '/' . count($Doodles_List) . ' => ID: ' . $item[0] . '</div>';
                        $crawler = new Crawler();
                        $content = File::get($FILE);
                        $crawler->addHtmlContent($content);
                        $update = array();
                        $_content = '';
                        if ($crawler->filter('#blog-card > div > div')->count() > 0) {
                            $_content = $crawler->filter('#blog-card > div > div')->text();
                        }

                        $update['descrt'] = $_content;
                        $update['id'] = $item[0];



                        $job_details = (new CrawlerJobDoodlesUpdateDetails($update))->onQueue('doodles_details')->delay(Carbon::now()->addSeconds(3));
                        dispatch($job_details);
                    }
                    ++$key;
                    $uri = URL::previous();
                    $uri = explode('/', $uri);
                    $uri = Business::cms_joinPath($uri[0], $uri[1], $uri[2], $uri[3], 'doodlesDetail');
                    echo '<title>[' . ($key - 1) . "] - Baby Crawling...</title><meta http-equiv='refresh' content='0;URL=\"" . Business::cms_joinPath($uri, $key) . "\"' />";
                }
            } else {
                $uri = URL::previous();
                $uri = explode('/', $uri);
                $uri = Business::cms_joinPath($uri[0], $uri[1], $uri[2], $uri[3], 'doodlesRemoveIMG');
                echo "Finished. <a href='" . $uri . "'>Remove IMAGE</a>";
                exit();
            }
        } catch (ErrorException $e) {
        }
    }

    public function details(Request $request)
    {
        $PostID_List = Cache::rememberForever('PostID_List', function () {
            return DB::table('cms_crawlers')->select('postid', 'link')->get()->toArray();
        });
        $tmp = array();
        foreach ($PostID_List as $key => $item) {
            $tmp[$key][] = $item->postid;
            $tmp[$key][] = $item->link;
        }

        $key_data = isset($request->postid) ? $request->postid : -1;
        if ($key_data == -1 || $key_data >= count($PostID_List)) {
            echo "<div style='width:100%; text-align:center; padding-top:20%; font-size:24px;'>Record Not Found</div>";

            return;
        }

        $item = isset($tmp[$key_data]) ? $tmp[$key_data] : '';
        $key = $key_data;
        if ($item[0] != '') {
            $link = 'https://thuephongtro123.com/' . $item[1];

            $PATH = resource_path('/crawler/details/');
            $FILE = $PATH . $item[0] . '.html';

            if (!File::exists($PATH)) {
                mkdir($PATH, 0777, true);
            }

            if (!Business::cms_isAvailable($link)) {
                DB::table('cms_crawlers')->where('postid', $item[0])->delete();
                Cache::forget('PostID_List');
                $refresh = URL::previous();

                return Redirect::to($refresh);
            } else {
                if (!File::exists($FILE)) {
                    $str = Business::cms_loginGetContent($link);
                    File::put($FILE, $str);
                    $refresh = URL::previous();

                    return Redirect::to($refresh);
                } else {
                    echo "<div style='width:100%; text-align:center; padding-top:20%; font-size:24px;'>PostID_List count: " . $key_data . '/' . count($PostID_List) . ' - PostID: ' . $item[0] . '</div>';
                    $crawler = new Crawler();
                    $content = File::get($FILE);
                    $crawler->addHtmlContent($content);
                    $update = array();
                    $update['phone'] = $crawler->filter('div:nth-child(4) > div.post_summary_left > div.summary_item_info > a')->text();
                    $update['email'] = $crawler->filter('div:nth-child(4) > div.post_summary_right > div.summary_item_info > a')->text();
                    $update['city'] = $crawler->filter('div:nth-child(3) > a > span')->text();
                    $update['district'] = $crawler->filter('div:nth-child(4) > a > span')->text();
                    $update['area'] = trim($crawler->filter('div:nth-child(5) > div.post_summary_left > div.summary_item_info.summary_item_info_area')->text());
                    $update['price'] = trim($crawler->filter('div:nth-child(5) > div.post_summary_right > div.summary_item_info.summary_item_info_price')->text());
                    $update['txt'] = trim(str_replace('Thông tin mô tả', '', $crawler->filterXPath('//div[@class="block-content-2"]')->text()));
                    $update['postid'] = $item[0];
                    if ($item[0]) {
                        $job_details = (new CrawlerJobUpdateDetails($update))->onQueue('crawler_details')->delay(Carbon::now()->addSeconds(3));
                        dispatch($job_details);
                    }
                }
                ++$key;
                $uri = URL::previous();
                $uri = explode('/', $uri);
                $uri = Business::cms_joinPath($uri[0], $uri[1], $uri[2], $uri[3], $uri[4]);
                echo "<meta http-equiv='refresh' content='0;URL=\"" . Business::cms_joinPath($uri, $key) . "\"' />";
            }
        } else {
            echo 'Finished.';
        }
    }

    public function getCurrency(Request $request)
    {
        if (!empty($request->params)) {
            $param = strtolower($request->params);
            $is_string = preg_replace('!\d+!', '', $param);
            $amount = filter_var($param, FILTER_SANITIZE_NUMBER_INT);
            $str = explode('to', $is_string, 2);

            $PATH = resource_path('/crawler/currency/');
            $FILE = $PATH . $str[0] . '-to-' . $str[1] . '.html';

            if (!empty(Cache::get('FROMtoTO')) && (Cache::get('FROMtoTO')['hash'] != md5($amount . $str[0] . $str[1]))) {
                Cache::forget('FROMtoTO');
                $refresh = URL::current();

                return Redirect::to($refresh);
            }

            $FROMtoTO = Cache::rememberForever('FROMtoTO', function () use ($request, $amount, $str, $PATH, $FILE) {
                $url = 'https://www.xe.com/currencyconverter/convert/?Amount=' . $amount . '&From=' . strtoupper($str[0]) . '&To=' . strtoupper($str[1]);
                $string = Business::cms_loginGetContent($url, $params = ['method' => 'get'], $fields = [], $return = 'html', $headers = []);
                if (!File::exists($PATH)) {
                    mkdir($PATH, 0777, true);
                }
                File::put($FILE, $string);
                $curr = array();
                $crawler = new Crawler();
                $content = File::get($FILE);
                $crawler->addHtmlContent($content);
                if ($crawler) {
                    $curr['hash'] = md5($amount . $str[0] . $str[1]);
                    $curr['currency'] = $crawler->filter('#ucc-container > span.uccAmountWrap > span.uccResultAmount')->text();
                }
                File::Delete($FILE);

                return $curr;
            });

            return response()->json($FROMtoTO);
        }
    }

    public function getImageFromTA(Request $request)
    {
        $ProductSKU_List = Cache::rememberForever('ProductSKU_List', function () {
            return DB::table('5x_furnitures_products')->select('sku')->get()->toArray();
        });
        $tmp = array();
        foreach ($ProductSKU_List as $key => $item) {
            $tmp[$key][] = $item->sku;
        }

        $key_data = isset($request->postid) ? $request->postid : -1;
        if ($key_data == -1 || $key_data >= count($ProductSKU_List)) {
            echo "<div style='width:100%; text-align:center; padding-top:20%; font-size:24px;'>Record Not Found</div>";

            return;
        }

        $item = isset($tmp[$key_data]) ? $tmp[$key_data] : '';
        $key = $key_data;
        if ($item[0] != '') {
            $PATH = resource_path('/crawler/Theodore[-]Alexander/' . $item[0]);

            $file_arr = array(
                '_scale_1',
                '_main_1?$&rect=0,0,1500,1500&scl=2',
                '_more_1?$&rect=0,0,870,870&scl=1',
                '_more_2?$&rect=0,0,870,870&scl=1',
                '_more_3?$&rect=0,0,870,870&scl=1',
                '_more_4?$&rect=0,0,870,870&scl=1',
                '_main_1?$&rect=0,0,2000,2000&scl=1',
                '_main_1?$&rect=2000,0,1000,2000&scl=1',
                '_main_1?$&rect=0,2000,2000,1000&scl=1',
                '_main_1?$&rect=2000,2000,1000,1000&scl=1',
                '_main_1?$ProductListList$',
                '_main_1?$ProductListGrid$',
                '_main_1?$ProductPage$',
            );

            $name_arr = array(
                '_scale_1',
                '_main_1',
                '_more_1',
                '_more_2',
                '_more_3',
                '_more_4',
                '_main_1_14',
                '_main_1_24',
                '_main_1_34',
                '_main_1_44',
                '1_' . $item[0] . '_main_1',
                '2_' . $item[0] . '_main_1',
                '3_' . $item[0] . '_main_1',
            );

            for ($i = 0; $i < count($file_arr); ++$i) {
                $SOURCE = 'http://s7d5.scene7.com/is/image/TheodoreAlexander';
                $FILENAME = $item[0] . $file_arr[$i];
                $LOCALNAME = $name_arr[$i] . '.jpg';

                if (!File::exists($PATH)) {
                    mkdir($PATH, 0777, true);
                }
                if (!File::exists($PATH . '/' . $LOCALNAME)) {
                    $file_headers = @get_headers($SOURCE . '/' . $FILENAME);
                    if ($file_headers[0] === 'HTTP/1.0 200 OK') {
                        file_put_contents($PATH . '/' . $LOCALNAME, file_get_contents($SOURCE . '/' . $FILENAME));
                    }
                }
            }

            ++$key;
            $uri = URL::previous();
            $uri = explode('/', $uri);
            $uri = Business::cms_joinPath($uri[0], $uri[1], $uri[2], $uri[3]);
            echo "<div style='width:100%; text-align:center; padding-top:10%; font-size:24px;'><img src='https://www.netatwork.com/uploads/AAPL/loaders/Teddy%20Bear%20Loading.gif'></div>";
            echo "<div style='width:100%; text-align:center; font-size:24px;'>ProductSKU_List count: " . $key . '/' . count($ProductSKU_List) . ' - SKU: ' . $item[0] . '</div>';
            echo "<meta http-equiv='refresh' content='0;URL=\"" . Business::cms_joinPath($uri, $key) . "\"' />";
        } else {
            echo 'Finished.';
        }
    }
}
