<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use SEO;
use View;
use App\SEOModel as SEOModel;
use App\Models\Domains;
use Session;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DomainController extends Controller {

    public $data = [];

    public function __construct(Request $request) {

        $SEO = Domains::getMetaData();
        SEO::setTitle($SEO['title']);
    }

    public function index(Request $request) {

        $Themes = $request->session()->get('themes');

        $data = $this->data;

        $THEMES_URL = $Themes['isset_url'];

        if ($Themes) {
            return View::make('themes.' . $Themes['themes'] . '.index', compact('data', 'THEMES_URL'));
        } else {
            return View::make('themes.default');
        }
    }

    public function listPlayerData() {

        $resultParent = DB::table('sollyken_group')
                        ->select('sollyken_details.ID as ID', 'sollyken_details.Name as Member', 'sollyken_group.Name as GroupName', 'sollyken_details.ClassA as ClassA', 'sollyken_details.ClassE as ClassE', 'sollyken_details.ClassF as ClassF', 'sollyken_details.ClassG as ClassG', 'sollyken_details.ClassH as ClassH', 'sollyken_details.ClassJ as ClassJ')
                        ->join('sollyken_details', 'sollyken_group.ID', '=', 'sollyken_details.GroupID')
                        ->get()->toArray();
        $data = array(
            'data' => $resultParent
        );

        $Themes = session()->get('themes');
        $THEMES_URL = $Themes['isset_url'];

        return View::make('themes.' . $Themes['themes'] . '.onClass', compact('data', 'THEMES_URL'));
    }

    public function listPlayer() {

        $Themes = session()->get('themes');
        $THEMES_URL = $Themes['isset_url'];

        return View::make('themes.' . $Themes['themes'] . '.listPlayer', compact('data', 'THEMES_URL'));
    }

    public function add() {
        $listGroup = DB::table('sollyken_group')
                        ->orderBy('ID', 'ASC')
                        ->get()->toArray();
        $this->data['listGroup'] = $listGroup;
        return View::make('themes.' . $this->data['Domains']['themes'] . '.add', array('data' => $this->data));
    }

    public function listMember(Request $request) {
        $id = $request->group_id;
        $listMember = DB::table('sollyken_details')
                        ->select('*')
                        ->where('GroupID', $id)
                        ->orderBy('ID', 'ASC')->get()->toArray();
        $this->data['listMember'] = $listMember;

        $Themes = session()->get('themes');
        $THEMES_URL = $Themes['isset_url'];

        return View::make('themes.' . $Themes['themes'] . '.listMember', compact('data', 'THEMES_URL'));
    }

    public function activeMember(Request $request) {
        $ID = $request->ID;
        $Active = $request->Active;
        if ($Active == 1) {
            $status = 0;
        } else {
            $status = 1;
        }
        $arr = array(
            'Active' => $status
        );

        DB::table('sollyken_details')
                ->where('ID', $ID)
                ->update($arr);
        echo "OK";
    }

    public function insertData(Request $request) {
        $GroupName = $request->GroupName;
        if ($GroupName != '') {
            $arr = array(
                'Name' => $GroupName
            );
            DB::table('sollyken_group')->insert($arr);
            echo "OK";
        } else {
            echo "Empty";
        }
    }

    public function deleteGroup(Request $request) {
        $id = $request->ID;
        DB::table('sollyken_details')->where('GroupID', $id)->delete();
        DB::table('sollyken_group')->where('ID', $id)->delete();
    }

    public function updateData(Request $request) {
        $Name = $request->GroupName;
        $ID = $request->ID;
        $arrUpdate = array(
            'Name' => $Name
        );
        DB::table('sollyken_group')
                ->where('ID', $ID)
                ->update($arrUpdate);
        return "OK";
    }

    public function edit(Request $request) {
        $id = $request->segment(2);
        $result = DB::table('sollyken_group')
                ->where('ID', $id)
                ->first();
        if (!empty($result)) {
            $this->data['dataEdit'] = $result->Name;
        } else {
            return redirect('/');
        }
        return View::make('themes.' . $this->data['Domains']['themes'] . '.index', array('data' => $this->data));
    }

    public function addMember(Request $request) {
        $Name = isset($request->Name) ? $request->Name : '';
        $Phone = isset($request->Phone) ? $request->Phone : '';
        $GroupID = isset($request->group) ? $request->group : '';
        if ($Name != '') {
            $arr = array(
                'Name' => $Name,
                'Phone' => $Phone,
                'GroupID' => $GroupID
            );
        }
    }

    public function deleteMember(Request $request) {
        $id = $request->ID;
        DB::table('sollyken_details')->where('ID', $id)->delete();
    }

    public function onClass(Request $request) {
        $ID = $request->ID;
        $Status = $request->Status;
        $Class = $request->Class;
        if ($Status == 1) {
            $status = 0;
        } else {
            $status = 1;
        }
        $arr = array(
            $Class => $status
        );

        DB::table('sollyken_details')
                ->where('ID', $ID)
                ->update($arr);
        echo "OK";
    }

    public function play() {
        $Domains = Domains::getThemes();
        $THEMES_URL = ($Domains['isset_url']) ? $Domains['isset_url'] : config('maps.THEMES_URL.' . strtoupper($Domains['themes']));
        $data['THEMES_URL'] = $THEMES_URL;

        return View::make('themes.' . $Domains['themes'] . '.play', $data);
    }

    public function getCount(Request $request) {
        $class = $request->Class;
        $count = DB::table('sollyken_details')
                ->where('Class' . $class, '=', '1')
                ->count();
        return $count;
    }

    public function sPlays(Request $request) {
        $Domains = Domains::getThemes();
        $THEMES_URL = ($Domains['isset_url']) ? $Domains['isset_url'] : config('maps.THEMES_URL.' . strtoupper($Domains['themes']));
        $data['THEMES_URL'] = $THEMES_URL;

        $class = $request->Class;
        $num = $request->Number;
        if ($class == '') {
            $where = "1";
        } else {
            $where = "sollyken_details.Class" . $class . " =1";
        }
        $result = DB::table('sollyken_details')
                        ->select('sollyken_details.GroupID as GroupID', 'sollyken_details.ID as ID', 'sollyken_group.Name as gName', 'sollyken_details.Name as Member')
                        ->join('sollyken_group', 'sollyken_details.GroupID', '=', 'sollyken_group.ID')
                        ->where("sollyken_details.Class" . $class, '=', 1)
                        ->get()->toArray();
        $arr = array();
        foreach ($result as $key => $aa) {
            $arr[$key]["gName"] = $aa->gName;
            $arr[$key]["Member"] = $aa->Member;
            $arr[$key]["class"] = $aa->GroupID;
        }

        $dataR = new sortteam();
        $dataR->team = array();
        $dataR->get_a_b(0, count($arr), $arr);

        $class = isset($request->Class) ? "Class " . $request->Class : '---';
        $data = array(
            'column' => $num,
            'data' => $dataR->team,
            'class' => $class
        );
        return View::make('themes.' . $Domains['themes'] . '.playTable', $data);
    }

}

ini_set('memory_limit', '9999M');

class sortteam {

    public $team = array();

    public function get_a_b($min, $max, $arr) {
        if (count($arr) < 2) {
            return $this->team;
        }
        $a = rand($min, $max);
        $b = rand($min, $max);
        if (($b - $a) == 1) {
            $this->get_a_b($min, $max, $arr);
        } else {
            if (isset($arr[$a]) && isset($arr[$b])) {
                $classa = $arr[$a]['class'];
                $classb = $arr[$b]['class'];
                $last = isset($this->team[count($this->team) - 1]) ? $this->team[count($this->team) - 1]['class'] : "";
                if ($classa != $classb && $last != $classa) {
                    $this->team[] = $arr[$a];
                    $this->team[] = $arr[$b];
                    unset($arr[$a]);
                    unset($arr[$b]);
                }
                $this->get_a_b($min, $max, $arr);
            } else {
                $this->get_a_b($min, $max, $arr);
            }
        }
    }

}

?>
