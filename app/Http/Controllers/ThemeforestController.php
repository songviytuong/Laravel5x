<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use SEO;
use View;
use Smarty;
use App\SEOModel as SEOModel;
use App\Http\Controllers\BaseController;
use App;
use Session;
use Route;
use URL;
use DB;
use App\Models\Fragment;
use Config;
use Business;
use Goutte\Client;
use GuzzleHttp\Client as GuzzleClient;
use Carbon\Carbon;
use App\Jobs\CrawlerJobUpdateDetails;
use Illuminate\Support\Facades\Redirect;
use Symfony\Component\DomCrawler\Crawler;
use File;
use Cache;

class ThemeforestController extends Controller
{
    public function index(Request $request){
        $category = isset($request->category) ? $request->category : 'art';
        $page = isset($request->page) ? $request->page : 1;

        $link = 'https://themeforest.net/category/site-templates/creative/' . $category;

        if ($page == -1) {
            echo "<div style='width:100%; text-align:center; padding-top:20%; font-size:24px;'>Record Not Found</div>";
            return;
        } else {
            $PATH = resource_path('/crawler/' . $category . '/');
            $FILE = $PATH . $category . '-p' . $request->page . '.html';

            if (!file_exists($PATH)) {
                mkdir($PATH, 0777, true);
            }

            if (!File::exists($FILE)) {
                $str = Business::cms_loginGetContent($link);
                File::put($FILE, $str);
            }

            $page++;

            echo "<meta http-equiv='refresh' content='0;URL=\"" . Business::cms_joinPath($uri, $request->category, $page) . "\"' />";
        }
    }
}
