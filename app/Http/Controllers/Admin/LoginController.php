<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller as BaseController;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use App\User;
use View;
use SEO;
use DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;

class LoginController extends BaseController {

    use AuthenticatesUsers;

    protected $layout = 'admin.layouts.master';

    public function __construct() {
        if (!is_null($this->layout)) {
            $this->layout = View::make($this->layout);
        }
//        $this->middleware('guest')->except('logout');
    }

    public function login() {

        DB::enableQueryLog();
        $user = User::get();
        $queries = DB::getQueryLog();
        foreach ($queries as $i => $query) {
            Log::debug("Query $i: " . json_encode($query));
        }
        SEO::addCss(['admin/css/bootstrap.min.css', '/admin/css/my-login.css']);
        SEO::addJs(['admin/js/jquery.min.js', 'admin/js/popper.js', 'admin/js/bootstrap.min.js', 'admin/js/my-login.js']);

        SEO::pushJs('abcd.js');
        return View::make('admin.login.index');
    }

    public function check() {
        // validate the info, create rules for the inputs
        $rules = array(
            'email' => 'required|email', // make sure the email is an actual email
            'password' => 'required|alphaNum|min:3' // password can only be alphanumeric and has to be greater than 3 characters
        );

// run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules);

// if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return Redirect::to('auth')
                            ->withErrors($validator) // send back all errors to the login form
                            ->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
        } else {

            // create our user data for the authentication
            $userdata = array(
                'email' => Input::get('email'),
                'password' => Input::get('password')
            );

            // attempt to do the login
            if (Auth::attempt($userdata)) {

                // validation successful!
                // redirect them to the secure section or whatever
                // return Redirect::to('secure');
                // for now we'll just echo success (even though echoing in a controller is bad)
                // return Redirect::to('/auth/home');
                return Redirect::to('/administrator');
            } else {

                // validation not successful, send back to form 
                return Redirect::to('/administrator');
            }
        }
    }

    public function logout() {
        if (!Auth::guest()) {
            \Cart::session(Auth::user()->id)->clear();
        }
        Auth::logout(); // log the user out of our application
        return Redirect::to('/administrator'); // redirect the user to the login screen
    }

}
