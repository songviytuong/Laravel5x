<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Cart;
use App\Models\Orders;
use App\Models\OrdersDetails;
use App\Models\Products;
use Illuminate\Support\Facades\DB;

class CartController extends Controller {

    protected $userId = 1;

    public function __construct() {
        
    }

    public function index() {

        if (request()->ajax()) {
            $items = [];

            \Cart::session($this->userId)->getContent()->each(function($item) use (&$items) {
                $items[] = $item;
            });

            return response(array(
                'success' => true,
                'data' => $items,
                'message' => 'cart get items success'
                    ), 200, []);
        } else {
            $Products = Products::get()->toArray();
            return view('cart', array('products' => $Products));
        }
    }

    public function save() {
        $DataCart = Cart::session($this->userId)->getContent();
        if ($DataCart && !(\Cart::isEmpty())) {
            DB::beginTransaction();
            try {
                $code = mt_rand(120000, 999999);
                $OrdersInserted = [
                    'code' => $code,
                    'customer_id' => $this->userId
                ];

                $OrdersLastID = Orders::insertGetId($OrdersInserted);
                foreach ($DataCart as $items) {
                    $CartItems = [
                        'order_id' => $OrdersLastID,
                        'product_id' => $items->id,
                        'quantity' => 1,
                        'price' => $items->price,
                    ];
                    if (OrdersDetails::insert($CartItems)) {
                        \Cart::clear();
                    }
                }
                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();
                #something went wrong
//            $result['message'] = $e->getMessage();
            }
        } else {
            echo "Cart is Empty";
        }
    }

    public function addCart() {
        $id = request('id');
        $name = request('name');
        $price = request('price');
        $qty = request('qty');

        \Cart::session($this->userId)->add($id, $name, $price, $qty);
    }

    public function add() {
        $id = request('id');
        $name = request('name');
        $price = request('price');
        $qty = request('qty');

        $customAttributes = [
            'color_attr' => [
                'label' => 'red',
                'price' => 10.00,
            ],
            'size_attr' => [
                'label' => 'xxl',
                'price' => 15.00,
            ]
        ];

        \Cart::session($this->userId)->add($id, $name, $price, $qty, $customAttributes);
    }

    public function delete($id) {

        \Cart::session($this->userId)->remove($id);

        return response(array(
            'success' => true,
            'data' => $id,
            'message' => "cart item {$id} removed."
                ), 200, []);
    }

    public function details() {

        return response(array(
            'success' => true,
            'data' => array(
                'total_quantity' => \Cart::session($this->userId)->getTotalQuantity(),
                'sub_total' => \Cart::session($this->userId)->getSubTotal(),
                'total' => \Cart::session($this->userId)->getTotal(),
            ),
            'message' => "Get cart details success."
                ), 200, []);
    }

}
