<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use SEO;
use View;
use Smarty;
use App\SEOModel as SEOModel;
use App\Http\Controllers\BaseController;
use App;
use Session;
use Route;
use URL;
use DB;
use App\Models\Fragment;
use Config;
use Business;
use Goutte\Client;
use GuzzleHttp\Client as GuzzleClient;
use Carbon\Carbon;
use App\Jobs\CrawlerJob;
use Illuminate\Support\Facades\Redirect;
use Symfony\Component\DomCrawler\Crawler;
use File;
use Auth;
use Cart;
use Modules\Docs\Entities\TestCases;
use Illuminate\Support\Facades\Artisan;

class TestController extends BaseController {

    public function __construct() {
        
    }

    public function setLocale($locale = 'en') {

        if (!in_array($locale, array_keys(config('app.locales')))) {
            $locale = 'en';
        }
        Session::put('locale', $locale);
        App::setLocale($locale);
        return redirect(URL::previous());
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {


//        echo Business::cms_joinPath('abc', 'def');

        SEO::setTitle(__('hello.welcome'));
        return View::make('default');

//        Smarty::view('post-1',array(
//            'name'  => 'Name Post 1',
//            'bio'   => array(
//                0 => array('attr'=>'三年级'),
//                1 => array('attr'=> '英语、数学、语文'),
//            ),
//            'header'=>'header',
//        ));
//        Smarty::assign('name','L');
//        Smarty::assign('bio',array(
//            0 => array('attr'=>'三年级'),
//            1 => array('attr'=> '英语、数学、语文'),
//        ));
//        Smarty::assign('header','header');
//        Smarty::display('post');
    }

    public function setTranslation($params, Fragment $fragment) {
        #home.hello|en|Hello world
        #home.hello|vi|Xin chào
        #home.hello|ja|こんにちは世界
        #echo trans('home.hello');
        $locales = Config::get('app.locales');
        $params_arr = explode("|", $params);
        if (count($params_arr) == 3) {
            foreach ($locales as $key => $local) {
                #Insert All Locales
            }
            $fragment->key = $params_arr[0];
            $fragment->setTranslation('text', $params_arr[1], $params_arr[2]);
            $fragment->locale = $params_arr[1];
            $fragment->save();
            die('Inserted Successfully :)');
        } else {
            die('Inserted Faild :(');
        }
    }

}
