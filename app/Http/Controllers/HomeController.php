<?php

namespace App\Http\Controllers;

use App\Helpers\Business;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function subd()
    {
        echo 'Hello - ';
        exit();
    }

    public function sentmail()
    {
        $email = request()->email;

        $mail_settings = config('master.MAIL_ACCOUNTS.main_smtp');
        $config_email['from'] = 'songviytuong@gmail.com';
        $config_email['from_name'] = 'Lê Bình';
        $config_email['to'] = 'songviytuong@gmail.com';
        $config_email['subject'] = config('master.EMAIL_TEMPLATES.Temp1.Subject');
        $config_email['params'] = ['author' => $email];
        $config_email['template'] = config('master.EMAIL_TEMPLATES.Temp1.BodyContent');
        $config_email['cc'] = null;
        $config_email['bcc'] = null;
        $config_email['pathToFile'] = null;

        Business::sendEmail($config_email, $mail_settings);
    }
}
