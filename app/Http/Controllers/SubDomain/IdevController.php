<?php

namespace App\Http\Controllers\SubDomain;

use SEO;
use View;
use Business;
use App\Models\Domains;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IdevController extends Controller {
    public $data = [];
    public function __construct(Request $request) {

        $SEO = Domains::getMetaData();
        SEO::setTitle($SEO['title']);
        return View::share(array('data' => $this->data));
    }

    public function index(Request $request){
        $Themes = $request->session()->get('themes');
        $data['root_url'] = Business::cms_getProtocol();
        if ($Themes) {
            $THEMES_URL = $Themes['isset_url'];
            return View::make('themes.' . $Themes['themes'] . '.index', compact('data', 'THEMES_URL'));
        } else {
            return View::make('themes.default');
        }
    }

    public function webmaster(Request $request){
        $Themes = $request->session()->get('themes');
        $data['root_url'] = Business::cms_getProtocol();
        if ($Themes) {
            $THEMES_URL = $Themes['isset_url'];
            return View::make('themes.' . $Themes['themes'] . '.webmaster', compact('data', 'THEMES_URL'));
        } else {
            return View::make('themes.default');
        }
    }

    public function getPortfolio(Request $request){
        $Themes = $request->session()->get('themes');
        $data['root_url'] = Business::cms_getProtocol();
        if ($Themes) {
            $THEMES_URL = $Themes['isset_url'];
            return View::make('themes.' . $Themes['themes'] . '.portfolio-detail', compact('data', 'THEMES_URL'));
        } else {
            return View::make('themes.default');
        }
    }
}
