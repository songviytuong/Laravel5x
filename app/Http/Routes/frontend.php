<?php

/*
|--------------------------------------------------------------------------
| Front-End Routes
|--------------------------------------------------------------------------
 */

use App\Jobs\SendEmailJob;
use Carbon\Carbon;

Route::group(array('domain' => Request::server('HTTP_HOST')), function () {
    $protocol = Business::cms_getProtocol();
    if ($protocol == env('APP_URL')) {
        Route::get('/', array('uses' => 'TestController@index'));
        Route::get('/{locale}/set-language', array('as' => 'setLocale', 'uses' => 'TestController@setLocale'));
        Route::get('/translates/{params}', array('uses' => 'TestController@setTranslation'));

        Route::get('/home', array('uses' => 'HomeController@index'));

        Route::get('/sentmail/{email?}', array('uses' => 'HomeController@sentmail'))->defaults('email', 'songviytuong@gmail.com');

        //Route::get('/{keyword}', function ($keyword) {
        //    fopen(resource_path('views/' . $keyword . '.blade.php'), 'w');
        //});

        Route::get('/crawler/doodles/{year?}/{month?}', array('uses' => 'CrawlerController@doodles'))->defaults('month', 1);
        Route::get('/crawler/doodlesDetail/{id?}', array('uses' => 'CrawlerController@doodlesDetail'))->defaults('id', 0);
        Route::get('/crawler/doodlesGetItem/{year?}/{month?}', array('uses' => 'CrawlerController@doodlesGetItem'))->defaults('year', 2018);
        Route::get('/crawler/doodlesRemoveIMG', array('uses' => 'CrawlerController@doodlesRemoveIMG'));

        Route::get('/crawler/details/{postid?}', array('uses' => 'CrawlerController@details'))->defaults('postid', 0);
        Route::get('/crawler/{alias?}/{id?}', array('uses' => 'CrawlerController@index'))->defaults('alias', 0);
        Route::get('/crawler-ta/{postid?}', array('uses' => 'CrawlerController@getImageFromTA'))->defaults('postid', 0);
        Route::get('/crawler-currency/{params?}', array('uses' => 'CrawlerController@getCurrency'))->defaults('params', '');

        Route::get('/themeforest/{category?}/{page?}', array('uses' => 'ThemeforestController@index'))->defaults('page', 1);

        Route::get('sendEmail', function () {
            $job = (new SendEmailJob())->onQueue('emails')->delay(Carbon::now()->addSeconds(1));
            dispatch($job);

            return 'Email send is property';
        });
    }
});

Route::group(['middleware' => ['subdomain']], function () {
    Route::get('/', 'DomainController@index');

    $thisdomain = Request::server('HTTP_HOST');
    switch ($thisdomain) {
        case 'hrc-boat.local':
            Route::group(['namespace' => 'SubDomain'], function () {
                Route::get('/', array('uses' => 'HrcBoatController@index'));
                Route::get('/listPlayer', array('uses' => 'HrcBoatController@listPlayer'));
                Route::get('/listPlayerData', array('uses' => 'HrcBoatController@listPlayerData'));
                Route::get('/play', array('uses' => 'HrcBoatController@play'));
                Route::get('/getCount', array('uses' => 'HrcBoatController@getCount'));
                Route::post('/splays', array('uses' => 'HrcBoatController@sPlays'));
                Route::post('/onClass', array('uses' => 'HrcBoatController@onClass'));
                Route::get('/add', array('uses' => 'HrcBoatController@add'));
                Route::get('/deleteMember', array('uses' => 'HrcBoatController@deleteMember'));
                Route::get('/edit/{id?}', array('uses' => 'HrcBoatController@edit'));
                Route::get('/listMember', array('uses' => 'HrcBoatController@listMember'));
                Route::post('/updateData', array('uses' => 'HrcBoatController@updateData'));
                Route::post('/addMember', array('uses' => 'HrcBoatController@addMember'));
                Route::post('/activeMember', array('uses' => 'HrcBoatController@activeMember'));
                Route::post('/insertData', array('uses' => 'HrcBoatController@insertData'));
                Route::post('/deleteGroup', array('uses' => 'HrcBoatController@deleteGroup'));
            });
            break;
        case 'sub1.laravel5x.local':
            Route::group(['namespace' => 'SubDomain'], function () {
                Route::get('/', array('uses' => 'IdevController@index'))->name('idev.index');
                Route::get('/webmaster', array('uses' => 'IdevController@webmaster'))->name('idev.webmaster');
                Route::get('/portfolio-detail', array('uses' => 'IdevController@getPortfolio'))->name('idev.webmaster.portfolio.detail');
            });
            break;
        case 'resume.local':

            break;
    }
});
