<?php

Route::group(['prefix' => 'cart'], function() {
    Route::get('/', 'CartController@index')->name('cart.index');
    Route::get('/save', 'CartController@save')->name('cart.save');
    Route::post('/', 'CartController@add')->name('cart.add');
    Route::post('/addCart', 'CartController@addCart')->name('cart.add');
    Route::get('/details', 'CartController@details')->name('cart.details');
    Route::delete('/{id}', 'CartController@delete')->name('cart.delete');
});

Route::group(['prefix' => 'wishlist'], function() {
    Route::get('/', 'WishListController@index')->name('wishlist.index');
    Route::post('/', 'WishListController@add')->name('wishlist.add');
    Route::get('/details', 'WishListController@details')->name('wishlist.details');
    Route::delete('/{id}', 'WishListController@delete')->name('wishlist.delete');
});
