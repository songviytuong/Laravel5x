<?php

/*
  |--------------------------------------------------------------------------
  | Back-End Routes
  |--------------------------------------------------------------------------
 */

Route::group(['prefix' => 'administrator', 'namespace' => 'Admin'], function () {

    Route::get('/', 'DashboardController@index');
    Route::get('/login', 'LoginController@login');
    Route::post('/check', 'LoginController@check');
    Route::get('/logout', 'LoginController@logout');

    Route::post('/custom/ajax', 'AjaxController@customLogin');

//    Route::get('/views/{name}', function($name) {
//        return View('admin.layouts.'.$name);
//    });
//
//    Route::any('{path?}', function () {
//        return View('admin.layouts.master');
//    })->where("path", ".+");
});

