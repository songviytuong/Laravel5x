<?php
/*
  |--------------------------------------------------------------------------
  | Auth Routes
  |--------------------------------------------------------------------------
 */
Route::group(['prefix' => 'auth', 'namespace' => 'Auth'], function () {
    Route::get('/', array('uses' => 'LoginController@index'));
});