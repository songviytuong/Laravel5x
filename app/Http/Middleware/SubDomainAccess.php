<?php

namespace App\Http\Middleware;

use App\Models\Domains;
use Closure;

class SubDomainAccess {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        $domain = $request->server('HTTP_HOST');
        // check if sub domain exists, replace with your own conditional check
        if (!Domains::where('domain', $domain)->first()) {
            $request->session()->forget('domain');
            return abort(404); // or redirect to your homepage route.
        } else {
            $request->session()->put('domain', $domain);
            $themes = Domains::getThemes(1,$domain,0);
            $request->session()->put('themes', $themes);
        }
        return $next($request);
    }

}
