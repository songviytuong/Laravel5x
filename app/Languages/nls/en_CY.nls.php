<?php

#Native language name
$nls['language']['en_CY'] = 'Welsh';
$nls['englishlang']['en_CY'] = 'Welsh';

#Possible aliases for language
$nls['alias']['cy'] = 'en_CY';
$nls['alias']['welsh'] = 'en_CY';
$nls['alias']['en_CY.ISO8859-1'] = 'en_CY';

#Possible locale for language
$nls['locale']['en_CY'] = 'en_CY,en_CY.utf8,en_CY.utf-8,en_CY.UTF-8,en_CY@pound,welsh,Welsh_Wales.1252';

#Encoding of the language
$nls['encoding']['en_CY'] = 'UTF-8';

#Location of the file(s)
$nls['file']['en_CY'] = array(dirname(__FILE__) . '/en_CY/admin.inc.php');

#Language setting for HTML area
# Only change this when translations exist in HTMLarea and plugin dirs
# (please send language files to HTMLarea development)
$nls['htmlarea']['en_CY'] = 'cy';
?>
