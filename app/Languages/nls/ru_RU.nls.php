<?php

#Native language name
#NOTE: Enocde me with HTML escape chars like &#231; or &ntilde; so I work on every page
$nls['language']['ru_RU'] = 'Русский';
$nls['englishlang']['ru_RU'] = 'Russian';

#Possible aliases for language
$nls['alias']['ru'] = 'ru_RU';
$nls['alias']['russian'] = 'ru_RU';
$nls['alias']['rus'] = 'ru_RU';

#Possible locale for language
$nls['locale']['ru_RU'] = 'ru_RU,ru_RU.cp1251,ru_RU.CP1251,ru_RU.CP866,ru_RU.koi8r,ru_RU.utf8,ru_RU.iso88595,russian,Russian_Russia.1251';

#Encoding of the language
$nls['encoding']['ru_RU'] = "utf-8";

#Location of the file(s)
$nls['file']['ru_RU'] = array(dirname(__FILE__) . '/ru_RU/admin.inc.php');

#Language setting for HTML area
# Only change this when translations exist in HTMLarea and plugin dirs
# (please send language files to HTMLarea development)

$nls['htmlarea']['ru_RU'] = 'ru';
?>
