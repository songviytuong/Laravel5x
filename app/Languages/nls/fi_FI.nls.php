<?php

#Native language name
$nls['language']['fi_FI'] = 'Suomi';
$nls['englishlang']['fi_FI'] = 'Finnish';

#Possible aliases for language
$nls['alias']['fi'] = 'fi_FI';
$nls['alias']['finnish'] = 'fi_FI';
$nls['alias']['fin'] = 'fi_FI';
$nls['alias']['fi_FI.ISO8859-1'] = 'fi_FI';
$nls['alias']['fi_FI.ISO8859-15'] = 'fi_FI';

#Possible locale for language
$nls['locale']['fi_FI'] = 'fi_FI,fi_FI.utf8,fi_FI.utf-8,fi_FI.UTF-8,fi_FI@euro,finnish,Finnish_Finland.1252';

#Encoding of the language
$nls['encoding']['fi_FI'] = 'UTF-8';

#Location of the file(s)
$nls['file']['fi_FI'] = array(dirname(__FILE__) . '/fi_FI/admin.inc.php');

#Language setting for HTML area
# Only change this when translations exist in HTMLarea and plugin dirs
# (please send language files to HTMLarea development)

$nls['htmlarea']['fi_FI'] = 'en';
?>
