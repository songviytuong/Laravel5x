<?php

#Native language name
$nls['language']['sl_SI'] = 'Slovensko';
$nls['englishlang']['sl_SI'] = 'Slovenian';

#Possible aliases for language
$nls['alias']['sl'] = 'sl_SI';
$nls['alias']['slovenian'] = 'sl_SI';
$nls['alias']['slo'] = 'sl_SI';
$nls['alias']['sl_SI'] = 'sl_SI';
$nls['alias']['sl_SI.WINDOWS-1250'] = 'sl_SI';
$nls['alias']['sl_SI.ISO8859-2'] = 'sl_SI';

#Possible locale for language
$nls['locale']['sl_SI'] = 'sl_SI,sl_SI.utf8,sl_SI.UTF-8,sl_SI.utf-8,slovenian,Slovenian_Slovenia.1250';

#Encoding of the language
$nls['encoding']['sl_SI'] = 'UTF-8';

#Location of the file(s)
$nls['file']['sl_SI'] = array(dirname(__FILE__) . '/sl_SI/admin.inc.php');

#Language setting for HTML area
# Only change this when translations exist in HTMLarea and plugin dirs
# (please send language files to HTMLarea development)

$nls['htmlarea']['sl_SI'] = 'sl';
?>
