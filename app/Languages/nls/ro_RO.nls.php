<?php

#Native language name
$nls['language']['ro_RO'] = 'Română';
$nls['englishlang']['ro_RO'] = 'Romanian';

#Possible aliases for language
$nls['alias']['ro'] = 'ro_RO';
$nls['alias']['Română'] = 'ro_RO';
$nls['alias']['rom'] = 'ro_RO';
$nls['alias']['romanian'] = 'ro_RO';
$nls['alias']['ro_RO.ISO8859-2'] = 'ro_RO';
$nls['alias']['ro_RO.ISO8859-16'] = 'ro_RO';

#Possible locale for language
$nls['locale']['ro_RO'] = 'ro_RO,ro_RO.utf8,ro_RO.UTF-8,ro_RO.utf-8,romanian,Romanian_Romania.1250';

#Encoding of the language
$nls['encoding']['ro_RO'] = 'UTF-8';

#Location of the file(s)
$nls['file']['ro_RO'] = array(dirname(__FILE__) . '/ro_RO/admin.inc.php');

#Language setting for HTML area
# Only change this when translations exist in HTMLarea and plugin dirs
# (please send language files to HTMLarea development)

$nls['htmlarea']['ro_RO'] = 'ro';
?>
