<?php

#Native language name
$nls['language']['hu_HU'] = 'Magyar';
$nls['englishlang']['hu_HU'] = 'Hungarian';

#Possible aliases for language
$nls['alias']['hu'] = 'hu_HU';
$nls['alias']['hungarian'] = 'hu_HU';
$nls['alias']['magyar'] = 'hu_HU';
$nls['alias']['hu_HU'] = 'hu_HU';
$nls['alias']['hu_HU.WINDOWS-1250'] = 'hu_HU';
$nls['alias']['hu_HU.ISO8859-2'] = 'hu_HU';

#Possible locale for language
$nls['locale']['hu_HU'] = 'hu_HU,hu_HU.utf8,hu_HU.utf-8,hu_HU.UTF-8,hungarian,Hungarian_Hungary.1250';

#Encoding of the language
$nls['encoding']['hu_HU'] = 'UTF-8';

#Location of the file(s)
$nls['file']['hu_HU'] = array(dirname(__FILE__) . '/hu_HU/admin.inc.php');

#Language setting for HTML area
# Only change this when translations exist in HTMLarea and plugin dirs
# (please send language files to HTMLarea development)

$nls['htmlarea']['hu_HU'] = 'hu';
?>
