<?php

#Native language name
$nls['language']['et_EE'] = 'Eesti';
$nls['englishlang']['et_EE'] = 'Estonian';

#Possible aliases for language
$nls['alias']['et'] = 'et_EE';
$nls['alias']['estonian'] = 'et_EE';
$nls['alias']['eti'] = 'et_EE';
$nls['alias']['et_EE.ISO8859-1'] = 'et_EE';
$nls['alias']['et_EE.ISO8859-15'] = 'et_EE';
$nls['alias']['et_EE.UTF-8'] = 'et_EE';

#Possible locale for language
$nls['locale']['et_EE'] = 'et_EE,et_EE.utf-8,et_EE.UTF-8,et_EE.iso885915,et_EE.utf8,estonian,Estonian_Estonia.1257';

#Encoding of the language
$nls['encoding']['et_EE'] = "UTF-8";

#Location of the file(s)
$nls['file']['et_EE'] = array(dirname(__FILE__) . '/et_EE/admin.inc.php');

#Language setting for HTML area
# Only change this when translations exist in HTMLarea and plugin dirs
# (please send language files to HTMLarea development)

$nls['htmlarea']['et_EE'] = 'en';
?>
