<?php

#Native language name
$nls['language']['de_DE'] = 'Deutsch';
$nls['englishlang']['de_DE'] = 'German';

#Possible aliases for language
$nls['alias']['de'] = 'de_DE';
$nls['alias']['deutsch'] = 'de_DE' ;
$nls['alias']['deu'] = 'de_DE' ;
$nls['alias']['de_DE.ISO8859-1'] = 'de_DE' ;

#Possible locale for language
$nls['locale']['de_DE'] = 'de_DE.utf8,de_DE.utf-8,de_DE.UTF-8,de_DE,de_DE@euro,deu,german,German_Germany.1252';

#Encoding of the language
$nls['encoding']['de_DE'] = 'UTF-8';

#Location of the file(s)
$nls['file']['de_DE'] = array(dirname(__FILE__).'/de_DE/admin.inc.php');

#Language setting for HTML area
# Only change this when translations exist in HTMLarea and plugin dirs
# (please send language files to HTMLarea development)

$nls['htmlarea']['de_DE'] = 'de';
?>
