<?php

#Native language name
$nls['language']['es_ES'] = 'Espa&ntilde;ol';
$nls['englishlang']['es_ES'] = 'Spanish';

#Possible aliases for language
$nls['alias']['es'] = 'es_ES';
$nls['alias']['espa&ntilde;ol'] = 'es_ES';
$nls['alias']['eng'] = 'es_ES';
$nls['alias']['es_AR'] = 'es_ES';
$nls['alias']['es_PE'] = 'es_ES';
$nls['alias']['es_MX'] = 'es_ES';
$nls['alias']['es_US.ISO8859-1'] = 'es_ES';

#Possible locale for language
$nls['locale']['es_ES'] = 'es_ES.utf8,es_ES.utf-8,es_ES.UTF-8,es_ES,es_ES@euro,spanish,Spanish_Spain.1252';

#Encoding of the language
$nls['encoding']['es_ES'] = 'UTF-8';

#Location of the file(s)
$nls['file']['es_ES'] = array(dirname(__FILE__) . '/es_ES/admin.inc.php');

#Language setting for HTML area
# Only change this when translations exist in HTMLarea and plugin dirs
# (please send language files to HTMLarea development)

$nls['htmlarea']['es_ES'] = 'es';
?>
