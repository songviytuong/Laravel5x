<?php

#Native language name
#NOTE: Enocde me with HTML escape chars like &#231; or &ntilde; so I work on every page
$nls['language']['ar_AR'] = 'العربية';
$nls['englishlang']['ar_AR'] = 'Arabic';

#Possible aliases for language
$nls['alias']['ar'] = 'ar_AR';
$nls['alias']['arabic'] = 'ar_AR';
$nls['alias']['arb'] = 'ar_AR';
$nls['alias']['ar_SY'] = 'ar_AR';
$nls['alias']['ar_SA'] = 'ar_AR';
$nls['alias']['ar_AR.ISO8859-6'] = 'ar_AR';
$nls['alias']['ar_AR.windows-1256'] = 'ar_AR';

#Possible locale for language
$nls['locale']['ar_AR'] = 'ar_AR,ar_AR.utf8,ar_AR.utf-8,ar_AR.UTF-8,ar_AE.utf-8,ar_AE.UTF-8,arabic,Arabic_Saudi Arabia.1256';

#Encoding of the language
$nls['encoding']['ar_AR'] = 'UTF-8';

#Direction of the language
$nls['direction']['ar_AR'] = 'rtl';

#Location of the file(s)
$nls['file']['ar_AR'] = array(dirname(__FILE__).'/ar_AR/admin.inc.php');

#Language setting for HTML area
# Only change this when translations exist in HTMLarea and plugin dirs
# (please send language files to HTMLarea development)
$nls['htmlarea']['ar_AR'] = 'ar';
?>
