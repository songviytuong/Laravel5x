<?php

#Native language name
$nls['language']['id_ID'] = 'Bahasa Indonesia';
$nls['englishlang']['id_ID'] = 'Indonesian';

#Possible aliases for language
$nls['alias']['id'] = 'id_ID';
$nls['alias']['ind'] = 'id_ID';
$nls['alias']['id_ID'] = 'id_ID';
$nls['alias']['id_ID.ISO8859-15'] = 'id_ID';

#Possible locale for language
$nls['locale']['id_ID'] = 'id_ID,id_ID.utf8,id_ID.utf-8,id_ID.UTF-8,indonesian,Indonesian_indonesia.1252';

#Encoding of the language
$nls['encoding']['id_ID'] = 'UTF-8';

#Location of the file(s)
$nls['file']['id_ID'] = array(dirname(__FILE__) . '/id_ID/admin.inc.php');

#Language setting for HTML area
# Only change this when translations exist in HTMLarea and plugin dirs
# (please send language files to HTMLarea development)

$nls['htmlarea']['id_ID'] = 'en';
?>
