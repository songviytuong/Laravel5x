<?php

#Native language name
$nls['language']['sv_SE'] = 'Svenska';
$nls['englishlang']['sv_SE'] = 'Swedish';

#Possible aliases for language
$nls['alias']['sv'] = 'sv_SE';
$nls['alias']['svenska'] = 'sv_SE';
$nls['alias']['sve'] = 'sv_SE';
$nls['alias']['sv_SE'] = 'sv_SE';
$nls['alias']['sv_SE.ISO8859-1'] = 'sv_SE';
$nls['alias']['sv_SE.ISO8859-15'] = 'sv_SE';

#Possible locale for language
$nls['locale']['sv_SE'] = 'sv_SE,sv_SE.utf8,sv_SE.UTF-8,sv_SE.utf-8,sv_SE.iso885915,swedish,Swedish_Sweden.1252';

#Encoding of the language
$nls['encoding']['sv_SE'] = 'UTF-8';

#Location of the file(s)
$nls['file']['sv_SE'] = array(dirname(__FILE__) . '/sv_SE/admin.inc.php');

#Language setting for HTML area
# Only change this when translations exist in HTMLarea and plugin dirs
# (please send language files to HTMLarea development)

$nls['htmlarea']['sv_SE'] = 'en';
?>
