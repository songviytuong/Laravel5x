<?php

#Native language name
$nls['language']['zh_TW'] = '&#32321;&#39636;&#20013;&#25991;';
$nls['englishlang']['zh_TW'] = 'Traditional Chinese';

#Possible aliases for language
$nls['alias']['chinese'] = 'zh_TW';
$nls['alias']['zh_TW.Big5'] = 'zh_TW';

#Possible locale for language
$nls['locale']['zh_TW'] = 'zh_TW,zh_TW.euctw,zh_TW.utf8,zh_TW.big5,zh_TW.Big5,zh_TW.utf8,zh_TW.UTF-8,chinese-traditional,Chinese_Taiwan.950';

#Encoding of the language
$nls['encoding']['zh_TW'] = 'UTF-8';

#Location of the file(s)
$nls['file']['zh_TW'] = array(dirname(__FILE__) . '/zh_TW/admin.inc.php');

$nls['htmlarea']['zh_TW'] = 'en';
?>
