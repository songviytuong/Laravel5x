<?php

#Native language name
$nls['language']['fa_IR'] = 'فارسی';
$nls['englishlang']['fa_IR'] = 'Farsi';

#Possible aliases for language
$nls['alias']['fa'] = 'fa_IR';
$nls['alias']['farsi'] = 'fa_IR';
$nls['alias']['fa_IR.UTF-8'] = 'fa_IR';

#Possible locale for language
$nls['locale']['fa_IR'] = 'fa_IR,fa_IR.utf-8,fa_IR.utf-8,fa_IR.UTF-8,farsi,Farsi_Iran.1256';

#Encoding of the language
$nls['encoding']['fa_IR'] = 'UTF-8';

#Direction of the language
$nls['direction']['fa_IR'] = 'rtl';

#Location of the file(s)
$nls['file']['fa_IR'] = array(dirname(__FILE__) . '/fa_IR/admin.inc.php');

#Language setting for HTML area
# Only change this when translations exist in HTMLarea and plugin dirs
# (please send language files to HTMLarea development)

$nls['htmlarea']['fa_IR'] = 'fa';
?>
