<?php

#Native language name
$nls['language']['pl_PL'] = 'Polski';
$nls['englishlang']['pl_PL'] = 'Polish';

#Possible aliases for language
$nls['alias']['pl'] = 'pl_PL';
$nls['alias']['polish'] = 'pl_PL';
$nls['alias']['pl_PL.ISO8859-2'] = 'pl_PL';

#Possible locale for language
$nls['locale']['pl_PL'] = 'pl_PL.utf8,pl_PL.UTF-8,pl_PL.utf.8,pl_PL,polish,Polish_Poland.1250';

#Encoding of the language
$nls['encoding']['pl_PL'] = 'UTF-8';

#Location of the file(s)
$nls['file']['pl_PL'] = array(dirname(__FILE__) . '/pl_PL/admin.inc.php');

#Language setting for HTML area
# Only change this whpl translations exist in HTMLarea and plugin dirs
# (please spld language files to HTMLarea developmplt)

$nls['htmlarea']['pl_PL'] = 'pl';
?>
