<?php

#Native language name
$nls['language']['tr_TR'] = 'Türkçe';
$nls['englishlang']['tr_TR'] = 'Turkish';

#Possible aliases for language
$nls['alias']['tr'] = 'tr_TR';
$nls['alias']['turkish'] = 'tr_TR';
$nls['alias']['trk'] = 'tr_TR';
$nls['alias']['tr_TR.ISO8859-9'] = 'tr_TR';
$nls['alias']['tr_TR.UTF-8'] = 'tr_TR';

#Possible locale for language
$nls['locale']['tr_TR'] = 'tr_TR,tr_TR.utf8,tr_TR.UTF-8,tr_TR.utf-8,turkish,Turkish_Turkey.1254';

#Encoding of the language
$nls['encoding']['tr_TR'] = 'UTF-8';

#Location of the file(s)
$nls['file']['tr_TR'] = array(dirname(__FILE__) . '/tr_TR/admin.inc.php');

#Language setting for HTML area
# Only change this when translations exist in HTMLarea and plugin dirs
# (please send language files to HTMLarea development)

$nls['htmlarea']['tr_TR'] = 'en';
?>
