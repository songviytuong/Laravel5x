<?php

#Native language name
$nls['language']['it_IT'] = 'Italiano';
$nls['englishlang']['it_IT'] = 'Italian';

#Possible aliases for language
$nls['alias']['it'] = 'it_IT';
$nls['alias']['italiano'] = 'it_IT';
$nls['alias']['ita'] = 'it_IT';
$nls['alias']['italian'] = 'it_IT';
$nls['alias']['it_IT.ISO8859-1'] = 'it_IT';
$nls['alias']['it_IT.ISO8859-15'] = 'it_IT';

#Possible locale for language
$nls['locale']['it_IT'] = 'it_IT,it_IT.utf8,it_IT.utf-8,it_IT.UTF-8,it_IT@euro,italian,Italian_Italy.1252';

#Encoding of the language
$nls['encoding']['it_IT'] = 'UTF-8';

#Location of the file(s)
$nls['file']['it_IT'] = array(dirname(__FILE__) . '/it_IT/admin.inc.php');

#Language setting for HTML area
# Only change this when translations exist in HTMLarea and plugin dirs
# (please send language files to HTMLarea development)

$nls['htmlarea']['it_IT'] = 'it';
?>
