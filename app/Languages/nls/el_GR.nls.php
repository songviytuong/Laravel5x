<?php

#Native language name
#NOTE: Encode me with HTML escape chars like &#231; or &ntilde; so I work on every page
$nls['language']['el_GR'] = 'Greek';
$nls['englishlang']['el_GR'] = 'Greek';

#Possible aliases for language
$nls['alias']['gr'] = 'el_GR';
$nls['alias']['greek'] = 'el_GR';
$nls['alias']['hellenic'] = 'el_GR';
$nls['alias']['el'] = 'el_GR';
$nls['alias']['el_GR.ISO8859-7'] = 'el_GR';

#Possible locale for language
$nls['locale']['el_GR'] = 'el_GR,el_GR.utf8,el_GR.utf-8,el_GR.UTF-8,greek,Greek_Greece.1253';

#Encoding of the language
$nls['encoding']['el_GR'] = "UTF-8";

#Location of the file(s)
$nls['file']['el_GR'] = array(dirname(__FILE__) . '/el_GR/admin.inc.php');

#Language setting for HTML area
# Only change this when translations exist in HTMLarea and plugin dirs
# (please send language files to HTMLarea development)

$nls['htmlarea']['el_GR'] = 'gr';
?>