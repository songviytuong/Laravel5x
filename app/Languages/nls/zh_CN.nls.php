<?php

#Native language name
$nls['language']['zh_CN'] = '&#31616;&#20307;&#20013;&#25991;';
$nls['englishlang']['zh_CN'] = 'Simplified Chinese';

#Possible aliases for language
$nls['alias']['zh_CN.EUC'] = 'zh_CN';
$nls['alias']['chinese_gb2312'] = 'zh_CN';

#Possible locale for language
$nls['locale']['zh_CN'] = 'zh_CN,zh_CN.utf8,zh_CN.UTF-8,zh_CN.eucCN,zh_CN.gbk,zh_CN.gb18030,zh_CN.gbk,chinese,chinese-simplified,Chinese_China.936';

#Encoding of the language
$nls['encoding']['zh_CN'] = 'UTF-8';

#Location of the file(s)
$nls['file']['zh_CN'] = array(dirname(__FILE__) . '/zh_CN/admin.inc.php');

$nls['htmlarea']['zh_CN'] = 'en';
?>
