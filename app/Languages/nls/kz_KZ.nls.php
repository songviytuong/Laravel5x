<?php

#Native language name
$nls['language']['kz_KZ'] = 'Kazakh';
$nls['englishlang']['kz_KZ'] = 'Kazakh';

#Possible aliases for language
$nls['alias']['kz'] = 'kz_KZ';
$nls['alias']['kazak'] = 'kz_KZ';
$nls['alias']['kazakh'] = 'kz_KZ';

#Possible locale for language
$nls['locale']['kz_KZ'] = 'kk_KZ,kk_KZ.utf-8,kk_KZ.UTF-8,kk_KZ.PT154,kazakh';

#Encoding of the language
$nls['encoding']['kz_KZ'] = 'UTF-8';

#Location of the file(s)
$nls['file']['kz_KZ'] = array(dirname(__FILE__) . '/kz_KZ/admin.inc.php');

#Language setting for HTML area
$nls['htmlarea']['kz_KZ'] = 'en';
?>
