<?php

#Native language name
$nls['language']['ca_ES'] = 'Catal&agrave;';
$nls['englishlang']['ca_ES'] = 'Catalan';

#Possible aliases for language
$nls['alias']['ca'] = 'ca_ES';
$nls['alias']['cat'] = 'ca_ES' ;
$nls['alias']['catal&agrave;'] = 'ca_ES' ;
$nls['alias']['ca_ES.ISO8859-1'] = 'ca_ES' ;

#Possible locale for language
$nls['locale']['ca_ES'] = 'ca_ES,ca_ES.utf8,ca_ES.utf8@valencia,ca_ES.utf-8,ca_ES.UTF-8,ca_ES@euro,ca_ES@valencia,catalan,Catalan_Spain.1252';

#Encoding of the language
$nls['encoding']['ca_ES'] = 'UTF-8';

#Location of the file(s)
$nls['file']['ca_ES'] = array(dirname(__FILE__).'/ca_ES/admin.inc.php');

#Language setting for HTML area
# Only change this when translations exist in HTMLarea and plugin dirs
# (please send language files to HTMLarea development)

$nls['htmlarea']['ca_ES'] = 'en';
?>
