<?php

#Native language name
$nls['language']['sr_YU'] = 'Srpski';
$nls['englishlang']['sr_YU'] = 'Serbian';

#Possible aliases for language
$nls['alias']['sr'] = 'sr_YU';
$nls['alias']['serbian'] = 'sr_YU';
$nls['alias']['srpski'] = 'sr_YU';
$nls['alias']['sr_YU'] = 'sr_YU';
$nls['alias']['sr_YU.WINDOWS-1250'] = 'sr_YU';
$nls['alias']['sr_YU.ISO8859-2'] = 'sr_YU';

#Possible locale for language
$nls['locale']['sr_YU'] = 'sr_YU,sr_YU.utf-8,sr_YU.UTF-8,sr_RS.sr_RS.utf-8,serbian';

#Encoding of the language
$nls['encoding']['sr_YU'] = 'UTF-8';

#Location of the file(s)
$nls['file']['sr_YU'] = array(dirname(__FILE__) . '/sr_YU/admin.inc.php');

#Language setting for HTML area
# Only change this when translations exist in HTMLarea and plugin dirs
# (please send language files to HTMLarea development)

$nls['htmlarea']['sr_YU'] = 'sr';
?>
