<?php

#Native language name
$nls['language']['hr_HR'] = 'Hrvatski';
$nls['englishlang']['hr_HR'] = 'Croatian';

#Possible aliases for language
$nls['alias']['hr'] = 'hr_HR';
$nls['alias']['croatian'] = 'hr_HR';
$nls['alias']['hrvatski'] = 'hr_HR';
$nls['alias']['hr_HR'] = 'hr_HR';
$nls['alias']['hr_HR.WINDOWS-1250'] = 'hr_HR';
$nls['alias']['hr_HR.ISO8859-2'] = 'hr_HR';

#Possible locale for language
$nls['locale']['hr_HR'] = 'hr_HR,hr_HR.utf8,hr_HR.utf-8,hr_HR.UTF-8,croatian,Croatian_Croatia.1250';

#Encoding of the language
$nls['encoding']['hr_HR'] = 'UTF-8';

#Location of the file(s)
$nls['file']['hr_HR'] = array(dirname(__FILE__) . '/hr_HR/admin.inc.php');

#Language setting for HTML area
# Only change this when translations exist in HTMLarea and plugin dirs
# (please send language files to HTMLarea development)

$nls['htmlarea']['hr_HR'] = 'hr';
?>
