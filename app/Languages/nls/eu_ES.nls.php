<?php

#Native language name
$nls['language']['eu_ES'] = 'Euskara';
$nls['englishlang']['eu_ES'] = 'Basque';

#Possible aliases for language
$nls['alias']['eu'] = 'eu_ES';
$nls['alias']['basque'] = 'eu_ES';
$nls['alias']['baq'] = 'eu_ES';
$nls['alias']['eus'] = 'eu_ES';
$nls['alias']['eu_ES'] = 'eu_ES';
$nls['alias']['eu_ES.ISO8859-1'] = 'eu_ES';

#Possible locale for language
$nls['locale']['eu_ES'] = 'eu_ES,eu_ES.utf8,eu_ES.utf-8,eu_ES.UTF-8,eu_ES@euro,basque,Basque_Spain.1252';

#Encoding of the language
$nls['encoding']['eu_ES'] = 'UTF-8';

#Location of the file(s)
$nls['file']['eu_ES'] = array(dirname(__FILE__) . '/eu_ES/admin.inc.php');

#Language setting for HTML area
$nls['htmlarea']['eu_ES'] = 'en';
?>
