<?php

#Native language name
#NOTE: Enocde me with HTML escape chars like &#231; or &ntilde; so I work on every page
$nls['language']['ko_KR'] = 'Korean';
$nls['englishlang']['ko_KR'] = 'Korean';

#Possible aliases for language
$nls['alias']['ko'] = 'ko_KR';

#Encoding of the language
$nls['encoding']['ko_KR'] = 'UTF-8';

#Location of the file(s)
$nls['file']['ko_KR'] = array(dirname(__FILE__) . '/ko_KR/admin.inc.php');

#Language setting for HTML area
# Only change this when translations exist in HTMLarea and plugin dirs
# (please send language files to HTMLarea development)

$nls['htmlarea']['ko_KR'] = 'en';
?>
