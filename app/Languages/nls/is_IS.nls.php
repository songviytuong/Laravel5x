<?php

#Native language name
$nls['language']['is_IS'] = 'Íslenska';
$nls['englishlang']['is_IS'] = 'Icelandic';

#Possible aliases for language
$nls['alias']['is'] = 'is_IS';
$nls['alias']['icelandic'] = 'is_IS';
$nls['alias']['ice'] = 'is_IS';
$nls['alias']['isl'] = 'is_IS';
$nls['alias']['is_IS'] = 'is_IS';
$nls['alias']['is_IS.ISO8859-1'] = 'is_IS';
$nls['alias']['is_IS.ISO8859-15'] = 'is_IS';

#Possible locale for language
$nls['locale']['is_IS'] = 'is_IS,is_IS.utf8,is_IS.utf-8,is_IS.UTF-8,icelandic,Icelandic_Iceland.1252';

#Encoding of the language
$nls['encoding']['is_IS'] = 'UTF-8';

#Location of the file(s)
$nls['file']['is_IS'] = array(dirname(__FILE__) . '/is_IS/admin.inc.php');

#Language setting for HTML area
# Only change this when translations exist in HTMLarea and plugin dirs
# (please send language files to HTMLarea development)

$nls['htmlarea']['is_IS'] = 'en';
?>
