<?php

#Native language name
$nls['language']['ja_JP'] = '日本語';
$nls['englishlang']['ja_JP'] = 'Japanese';

#Possible aliases for language
$nls['alias']['ja'] = 'ja_JP';
$nls['alias']['japanese'] = 'ja_JP';
$nls['alias']['ja_JP.EUC-JP'] = 'ja_JP';
$nls['alias']['ja_JP.Shift_JIS'] = 'ja_JP';
$nls['alias']['ja_JP.UTF-8'] = 'ja_JP';

#Possible locale for language
$nls['locale']['ja_JP'] = 'ja_JP,ja_JP.utf8,ja_JP.utf-8,ja_JP.UTF-8,ja_JP.SJIS,ja_JP.eucjp,japanese,Japanese_Japan.932';

#Encoding of the language
$nls['encoding']['ja_JP'] = 'UTF-8';

#Location of the file(s)
$nls['file']['ja_JP'] = array(dirname(__FILE__) . '/ja_JP/admin.inc.php');

#Language setting for HTML area
# Only change this when translations exist in HTMLarea and plugin dirs
# (please send language files to HTMLarea development)

$nls['htmlarea']['ja_JP'] = 'ja';
?>
