<?php

namespace App\Jobs;

use File;
use App\Helpers\Business;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\DB;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CrawlerJobDoodlesUpdateDetails implements ShouldQueue
{
    use Dispatchable,
    InteractsWithQueue,
    Queueable,
        SerializesModels;

    protected $request;
    protected $table = 'jobs';

    /**
     * Create a new job instance.
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Execute the job.
     */
    public function handle()
    {
        $PATH = 'D:/GitLab/Laravel5x/resources/crawler/doodlesDetail/';
        $FILE = 'google-'.$this->request['id'].'.html';

        $desc = $this->request['descrt'];

        if ($desc == '') {
            File::Delete($PATH.$FILE);
            echo "\n ❤  Remove ID: ".$this->request['id']."\n\n";
        } else {
            $Update = [
                'description' => $this->request['descrt'],
            ];
            if (Business::cms_existsFieldWithValue('id', $this->request['id'], '5x_doodles')) {
                if (DB::table('5x_doodles')->where('id', $this->request['id'])->update($Update)) {
                    echo "\n ❤  Updated ID: ".$this->request['id']."\n\n";
                    //File::Delete($PATH.$FILE);
                    //echo "\n ❤  Remove ID: ".$this->request['id']."\n\n";
                }
            }
        }
        DB::table('5x_doodles')->where('id', $this->request['id'])->update(['flg' => 1]);
        echo '------  SET flg = 1 WHERE ID: '.$this->request['id']."\n\n";
    }
}
