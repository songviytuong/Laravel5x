<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use DB;
use Business;

class CrawlerJobUpdateDetails implements ShouldQueue {

    use Dispatchable,
        InteractsWithQueue,
        Queueable,
        SerializesModels;

    protected $request;
    protected $table = 'jobs';
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($request) {
        $this->request = $request;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {

        $Update = [
            'phone' => $this->request['phone'],
            'area' => $this->request['area'],
            'district' => $this->request['district'],
            'city' => $this->request['city'],
            'price' => $this->request['price'],
            'txt' => $this->request['txt'],
        ];
        if (Business::cms_existsFieldWithValue('postid', $this->request['postid'], 'cms_crawlers')) {
            if (DB::table('cms_crawlers')->where('postid', $this->request['postid'])->update($Update)) {
                echo "\n ❤ Updated PostID: " . $this->request['postid'] . "\n\n";
            }
        };
    }

}
