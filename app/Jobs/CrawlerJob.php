<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use DB;
use Business;

class CrawlerJob implements ShouldQueue {

    use Dispatchable,
        InteractsWithQueue,
        Queueable,
        SerializesModels;

    protected $request;
    protected $table = 'jobs';

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($request) {
        $this->request = $request;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {

        $InsertData = [
            'postid' => $this->request['postid'],
            'title' => $this->request['title'],
            'link' => $this->request['link'],
        ];
//        var_dump($InsertData); exit();
        if (!Business::cms_existsFieldWithValue('postid', $this->request['postid'], 'cms_crawlers')) {
            if (DB::table('cms_crawlers')->insert($InsertData)) {
                echo "-> Inserted.\n";
            }
        }
    }

}
