<!DOCTYPE html>
<html data-style-switcher-options="{'changeLogo': false, 'borderRadius': 0, 'colorPrimary': '#ffd93e', 'colorSecondary': '#ecf1f7', 'colorTertiary': '#403f3d', 'colorQuaternary': '#1f1e1c'}" class=" js no-touch history boxshadow csstransforms3d csstransitions video svg webkit chrome win js" style="">
    <head>
        <!-- Basic -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        {{-- Part: site title with default value in parent --}}
        @section('head-title')
        <title>{!!SEO::getTitle()!!}</title>
        @stop
        @yield('head-title')

        @section('head-meta')
        {!!SEO::getMeta()!!}
        @stop

        {{-- Part: all meta-related contents --}}
        @yield('head-meta')
        <meta name="csrf-token" content="{{ csrf_token() }}" />

        {{-- Part: load fonts --}}
        @yield('head-fonts')
        {{-- Part: load styles for the page --}}
        @section('head-styles')

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

        <!-- Web Fonts  -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

        <!-- Vendor CSS -->
        <link rel="stylesheet" href="{{$THEMES_URL}}/vendor/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{url('/themes/r00001')}}/vendor/font-awesome/css/fontawesome-all.min.css">
        <link rel="stylesheet" href="{{url('/themes/r00001')}}/vendor/animate/animate.min.css">
        <link rel="stylesheet" href="{{url('/themes/r00001')}}/vendor/simple-line-icons/css/simple-line-icons.min.css">
        <link rel="stylesheet" href="{{$THEMES_URL}}/vendor/owl.carousel/assets/owl.carousel.min.css">
        <link rel="stylesheet" href="{{$THEMES_URL}}/vendor/owl.carousel/assets/owl.theme.default.min.css">
        <link rel="stylesheet" href="{{$THEMES_URL}}/vendor/magnific-popup/magnific-popup.min.css">

        <!-- Theme CSS -->
        <link rel="stylesheet" href="{{$THEMES_URL}}/css/theme.css">
        <link rel="stylesheet" href="{{$THEMES_URL}}/css/theme-elements.css">
        <link rel="stylesheet" href="{{$THEMES_URL}}/css/theme-blog.css">
        <link rel="stylesheet" href="{{$THEMES_URL}}/css/theme-shop.css">

        <!-- Current Page CSS -->
        <link rel="stylesheet" href="{{$THEMES_URL}}/vendor/rs-plugin/css/settings.css">
        <link rel="stylesheet" href="{{$THEMES_URL}}/vendor/rs-plugin/css/layers.css">
        <link rel="stylesheet" href="{{$THEMES_URL}}/vendor/rs-plugin/css/navigation.css">

        <!-- Demo CSS -->
        <link rel="stylesheet" href="{{$THEMES_URL}}/css/demos/demo-resume.css">

        <!-- Skin CSS -->
        <link rel="stylesheet" href="{{$THEMES_URL}}/css/skins/skin-resume.css">		

        <!-- Theme Custom CSS -->
        <link rel="stylesheet" href="{{$THEMES_URL}}/css/custom.css">

        <!-- Head Libs -->
        <script src="{{$THEMES_URL}}/vendor/modernizr/modernizr.min.js"></script>

        @stop
        @yield('head-styles')
        {{-- Part: load scripts needed --}}
        @section('head-scripts')

        @stop
        @yield('head-scripts')
        {{-- Part: anything else in head --}}
        @yield('head-extra')

        <link rel="shortcut icon" href="{{ URL::asset('images/favicon.png') }}">

    </head>
    <body data-spy="scroll" data-target=".wrapper-spy" dns-source="{{$THEMES_URL}}">
        {{-- Part: something at start of body --}}
        @yield('body-start')

        {{-- Part: create main content of the page --}}
        @yield('body-content')

        {{-- Part: load scripts --}}
        <!-- Vendor -->
        <script src="{{$THEMES_URL}}/vendor/jquery/jquery.min.js"></script>
        <script src="{{$THEMES_URL}}/vendor/jquery.appear/jquery.appear.min.js"></script>
        <script src="{{$THEMES_URL}}/vendor/jquery.easing/jquery.easing.min.js"></script>
        <script src="{{$THEMES_URL}}/vendor/jquery-cookie/jquery-cookie.min.js"></script>
        <script src="{{$THEMES_URL}}/vendor/style-switcher/style.switcher.js" id="styleSwitcherScript" data-base-path="" data-skin-src="{{$THEMES_URL}}/vendor/less/skin-resume.less"></script>
        <script src="{{$THEMES_URL}}/vendor/popper/umd/popper.min.js"></script>
        <script src="{{$THEMES_URL}}/vendor/bootstrap/js/bootstrap.min.js"></script>
        <script src="{{$THEMES_URL}}/vendor/common/common.min.js"></script>
        <script src="{{$THEMES_URL}}/vendor/jquery.validation/jquery.validation.min.js"></script>
        <script src="{{$THEMES_URL}}/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
        <script src="{{$THEMES_URL}}/vendor/jquery.gmap/jquery.gmap.min.js"></script>
        <script src="{{$THEMES_URL}}/vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
        <script src="{{$THEMES_URL}}/vendor/isotope/jquery.isotope.min.js"></script>
        <script src="{{$THEMES_URL}}/vendor/owl.carousel/owl.carousel.min.js"></script>
        <script src="{{$THEMES_URL}}/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
        <script src="{{$THEMES_URL}}/vendor/vide/vide.min.js"></script>

        <!-- Theme Base, Components and Settings -->
        <script src="{{$THEMES_URL}}/js/theme.js"></script>

        <!-- Current Page Vendor and Views -->
        <script src="{{$THEMES_URL}}/vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
        
        <script src="{{$THEMES_URL}}/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

        <!-- Current Page Vendor and Views -->
        <script src="{{$THEMES_URL}}/js/views/view.contact.js"></script>

        <!-- Demo -->
        <script src="{{$THEMES_URL}}/js/demos/demo-resume.js"></script>

        <!-- Theme Custom -->
        <script src="{{$THEMES_URL}}/js/custom.js"></script>

        <!-- Theme Initialization Files -->
        <script src="{{$THEMES_URL}}/js/theme.init.js"></script>

        <a href="index.html" class="go-to-demos"><i class="fas fa-arrow-left"></i> More Demos</a>
    </body>
