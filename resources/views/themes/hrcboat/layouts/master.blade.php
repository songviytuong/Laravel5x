<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>© HRC-BOAT 2018 - Webmaster by Lee Peace</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <link rel="stylesheet" href="{{$THEMES_URL}}/css/reset.css">
        <link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900'>
        <link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Montserrat:400,700'>
        <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'>
        <link rel='stylesheet prefetch' href='{{$THEMES_URL}}/css/bootstrap.min.css'>
        <link rel="stylesheet" href="{{$THEMES_URL}}/css/style.css">
    </head>
    <body>
        <div class="container">
            <div class="info">
                <h1>HRC-BOAT 2<i class="fa fa-heart" style="color:red;"></i>18</h1><span>© Copyright by <a href="{!!url('/')!!}">Sollyken</a></span>
            </div>
        </div>
        @yield('body-content')
        <script src='{{$THEMES_URL}}/js/jquery.min.js'></script>
        <script src="{{$THEMES_URL}}/js/index.js"></script>
    </body>
</html>