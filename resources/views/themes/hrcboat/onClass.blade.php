
<div class="col-lg-2"></div>
<div class="col-lg-8" style="background-color: #fff; padding-top: 10px;">
    <table class="table table-striped table-bordered table-hover table-condensed">
        <tr style="background-color: #0184ff; color:#fff;">
            <th class="text-center col-lg-1"></th>
            <th class="text-center col-lg-3">Tên Nhóm</th>
            <th class="text-center"></th>
            <th class="text-center"><i class="fa fa-undo" rel="ClassA" aria-hidden="true" style="cursor:pointer"></i> ClassA</th>
            <th class="text-center"><i class="fa fa-undo" rel="ClassE" aria-hidden="true" style="cursor:pointer"></i> ClassE</th>
            <th class="text-center"><i class="fa fa-undo" rel="ClassF" aria-hidden="true" style="cursor:pointer"></i> ClassF</th>
            <th class="text-center"><i class="fa fa-undo" rel="ClassG" aria-hidden="true" style="cursor:pointer"></i> ClassG</th>
            <th class="text-center"><i class="fa fa-undo" rel="ClassH" aria-hidden="true" style="cursor:pointer"></i> ClassH</th>
            <th class="text-center"><i class="fa fa-undo" rel="ClassJ" aria-hidden="true" style="cursor:pointer"></i> ClassJ</th>
        </tr>
        <?php
        $i = 1;
        foreach ($resultParent as $row) {
            ?>
            <tr>
                <td class="text-center"><?= $i; ?></td>
                <td><?= $row->Member ?></td>
                <td><?= $row->GroupName ?></td>
                <td class="text-center text-success">
                    <?php
                    if ($row->ClassA == 0) {
                        echo "<i class='fa fa-toggle-off onClass fa-lg' style='cursor:pointer' data-class='ClassA' data-source='" . $row->ClassA . "' rel='" . $row->ID . "'></i>";
                    } else {
                        echo "<i class='fa fa-toggle-on onClass fa-lg' style='cursor:pointer' data-class='ClassA' data-source='" . $row->ClassA . "' rel='" . $row->ID . "'></i>";
                    }
                    ?>
                </td>
                <td class="text-center">
                    <?php
                    if ($row->ClassE == 0) {
                        echo "<i class='fa fa-toggle-off onClass fa-lg' style='cursor:pointer' data-class='ClassE' data-source='" . $row->ClassE . "' rel='" . $row->ID . "'></i>";
                    } else {
                        echo "<i class='fa fa-toggle-on onClass fa-lg' style='cursor:pointer' data-class='ClassE' data-source='" . $row->ClassE . "' rel='" . $row->ID . "'></i>";
                    }
                    ?>
                </td>
                <td class="text-center">
                    <?php
                    if ($row->ClassF == 0) {
                        echo "<i class='fa fa-toggle-off onClass fa-lg' style='cursor:pointer' data-class='ClassF' data-source='" . $row->ClassF . "' rel='" . $row->ID . "'></i>";
                    } else {
                        echo "<i class='fa fa-toggle-on onClass fa-lg' style='cursor:pointer' data-class='ClassF' data-source='" . $row->ClassF . "' rel='" . $row->ID . "'></i>";
                    }
                    ?>
                </td>
                <td class="text-center">
                    <?php
                    if ($row->ClassG == 0) {
                        echo "<i class='fa fa-toggle-off onClass fa-lg' style='cursor:pointer' data-class='ClassG' data-source='" . $row->ClassG . "' rel='" . $row->ID . "'></i>";
                    } else {
                        echo "<i class='fa fa-toggle-on onClass fa-lg' style='cursor:pointer' data-class='ClassG' data-source='" . $row->ClassG . "' rel='" . $row->ID . "'></i>";
                    }
                    ?>
                </td>
                <td class="text-center">
                    <?php
                    if ($row->ClassH == 0) {
                        echo "<i class='fa fa-toggle-off onClass fa-lg' style='cursor:pointer' data-class='ClassH' data-source='" . $row->ClassH . "' rel='" . $row->ID . "'></i>";
                    } else {
                        echo "<i class='fa fa-toggle-on onClass fa-lg' style='cursor:pointer' data-class='ClassH' data-source='" . $row->ClassH . "' rel='" . $row->ID . "'></i>";
                    }
                    ?>
                </td>
                <td class="text-center">
                    <?php
                    if ($row->ClassJ == 0) {
                        echo "<i class='fa fa-toggle-off onClass fa-lg' style='cursor:pointer' data-class='ClassJ' data-source='" . $row->ClassJ . "' rel='" . $row->ID . "'></i>";
                    } else {
                        echo "<i class='fa fa-toggle-on onClass fa-lg' style='cursor:pointer' data-class='ClassJ' data-source='" . $row->ClassJ . "' rel='" . $row->ID . "'></i>";
                    }
                    ?>
                </td>
            </tr>
            <?php $i++;
        }
        ?>
    </table>
</div>
<div class="col-lg-2"></div>