@extends('themes.hrcboat.layouts.master')
@section('body-content')
<div class="form">
    <div class="btn-group">
        <a class="btn btn-default goPlay">
            <i class="fa fa-play" title=""></i>
        </a>
    </div>
    <div class="clearfix"><br/></div>
    <input type="text" name="GroupName" id="GroupName" value="<?= isset($data['dataEdit']) ? $data['dataEdit'] : ''; ?>" placeholder="Nhập tên nhóm..."/>
    <a class="btn btn-danger <?= isset($data['dataEdit']) ? 'uGroup' : 'sGroup'; ?>"><i class="fa fa-credit-card"></i> <?= isset($data['dataEdit']) ? 'Cập nhật' : 'Tạo nhóm'; ?></a>
    <a class="btn btn-primary listPlayer"><i class="fa fa-list"></i> Danh sách</a>
    <p class="message">Đã có nhóm - <a href="/add">Thêm thành viên</a></p>
    <?php
    if (isset($dataEdit)) {
        ?>
        <input type="hidden" id="idEdit" value=""/>
    <?php } ?>
</div>

<?php
$action = Request::segment(1);
if ($action != "edit") {
    ?>
    <div class="form">
        <table class="table table-striped table-bordered table-hover table-condensed" style="margin-bottom:0px;">
            <tr style="background-color: #060; color:#fff;">
                <th class="text-center col-xs-2"></th>
                <th class="text-center col-lg-6">Nhóm</th>
                <th class="text-center col-xs-2"></th>
            </tr>
            <?php
            $i = 1;
            if ($listGroup) {
                foreach ($listGroup as $row) {
                    ?>
                    <tr>
                        <td class="text-center"><?= $i; ?></td>
                        <td class="text-left gname"><a href="/edit/<?= $row->ID ?>"><?= $row->Name ?></a></td>
                        <td class="text-center">
                            <i class="fa fa-trash-o text-danger delGroup" rel="<?= $row->ID ?>" style="cursor: pointer"></i>
                        </td>
                    </tr>
                    <?php
                    $i++;
                }
            }
            ?>
        </table>
    </div>
<?php } ?>
@stop
