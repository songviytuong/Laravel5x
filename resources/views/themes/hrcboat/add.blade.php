@extends('themes.hrcboat.layouts.master')
@section('body-content')
<div class="form">
    <div class="btn-group">
        <a class="btn btn-default goPlay">
            <i class="fa fa-play" title=""></i>
        </a>
        <a class="btn btn-default goRandom">
            <i class="fa fa-random" title=""></i>
        </a>
    </div>
    <div class="clearfix"><br/></div>
    <form name="frmCreateMember" id="frmCreateMember" method="POST">
        <select class="form-group form-control group" name="group">
            <option value="-1">-- Lựa chọn --</option>
            <?php
            foreach ($listGroup as $row) {
                ?>
                <option value="<?= $row->ID; ?>"><?= $row->Name; ?></option>
            <?php } ?>
        </select>
        <input type="text" name="Name" id="Name" placeholder="Họ tên"/>
        <input type="text" name="Phone" id="Phone" placeholder="Số điện thoại" class="hidden"/>
        <a class="btn btn-warning addMember"><i class="fa fa-plus-circle"></i> Tạo thành viên</a>
        <p class="message">Chưa có nhóm? <a href="./">Tạo nhóm</a></p>
    </form>
</div>
<div class="form showMember"></div>
@stop