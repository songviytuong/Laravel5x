<style>
    .row{
        margin-left:0px;
        margin-right:0px;
    }
</style>
<div class="row">
    <?php
        function partition($list, $p) {
        $listlen = count($list);
        $partlen = floor($listlen / $p);
        $partrem = $listlen % $p;
        $partition = array();
        $mark = 0;
        for ($px = 0; $px < $p; $px++) {
            $incr = ($px < $partrem) ? $partlen + 1 : $partlen;
            $partition[$px] = array_slice($list, $mark, $incr);
            $mark += $incr;
        }
        return $partition;
    }

    $k = 1;
    $newArr = partition($data['data'], $data['column']);
    
    foreach($newArr as $key=>$row){
        echo "<div class='col-lg-4'><table class='table table-striped table-bordered table-hover table-condensed' style='background-color:#fff'>
        <tr>
            <th class='text-center' style='width:40px !important;'></th>
            <th class='text-center col-lg-4' style='background-color: red; color:#fff;'>Bảng ".($key+1)."</th>
            <th class='text-center col-lg-3'>".$data['class']."</th>
        </tr>
        <tr style='background-color: #060; color:#fff;'>
            <th class='text-center col-xs-2'></th>
            <th class='text-center col-lg-4'>Tên thành viên</th>
            <th class='text-center col-lg-3'>Nhóm</th>
        </tr>";
        
        foreach ($row as $item){
            echo "<tr><td class='text-center col-lg-2'>".$k."</td><td class='col-lg-4'>".$item["Member"]."</td><td class='text-center col-lg-3' style='font-size:75%; font-weight:bold; padding-top:10px;'>".$item["gName"]."</td></tr>";
            $k++;
        }
        echo "</table></div>";
        $k = 1;
    }
    ?>
</div>
