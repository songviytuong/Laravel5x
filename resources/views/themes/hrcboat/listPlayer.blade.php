@extends('themes.hrcboat.layouts.master')
@section('body-content')
<div class="form">
    <div class="btn-group">
        <a class="btn btn-default goPlay">
            <i class="fa fa-play" title=""></i>
        </a>
        <a class="btn btn-default goRandom">
            <i class="fa fa-random" title=""></i>
        </a>
    </div>
</div>
<div id="showPlayer"></div>
@stop