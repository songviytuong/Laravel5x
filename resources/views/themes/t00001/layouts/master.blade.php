<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        {{-- Part: site title with default value in parent --}}
        @section('head-title')
        <title>{!!SEO::getTitle()!!}</title>
        @stop
        @yield('head-title')

        @section('head-meta')
        {!!SEO::getMeta()!!}
        @stop

        {{-- Part: all meta-related contents --}}
        @yield('head-meta')
        <meta name="csrf-token" content="{{ csrf_token() }}" />

        {{-- Part: load fonts --}}
        @yield('head-fonts')
        {{-- Part: load styles for the page --}}
        @section('head-styles')
        {!!SEO::addCss([$THEMES_URL.'/css/cgejs_dd4d11152e1842d7c2b219f1fc770ca0.css', $THEMES_URL.'/css/stylesheet_combined_dd1ffdfea14bf1f09683d94e56ba96c9.css'])!!}
        {!!SEO::getCss()!!}
        @stop
        @yield('head-styles')
        {{-- Part: load scripts needed --}}
        @section('head-scripts')

        @stop
        @yield('head-scripts')
        {{-- Part: anything else in head --}}
        @yield('head-extra')

        <link rel="shortcut icon" href="{{ URL::asset('images/favicon.png') }}">

    </head>
    <body class="my-login-page">
        {{-- Part: something at start of body --}}
        @yield('body-start')

        {{-- Part: create main content of the page --}}
        @yield('body-content')

        <script type="text/javascript">
            var BASEPATH = '/administrator/';
        </script>

        {{-- Part: load scripts --}}
        @section('body-scripts')
        {!!SEO::addJs([$THEMES_URL.'/js/cgejs_4fc33a52d714d301d6ff3fb8492e3fbf.js'])!!}
        {!!SEO::getJs()!!}
        @stop
        @yield('body-scripts')
        {{-- Part: something else to do --}}
        @yield('body-others')
        {{-- Part: finalize stuffs if there is --}}
        {{-- Part: footer --}}
        @yield('body-end')
    </body>
</html>