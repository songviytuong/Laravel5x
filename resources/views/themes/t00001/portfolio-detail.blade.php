<div id="portfolio-gallery">
	<a href="http://idev.local/uploads/images/_portfolio/project2/01-o.jpg"></a>
	<a href="http://idev.local/uploads/images/_portfolio/project2/02-o.jpg"></a>
	<a href="http://idev.local/uploads/images/_portfolio/project2/03-o.jpg"></a>
	<a href="http://idev.local/uploads/images/_portfolio/project2/04-o.jpg"></a>
	<a href="http://idev.local/uploads/images/_portfolio/project2/05-o.jpg"></a>
</div>
<div class="portfolio-container">
	<div class="portfolio-main col-md-9">

	</div>
	<div class="portfolio-side col-md-3">
		<div class="ps-wrapper">
			<div class="head">
				<h3 class="title">Brand Manual</h3>
				<ul class="list-items">
					<li>
						<div class="list-label">Author</div>
						<div class="list-des">Lee Peace</div>
					</li>					<li>
						<div class="list-label">Date</div>
						<div class="list-des">05/06/2016</div>
					</li>
					<li>
						<div class="list-label">Duration</div>
						<div class="list-des">2 years ago</div>
					</li>
					<li>
						<div class="list-label">Template</div>
						<div class="list-des">Brochures</div>
					</li>

				</ul>
<div style="float:left;">&nbsp;<iframe src="http://www.facebook.com/plugins/like.php?href=http://idev.local/portfolio/brand-manual/35/&amp;layout=button_count&amp;show_faces=false&amp;width=0&amp;action=like&amp;locale=en_US&amp;colorscheme=light&amp;send=false" scrolling="no" frameborder="0" allowtransparency="true" style="border:none; overflow:hidden; width: 50px; height:21px;clear:both;"></iframe>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.3&appId=364679453649876";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div class="fb-share-button" data-href="http://idev.local/portfolio/brand-manual/35/" data-layout="button_count" style="float:left"></div></div>
			</div>
			<div class="comments-wrapper" style="height: 319px;">
				<div class="inner-wrapper scrollbar ps-container">
					<div class="portfolio-comments">
					<div id="fb-root"></div>
					
					<script>(function(d, s, id) {
					  var js, fjs = d.getElementsByTagName(s)[0];
					  if (d.getElementById(id)) return;
					  js = d.createElement(s); js.id = id;
					  js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1&appId=364679453649876";
					  fjs.parentNode.insertBefore(js, fjs);
					}(document, 'script', 'facebook-jssdk'));</script>
					
					<div class="fb-comments" data-href="http://idev.local/portfolio/brand-manual/35/" data-num-posts="5" data-width="100%" data-height="50px"></div>
					</div>
					
					<div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 0px; width: 388px; display: none;"><div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 0px; right: 0px; height: 319px; display: none;"><div class="ps-scrollbar-y" style="top: 0px; height: 0px;"></div></div></div>
				</div>
					
			</div>
		</div>	
	</div>
</div>