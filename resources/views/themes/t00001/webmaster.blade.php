
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en_US">
<head>
<title>Webmaster - @songviytuong | The Official Website</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui, user-scalable=no">
<meta name="format-detection" content="telephone=no">
<meta name="Generator" content="Lee Peace"/>
<meta http-equiv="Expires" content="0"/>
<meta name="Resource-type" content="Document"/>
<meta name="Language" content="Vietnamese, English"/>
<meta name="Keywords" content="idea, songviytuong, webmaster, website design, designer, borchure, card, invitation letter"/>
<meta name="Description" content="Design portfolio & blog of Website, a freelance designer in Vietnam specializing in logo, web & brand identity"/>
<meta name="Identifier-URL" content="" />
<meta name="Original-source" content="" />
<link href="" rel="canonical"/>
<meta name="Revised" content="06/05/2016 - 10:00:23" />
<meta name="Robots" content="index, follow"/>
<meta name="Revisit-After" content="1 days"/>
<meta name="Rating" content="search engine optimization"/>
<meta name="Copyright" content="@songviytuong ©2004-2018 - Webmaster designer by songviytuong"/>
<meta name="Distribution" content="Global"/>
<meta name="Classification" content="Seo"/>
<link rel="author" href="https://plus.google.com/u/0/117664319647" />
<meta property="og:url" content=""/>
<meta property="og:type" content="website" /> 
<meta property="og:image" content="images/facebook.jpg"/>
<meta property="og:title" content="Webmaster - @songviytuong | The Official Website"/>
<meta property="og:description" content="Design portfolio & blog of Website, a freelance designer in Vietnam specializing in logo, web & brand identity"/>
<meta property="og:site_name" content="@songviytuong | The Official Website"/>

<!-- Social: Twitter -->
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@songviytuong">
<meta name="twitter:creator" content="songviytuong">
<meta name="twitter:url" content="" />
<meta name="twitter:title" content="@songviytuong | The Official Website">
<meta name="twitter:description" content="Design portfolio & blog of Website, a freelance designer in Vietnam specializing in logo, web & brand identity">
<meta name="twitter:image:src" content="images/twitter.jpg">
<!-- End Social: Twitter -->

<meta itemprop="ratingValue" content="5" />
<meta itemprop="ratingCount" content="9999" />
<meta itemprop="price" content="1000000.00" />

<!--CSS Styles-->
<link rel="stylesheet" type="text/css" href="{{$THEMES_URL}}/assets/css/vendors/idangerous.swiper.css">
<link rel="stylesheet" type="text/css" href="{{url('/themes/t00001')}}/assets/css/vendors/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="{{$THEMES_URL}}/assets/css/vendors/blue.monday/jplayer.blue.monday.css" />
<link rel="stylesheet" type="text/css" href="{{$THEMES_URL}}/assets/css/vendors/bootstrap.css" >
<link rel="stylesheet" type="text/css" href="{{$THEMES_URL}}/assets/css/vendors/blueimp-gallery.css">
<link rel="stylesheet" type="text/css" href="{{$THEMES_URL}}/assets/css/vendors/perfect-scrollbar.css">
<link rel="stylesheet" type="text/css" href="{{$THEMES_URL}}/assets/css/style.css">
<link rel="stylesheet" type="text/css" href="{{$THEMES_URL}}/assets/css/skins/skin3/style.css">
<!--/CSS Styles-->
<!-- Google Fonts -->
<link href='http://fonts.googleapis.com/css?family=PT+Sans:100,200,300,400,700,400italic,700italic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Exo+2:400,100,200,200italic,300,300italic,400italic,500,600,500italic,600italic,700,800,700italic,800italic,900,900italic' rel='stylesheet' type='text/css'>
<!-- /Google Fonts -->

</head><body>

	<!-- Preloader -->
    <div id="preloader">
        <div id="text-load">
        </div>
        <div id="userguid">
        	<p>Website scrolles horizontally by <img src="{{$THEMES_URL}}/assets/images/mouse-drag.png" alt=""> or <img src="http://idev.local/assets/images/keyboard-arow.png" alt=""></p>
        	<a class="startbtn">Go</a>
        </div>
    </div>
    <!-- Preloader -->

    <!-- Main Wrapper-->
	<div id="wrapper">
		<div id="container">

			<!-- Upstream -->
			<div id="upstream">	
			
			</div>
			<!-- /Upstream-->
			
			<!-- Contents -->
			<div id="contents">

				<!-- Swiper Pages -->
				<div id="swiper-content">
					<div class="swiper-wrapper">
						
						<!-- Home Page -->
						<div id ="home" class="sub-page swiper-slide" data-hash="hello">
							<div class="inner-wrapper scrollbar">
								<!-- Kenburn Slider -->
								<div id="kb-container">

								</div>
								<!-- /Kenburn Slider -->
								
								<!-- Home introduction bar -->
								<div id="hello-inner">
									<!-- Introduction hex image -->
									<div id="hex-container">
										<div class="hex-wrapper">
											<div class="hex-1">
												<div class="hex-2">
													<a href="/"><div class="hex-3 hidden-xs">
														
													</div></a>
												</div>
											</div>
										</div>
									</div>
									<!-- /Introduction hex image -->

									<!-- Introduction Contents -->
									<div id="hello-contents">
										<h1>@songviytuong</h1>
										<h2>Webmaster Director</h2>
									</div>
									<!-- /Introduction Contents -->

									<!-- Next page shortcut -->
									<div id="enhancer">
										<span>Portfolio</span>
										<i class="fa fa-angle-right"></i>
									</div>
									<!-- /Next page shortcut -->

								</div>
								<!-- /Home introduction bar -->

							</div>
						</div>
						<!-- /Home Page -->

						<!-- Grid Portfolio -->
						<div class="sub-page swiper-slide" data-hash="grid-portfolio">
							
							<!-- Page side bar -->	
							<div class="page-side scrollbar">
								<div class="inner-wrapper scrollbar">
									<!-- Side bar title -->	
									<div class="sub-page-title">
										<h2 class="mycontainer">Portfolio</h2>
									</div>
									<!-- Side bar title -->	

									<!-- Side bar image -->	
									<img src="{{$THEMES_URL}}/assets/images/songviytuong.jpg" class="img-responsive hidden-xs" alt="songviytuong">
									<!-- /Side bar image -->	

									<!-- Side bar contents -->	
									<div class="mycontainer hidden-xs">
										<h3>Our Projects</h3>
										<p>Updating...</p>
									</div>
									<!-- /Side bar contents -->	

								</div>
							</div>
							<!-- /Page side bar -->	

							<!-- Page Contents -->	
							<div class="page-container">
								<div class="inner-wrapper scrollbar">

									<!-- Portfolio wrapper -->	
									<div class="grid-portfolio masonryGrid">
												<!-- Portflio Item -->
<!--
- ajax-portfolio video-item normal
- portfolio-item-vimeo[-youtube]
-->		
<div class="gp-item da-item">
	<a href="{{route('idev.webmaster.portfolio.detail')}}" class="ajax-portfolio normal">
		<img src="{{$THEMES_URL}}/assets/images/no-image.png" alt="alt" class="img-responsive">
		<!-- Item Overlay -->
		<div class="info da-overlay">
			<div class="centerit">
				<h3>Project 2</h3>
				<p>Brand Manual</p>
<p class="author"><b>By</b>: <strong>@songviytuong</strong> - <b>Templates</b>: </p>
			</div>
		</div>
		<!-- /Item Overlay -->	
	</a>
</div>
<!-- /Portflio Item -->	
<!-- Portflio Item -->
<!--
- ajax-portfolio video-item normal
- portfolio-item-vimeo[-youtube]
-->		
<div class="gp-item da-item">
	<a href="http://idev.local/portfolio/kinney-proposal/35/" class="ajax-portfolio normal">
		<img src="{{$THEMES_URL}}/assets/images/no-image.png" alt="alt" class="img-responsive">
		<!-- Item Overlay -->	
		<div class="info da-overlay">
			<div class="centerit">
				<h3>Project 1</h3>
				<p>Kinney Proposal</p>
<p class="author"><b>By</b>: <strong>@songviytuong</strong> - <b>Templates</b>: </p>
			</div>
		</div>
		<!-- /Item Overlay -->	
	</a>
</div>
<!-- /Portflio Item -->	

									</div>
									<!-- /Portfolio wrapper -->	
								</div>
							</div>
							<!-- /Page Contents -->	

						</div>
						<!-- /Grid Portfolio -->
						
						<!-- Grid Gallery -->
						<div class="sub-page reverse swiper-slide" data-hash="grid-gallery">
							
							<!-- Page side bar -->		
							<div class="page-side">
								<div class="inner-wrapper scrollbar">

									<!-- Side bar title -->		
									<div class="sub-page-title">
										<h2 class="mycontainer">Grid Gallery</h2>
									</div>
									<!-- /Side bar title -->	

									<!-- Side bar image -->
									<img src="{{$THEMES_URL}}/assets/images/gal1/side2.jpg" class="img-responsive hidden-xs" alt="img">
									<!-- /Side bar image -->	

									<!-- Side bar contents -->	
									<div class="mycontainer hidden-xs">
										<h3>Portrates</h3>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
									</div>
									<!-- /Side bar contents -->

								</div>
							</div>
							<!-- /Page side bar -->		

							<!-- Page contents -->
							<div class="page-container">
								<div class="inner-wrapper scrollbar">

									<!-- Gallery wrapper -->
								    <div id="gal1" class="masonryGrid fullscreen-enable col-5">
									 	
									 	<!-- Gallery Item -->
									    <div class="gal1-item da-item">
									    	<a href="{{$THEMES_URL}}/assets/images/gal1/large/01.jpg">
										  		<img class="img-responsive" src="{{$THEMES_URL}}/assets/images/gal1/thumb/01.jpg" alt="img" />

										  		<!-- Gallery Overlay -->
										  		<div class="da-overlay">
										   			<span class="gal1-view"></span>
									    		</div>
									    		<!-- Gallery Overlay -->
										    </a>
							    		</div>
							    		<!-- /Gallery Item -->

							    		<!-- Gallery Item -->
									    <div class="gal1-item da-item">
									    	<a href="{{$THEMES_URL}}/assets/images/gal1/large/02.jpg">
									    		<img class="img-responsive" src="{{$THEMES_URL}}/assets/images/gal1/thumb/02.jpg" alt="img" />

									    		<!-- Gallery Overlay -->
									    		<div class="da-overlay">
									    			<span class="gal1-view"></span>
									    		</div>
									    		<!-- Gallery Overlay -->
									    	</a>
									    	
									    </div>
									    <!-- /Gallery Item -->

									    <!-- Gallery Item -->
									    <div class="gal1-item da-item">
									    	<a href="{{$THEMES_URL}}/assets/images/gal1/large/03.jpg">
									    		<img class="img-responsive" src="{{$THEMES_URL}}/assets/images/gal1/thumb/03.jpg" alt="img" />

									    		<!-- Gallery Overlay -->
										    	<div class="da-overlay">
									    			<span class="gal1-view"></span>
									    		</div>
									    		<!-- Gallery Overlay -->
									    	</a>
									    </div>
									    <!-- /Gallery Item -->

									    <!-- Gallery Item -->
									    <div class="gal1-item da-item">
									    	<a href="{{$THEMES_URL}}/assets/images/gal1/large/04.jpg">
									    		<img class="img-responsive" src="{{$THEMES_URL}}/assets/images/gal1/thumb/04.jpg" alt="img" />

												<!-- Gallery Overlay -->
									    		<div class="da-overlay">
									    			<span class="gal1-view"></span>
									    		</div>
									    		<!-- /Gallery Overlay -->
									    	</a>
									    </div>
									    <!-- /Gallery Item -->

									    <!-- Gallery Item -->
									    <div class="gal1-item da-item">
									    	<a href="{{$THEMES_URL}}/assets/images/gal1/large/05.jpg">
									    		<img class="img-responsive" src="{{$THEMES_URL}}/assets/images/gal1/thumb/05.jpg" alt="img" />

												<!-- Gallery Overlay -->
									    		<div class="da-overlay">
									    			<span class="gal1-view"></span>
									    		</div>
									    		<!-- /Gallery Overlay -->
									    	</a>
									    </div>
									    <!-- /Gallery Item -->

									    <!-- Gallery Item -->
									    <div class="gal1-item da-item">
									    	<a href="{{$THEMES_URL}}/assets/images/gal1/large/06.jpg">
									    		<img class="img-responsive" src="{{$THEMES_URL}}/assets/images/gal1/thumb/06.jpg" alt="img" />

												<!-- Gallery Overlay -->
									    		<div class="da-overlay">
									    			<span class="gal1-view"></span>
									    		</div>
									    		<!-- /Gallery Overlay -->
									    	</a>
									    </div>
									    <!-- /Gallery Item -->

									    <!-- Gallery Item -->
									    <div class="gal1-item da-item">
									    	<a href="{{$THEMES_URL}}/assets/images/gal1/large/07.jpg">
									    		<img class="img-responsive" src="{{$THEMES_URL}}/assets/images/gal1/thumb/07.jpg" alt="img" />

												<!-- Gallery Overlay -->
									    		<div class="da-overlay">
									    			<span class="gal1-view"></span>
									    		</div>
									    		<!-- /Gallery Overlay -->
									    	</a>
									    </div>
									    <!-- /Gallery Item -->

									    <!-- Gallery Item -->
									    <div class="gal1-item da-item">
									    	<a href="{{$THEMES_URL}}/assets/images/gal1/large/08.jpg">
									    		<img class="img-responsive" src="{{$THEMES_URL}}/assets/images/gal1/thumb/08.jpg" alt="img" />

												<!-- Gallery Overlay -->
									    		<div class="da-overlay">
									    			<span class="gal1-view"></span>
									    		</div>
									    		<!-- /Gallery Overlay -->
									    	</a>
									    </div>
									    <!-- /Gallery Item -->

									    <!-- Gallery Item -->
									    <div class="gal1-item da-item">
									    	<a href="{{$THEMES_URL}}/assets/images/gal1/large/09.jpg">
									    		<img class="img-responsive" src="{{$THEMES_URL}}/assets/images/gal1/thumb/09.jpg" alt="img" />

												<!-- Gallery Overlay -->
									    		<div class="da-overlay">
									    			<span class="gal1-view"></span>
									    		</div>
									    		<!-- /Gallery Overlay -->
									    	</a>
									    </div>
									    <!-- /Gallery Item -->

									    <!-- Gallery Item -->
									    <div class="gal1-item da-item">
									    	<a href="{{$THEMES_URL}}/assets/images/gal1/large/10.jpg">
									    		<img class="img-responsive" src="{{$THEMES_URL}}/assets/images/gal1/thumb/10.jpg" alt="img" />

												<!-- Gallery Overlay -->
									    		<div class="da-overlay">
									    			<span class="gal1-view"></span>
									    		</div>
									    		<!-- /Gallery Overlay -->
									    	</a>
									    </div>
									    <!-- /Gallery Item -->


								    </div>
								    <!-- /Gallery wrapper -->

							    </div>
							</div>
							<!-- /Page contents -->

						</div>
						<!-- /Grid Gallery -->

						<!-- Our Team-->
						<div class="sub-page swiper-slide" data-hash="our-team">

							<!-- Team side bar -->
							<div class="team-side scrollbar">

								<!-- Team Image Carousel Wrapper -->
								<div class="team-image-wrapper">

									<!-- Team Item -->
<div class="item">
	<img src="{{$THEMES_URL}}/assets/images/team1.jpg" alt="MrLee">
</div>
<!-- /Team Item -->
<!-- Team Item -->
<div class="item">
	<img src="{{$THEMES_URL}}/assets/images/team2.jpg" alt="HoangDo">
</div>
<!-- /Team Item -->


								</div>
								<!-- Team Image Carousel Wrapper -->

							</div>
							<!-- /Team side bar -->

							<!-- Team contents -->
							<div class="team-container">
								
								<!-- Page Title -->
								<div class="title">
									<h2>Our Team <span>		1 Webmaster,	1 Designer</span></h2>
								</div>
								<!-- /Page Title -->

								<!-- Team Contents Carousel Wrapper -->
								<div class="team-carousel-wrapper">
									<div class="team-carousel">
										<!-- Team Item -->
<div class="item">
	<div class="item-wrapper">

		<!-- Team Item Title-->
		<h3 class="team-title"><span>MrLee</span></h3>
		<!-- /Team Item Title-->

		<!-- Team Item Informartion-->
		<div class="info">
			<!-- Team Item Description-->
			<div class="description">
				<p>Updating...</p>
			</div>
			<!-- /Team Item Description-->

			<!-- Team Item Social Icons-->
			<ul class="social-icons">
																							</ul>
			<!-- /Team Item Social Icons-->
		</div>
		<!-- /Team Item Informartion-->

	</div>
</div>
<!-- /Team Item -->
<!-- Team Item -->
<div class="item">
	<div class="item-wrapper">

		<!-- Team Item Title-->
		<h3 class="team-title"><span>HoangDo</span></h3>
		<!-- /Team Item Title-->

		<!-- Team Item Informartion-->
		<div class="info">
			<!-- Team Item Description-->
			<div class="description">
				<p>Updating...</p>
			</div>
			<!-- /Team Item Description-->

			<!-- Team Item Social Icons-->
			<ul class="social-icons">
																							</ul>
			<!-- /Team Item Social Icons-->
		</div>
		<!-- /Team Item Informartion-->

	</div>
</div>
<!-- /Team Item -->

									</div>
								</div>
								<!-- /Team Contents Carousel Wrapper -->

								<!-- Team Counter -->
								<div class="team-counter">
									<span class="counter-current">1</span>
									<span class="counter-divider">/</span>
									<span class="counter-total"></span>
								</div>
								<!-- /Team Counter -->

								<!-- Team Carousel Previous -->
								<div class="team-prev">
									<a href="{{route('idev.webmaster')}}#our-team#"></a>
								</div>
								<!-- /Team Carousel Previous -->

								<!-- Team Carousel Next -->
								<div class="team-next">
									<a href="{{route('idev.webmaster')}}#our-team#"></a>
								</div>
								<!-- /Team Carousel Next -->

							</div>
							<!-- /Team contents -->

						</div>
						<!-- /Our Team-->

						<!-- Blog -->
						<div class="sub-page swiper-slide" data-hash="blog">
							
							<!--Page side bar-->
							<div class="page-side">
								<div class="inner-wrapper scrollbar">

									<!--Side bar title-->
									<div class="sub-page-title">
										<h2 class="mycontainer">Blog</h2>
									</div>
									<!--/Side bar title-->

									<!--Side bar image-->
									<img src="{{$THEMES_URL}}/assets/images/blog/side.jpg" class="img-responsive hidden-xs" alt="img">
									<!--/Side bar image-->

									<!--Side bar contents-->
									<div class="mycontainer hidden-xs">
										<h3>Welcome to my Blog</h3>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>

										<h3>Categories</h3>
										<ul id="blog-filters">
										  <li class="active-cat"><a href="#" data-filter="*">all</a></li>
										  <li><a href="#" data-filter=".expo">My Expo</a></li>
										  <li><a href="#" data-filter=".diary">Diary</a></li>
										  <li><a href="#" data-filter=".daily">Daily Photo</a></li>
										</ul>
									</div>
									<!--/Side bar contents-->

								</div>
							</div>
							<!--/Page side bar-->

							<!--Page contents-->
							<div class="page-container">
								<div class="inner-wrapper scrollbar">

									<!--Blog posts wrapper-->
								    <div id="blog-posts">
								    	<!--Blog item-->
								    	<div class="blog-item diary">

								    		<div class="inside">
								    			<!--Post image-->
								    			<img src="{{$THEMES_URL}}/assets/images/blog/posts/thumb/01.jpg" alt="image" class="img-responsive">
								    			<!--/Post image-->

								    			<!--Post Contents-->
									    		<div class="blog-item-contents">
									    			<!--Post title-->
									    			<h3><a href="ajax_pages/single-post.html" class="upstream-post">My latest Portraits of Angella</a></h3>
									    			<!--/Post title-->

									    			<!--Post meta-->
									    			<div class="post-meta">
									    				<div class="post-author"><i class="fa fa-edit"></i>Erica Franklin</div>
									    				<div class="post-date"><i class="fa fa-calendar"></i>2014/01/01</div>
									    				<div class="post-cat"><i class="fa fa-folder"></i>Portraits</div>
									    			</div>
									    			<!--/Post meta-->

									    			<!--Post introduction-->
									    			<div class="post-intro">
									    				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor.
									    			</div>
									    			<!--/Post introduction-->

									    			<!--Post read more-->
									    			<div class="post-more">
									    				<a href="ajax_pages/single-post.html" class="upstream-post">read more</a>
									    			</div>
									    			<!--/Post read more-->
									    		</div>
								    		</div>
								    	</div>
								    	<!--/Blog item-->
								    	
								    	<!--Blog item-->
								    	<div class="blog-item expo">
								    		<div class="inside">

								    			<!--Post Image-->
								    			<img src="{{$THEMES_URL}}/assets/images/blog/posts/thumb/02.jpg" alt="image" class="img-responsive">
								    			<!--/Post Image-->

								    			<!--Post Contents-->
									    		<div class="blog-item-contents">

									    			<!--Post Title-->
									    			<h3><a href="ajax_pages/single-post.html" class="upstream-post">My Gallery at London Hall</a></h3>
									    			<!--/Post Title-->

									    			<!--Post Meta-->
									    			<div class="post-meta">
									    				<div class="post-date"><i class="fa fa-calendar"></i>2014/01/01</div>
									    				<div class="post-cat"><i class="fa fa-folder"></i>Portraits</div>
									    			</div>
									    			<!--/Post Meta-->

									    			<!--Post Introduction-->
									    			<div class="post-intro">
									    				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.
									    			</div>
									    			<!--/Post Introduction-->

									    			<!--Post read more-->
									    			<div class="post-more">
									    				<a href="ajax_pages/single-post.html" class="upstream-post">read more</a>
									    			</div>
									    			<!--/Post read more-->

									    		</div>
									    		<!--/Post Contents-->

								    		</div>
								    	</div>
								    	<!--/Blog item-->

								    	<!--Blog item-->
								    	<div class="blog-item daily">
								    		<div class="inside">

								    			<!--Post Image-->
								    			<img src="{{$THEMES_URL}}/assets/images/blog/posts/thumb/03.jpg" alt="image" class="img-responsive">
								    			<!--/Post Image-->

								    			<!--Post Contents-->
									    		<div class="blog-item-contents">

									    			<!--Post Title-->
									    			<h3><a href="ajax_pages/single-post.html" class="upstream-post">Newest Sport Photos</a></h3>
									    			<!--/Post Title-->

									    			<!--Post meta-->
									    			<div class="post-meta">
									    				<div class="post-date"><i class="fa fa-calendar"></i>2014/01/01</div>
									    				<div class="post-cat"><i class="fa fa-folder"></i>Portraits</div>
									    			</div>
									    			<!--/Post meta-->

									    			<!--Post Introduction-->
									    			<div class="post-intro">
									    				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
									    			</div>
									    			<!--/Post Introduction-->

									    			<!--Post read more-->
									    			<div class="post-more">
									    				<a href="ajax_pages/single-post.html" class="upstream-post">read more</a>
									    			</div>
									    			<!--/Post read more-->

									    		</div>
									    		<!--/Post Contents-->

								    		</div>
								    	</div>
								    	<!--/Blog item-->

								    	<!--Blog item-->
								    	<div class="blog-item expo">
								    		<div class="inside">

								    			<!--Post Image-->
								    			<img src="{{$THEMES_URL}}/assets/images/blog/posts/thumb/04.jpg" alt="image" class="img-responsive">
								    			<!--/Post Image-->
								    			<!--Post Contents-->
									    		<div class="blog-item-contents">

									    			<!--Post Title-->
									    			<h3><a href="ajax_pages/single-post.html" class="upstream-post">Todays pickups of my trip to country side</a></h3>
									    			<!--/Post Title-->

									    			<!--Post meta-->
									    			<div class="post-meta">
									    				<div class="post-date"><i class="fa fa-calendar"></i>2014/01/01</div>
									    				<div class="post-cat"><i class="fa fa-folder"></i>Portraits</div>
									    			</div>
									    			<!--/Post meta-->

									    			<!--Post Introduction-->
									    			<div class="post-intro">
									    				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
									    			</div>
									    			<!--/Post Introduction-->

									    			<!--Post read more-->
									    			<div class="post-more">
									    				<a href="ajax_pages/single-post.html" class="upstream-post">read more</a>
									    			</div>
									    			<!--/Post read more-->

									    		</div>
									    		<!--/Post Contents-->
								    		</div>
								    	</div>
								    	<!--/Blog item-->

								    	<!--Blog item-->
								    	<div class="blog-item diary daily">
								    		<div class="inside">
								    			<!--Post Image-->
								    			<img src="{{$THEMES_URL}}/assets/images/blog/posts/thumb/05.jpg" alt="image" class="img-responsive">
								    			<!--/Post Image-->

								    			<!--Post Contents-->
									    		<div class="blog-item-contents">
									    			<!--Post Title-->
									    			<h3><a href="ajax_pages/single-post.html" class="upstream-post">Post title</a></h3>
									    			<!--/Post Title-->

									    			<!--Post meta-->
									    			<div class="post-meta">
									    				<div class="post-date"><i class="fa fa-calendar"></i>2014/01/01</div>
									    				<div class="post-cat"><i class="fa fa-folder"></i>Portraits</div>
									    			</div>
									    			<!--/Post meta-->

									    			<!--Post Introduction-->
									    			<div class="post-intro">
									    				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in.
									    			</div>
									    			<!--/Post Introduction-->

									    			<!--Post read more-->
									    			<div class="post-more">
									    				<a href="ajax_pages/single-post.html" class="upstream-post">read more</a>
									    			</div>
									    			<!--/Post read more-->

									    		</div>
									    		<!--/Post Contents-->

								    		</div>
								    	</div>
								    	<!--/Blog item-->
								    </div>
								    <!--/Blog posts wrapper-->

								    <!--Load more button-->
								    <div>
								    	<a href="" id="blog-more"><i class="fa fa-plus"></i> Load More</a>
								    </div>
								    <!--/Load more button-->
								</div>
							</div>
							<!--/Page contents-->
						</div>
						<!-- /Blog -->
						<!-- Contact me -->
						<div class="sub-page swiper-slide" data-hash="contact-us">
							
							<!--Contact Left Side-->	
							<div id="contact-left">
								<div class="inner-wrapper scrollbar">
									<div id="contact">
										<h2 class="contact-title">Want to Talk?</h2>

										<!--Contact details-->
										<ul class="contact-details">
											<li><span>Address:</span> Ho Chi Minh, Vietnam</li>
											<li><span>Phone:</span> 0989.466.466</li>
											<li><span>Email:</span> songviytuong@gmail.com</li>
										</ul>
										<!--Contact details-->

										<h3 class="form-title">Leave a Message</h3>

										<!--Contact form-->
										<form id="contact-form" method="post" action="contact.php">
											  <div class="form-group clearfix">
											    	<input type="email" class="form-control requiredField email" id="contact-email" name="email" placeholder="Email">
											  </div>
											  <div class="form-group clearfix">
											    	<input type="text" class="form-control requiredField" id="name" name="name" placeholder="Name">
											  </div>
											  <div class="form-group">
											    	<textarea class="form-control requiredField" id="message" name="message" rows="1" placeholder="Message"></textarea>
											  </div>
											  <button type="submit" name="submit" class="btn btn-theme">Send</button>
										</form>	
										<!--/Contact form-->
										<div class="clearfix"></div>
									</div>	
								</div>
							</div>
							<!--/Contact Left Side-->	

							<!--Contact Right Side-->	
							<div id="contact-right">

								<!--Google map container-->	
								<div  id="gmap-container">
									<div id="gmap" class="swiper-no-swiping"></div>
								</div>
								<!--/Google map container-->	

							</div>
							<!--/Contact Right Side-->	

						</div>
						<!-- /Contact me -->

					</div>
				</div>
				<!-- /Swiper Pages -->

				<!-- Taskbar -->
				<div id="taskbar">
					<div id="menu-trigger" rel="tooltip" data-toggle="tooltip" data-original-title="Menu">
						<a>
							<i class="fa fa-list-ul"></i>
						</a>	
					</div>
					<div id="audioplayback">
						<a><i class="fa fa-music"></i></a>
					</div>

					<!--Taskbar social icons-->
					<ul id="socialicons">
						<li><a href="#"><i class="fa fa-instagram"></i></a></li>
						<li><a href="#"><i class="fa fa-flickr"></i></a></li>
						<li><a href="#"><i class="fa fa-facebook"></i></a></li>
						<li><a href="#"><i class="fa fa-twitter"></i></a></li>
					</ul>
					<!--/Taskbar social icons-->

					<!--Taskbar logo-->
					<div id="logo">
						<h2><i class="fa fa-camera-retro"></i>@songviytuong</h2>
						<span class="slogan">.: No gains without pains :.</span></span>
					</div>
					<!--/Taskbar logo-->

					<div class="swiper-nav-thumbs"></div>
					
					<!-- Audio player -->
					<div id="jquery_jplayer_1" class="jp-jplayer"></div>
					<div id="jp_container_1" class="jp-audio">
						<div class="jp-type-single">
						  <div class="jp-gui jp-interface">
						    <ul class="jp-controls">
								
								<li><a href="javascript:;" class="jp-play" tabindex="1" title="play">
									<i class="fa fa-play"></i>
								</a></li>
								<li><a href="javascript:;" class="jp-pause" tabindex="1" style="display: none;" title="pause">
									<i class="fa fa-pause"></i>
								</a></li>
								<li><a href="javascript:;" class="jp-stop" tabindex="1" title="stop">
									<i class="fa fa-stop"></i>
								</a></li>
								<li><a href="javascript:;" class="jp-next" tabindex="1" title="next">
									<i class="fa fa-forward"></i>
								</a></li>
								<li><a href="javascript:;" class="jp-previous" tabindex="1" title="prev">
									<i class="fa fa-backward"></i>
								</a></li>
								<li><a href="javascript:;" class="jp-mute" tabindex="1" title="mute">
									<i class="fa fa-volume-off"></i>
								</a></li>
								<li><a href="javascript:;" class="jp-unmute" tabindex="1" title="unmute" style="display: none;">
									<i class="fa fa-volume-up"></i>
								</a></li>
							</ul>
						  </div>
						  <div class="jp-no-solution">
						    <span>Update Required</span>
						    To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
						  </div>
						</div>
					</div>
					<!-- /Audio Player-->
				</div>
				<!-- /Taskbar -->

			</div>
			<!-- /Contents-->

		</div>

		<!--Menu-pannel -->
		<div id="menu-pannel">
			<div id="menu-pannel-inner">
				
				<div id="close-menu">
					<a></a>
				</div>

				<!--Menu Items Images-->	
				<div id="menu-pannel-image">
					<div class="hex-wrapper">
						<div class="hex-1">
							<div class="hex-2">
								<div class="hex-3">
									<!-- images will be populated here by js -->
								</div>
							</div>
						</div>
					</div>		
				</div>
				<!--/Menu Items Images-->	

				<!--Menu Items contents-->	
				<div id="menu-pannel-thumbs">

					<!--Menu Items Carousel-->	
					<ul id="slider-container">
						<!--Menu Item-->	
						<li class="menu-pannel-thumb active">
							<h2>Home</h2>
							<h3><span class="digit">01</span>First page of my Website!</h3>
							<img src="{{$THEMES_URL}}/assets/images/menu/m1.jpg" alt="image"/>
						</li>
						<!--/Menu Item-->

						<!--Menu Item-->		
						<li class="menu-pannel-thumb">
							<h2>Grid Portfolio</h2>
							<h3><span class="digit">02</span>Our works in a grid</h3>
							<img src="{{$THEMES_URL}}/assets/images/menu/m1.jpg" alt="image"/>
						</li>
						<!--/Menu Item-->	

						<!--Menu Item-->	
						<li class="menu-pannel-thumb">
							<h2>Grid Gallery</h2>
							<h3><span class="digit">03</span>Grid gallery template</h3>
							<img src="{{$THEMES_URL}}/assets/images/menu/m1.jpg" alt="image"/>
						</li>
						<!--/Menu Item-->	

						<!--Menu Item-->	
						<li class="menu-pannel-thumb">
							<h2>Team</h2>
							<h3><span class="digit">04</span>Meet our personel</h3>
							<img src="{{$THEMES_URL}}/assets/images/menu/m1.jpg" alt="image"/>
						</li>
						<!--/Menu Item-->	

						<!--Menu Item-->	
						<li class="menu-pannel-thumb">
							<h2>Blog</h2>
							<h3><span class="digit">05</span>Read my posts</h3>
							<img src="{{$THEMES_URL}}/assets/images/menu/m1.jpg" alt="image"/>
						</li>
						<!--/Menu Item-->	
						<!--Menu Item-->	
						<li class="menu-pannel-thumb">
							<h2>ABC</h2>
							<h3><span class="digit">06</span>Let's have a Coffee!</h3>
							<img src="{{$THEMES_URL}}/assets/images/menu/m1.jpg" alt="image"/>
						</li>
						<!--/Menu Item-->	
					</ul>
					<!--/Menu Items Carousel-->

				</div>
				<!--/Menu Items contents-->	

				<!--Menu Items Navigation Bar-->	
				<div id="menu-pannel-navigation"></div>
				<!--Menu Items Navigation Bar-->	

			</div>
		</div>
		<!--/Menu-pannel-->


	</div>
	<!-- /Main Wrapper-->

	<script type="text/javascript">
		var myproject = {"root_url":"{{$data['root_url']}}","url_patch":"\/themes\/t00001\/"};
	</script>

	<!-- javascript Plugins-->
    <script type="text/javascript" src="{{$THEMES_URL}}/assets/js/vendors/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="{{$THEMES_URL}}/assets/js/vendors/jquery.cookie.js"></script>
    <script type="text/javascript" src="{{$THEMES_URL}}/assets/js/vendors/bootstrap.min.js"></script>
    <script type="text/javascript" src="{{$THEMES_URL}}/assets/js/vendors/jquery.slides.min.js"></script>
    <script type="text/javascript" src="{{$THEMES_URL}}/assets/js/vendors/imagesloaded.pkgd.min.js"></script>
    <script type="text/javascript" src="{{$THEMES_URL}}/assets/js/vendors/masonry.pkgd.min.js"></script>
    <script type="text/javascript" src="{{$THEMES_URL}}/assets/js/vendors/jquery.caroufredsel-6.2.1-packed.js"></script>
    <script type="text/javascript" src="{{$THEMES_URL}}/assets/js/vendors/blueimp-gallery.min.js"></script>
    <script type="text/javascript" src="{{$THEMES_URL}}/assets/js/vendors/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="{{$THEMES_URL}}/assets/js/vendors/jquery.mousewheel.js"></script>
    <script type="text/javascript" src="{{$THEMES_URL}}/assets/js/vendors/jquery.isotope.min.js"></script>
    <script type="text/javascript" src="{{$THEMES_URL}}/assets/js/vendors/jquery.lazyload.min.js"></script>
    <script type="text/javascript" src="{{$THEMES_URL}}/assets/js/vendors/perfect-scrollbar.js"></script>
    <script type="text/javascript" src="{{$THEMES_URL}}/assets/js/vendors/jquery.placeholder.js"></script>
    <script type="text/javascript" src="{{$THEMES_URL}}/assets/js/jquery.owwwlab-kenburns.js"></script>
    <script type="text/javascript" src="{{$THEMES_URL}}/assets/js/custom.js"></script>

    
	<script type="text/javascript" src="{{$THEMES_URL}}/assets/js/vendors/idangerous.swiper-2.4.min.js"></script>
	<script type="text/javascript" src="{{$THEMES_URL}}/assets/js/vendors/idangerous.swiper.hashnav-1.0.js"></script>
	<script type="text/javascript" src="{{$THEMES_URL}}/assets/js/vendors/TweenMax.min.js"></script>
	
    <script>
    /* home kenburn slider
    ----------------------------------------------*/
        var kbs,kenburn = {
            settings : {
            viewport : $('#kb-container')
            },
            
            init :function(){
            kbs = this.settings;
            this.bindUIActions();
            this.buildKenburn();
            },
            
            buildKenburn : function(){
                var kb = kbs.viewport.kenburnIt({
                images : ["{{$THEMES_URL}}/assets/images/background/1.jpg","{{$THEMES_URL}}/assets/images/background/2.jpg","{{$THEMES_URL}}/assets/images/background/3.jpg","{{$THEMES_URL}}/assets/images/background/4.jpg","{{$THEMES_URL}}/assets/images/background/5.jpg"],
                zoom: 1,
                duration: 10
                });
            },
            bindUIActions : function(){
            },
            updateContent : function(){
            }
        }
		/* run what you need
		----------------------------------------------*/
		initRequired.init();
		preloader.init();
		//audioBackground.init();
		kenburn.init();
		//videoBackground.init();
		aboutMeSlider.init();
		gridPortfolio.init();
		upstreamPortfolio.init();
		verticalGallery.init();
		HorizontalGallery.init();
		teamCarousel.init();
		blog.init();
		//helloSlider.init();
		//videoPortfolio.init();
	</script>
	
</body>
</html>