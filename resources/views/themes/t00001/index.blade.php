@extends('themes.t00001.layouts.master')
@section('body-content')
<div id='sidebar'>
    <div class="sb-inner">
        <img src="{{$THEMES_URL}}/img/idev.png" alt="Logo" width="140">
        <h1>@songviytuong</h1>
    </div>
</div>
<div id='contents'>
    <a id="left" href="{!!Plugin::cms_selflink(['href'=>'/webmaster/','target'=>'','title'=>'Webmaster'])!!}">
        <div class="con-inner">
            <img src="{{$THEMES_URL}}/img/webmaster.png" alt="version webmaster">
            <h2>Webmaster</h2>
            <ul>
                <li>(+84) 935.24.5885</li>
            </ul>
        </div>
    </a>
    <a id="right" href="{!!Plugin::cms_selflink(['href'=>'/designer/','target'=>'','title'=>'Designer'])!!}">
        <div class="con-inner">
            <img src="{{$THEMES_URL}}/img/designer.png" alt="version designer">
            <h2>Designer</h2>
            <ul>
                <li>(+84) 938.24.5885</li>
            </ul>
        </div>
    </a>
</div>
@stop