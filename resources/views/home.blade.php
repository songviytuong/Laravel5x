@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                    @endif
                    @if (!Auth::guest())
                    You are logged in! <a href="{{ route('logout') }}"
                                          onclick="event.preventDefault();
                                                  document.getElementById('logout-form').submit();">
                        Logout
                    </a>
                    @else
                    Please <a href="{{ route('login') }}">
                        Login
                    </a> into!
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
