@extends('admin.layouts.master')
@section('body-content')
<section class="h-100">
    <div class="container h-100">
        <div class="row justify-content-md-center h-100">
            <div class="card-wrapper">
                <div class="brand">
                    <img src="{!!asset('admin/images/logo.jpg')!!}">
                </div>
                <div class="card fat">
                    <div class="card-body">
                        @if (Auth::guest())
                        <h4 class="card-title">Login</h4>
                        @else
                        <h4 class="card-title">Welcome {{ Auth::user()->name }}</h4>
                        <spam>Email: {{ Auth::user()->email }} [<a href="{{ url('/administrator/logout') }}">Logout</a>]</spam>
                        @endif
                        @if (Auth::guest())
                        <form  method="POST" action="{{ url('/administrator/check') }}">
                            {{ csrf_field() }}
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="control-label">E-Mail Address</label>
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="control-label">Password
                                    <a href="forgot.html" class="float-right">
                                        Forgot Password?
                                    </a>
                                </label>
                                <input id="password" type="password" class="form-control" name="password" required data-eye>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label>
                                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                </label>
                            </div>

                            <div class="form-group no-margin">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>
                            </div>
                            <div class="margin-top20 text-center">
                                Don't have an account? <a href="register.html">Create One</a>
                            </div>
                        </form>
                        @endif
                    </div>
                </div>
                <div class="footer">
                    Copyright &copy; 2017 &mdash; Your Company 
                </div>
            </div>
        </div>
    </div>
</section>
@stop