@section('body-end')
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript">
    var queries = {{ json_encode(DB::getQueryLog()) }};
    console.log('/****************************** Database Queries ******************************/');
    console.log(' ');
    $.each(queries, function(id, query) {
    console.log('   ' + query.time + ' | ' + query.query);
    });
    console.log(' ');
    console.log('/****************************** End Queries ***********************************/');
</script>
@stop