{{-- file: /app/views/layouts/master.blade.php --}}
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        {{-- Part: site title with default value in parent --}}
        @section('head-title')
        <title>{!!SEO::getTitle()!!}</title>
        <base href="/">
        @stop
        @yield('head-title')

        @section('head-meta')
        {!!SEO::getMeta()!!}
        @stop

        {{-- Part: all meta-related contents --}}
        @yield('head-meta')
        <meta name="csrf-token" content="{{ csrf_token() }}" />

        {{-- Part: load fonts --}}
        @yield('head-fonts')
        {{-- Part: load styles for the page --}}
        @section('head-styles')
        {!!SEO::getCss()!!}
        @stop
        @yield('head-styles')
        {{-- Part: load scripts needed --}}
        @section('head-scripts')

        @stop
        @yield('head-scripts')
        {{-- Part: anything else in head --}}
        @yield('head-extra')

        <link rel="shortcut icon" href="{{ URL::asset('images/favicon.png') }}">

    </head>
    <body class="my-login-page">
        {{-- Part: something at start of body --}}
        @yield('body-start')

        {{-- Part: create main content of the page --}}
        @yield('body-content')

        <script type="text/javascript">
            var BASEPATH = '/administrator/';
        </script>
        
        {{-- Part: load scripts --}}
        @section('body-scripts')
        {!!SEO::getJs()!!}
        @stop
        @yield('body-scripts')
        {{-- Part: something else to do --}}
        @yield('body-others')
        {{-- Part: finalize stuffs if there is --}}
        {{-- Part: footer --}}
        @yield('body-end')
    </body>
</html>