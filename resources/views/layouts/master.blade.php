<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        {{-- Part: site title with default value in parent --}}
        @section('head-title')
        <title>{!!SEO::getTitle()!!}</title>
        @stop
        @yield('head-title')

        @section('head-meta')
	{!!SEO::getMeta()!!}
	@stop
	{!!SEO::getDescription()!!}
	{!!SEO::getCanonical()!!}
	{!!SEO::getFacebookTags()!!}

        {{-- Part: all meta-related contents --}}
        @yield('head-meta')

        {{-- Part: load fonts --}}
        @yield('head-fonts')
        {{-- Part: load styles for the page --}}
        @yield('head-styles')
        {{-- Part: load scripts needed --}}
        @yield('head-scripts')
        {{-- Part: anything else in head --}}
        @yield('head-extra')

        <link rel="shortcut icon" href="{{ URL::asset('images/favicon.png') }}">
	
	@if(!in_array(env('APP_ENV'),['developement']))
	<!-- Google Tag Manager -->
	<script>
		(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-PNZQSJ8');
	</script>
	<!-- End Google Tag Manager -->
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-125591340-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'UA-125591340-1');
	</script>

	<script src="https://sp.zalo.me/plugins/sdk.js"></script>
	@endif
    </head>
    <body>
        {{-- Part: something at start of body --}}
        @yield('body-start')

        {{-- Part: header of body --}}
        @section('body-header')
        @include('layouts.header')
        @show

        {{-- Part: create main content of the page --}}
        @yield('body-content')

        {{-- Part: footer --}}
        @section('body-footer')
        @include('layouts.footer')
	<!-- Scripts -->
	<script type="text/javascript">
		var SITE_URL = '';
		var LANG = '{{$lang = Session::get('locale')}}';
		var SUBSCRIBERS_MSG = '@php echo json_encode(config('master.SUBSCRIBERS_MSG')) @endphp';
	</script>
        @stop

        {{-- Part: load scripts --}}
        @yield('body-scripts')
        {{-- Part: something else to do --}}
        @yield('body-others')
        {{-- Part: finalize stuffs if there is --}}
        @yield('body-end')
    </body>
</html>