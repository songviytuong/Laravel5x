require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example', require('./components/Example.vue'));

// const app = new Vue({
//     el: '#app'
// });

const login = new Vue({
    el: '#login',
    data: {
        item: {
            email: '',
            password: '',
        }
    },
    mounted: function () {
        // this.doLogin();
    },
    methods: {
        doLogin: function () {
            var _this = this;
            axios.post("/cpanel/check-login", {
                email: _this.item.email,
                password: _this.item.password,
            }).then(function (success) {
                if (success.status) {
                    window.location.href = "/cpanel";
                }
            }, function (error) {
                console.log(error);
            });
        },
        onChangeColor: function (event) {
            axios.post("/cpanel/switch-colors", {
                color: event.target.value
            }).then(function (success) {
                if (success.status) {
                    location.reload();
                }
            }, function (error) {
                console.log(error);
            });
        },
        onChangeThemes: function (event) {
            axios.post("/cpanel/switch-themes", {
                theme: event.target.value
            }).then(function (success) {
                if (success.status) {
                    location.reload();
                }
            }, function (error) {
                console.log(error);
            });
        }
    }
});