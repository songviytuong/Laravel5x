<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

Notes:
```bash
+ current url: {{url()->current()}}
+ valid last segment url {{ strpos(url()->current(),$project->alias) !== false ? 'class=active':''}}
```

PHP Upgrade: my.ini

```bash
[mysqld]
innodb_large_prefix
innodb_file_format=BARRACUDA
innodb_file_per_table
```

Tests [[--]]

```bash
$ php artisan make:test UserTest [--unit]
```

We will now use seven of the basic [PHPUnit assertions](http://apigen.juzna.cz/doc/sebastianbergmann/phpunit/class-PHPUnit_Framework_TestCase.html) to write tests for our Box class. These assertions are:

```bash

```

Run PHPUnit

```bash
$ alias phpunit="./vendor/bin/phpunit"
```

Mail

```bash
php artisan make:mail SendEmailMailable
```

Queue

```bash
php artisan queue:table
php artisan migrate
php artisan make:job SendEmailJob
```

```bash
class SendEmailJob implements ShouldQueue
{
    ...

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to('songviytuong@gmail.com')->send(new SendEmailMailable());
    }
}
```

```bash
Route::get('sendEmail', function() {
    $job = (new SendEmailJob())->onQueue('emails')->delay(Carbon::now()->addSeconds(3));
    dispatch($job);
    return "This job has been sent!";
});
```
+++ Run: Database Driver

```bash
php artisan queue:listen --queue=emails
```
+++ Run: Redis Driver [Download](https://github.com/rgl/redis/downloads) +++

```bash
php artisan queue:listen redis --queue=emails
```

Sub-domain Dynamic

```bash
Enable:
LoadModule vhost_alias_module modules/mod_vhost_alias.so (httpd.conf)

Add (httpd-vhosts.conf):

<VirtualHost *:443>
    DocumentRoot "D:\xampp\htdocs\domain-system"
    ServerName domain-system.local
	SSLEngine On
	SSLCertificateFile "D:\xampp\apache\conf\ssl.crt\server.crt"
	SSLCertificateKeyFile "D:\xampp\apache\conf\ssl.key\server.key"
</VirtualHost>

<VirtualHost *:80>
       ServerAlias *.localhost
	   DocumentRoot "D:\xampp\htdocs\project\public"
       <Directory "D:\xampp\htdocs\project">
        AllowOverride All
        Order deny,allow
        Allow from all
        Require all granted
    </Directory>
</VirtualHost>
```

Shopping Cart [Link](https://github.com/darryldecode/laravelshoppingcart/tree/42211034bac6f663259a9a61f729c9b165e8cd60) & [Modules](https://nwidart.com/laravel-modules/v1/advanced-tools/artisan-commands)
