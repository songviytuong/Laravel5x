<?php

return [
    'MAIL_ACCOUNTS' => [
        'main_smtp' => [
            'Driver' => env('MAIL_DRIVER'),
            'Host' => env('MAIL_HOST'),
            'Port' => env('MAIL_PORT'),
            'Username' => env('MAIL_USERNAME'),
            'Password' => env('MAIL_PASSWORD'),
            'Encryption' => env('MAIL_ENCRYPTION'),
        ],
    ],

    'EMAIL_TEMPLATES' => [
        'Temp1' => [
            'Subject' => '[TEMP1] This is a Email Subject',
            'BodyContent' => 'emails.temp1_temporary',
        ],
    ],
];
