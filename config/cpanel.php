<?php
return [
    'name' => 'Cpanel',
    'themes' => [
        'PreAdmin' => [
            'color' => [
                'light', 'orange', 'purple', 'blue', 'dark'
            ]
        ],
        'OneEleven' => [
            'color' => [
                
            ]
        ],
        'PortoAdmin' => [
            'color' => [
                
            ]
        ],
    ],
    'theme_active' => 'OneEleven',
    'color_active' => '',
    'records_per_page' => 5
];
